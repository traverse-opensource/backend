# Traverse Xpr Backend

Full stack app using:
#### MongoDB + React/Redux + NodeJs/Express


Pré-requis: 
-- NodeJs
-- NPM
-- Mongodb -> le service mongo doit être activé avant de lancer l'application

Installation: 

1. Cloner le repo en local
2. Ouvrir un terminal, se rendre au repo local puis "npm install"
3. Attendre que les modules node s'installe puis "npm run dev"
4. Attendre le message "webpack built" puis ouvrir un navigateur sur "localhost:3000"

