/**
 * Created by hasj on 21/11/2016.
 */
/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import axios from 'axios';
import * as types from 'types';
import * as constant from 'constant'
import Promise from 'es6-promise';

polyfill();
/*
 * @param Object data -> les données qu'on veut passer au serveur
 * @param String HTTP method -> les requêtes http comme post, get, put, delete
 * @param String id -> l'id de l'objet sur lequel on fait une requête
 * @param String api -> la route la route correspondante à celle du serveur
 * @return Promesse puisque c'est un call ASYNC
 * Le matching view - server se fait dans le fichier server.jsx
 *
 */

/*TODO refactor these methods*/

export function makeFicheRequest(method, id, data, api = '/fiches') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makeMediaRequest(method, id, data, api = '/media') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makeObjectRequest(method, id, data, api = '/objects') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makePlaceRequest(method, id, data, api = '/places') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makePeopleRequest(method, id, data, api = '/people') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makeEventRequest(method, id, data, api = '/events') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makeUploadRequest(method, id, data, api = '/upload') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}
export function makeRequest(method, id, data, api) {
    return request[method](api + (id ? ('/' + id) : ''), data);
}

export function createFicheRequest(data, message = null) {
    return {
        type: types.CREATE_FICHE_REQUEST,
        data: data,
        message: message
    };
}

export function uploadSuccess(data) {
    return {
        type: types.UPLOAD_SUCCESS,
        data: data
    };
}

export function editFicheRequest(data, message = null){
    return {
        type: types.EDIT_FICHE_REQUEST,
        _id: data._id,
        text: data.text,
        name : data.name,
        fiche: data,
        message: message
    }
}
export function destroy(id){
    return {
        type: types.DESTROY_FICHE,
        message: "Fiche supprimée avec succès",
        _id : id
    }
}
export function flushFiche(){
    return {
        type: types.FLUSH_FICHE
    }
}
export function flushFicheSearchAction(){
    return {
        type: types.FLUSH_FICHE_SEARCH
    }
}
export function flushSlectedTagsAction(){
    return{
        type: types.FLUSH_TAGS
    }
}
export function selectedTags(data) {
    return (dispatch) => {
        return dispatch(selectedTagAction(data))
    }
}
function selectedTagAction(tags){
    return{
        type: types.SELECTED_TAG,
        data: tags
    }
}

export function getSelectedTag(tags){
    return(dispatch) => {
        return dispatch(getSelectedTagAction(tags)
        )
    }
}
function getSelectedTagAction(tags){
    return {
        type : types.INITIALISE_TAGS,
        data: tags
    }
}

export function addTheme(themes){
    return (dispatch) => {
        return dispatch({
            type: types.ADD_THEME,
            data: themes
        })
    }
}
export function flushThemes(){
    return (dispatch) => {
        return dispatch({
            type: types.FLUSH_THEMES
        })
    }
}

export function destroyFiche(id){
    return (dispatch) => {
        return makeFicheRequest('post', id, {status: 2})
          .then(() => dispatch(destroy(id)))
    }
}
export function restoreFiche(id){
    return(dispatch) => {
        return makeFicheRequest('post', id, {status  : 1}.then(() => dispatch(restore(id))))
    }
}
function restore(id){
    return{
        type: types.RESTORE_FICHE,
        id : id}
}
export function editFiche(_id, data){
    return (dispatch) => {
        return makeFicheRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}
export function editFichePlace(_id, data){
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            /*if (!data.latitude || !data.latitude) {
             missingField = true;
             dispatch(missingLocationAction())
             }*/
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makePlaceRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche Place modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}

export function editFichePerson(_id, data){
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*  if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_bio){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makePeopleRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche Personne modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}
export function editFicheMedia(_id, data){
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if ( data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeMediaRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche Média modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}
export function editFicheObject(_id, data){
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeObjectRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche Objet modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}
export function editFicheEvent(_id, data){
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /* if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeEventRequest('post', _id, data)
          .then((res) => {
              if(res.status === 200) {
                  dispatch(editFicheRequest(res.data, "Fiche Evènement modifiée avec succès"));
                  return res; }})
          .catch(() => (dispatch(editFicheRequest({_id, error: 'Oops! Something went wrong and we couldn\'t edit your fiche'}))))
    }
}

export function submitPerson(data) {
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /* if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_bio){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makePeopleRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(createFicheRequest(res.data, "L'ajout de la fiche Personne s'est bien effectuée"));
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}
export function submitMedia(data) {
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeMediaRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(createFicheRequest(res.data, "L'ajout de la fiche Média s'est bien effectuée"));
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}
export function submitImage(data) {
    return (dispatch)=> {
        return makeUploadRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(uploadSuccess(res.data));
              }else {
                dispatch(uploadFailure({error: res.errorMessage}));
              }
          })
          .catch((res) => {
            dispatch(uploadFailure({error: "La taille maximale d'une image est fixée à 2Mo, veuillez changer ou redimensionner l'image désirée"}))
          });
    }
}
function missingFieldsAction(){
    return {
        type :types.CHAMPS_MANQUANTS,
        message: constant.MISSING_FIELDS
    }
}
function missingCategoriesAction(){
    return{
        type :types.MISSING_CATEGORY,
        message: constant.MISSING_CATEGORY
    }
}
function missingLocationAction(){
    return{
        type :types.MISSING_LOCATION,
        message: constant.MISSING_LOCATION
    }
}
function flushMessageAction() {
    return{
        type: types.CHAMPS_TOUS_REMPLIS,
        message: ''
    }
}
function alertFieldMissing(){
    return {
        type: types.SHOW_ALERT,
        message: constant.ALERT_PUBLISH
    }

}
export function submitPlace(data) {
    let missingField = false;

    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            /*if (!data.latitude || !data.latitude) {
             missingField = true;
             dispatch(missingLocationAction())
             }*/
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makePlaceRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(createFicheRequest(res.data, "L'ajout de la fiche Site s'est bien effectuée"));
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}

export function submitEvent(data) {
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeEventRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(createFicheRequest(res.data, "L'ajout de la fiche Evènement s'est bien effectuée"));
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}

export function submitTag(data) {
    return (dispatch)=> {
        return makeRequest('post', '', data, '/tags').then(
          res => {
              if(res.status === 200){
                  dispatch(createTagRequest(res.data))
                  return res.data;
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}

function createTagRequest(data){
    return{
        type: types.CREATE_TAG,
        data: data
    }
}

export function submitObject(data) {
    let missingField = false;
    return (dispatch)=> {
        //Contrôle si la fiche enregistrée doit être publiée --> alors contrôle des champs requis
        if(data.status==1) {
            /*if (data.categories.length < 1) {
             missingField = true;
             dispatch(missingCategoriesAction());
             }*/
            if(!data.name||!data.short_description){
                missingField = true;
                dispatch(missingFieldsAction());
            }
            if(missingField){
                return dispatch(alertFieldMissing())
            }
        }
        dispatch(flushMessageAction());
        missingField = false;
        return makeObjectRequest('post', '', data)
          .then(res => {
              if(res.status === 200 ) {
                  dispatch(createFicheRequest(res.data, "L'ajout de la fiche Objet s'est bien effectuée"));
              }
          })
          .catch(() => dispatch(createFicheFailure({error: 'Oops! Something went wrong and we couldn\'t create your fiche'})));
    }
}


export function flushDisplayedFiche(){
    return (dispatch) => {
        dispatch(flushFiche());
    }
}

export function flushFicheSearch(){
    return (dispatch) => {
        dispatch(flushFicheSearchAction());
    }
}
export function flushSlectedTags(){
    return (dispatch) => {
        dispatch(flushSlectedTagsAction())
    }
}

export function fetchOneFiche(id) {
    return (dispatch) => {
        return request['get']('/fiches/fiche/'+id)
          .then((res) => {
              if(res.status ===200){
                  return dispatch({
                      type: types.ONE_FICHE,
                      data : res.data
                  })
              }
          })
    }
}

export function getCreator(_id = null){
  return (dispatch) => {
      //since we send the populate created_by object now instead of pure objectID
    let targetId = Object.keys(_id).length > 1 ?
        //typeof _id === 'object' ? 
        _id._id: _id;

    return request['get']('/user/'+targetId)
      .then((res) =>{
            if(res.status === 200) {
                return res.data;
            }
        }
      )  
  }
}
export function fetchFichesSuccess(data){
    return {
        type: types.LOAD_FICHES,
        data: data
    }
}

export function fetchFichesFailure(data){
    return {
        type: types.ERROR_FICHES
    }
}

export function fetchFicheFailure(data){
    return {
        type: types.ERROR_FICHE
    }
}

export function makeAddRelatedFicheRequest(id, data) {
    return request['post']('/fiches/' + id + '/addRelation', data);
}

export function makeRemoveRelatedFicheRequest(id, data) {
    return request['post']('/fiches/' + id + '/removeRelation', data);
}


export function addRelatedFicheToFiche(id, data) {
    return (dispatch) => {

        makeAddRelatedFicheRequest(id, data)
          .then(function(res) {
              if (res.status === 200){

                  return dispatch({
                      type: types.ONE_FICHE,
                      data: res.data
                  });
              }
          })
          .catch((e) => {
              dispatch(fetchFicheFailure({error : 'Impossible de lier les fiches'}))})
    }
}
export function removeRelatedFicheToFiche(id, data) {
    return (dispatch) => {

        makeRemoveRelatedFicheRequest(id, data)
          .then(function(res) {
              if (res.status === 200){

                  return dispatch({
                      type: types.ONE_FICHE,
                      data: res.data
                  });
              }
          })
          .catch((e) => {
              dispatch(fetchFicheFailure({error : 'Impossible de supprimer le lien entre les fiches'}))})
    }
}

export function uploadFailure(data){
    return {
        type: types.ERROR_UPLOAD_FAIL,
        message: data.error
    }
}
export function setMap(data){
    return {
        type: types.SET_MAP_DATA,
        data
    }
}

export function flushMapData(){
  return {
    type: types.FLUSH_MAP_DATA
  }
}

export function searchFiche(query, filters, extraParams = null) {

    let requestArray = [];
    for (let filter of filters) {
        if (filter.value == true) {
          //requestArray.push(axios.get('/' + filter.key + '/search/list?q=' + query + '&bob=3'));
            if (query.length > 0) {
              requestArray.push(axios.get('/' + filter.key + '/search/list?q=' + query));
            }else {
              requestArray.push(axios.get('/' + filter.key + '/search/list'));  
            }
            
        }
    }

    return (dispatch) => {
        axios.all(requestArray).then(function (results) {

            let res = [];
          
            for (let subResult of results) {
                
                res.push(...subResult.data);
            }

            function compare(a,b) {
              if (a.created_at < b.created_at)
                return -1;
              if (a.created_at > b.created_at)
                return 1;
              return 0;
            }

            res.sort(compare);
          
            dispatch(fetchFichesSuccess(res));
        }).catch(() => dispatch(fetchFichesFailure({error: "Impossible d'obtenir la liste des fiches"})))
    }
}

function fetchEnumRequest(data) {
    return {
        type : types.FETCH_ENUM,
        data: data
    }
}
export function fetchEnum() {
    return (dispatch) => {
        let enumerations = {}
        let sousCategories =[];
        Promise.all([axios.get( '/categories'), axios.get( '/types'),axios.get('/tags'), axios.get('/themes')])
          .then(results => {
              enumerations['categories'] = results[0].data;
              enumerations['types'] = results[1].data;
              enumerations['tags'] = results[2].data;
              enumerations['themes'] = results[3].data;
              results[0].data.map((cat) => {
                  if(cat.sousCategories) {
                      cat.sousCategories.map((sousCat) => {
                          sousCategories.push(sousCat);
                      })
                  }
              });
              enumerations['sousCategories'] = sousCategories;
              return dispatch(fetchEnumRequest(enumerations))
          })
          .catch(error =>
          {
              dispatch(fetchFichesFailure({error: "Impossible d'obtenir les enums " + error}))
              return enumerations;
          })
    }
}