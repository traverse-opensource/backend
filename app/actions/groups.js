import { polyfill } from 'es6-promise';
import request from 'axios';
import { push } from 'react-router-redux';
import * as types from '../types';

polyfill();
function makeGroupRequest(method, data, api = '/entities') {
    return request[method](api, data);
}
function createGroupsSuccess(data) {
    return{
        type: types.CREATE_GROUPS_SUCCESS,
        data
    }

}
function createGroupsError(data) {
    return{
        type:types.CREATE_GROUPS_ERROR,
        data
    }
}
export function createGroup(data){
    return dispatch => {
        return makeGroupRequest('post', data )
            .then(response=> {
                if (response.status === 200) {
                    dispatch(createGroupsSuccess(response.data));
                } else {
                    dispatch(createGroupsError('Oops! Something went wrong'));
                }
            })
            .catch(err => {
                dispatch(createGroupsError(getMessage(err)));
            });
    }
}
function deleteGroupRequest(id) {
    return {
        type: types.DELETE_GROUP_REQUEST,
        id
    }
}

export function deleteGroup(id){
    return dispatch => {
        return request['delete']('/entities/'+id)
            .then(()=> dispatch(deleteGroupRequest(id)))
            .catch(() => dispatch("Cannot delete user with id " + id))
    }
}