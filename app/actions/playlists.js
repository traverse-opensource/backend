import { polyfill } from 'es6-promise';
import request from 'axios';
import axios from 'axios';
import md5 from 'spark-md5';
import * as types from '../types';
import { playlistService } from '../services';

polyfill();
export function makePlaylistRequest(method, id, data, api = '/playlists') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}

export function makePlaylistSwapRequest(id, data) {
    return request['post']('/playlists/' + id + '/swap', data);
}

export function makePlaylistReplaceRequest(id, data) {
    return request['post']('/playlists/' + id + '/replace', data);
}

export function makeAddLinkedFicheRequest(id, data) {
    return request['post']('/playlists/' + id + '/addLink', data);
}

export function makePlaylistDeleteFicheRequest(id, data) {
    return request['post']('/playlists/' + id + '/deleteFiche', data);
}

export function makePlaylistUpdateLinkFicheRequest(id, data) {
    return request['post']('/playlists/' + id + '/updateLink', data);
}

export function makePlaylistCoverRequest(id, data) {
    return request['post']('/playlists/' + id + '/updateCover', data);
}

export function makePlaylistLabelsRequest(id, data) {
    return request['post']('/playlists/' + id + '/updateLabels', data);
}

export function fetchData(){
    return{
        type: types.GET_PLAYLISTS
    }
}
export function createPlaylistRequest(data){
    return {
        type: types.CREATE_PLAYLIST_REQUEST,
        payload: {
            _id: data._id,
            name: data.name,
            description: data.description,
            linkedFiches: data.linkedFiches
        }
    }
}

export function newPlaylistCreated(data) {
    return {
        type: types.NEW_PLAYLIST_CREATED,
        data: data
    }
}

export function createPlaylistFailure(data) {
    return {
        type: types.CREATE_PLAYLIST_FAILURE,
        error :data.error,
        message: data.message || data.error
    }
}
export function fetchFicheData(){
    return{
    type: types.GET_FICHES_REQUEST,
    promise:  makePlaylistRequest('get', '', '', api='/fiches')
    }
}
export function fetchFichesSuccess(data){
    return {
        type: types.GET_PLAYLIST_FICHES_SUCCESS,
        payload: data
    }
}

export function getFicheFailure(param) {
    return{
        type: types.GET_FICHES_FAILURE,
        error: param.error
    }

}
export function destroyPlaylist(id, message){
    return {
        type: types.DESTROY_PLAYLIST,
        id: id,
        message: message
    }
}
function updatePlaylist(_id, data) {
    return {
        type : types.EDIT_PLAYLIST,
        payload: data
    }

}

export function resetFicheResults(){
    return{
        type: types.RESET_FICHE_RESULTS
    }
}

export function setFicheSelected(data) {
    return{
        type : types.SELECTED_PLAYLIST,
        payload: data
    }
}

export function setFicheToReplaceBy(data) {
    return{
        type : types.SELECTED_FICHE_TO_REPLACE_BY_PLAYLIST,
        payload: data
    }
}


export function setPlaylistFicheSelected(data) {
    return{
        type : types.SELECTED_FICHE_PLAYLIST,
        payload: data
    }
}

export function setPlaylistFicheUnselected() {
    return{
        type : types.UNSELECTED_FICHE_PLAYLIST
    }
}

export function setPlaylistLinkSelected(data) {
    return{
        type : types.SELECTED_LINK_PLAYLIST,
        payload: data
    }
}

export function setPlaylistLinkUnselected() {
    return{
        type : types.UNSELECTED_LINK_PLAYLIST,
    }
}


export function deselectFiche(data) {
    return{
        type : types.SELECTED_PLAYLIST_CANCEL
    }
}

export function linkedFicheAddedSucess(data) {
    return{
        type : types.FICHE_ADDED_TO_PLAYLIST_SUCCESS,
        payload: data
    }
}

export function replaceSucess(data) {
    return{
        type : types.SELECTED_FICHE_TO_REPLACE_BY_PLAYLIST_SUCCESS,
        payload: data
    }
}

function updatePlaylistFailed(_id, error) {
    return{
        type : types.EDIT_PLAYLIST_FAILURE,
        error : error
    }
}

export function addLinkedFicheToPlaylist(playlistId, data, listener){

    return (dispatch) => {
        makeAddLinkedFicheRequest(playlistId, data)
            .then(function(res) {
                if (res.status === 200){

                    // var ficheAddRequests = [];
                    //
                    // for (let fiche of data.fiches) {
                    //     ficheAddRequests.push(createAddFicheToPlaylistRequest(res.data._id, fiche));
                    // }
                    //
                    // Promise.all(ficheAddRequests).then(function(results) {
                    dispatch(linkedFicheAddedSucess(res.data));
                    return listener(res.data);
                    // })
                }
            })
            .catch((e) => {
                dispatch(createPlaylistFailure({error : 'Impossible de lier la fiche à la playlist'}))})
    }
}

function getPlaylistRequest(playlistId) {
    return request['get']('/playlists' + '/' + 'playlist' + '/' + playlistId);
}

function createAddFicheToPlaylistRequest(playlistId, ficheData) {
    return request['post']('/playlists' + '/' + playlistId + '/' + 'add', ficheData);
}

export function getPlaylistData(id) {

    return(dispatch) => {
        return getPlaylistRequest(id).then(function (playlist) {
            return dispatch({
                type: types.PLAYLIST_LOADED_SUCESS,
                payload: playlist.data
            });
        }).catch((e) => {
            return dispatch(createPlaylistFailure({error : 'Impossible de créer la playlists'}))})
    }
}

export function addPlaylist(data, listener){

    return (dispatch) => {
        makePlaylistRequest('post', '', data)
            .then(function(res) {
                if (res.status === 200){

                    // var ficheAddRequests = [];
                    //
                    // for (let fiche of data.fiches) {
                    //     ficheAddRequests.push(createAddFicheToPlaylistRequest(res.data._id, fiche));
                    // }
                    //
                    // Promise.all(ficheAddRequests).then(function(results) {
                        dispatch(createPlaylistRequest(res.data));
                        return listener(res.data);
                    // })
                }
            })
            .catch((e) => {
                dispatch(createPlaylistFailure({error : 'Impossible de créer la playlists'}))})
    }
}

export function searchFiche(query, filters) {
    let requestArray = [];
    for (let filter of filters) {
        if (filter.value == true) {
            requestArray.push(axios.get('/' + filter.key + '/search/list?q=' + query + '&forceStatus=1'));
        }
    }

    return (dispatch) => {
        axios.all(requestArray).then(function (results) {

            let res = [];

            for (let subResult of results) {
                res.push(...subResult.data);
            }

            dispatch(fetchFichesSuccess(res));
        }).catch(() => dispatch(fetchFichesFailure({error: "Impossible d'obtenir la liste des fiches"})))
    }
}

export function getFicheList(){
    return(dispatch) => {

        axios.all([
            axios.get('/people'),
            axios.get('/places'),
            axios.get('/events'),
            axios.get('/media'),
            axios.get('/objects')
        ]).then(axios.spread(function (people, places, events, media, objects) {
            let res = [...people.data];
            res.push(...people.data);
            res.push(...places.data);
            res.push(...events.data);
            res.push(...media.data);
            res.push(...objects.data);

            dispatch(fetchFichesSuccess(res));
        })).catch(() => dispatch(getFicheFailure({error: "Impossible d'obtenir la liste des fiches"})))

        // request['get']('/fiches')
        //     .then(function(res) {
        //         if(res.status === 200){
        //             dispatch(fetchFichesSuccess(res.data));
        //             return JSON.stringify(res.data);
        //
        //         }
        //     })

    }
}

export function removePlaylist(id){
    return (dispatch) => {
        makePlaylistRequest('delete', id)
            .then(function(result) {dispatch(destroyPlaylist(id, result.message))
            })
    }
}

export function editPlaylist(_id, data, playlist, callback = null) {

  const sendWhenValidated = (dispatch) => {
    return makePlaylistRequest('put', _id, data)
      .then((result) => {
          if (callback) {
            callback();
            return dispatch(updatePlaylist(_id, result.data))
          }
      })
      .catch((error) => {
      return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
  }
  
  return (dispatch)=> {

    //check here some fields, if want to udpate, so we need to take care of the status thus name and description!

    if (data.status === 1) {

      //need to have at least one fiche, a description and a name over that playlist
      let validateName = data.name != null && data.name != "",
          validateDescription = data.description != null && data.description != "",
          validateFiche = playlist.linkedFiches && playlist.linkedFiches.length > 0;
      if (validateName && validateDescription && validateFiche) {
        return sendWhenValidated(dispatch);
      } else {
        
        let message = !validateName ?
            "Un nom est requis": !validateDescription?
            "Une description est requise": !validateFiche?
            "Au moins une fiche est requise": ""; 
        //should not arrive at the empty string case since it's tested from above
        
        return dispatch({
          type: types.EDIT_PLAYLIST_FAILURE,
          error: "Mother Focker!",
          message: message
        });
      }
    }else {
      console.log('editPlaylist3');
      return sendWhenValidated(dispatch);
    }
  }
}


export function swapPlaylistFiches(_id, data) {

    return (dispatch)=> {

        return makePlaylistSwapRequest(_id, data)
            .then((result) => {
                return dispatch(updatePlaylist(_id, result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}

export function replacePlaylistFiche(_id, data) {

    return (dispatch)=> {

        return makePlaylistReplaceRequest(_id, data)
            .then((result) => {
                dispatch(updatePlaylist(_id, result.data));

                return dispatch(replaceSucess(result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}

export function removeFicheFromPlaylist(_id, data) {

    return (dispatch)=> {

        return makePlaylistDeleteFicheRequest(_id, data)
            .then((result) => {
                return dispatch(updatePlaylist(_id, result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}

export function updateLinkFichePlaylist(_id, data) {

    return (dispatch)=> {

        return makePlaylistUpdateLinkFicheRequest(_id, data)
            .then((result) => {
                return dispatch(updatePlaylist(_id, result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}

export function updatePlaylistCover(_id, data) {
    return (dispatch)=> {

        return makePlaylistCoverRequest(_id, data)
            .then((result) => {
                return dispatch(updatePlaylist(_id, result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}

export function updatePlaylistLabels(_id, data) {
    return (dispatch)=> {

        return makePlaylistLabelsRequest(_id, data)
            .then((result) => {
                return dispatch(updatePlaylist(_id, result.data))
            })
            .catch((error) => {
                return dispatch(updatePlaylistFailed(_id, {error : "Impossible d'update la playlist"}))})
    }
}