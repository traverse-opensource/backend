import { polyfill } from 'es6-promise';
import request from 'axios';
import { push } from 'react-router-redux';
import {browserHistory} from 'react-router'
import * as types from '../types';

polyfill();

const getMessage = res => res.response && res.response.data && res.response.data.message;

function makeRequest(method, data, api = '/login') {
    return request[method](api, data);
}

// Log In Action Creators
export function beginLogin() {
    return { type: types.MANUAL_LOGIN_USER };
}

export function loginSuccess(message) {
    return {
        type: types.LOGIN_SUCCESS_USER,
        message
    };
}

export function loginError(message) {
    return {
        type: types.LOGIN_ERROR_USER,
        message
    };
}

// Update Actions Creators
export function beginUpdate() {
    return { type: types.UPDATE_USER };
}

export function updateSuccess(data, isSelf) {
    return {
        type: isSelf ? types.UPDATE_USER_SUCCESS_SELF: types.UPDATE_USER_SUCCESS,
        message: isSelf ?
            "Votre profil vient d'être mis à jour": "L'utilisateur " + data.user.profile.name + " a bien été mis à jour",
        user: data.user
    };
}

export function updateError(message) {
    return {
        type: types.UPDATE_USER_ERROR,
        message
    };
}

// Sign Up Action Creators
export function signUpError(message) {
    return {
        type: types.SIGNUP_ERROR_USER,
        message
    };
}

export function beginSignUp() {
    return { type: types.SIGNUP_USER };
}

export function signUpSuccess(message) {
    return {
        type: types.SIGNUP_SUCCESS_USER,
        message
    };
}
//creating user, only allowed to admin nor super_admin
export function beginCreateUser() {
    return { type: types.CREATE_USER };
}

export function createUserSuccess(message) {
    return {
        type: types.CREATE_USER_SUCCESS,
        message: message.message,
        user: message.user
    };
}

export function createUserError(message) {
    return {
        type: types.CREATE_USER_ERROR,
        message
    };
}

// Upload Profile Picture Actions
export function uploadSuccess(data) {
    return {
        type: types.UPLOAD_PROFILE_PICTURE_SUCCESS,
        data: data
    };
}

export function uploadFailure(data){
    return {
        type: types.UPLOAD_PROFILE_PICTURE_FAIL
    }
}

//saving only on front side the user preferences
export function beginUserPrefs() {
    return { type: types.USER_PREFS };
}

export function uppdateUserPrefsSuccess(data) {
    return {
        type: types.USER_PREFS_SUCCESS,
        message: data.message,
        prefs: data.prefs
    };
}

export function uppdateUserPrefsError(data, err) {
    return {
        type: types.USER_PREFS_FAIL,
        message: data.message
    };
}

// Log Out Action Creators
export function beginLogout() {
    return { type: types.LOGOUT_USER};
}

export function logoutSuccess() {
    return { type: types.LOGOUT_SUCCESS_USER };
}

export function logoutError() {
    return { type: types.LOGOUT_ERROR_USER };
}

export function serverAutoLogout() {
  return { type: types.LOGIN_EXPIRED_USER };
}

export function toggleLoginMode() {
    return { type: types.TOGGLE_LOGIN_MODE };
}
export function userProfile(user){
    return {type: types.FETCH_USER,
        user}
}

export function autoLogout() {
  return dispatch => {
      dispatch(serverAutoLogout());
  };
}

export function retrieveUserState() {
  return dispatch => {
    dispatch({ type: types.USER_STATE });
  }
}

export function manualLogin(data) {
    return dispatch => {
        dispatch(beginLogin());

        return makeRequest('post', data, '/login')
            .then(response => {
                if (response.status === 200) {
                    dispatch(loginSuccess(response.data.message));
                    dispatch(userProfile(response.data));
                    dispatch(push('/fichesView'));
                } else {
                    dispatch(loginError('Oops! Something went wrong!'));
                }
            })
            .catch(err => {
                dispatch(loginError(getMessage(err)));
            });
    };
}

export function createUser(data) {
    return dispatch => {
        dispatch(beginCreateUser());
        return makeRequest('post', data, '/users/create')
            .then(response => {
                if (response.status === 200) {
                    dispatch(createUserSuccess(response.data));
                } else {
                    dispatch(createUserError('Oops! Something went wrong'));
                }
            })
            .catch(err => {
                dispatch(createUserError(getMessage(err)));
            });
    };
}
function changePasswordRequest(message){
    return {
        type:types.CHANGE_PASSWORD_REQUEST,
        message
    }
}
function changePasswordRequestFailed(message){
    return{
        type:types.CHANGE_PASSWORD_REQUEST_FAILED,
        message
    }
}
function NoRecordForThisUser(message){
    return{
        type:types.NO_RECORD_FOR_USER,
        message
    }
}
export function forgotPassword(email){
    return dispatch => {
        return makeRequest('post', {email}, '/users/forgotPassword')
            .then(response => {
                if(response.status=== 200) {
                    dispatch(changePasswordRequest(response.data.message));
                } else{
                    dispatch(changePasswordRequestFailed(response.data.message));
                }
            })
            .catch(err => {dispatch(NoRecordForThisUser(getMessage(err)))})
    }

}

/**
 * This action allow the user to update its preferences, so keeping the context of search, filter
 * don't need user id since this action only affect session user
 */
export function updateUserPrefs(data) {
    return dispatch => {

        try {
            dispatch(beginUserPrefs());
            dispatch(uppdateUserPrefsSuccess(data));
        }catch (err) {
            dispatch(uppdateUserPrefsError(data, err));
        }
    };
}

/**
 This action can update a user if an admin call this, or the logged user if any user update their profile information
 */
export function updateUser(id, data, isSelf = false) {
    return dispatch => {
        dispatch(beginUpdate());

        return makeRequest('post', data, '/user/' + id)
            .then(response => {
                if (response.status === 200) {
                    dispatch(updateSuccess(response.data, isSelf));
                } else {
                    dispatch(updateError('Oops! Something went wrong'));
                }
            })
            .catch(err => {
                dispatch(updateError(getMessage(err)));
            });
    };
}

//TODO shall be disabled from this part of the website
export function signUp(data) {
    return dispatch => {
        dispatch(beginSignUp());

        return makeRequest('post', data, '/signup')
            .then(response => {
                if (response.status === 200) {
                    dispatch(signUpSuccess(response.data.message));
                    dispatch(userProfile(response.data));
                    dispatch(push('/profile'));
                } else {
                    dispatch(signUpError('Oops! Something went wrong'));
                }
            })
            .catch(err => {
                dispatch(signUpError(getMessage(err)));
            });
    };
}

export function logOut() {
    return dispatch => {
        dispatch(beginLogout());

        return makeRequest('post', null, '/logout')
            .then(response => {
                if (response.status === 200) {
                    dispatch(logoutSuccess());
                    dispatch({type: types.FLUSH_USER});
                } else {
                    dispatch(logoutError());
                }
            });
    };
}

export function fetchUser(email) {
    return dispatch => {
        return request['get']('/user/'+email)
            .then((res) => {
                if(res.status ===200){
                    return dispatch({type: types.FETCH_USER_FROM_SERVER,
                        user: res.data})
                }
            })
    }
}

export function fetchUserCreationCount(userId){
    return dispatch => {
        return request['get']('/user/' + userId + '/creation-counts')
            .then((res) => {
                if(res.status ===200){
                    return dispatch({type: types.FETCH_USER_CREATION_COUNT,
                        items: res.data})
                }
            })
    }
}

export function getUsers(){
    return dispatch => {
        return request['get']('/users')
            .then((res) => {
                if(res.status === 200){
                    return dispatch(({type: types.FETCH_USERS,
                        users: res.data}))
                }
            })
    }
}

export function getUserGroups(){
    return dispatch => {
        return request['get']('/groups')
            .then((res) => {
                if(res.status === 200){
                    return dispatch(({type: types.FETCH_USERS_GROUPS,
                        groups: res.data}))
                }
            })
    }
}

export function submitImage(userId, data) {
    return (dispatch)=> {
        return makeRequest('post', data, '/user/' + userId + '/profile-picture')
            .then(res => {
                if(res.status === 200 ) {
                    dispatch(updateSuccess(res.data, true));
                }
            })
            .catch(() => dispatch(uploadFailure({error: 'Oops! Something went wrong and we couldn\'t get your profile picture'})));
    }
}

function deleteUserRequest(data) {
    return{
        type: types.DELETE_USER_REQUEST,
        user: data.user,
        id: data.user._id
    }
}

//needs userId and superAdmin ID, checks are done from back end part
export function deleteUser(id, saID){
    return dispatch => {
        return request['post']('/users/'+id+'/deactivate', {superAdmin: saID})
            .then((response)=> dispatch(deleteUserRequest(response.data)))
            .catch(() => console.log("Cannot delete user with id " + id))
    }
}
function activateUserRequest(data){
    return {
        type:types.ACTIVATE_USER_REQUEST,
        user: data.user
    }
}
export function activateUser(id, saID){
    return dispatch => {
        return request['post']('/users/'+id+'/restore',{superAdmin: saID})
          .then((response) => dispatch(activateUserRequest(response.data)))
          .catch((err) => console.log(err))
    }
}

function restoreUserRequest(id) {
    return{
        type: types.RESTORE_USER_REQUEST,
        id: id
    }
}

export function restoreUser(id, saID){
    return dispatch => {
        return request['post']('/users/'+id+'/restore', {superAdmin: saID})
            .then(()=> dispatch(restoreUserRequest(id)))
            .catch(() => console.log("Cannot restore user with id " + id))
    }
}

function changePasswordSuccess(message){
    return{
        type : types.CHANGE_PASSWORD_SUCCESS,
        message
    }
}

function changePasswordFailed(message){
    return {
        type: types.CHANGE_PASSWORD_FAILED,
        message
    }
}

export function changePassword(newPassword, confirmPass, token) {
    return dispatch => {
        return makeRequest('post', {newPassword, confirmPass}, '/reset/'+token)
            .then((res) =>
            {
                if(res.status===200) {
                    dispatch(changePasswordSuccess(res.data.message));
                    browserHistory.push('/')}
                else {dispatch(changePasswordFailed(res.data.message))}
            })
            .catch((err) =>  {return dispatch(changePasswordFailed(getMessage(err)))
            });
    }
}

export function progressMode(hasToShow) {
  return dispatch => {
    type: hasToShow? types.USER_SHOW_PROGRESS: types.USER_HIDE_PROGRESS
  };
}

export function showProgress() {
  return dispatch => {
      dispatch(progressMode(true));
  };
}

export function hideProgress() {
  return dispatch => {
      dispatch(progressMode(false));
  };
}
