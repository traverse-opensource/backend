import * as userActions from './users.js';
import * as ficheActions from './fiches.js';

module.exports = [
  userActions,
  ficheActions,
]

/*export {default as userActions} from './users.js';
export {default as ficheActions} from './fiches.js';*/
