import React , {Component}from 'react';
import { Modal, Button } from 'react-bootstrap';

class AlertFiche extends Component {
    constructor(props) {
        super(props);
        this.onDestroy = this.onDestroy.bind(this);
        this.state = {
            close: false,
        };

    };
    onDestroy() {
        const { toDelete, loggedUser, onDestroy, onHide , show} = this.props;
        //no need a full view refresh here

        onDestroy(toDelete, loggedUser).then(() => {
            onHide();
            return true;
        });
    };
    render() {
        const {toDelete} = this.props;
        return (
            <Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">
                        Suppression de {toDelete.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="alert alert-danger">
                    <div>
                        <strong >Attention!</strong>
                    </div>
                    <div>
                        <br/>
                        Êtes vous sûr de vouloir supprimer cet utilisateur?
                        <div> {toDelete.name} </div>
                        <div>
                            <Button bsStyle="danger" onClick={this.onDestroy}>Supprimer</Button>
                            <Button bsStyle="primary" onClick={() => this.props.onHide()}>Annuler</Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}
export default AlertFiche;