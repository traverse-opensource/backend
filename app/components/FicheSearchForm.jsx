import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import classNames from 'classnames/bind';
import styles from '../css/components/search-form.css';
const cx = classNames.bind(styles);

export default class FicheSearchForm extends Component {

  constructor(props){
    super(props);
    
    this.state = this.props.prefs && this.props.query?
      {
        types: this.props.prefs,
        query: this.props.query
      }:
      {
      types: {
        events: {checked: true, label: 'Evénement', key: 'events'},
        media: {checked: true, label: 'Media', key: 'media'},
        objects: {checked: true, label: 'Objet', key: 'objects'},
        people: {checked: true, label: 'Personne', key: 'people'},
        places: {checked: true, label: 'Site', key: 'places'}
      }
    };
    
    this.ready = false;
    
    let me = this;
    
    this.resetSearch = this.resetSearch.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
  }
  
  getQuery() {
    return this.refs.search.value;
  }

  getFilters(fullObject = false) {
    let toReturn;
    if (!fullObject) {
      toReturn = Object.keys(this.state.types).map((key, value) => {
        return {key: key, value: this.state.types[key].checked}
      }
    );
    
    
    }else {
      toReturn = this.state.types
    }
    return toReturn;  
  }
  
  handleTextChange(event) {
    this.setState({query: event.target.value});
  }
  
  //TODO use should component update here
  
  resetSearch() {
    let me = this,
        types = this.state.types,
        keys = Object.keys(types);
    
    keys.forEach((key) => {
      types[key].checked = true;
    });
    
    if (this.ready) {
      me.setState({
        query: "",
        types: types
      }, () => {
        me.props.search();
      });  
    }
    
  }

  toggleCheck(event) {
    let newState = this.state.types;

    newState[event].checked = !newState[event].checked;
    this.setState({...this.state.types});
  }
  
  render() {
    //console.info("Fiche Search Form @render searchFilters", this.props.searchFilters);
    
    let query = "";
    
    query = this.state.query;
    
    let input = <input type="text" ref="search" placeholder="Rechercher..." className="form-control" value={query} onChange={this.handleTextChange}/>;
    
    return (
      <form onSubmit={this.props.search} ref="searchFicheForm" className="search edit-search-form">
        <div className="row">
          <div className="col-md-12">
            <div className="input-group">
              {input}
              <span className="input-group-btn">
                  <button type="submit" className="btn btn-info" ref="bob">
                      <span className="glyphicon glyphicon-search"/>
                  </button>
              </span>
            </div>
          </div>
        </div>
        <div className={cx("filter") + " row"}>
          {
            Object.keys(this.state.types).map((key) => {
              return <CheckboxFilterComponent
                key={this.state.types[key].key}
                filterKey={this.state.types[key].key}
                icon={this.state.types[key].icon}
                label={this.state.types[key].label}
                checked={this.state.types[key].checked}
                toggleCheck={this.toggleCheck.bind(this, this.state.types[key].key)}/>
            })
          }
        </div>
      </form>
    );
  }

  /*
  {this.props.initSearch &&
        <button 
          type="button" 
          className="btn btn-warning"
          onClick={() => {
            //flush search value and set full fiche type search
            //this.resetSearch();
          }}>
          Réinitialiser la recherche
        </button>
        }
  */

};

class CheckboxFilterComponent extends Component {

  render() {
    return (
      <div className={cx("filter-item") + " col-lg-2 col-md-4"}>
        <label>
          <span className={"glyphicon glyphicon-"+this.props.icon}/>
          <span className={cx("filter-label")}>{this.props.label}</span>
          <input
            name={this.props.filterKey}
            type="checkbox"
            checked={this.props.checked}
            onChange={this.props.toggleCheck}
          />
        </label>
      </div>
    );
  }
}