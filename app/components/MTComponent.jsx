import React, {Component} from 'react';
/**
 This class as convenient method to autobind what we need
 Just using an array of string that represents the method name, must be camel case
 */
class MTComponent extends Component{
    constructor(props){
        super(props);
    }
  
    //must be called by its children to autobind methods
    setAutoBindMethods = (arrayOfString) => {
        this.toBind = arrayOfString;
        return this.autoBind();
    }
    
    //this method is only called internally, shall not be called from its children
    //TODO need to find a proper way to prevent such behaviour
    autoBind = () => {
        let l = this.toBind.length,
            currentMethod = null;
        while (l--){
            currentMethod = this.toBind[l];
            this[currentMethod] = this[currentMethod].bind(this);
        }
        
        return this;
    }

    //for its children it must be impossible to access this class render
    render(){
        return (<div>
            {this.props.children}
        </div>);
    }
}

export default MTComponent;