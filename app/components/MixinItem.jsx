import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {Button, Modal} from 'react-bootstrap';

import classNames from 'classnames/bind';
import playlistStyles from '../css/components/playlist';
import ficheStyles from '../css/components/fiche';

import { COVER_ON_IMAGE_LOAD } from '../constant';

const cxPlay = classNames.bind(playlistStyles);
const cxFiche = classNames.bind(ficheStyles);

import {border} from './fiche/FormUtils.jsx';

export default class MixinItem extends Component {
    constructor(props) {
      super(props);

      this.state= {
        show: false,
        dimens: {}
      }

      this.onDestroy = this.onDestroy.bind(this);
      this.onOpen = this.onOpen.bind(this);
      this.onImageLoad = this.onImageLoad.bind(this);

      this.choosenObj = {};
    }
    onDestroy() {
        const {onDeleted, order} = this.props;
        onDeleted(order);
    }

    onOpen() {
        const {onSelected, mixinItem} = this.props;

        onSelected(this.choosenObj.isFiche, mixinItem, this.props.order);
    }

    onImageLoad({target: img}) {
      COVER_ON_IMAGE_LOAD(img, (resultingDimens) => {
        this.setState({dimens: {h: resultingDimens[1], w: resultingDimens[0], l: resultingDimens[2], t: resultingDimens[3]}})
      });
    }

    render() {

        const {mixinItem, order, loggedUser} = this.props,
              {name, description, _id, __t} = mixinItem,
              DESC_MAX_CHAR = 100,
              {w, h, l} = this.state.dimens;

        let close = () => this.setState({show : false});
        var options = {
            year: "numeric", month: "numeric",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };

        var computeRemotePath = function(toTest){
          if (toTest){
            return toTest.path.indexOf('http') !== -1 ?
              '' + toTest.path: '/' + toTest.path;
          }
          return "/images/greybackground.png";
        };

        const styles = {
          selected: {
            backgroundColor: "#5bc0de",
            borderColor: "5bc0de",
            color: "white"
          },
          unselected: {
          }
        };

        var choosenObj = {
          icon: null,
          clazz: border(__t),
          description: description,
          //background: cxFiche('background-' + border(__t)),
          style: this.props.selected? styles.selected: styles.unselected,
          isFiche: true,
          cx: cxFiche
        };

        switch(__t) {
          case "People":
            choosenObj.description = mixinItem.short_bio;
            choosenObj.icon = '/images/ic_people.svg';
            break;
          case "Media":
            choosenObj.description = mixinItem.description;
            choosenObj.icon = '/images/ic_media.svg';
            break;
          case "Events":
            choosenObj.description = mixinItem.presentation;
            choosenObj.icon = '/images/ic_events.svg';
            break;
          case "Objects":
            choosenObj.description = mixinItem.short_description;
            choosenObj.icon = '/images/ic_objects.svg';
            break;
          case "Places":
            choosenObj.description = mixinItem.short_description;
            choosenObj.icon = '/images/ic_places.svg';
            break;
          default:
            choosenObj.isFiche = false;
            choosenObj.description = description;
            //choosenObj.background = cxPlay('background-playlist');
            choosenObj.clazz = 'playlist';
            choosenObj.icon = '/images/ic_playlist.svg',
            choosenObj.cx = cxPlay;
            break;
        }

        this.choosenObj = choosenObj;

        let targetClass = choosenObj.cx("background-" + choosenObj.clazz);

        const shortenDescription = (description) => {
          let toReturn = description;
          if (description.length > DESC_MAX_CHAR) {
            toReturn = toReturn.substring(0, DESC_MAX_CHAR) + "..."
          }
          return toReturn;
        }

        const buildOverlay = (mixin) => {
          if (this.props.isSuperAdmin) {
            return (
              <div className={cxPlay('overlay')}>
                <div className={cxPlay('pull-right')}>
                  <Button bsStyle="info" className="glyphicon glyphicon-retweet "
                          onClick={() => {this.onOpen()}}/>
                  <Button bsStyle="danger" className="glyphicon glyphicon-remove "
                          onClick={this.onDestroy}/>
                </div>
                <div className="clearfix"></div>
              </div>
            )
          }else {
            return (
              <div></div>
            )
          }
        }

        return (
            <div className={cxPlay('panel panel-default') + " " + cxPlay('cards')} style={choosenObj.style}>
                <div className={cxPlay('panel-heading')} style={choosenObj.style}>
                  <div className={cxPlay('imgcard')} style={{width: '100%', height: '160px', backgroundPosition: 'center center', backgroundRepeat: 'no-repeat', overflow: 'hidden', borderColor:mixinItem.theme.color}}>
                    <img src={computeRemotePath(mixinItem.cover)} style={{position: 'relative', width: w + 'px', height: h + 'px', left: l + 'px'}} onLoad={this.onImageLoad}/>
                  </div>
                  <div className={cxPlay('picto-card')} style={{backgroundColor:mixinItem.theme.color}}>
                    <img src={choosenObj.icon} className={targetClass}/>
                  </div>
                  <div className={cxPlay('picto-number')} style={{backgroundColor:mixinItem.theme.color}}>
                    <div style={{position: "relative", marginTop: "-6px"}}>{order? order: ""}</div>
                  </div>
                </div>
                <div className={cxPlay('panel-body') + " " + cxPlay('hovereffect')}>
                    <p>{shortenDescription(choosenObj.description)}</p>
                    <div style={{position: "relative", bottom: 0}}>
                      <p>Auteur :  {mixinItem.created_by.profile.name}</p>
                      <i>Créée le : {new Date(mixinItem.created_at).toLocaleDateString("fr-FR", options)}</i>
                    </div>
                    {buildOverlay()}
                </div>
            </div>
        )
    }
}
MixinItem.propTypes = {
    mixinItem: PropTypes.object.isRequired
};
