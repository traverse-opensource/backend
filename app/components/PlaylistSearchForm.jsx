import React, {Component} from 'react';

export default class PlaylistSearchForm extends Component {
    constructor(props){
      super(props);
      this.state={searchValue: ''};
      this.handleSubmit= this.handleSubmit.bind(this);
      this.handleChange= this.handleChange.bind(this);
      this.handleEnterPressSubmit = this.handleEnterPressSubmit.bind(this);
    }
    handleChange(e){
        this.setState({searchValue: e.target.value})
    }
    handleSubmit(e){
      if (this.props.search) {
        this.props.search(e);
      }else {
        this.props.filterWord(this.state.searchValue);
      }
    }
    handleEnterPressSubmit(e) {
      if (this.props.search) {
        this.props.search(e);
      }else {
        e.preventDefault();
        this.handleSubmit();  
      }
    }
  
    getQuery() {
      return this.state.searchValue;
    }

    render() {
        return (
            <form onSubmit={this.handleEnterPressSubmit}>
                <div className="input-group">
                    <input type="text" ref="search"
                           placeholder="Rechercher..."
                           className="form-control"
                           onChange={this.handleChange}/>
                    <span className="input-group-btn">
                    <button type="button" className="btn btn-info" onClick={this.handleSubmit}>
                        <span className="glyphicon glyphicon-search"/>
                    </button>
                </span>
                </div>
            </form>
        );
    }

};