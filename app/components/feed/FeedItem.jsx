import React, {Component, PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {Button} from 'react-bootstrap';
import Paper from 'material-ui/Paper';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import classNames from 'classnames/bind';
import feedStyles from '../../css/components/feed';

import { COVER_ON_IMAGE_LOAD, FEED_STATUS, FEED_STATUS_LABELS } from '../../constant';

const cx = classNames.bind(feedStyles);

import {border} from '../fiche/FormUtils.jsx';

export default class FeedItem extends Component {
    constructor(props) {
      super(props);

      this.state= {
        show: false,
        dimens: {}
      }

      this.onDestroy = this.onDestroy.bind(this);
      this.onOpen = this.onOpen.bind(this);
      this.onImageLoad = this.onImageLoad.bind(this);

      this.choosenObj = {};
    }
    onDestroy() {
        const {onDeleted, order} = this.props;
        onDeleted(order);
    }

    onOpen() {
        const {onSelected, mixinItem} = this.props;

        onSelected(this.choosenObj.isFiche, mixinItem, this.props.order);
    }

    onImageLoad({target: img}) {
      COVER_ON_IMAGE_LOAD(img, (resultingDimens) => {
        this.setState({dimens: {h: resultingDimens[1], w: resulonDestroytingDimens[0], l: resultingDimens[2], t: resultingDimens[3]}})
      });
    }

    render() {

        const {item, order} = this.props,
              //{name, description, _id, __t} = item,
              {fiche, feed} = item,
              {created_by, description, kind, cover, type, permalink, _id} = feed,
              {path, title} = fiche,
              __t = fiche.__t,
              DESC_MAX_CHAR = 100,
              {w, h, l} = this.state.dimens;

        let close = () => this.setState({show : false});
        var options = {
            year: "numeric", month: "numeric",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };

        var computeRemotePath = function(toTest){
          if (toTest){
            return toTest.path.indexOf('http') !== -1 ?
              '' + toTest.path: '/' + toTest.path;
          }
          return "/images/greybackground.png";
        };

        const styles = {
          selected: {
            backgroundColor: "#5bc0de",
            borderColor: "5bc0de",
            color: "white"
          },
          unselected: {
          }
        };

        const shortenDescription = (description) => {
          let toReturn = description;
          if (description.length > DESC_MAX_CHAR) {
            toReturn = toReturn.substring(0, DESC_MAX_CHAR) + "..."
          }
          return toReturn;
        }

        var choosenObj = {
          icon: null,
          clazz: border(__t),
          description: shortenDescription(feed.description),
          //background: cxFiche('background-' + border(__t)),
          style: this.props.selected? styles.selected: styles.unselected,
          isFiche: true,
          cx: cx
        };

        switch(__t) {
          case "People":
            choosenObj.icon = '/images/ic_people.svg';
            break;
          case "Media":
            choosenObj.icon = '/images/ic_media.svg';
            break;
          case "Events":
            choosenObj.icon = '/images/ic_events.svg';
            break;
          case "Objects":
            choosenObj.icon = '/images/ic_objects.svg';
            break;
          case "Places":
            choosenObj.icon = '/images/ic_places.svg';
            break;
          default:
            break;
        }

        this.choosenObj = choosenObj;

        let targetClass = choosenObj.cx("background-" + choosenObj.clazz);

        const buildOverlay = (reactItem, top, containerHeight, targetWidth = "230px") => {

          let holderStyle = {position: "absolute",width: targetWidth,display: "inline-block",top: "0px",height: containerHeight};
          let secondStyle = {position: "absolute", top: "0px", right: "0px", left: "0px", paddingTop: "4px", background: "rgba(0, 0, 0, 0.54)"};

          if (!top) {
            holderStyle = {position: "absolute",width: targetWidth,display: "inline-block",bottom: "0px",height: containerHeight};
            secondStyle = {position: "absolute", bottom: "0px", right: "0px", left: "0px", paddingTop: "4px", background: "rgba(0, 0, 0, 0.54)"};
          }
          return (
            <div style={holderStyle}>
              <div style={{height: "100%", position: "relative"}}>
                <div style={secondStyle}>
                  {reactItem}
                </div>
              </div>
            </div>
          );
        }

        const buildTitle = (fiche) => {
          let toInsert = (
            <div style={{padding: "12px", position: "relative"}}>
              <span style={{fontSize: "24px", color: "rgba(255, 255, 255, 0.87)", display: "block", lineHeight: "18px"}}></span>
              <span style={{fontWeight: '500', fontSize: "14px", color: "white", display: "block"}}>{fiche.title}</span>
            </div>
          );

          return buildOverlay(toInsert, true, "70px");
        };

        const moveToPage = (fiche, hasToEdit = false) => {
          //alert("moda focka" + hasToEdit + ficheId)

          let nextPath = "/fichesView/";
          let ficheType = fiche.__t;

          ficheType = ficheType[0].toLocaleUpperCase("fr") + ficheType.substring(1);

          const editSeeHashmap = {
            "Events": "editEvenement",
            "Objects": "editObjet",
            "People": "editPersonne",
            "Places": "editPlace",
            "Media": "editMedia"
          };

          if (hasToEdit) {
            //fichesView/editMedia
            nextPath += editSeeHashmap[ficheType] + "/" + fiche._id;
          }else {
            //fichesView/Media
            nextPath += ficheType + "/" + fiche._id;
          }

          browserHistory.push(nextPath);
        }

        const buildEditFicheButtons = (fiche) => {
          let toInsert = (
            <div style={{textAlign: "right"}}>
              <FlatButton label="voir" labelStyle={{color: "white"}} onClick={() => { moveToPage(fiche) }}/>
              <FlatButton label="éditer" labelStyle={{color: "white"}} onClick={() => { moveToPage(fiche, true) }}/>
            </div>
          );

          return buildOverlay(toInsert, false, "28px");
        };

        const buildEditFeedButtons = (feed, mode) => {

          let { deleteCallback, approveCallback } = this.props;

          //posted by default
          let left = {
            callback: deleteCallback,
            label: FEED_STATUS_LABELS.DELETED
          },
          right = {
            callback: approveCallback,
            label: FEED_STATUS_LABELS.APPROVED
          };

          switch(mode) {
            case FEED_STATUS.DELETED:
              left = null;
              right = { callback: approveCallback, label: FEED_STATUS_LABELS.RESTORED };
              break;
            case FEED_STATUS.FLAGGED:
              left = { callback: deleteCallback, label: FEED_STATUS_LABELS.DELETED };
              right = { callback: approveCallback, label: FEED_STATUS_LABELS.APPROVED };
              break;
            case FEED_STATUS.APPROVED:
              left = { callback: deleteCallback, label: FEED_STATUS_LABELS.DELETED };
              right = null;
              break;
          }

          let toInsert = (
            <div style={{textAlign: "right"}}>
              {left &&
                <FlatButton label={left.label} labelStyle={{color: "white"}} onClick={function() {left.callback(feed._id)}}/>
              }
              {right &&
                  <FlatButton label={right.label} labelStyle={{color: "white"}} onClick={function() {right.callback(feed._id)}}/>
              }

            </div>
          );

          return buildOverlay(toInsert, false, "28px", "270px");
        }


        return (
          <Paper zDepth={2} className="col-md-12" style={{marginTop:"8px", paddingTop: "4px", paddingBottom: "4px"}}>
            <div className="row">
              <div className="col-md-4">
                <div
                  className={cx("center-cropped")}
                  style={{backgroundImage: 'url("'+computeRemotePath(fiche)+'")', float: 'left', width: '230px'}}>
                  {buildTitle(fiche)}
                  {buildEditFicheButtons(fiche)}
                </div>
              </div>
              <div className="col-md-8">
                <div className="row">
                  <div className="col-md-4">
                    <CardHeader
                      title={feed.created_by.name}
                      subtitle={feed.description}
                      actAsExpander={true}
                      showExpandableButton={true}
                    />
                  </div>
                  <div className="col-md-8" style={{float: 'right'}}>
                    <div
                      className={cx("center-cropped")}
                      style={{backgroundImage: 'url("'+computeRemotePath(feed.cover)+'")', float: 'right', width: '270px'}}>
                      {buildEditFeedButtons(feed, this.props.mode)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        );
    }
}
FeedItem.propTypes = {
    mixinItem: PropTypes.object.isRequired
};
