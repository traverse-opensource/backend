import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactScrollPagination from 'react-scroll-pagination';
import FeedItem from './FeedItem.jsx';

//using user prefs here and we don't need fiche actions witgin this component
import * as actions from '../../actions/users.js';
import {DropdownButton, MenuItem} from 'react-bootstrap';
import { status, FICHE_FILTERS, FEED_STATUS, FEED_STATUS_STRING, FEED_STATUS_PATH, EDITORIAL_HEADER } from '../../constant';

import axios from 'axios';

class FeedSection extends Component {
  constructor(props){
    super(props);

    let useUserPrefs = false;

    this.state = {
      shown: FEED_STATUS.POSTED,
      shownTitle: FEED_STATUS_STRING.POSTED,
      path: FEED_STATUS_PATH.POSTED,
      feeds: [],
      apiPort: null,
      pagination: {
        page: 1,
        limit: 18
      },
      needToFetch: true
    };

    this.fetchNew = this.fetchNew.bind(this);
    this.updateSearchFilter = this.updateSearchFilter.bind(this);
    this.notifyParentDestroy = this.notifyParentDestroy.bind(this);
    this.fetchApiPort = this.fetchApiPort.bind(this);

    this.buildBaseUrl = this.buildBaseUrl.bind(this);
    this.buildHeader = this.buildHeader.bind(this);
    this.deleteFeed = this.deleteFeed.bind(this);
    this.approveFeed = this.approveFeed.bind(this);

    this.ready = true;
  }

  deleteFeed(feedId) {
    let me = this;
    let baseUrl = this.buildBaseUrl();
    let url = baseUrl + feedId + "/" + this.props.sessionUser._id;

    me.ready = false;

    fetch(url, me.buildHeader('DELETE'))
    .then(function(response) {
      return response.json();
    })
    .then(function(toRemove) {

      let newFeeds = me.state.feeds.filter((feed) => {
        //feed object holder the fiche and the feed itself
        return feed.feed._id != toRemove._id;
      });

      me.setState({feeds: newFeeds});
      me.ready = true;
    });
  }

  approveFeed(feedId) {
    let me = this;
    let baseUrl = this.buildBaseUrl();
    let url = baseUrl + feedId + "/" + this.props.sessionUser._id + '/approve';

    me.ready = false;

    fetch(url, me.buildHeader('PUT'))
    .then(function(response) {
      return response.json();
    })
    .then(function(toRemove) {

      let newFeeds = me.state.feeds.filter((feed) => {
        //feed object holder the fiche and the feed itself
        return feed.feed._id != toRemove._id;
      });

      me.setState({feeds: newFeeds});
      me.ready = true;
    });
  }

  buildBaseUrl() {
    let protocol = location.protocol;
    let slashes = protocol.concat("//");
    let host = slashes.concat(window.location.hostname);
    let port = this.state.apiPort;
      //config.api.https;

    // since it's a weird configuration, just redirecting to the real one for the feeds
    if (host.indexOf("edit") > -1) {
      host = host.replace("edit.", "");
    }

    return host + ":" + port + "/api/v1.2/feeds/";
  }

  buildHeader(method) {
    let myHeaders = new Headers(EDITORIAL_HEADER);

    var config = {
      method: method,
      headers: myHeaders,
      mode: 'cors',
      cache: 'default'
    };
    return config;
  }

  fetchNew() {

    if (this.state.apiPort === null) {
      this.fetchApiPort();
    }else {
      let me = this,
          {pagination} = me.state;

      let baseUrl = this.buildBaseUrl();

      let url = baseUrl + this.state.path + '/?page='+pagination.page+'&limit='+pagination.limit;

      if (me.ready) {
        me.ready = false;

        fetch(url, me.buildHeader('GET'))
        .then(function(response) {
          return response.json();
        })
        .then(function(data) {

          let previousFeeds = me.state.feeds,
            newFeeds = data,
            newPagination = me.state.pagination;

          newPagination.page++;

          me.setState({feeds: [...previousFeeds, ...newFeeds], pagination: newPagination, needToFetch: newFeeds.length === newPagination.limit});
          me.ready = true;
        });
      }
    }
  }

  componentDidMount() {
    let eventName = "Toutes les fiches publiées";
    let me = this;

    //request api port first

    this.fetchApiPort();
  }

  fetchApiPort() {
    let me = this;
    fetch('/api/port')
      .then((response) => {return response.json()})
      .then((data) => {
        let port = data.port;
        me.setState({ apiPort: port }, () => {
          me.fetchNew();
        })
      })
  }

  handleEvent(e){
    let me = this;

    //first case for "Toutes les fiches publiées"
    /*let filter = FICHE_FILTERS.ALL,
        status = 1,
        toSet = {draft: false, all: true, filter: filter, status: status};*/
    //ALL: 0, ALL_DRAFT: 1, MY_PUBLISHED: 2, MY_DRAFT: 3

    let newTitle = "",
      newPath = "";

    switch (e) {
      case FEED_STATUS.DELETED:
        newTitle = FEED_STATUS_STRING.DELETED;
        newPath = FEED_STATUS_PATH.DELETED;
        break;
      case FEED_STATUS.FLAGGED:
        newTitle = FEED_STATUS_STRING.FLAGGED;
        newPath = FEED_STATUS_PATH.FLAGGED;
        break;
      case FEED_STATUS.POSTED:
        newTitle = FEED_STATUS_STRING.POSTED;
        newPath = FEED_STATUS_PATH.POSTED;
        break;
      case FEED_STATUS.APPROVED:
        newTitle = FEED_STATUS_STRING.APPROVED;
        newPath = FEED_STATUS_PATH.APPROVED;
        break;
      default:
        //no need moda foka
        break;
    }

    //when using this, we shall reset everythin
    let pagination = { page: 1, limit: 18 },
      feeds = [];

    //shall be different here!
    me.setState({shownTitle: newTitle, path: newPath, shown: e, pagination, feeds}, () => {
      me.fetchNew();
    });
    /*me.setState({shownTitle: newTitle}, () => {
      me.setState({
        pagination: {
          page: 1,
          limit: 18
        },
        fiches: []
      }, () => {
        me.fetchNew();
      });
    });*/
  }

  updateUserPrefsFromInner(query, filters, realFilters) {
    let searchFilters = {
      query: query,
      filters: realFilters
    };

    let me =this;

    this.updateSearchFilter(query, filters);
  }

  updateSearchFilter(query, filters, extraParams = null){

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    let types = filters.filter((f) => {
      return f.value;
    }).map((f) => {
      return capitalizeFirstLetter(f.key);
    });

    let me = this,
        newPagination = me.state.pagination;

    newPagination.isCustom = true;
    newPagination.page = 1;
    newPagination.query = query;
    newPagination.filters = types;

    if (extraParams !== null) {
      newPagination.status = extraParams.status;
      newPagination.filter = extraParams.filter;
    }

    me.setState({
      pagination: newPagination,
      fiches: []
    }, () => {
      me.fetchNew();
    });
  }

  theFuncToFetchNextPage() {
    //alert('should load next page');
    if (this.state.needToFetch) {
      this.fetchNew();
    }
  }

  notifyParentDestroy(ficheId, cb) {
    let me = this;

    /*let userPrefs = me.props.sessionUser.prefs;

    //TODO need to handle the case when we unpublished a fiche from the edit fiche container
    let newFiches = me.state.fiches.filter((fiche) => {
      return fiche._id !== ficheId;
    });*/

    me.setState({fiches: newFiches});
  }

  render() {
      const {onDestroy, getCreator, fiches} = this.props;
      let allPublishedFiches, AllDrafts, myPublishedFiches, myDraft;
      let searchFilters = null;

      if (this.state.filters && this.state.query) {
        searchFilters = {query: this.state.query, filters: this.state.filters};
      }


      const feedItem = (feed, key) => {
        return(
          <div className="col-md-12" key={key}>
              <FeedItem
                item={feed}
                order={key}
                mode={this.state.shown}
                sessionUser={this.props.sessionUser}
                deleteCallback={this.deleteFeed}
                approveCallback={this.approveFeed}/>
          </div>
        )
      };

      //let copyFiches = JSON.parse(JSON.stringify(fiches));

      let loadedFeeds = this.state.feeds;
      let items = [];
      if(loadedFeeds){
        items = loadedFeeds.map((feed, key) => {
          return feedItem(feed, key);
        });
      }
      return (
          <div className="row">
              <div className="col-md-2">
                  <div className="sidebar-nav-fixed affix">
                      <br/>
                      <DropdownButton key={1} id="flagFiches" title={this.state.shownTitle} onSelect={this.handleEvent.bind(this)}>
                          <MenuItem eventKey={FEED_STATUS.POSTED}>{FEED_STATUS_STRING.POSTED}</MenuItem>
                          <MenuItem eventKey={FEED_STATUS.FLAGGED}>{FEED_STATUS_STRING.FLAGGED}</MenuItem>
                          <MenuItem divider />
                          <MenuItem eventKey={FEED_STATUS.DELETED}>{FEED_STATUS_STRING.DELETED}</MenuItem>
                          <MenuItem eventKey={FEED_STATUS.APPROVED}>{FEED_STATUS_STRING.APPROVED}</MenuItem>
                      </DropdownButton>
                  </div>
              </div>
              <div className="col-md-10">
                  <div className="row">
                    {items}
                    <ReactScrollPagination
                      fetchFunc={this.theFuncToFetchNextPage.bind(this)}/>
                  </div>
              </div>
          </div>
      );
  };
}

FeedSection.propTypes = {
    //onDestroy : PropTypes.func.isRequired
};


export default connect(() => { return {}}, actions)(FeedSection);
