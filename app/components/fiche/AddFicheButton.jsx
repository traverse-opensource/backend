import React, { Component, PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Field, reduxForm, FormGroup, ControlLabel, FormControl } from 'redux-form'
import {browserHistory} from 'react-router'

export default class AddFicheButton extends Component {
    constructor(props) {
        super(props);
        this.state = {show : false} ;
    }
    render(){
        let close = () => this.setState({ show : false});
        return(
            <div>
                <Button bsStyle="primary" onClick={()=> this.setState({show : true})}>
                    + Ajouter une fiche
                </Button>
                <ModalFiche show={this.state.show} onHide={close}/>
            </div>
        )
    }
}

class ModalFiche extends Component {
    onClick(){
        if(this.state != null && this.state.type != null){
            switch(this.state.type){
                case "person":
                    browserHistory.push('/fichesView/fichePersonne');
                    break;
                case "place":
                    browserHistory.push('/fichesView/fichePlace');
                    break;
                case "media":
                    browserHistory.push('/fichesView/ficheMedia');
                    break;
                case "object":
                    browserHistory.push('/fichesView/ficheObjet');
                    break;
                case "event":
                    browserHistory.push('/fichesView/ficheEvenement');
                    break;
                default:
                    break;
            }
        }
        else{console.log("bad request")}

    }
    onSelected(e){
        this.setState({
            type:e.target.value,
        });
    }
    render() {
        return (
            <Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Ajouter une fiche</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <label>Quel type de fiche? </label>
                    <div>
                        <div className="form-group">
                            <select name="ficheType" onChange={this.onSelected.bind(this)} className="form-control">
                                <option> </option>
                                <option value="place">Site</option>
                                <option value="person">Personne</option>
                                <option value="media">Media</option>
                                <option value="object">Objet</option>
                                <option value="event">Événement</option>
                            </select>
                        </div>
                        <br/>
                        <Button bsStyle="primary" onClick={this.onClick.bind(this)}> Créer une fiche</Button>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}
ModalFiche.propTypes = {
    name: PropTypes.string,
    text:PropTypes.string,
};
