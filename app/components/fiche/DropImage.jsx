import React, { Component } from 'react';
import Dropzone from 'react-dropzone';

class DropImage extends Component {
    constructor(props){
        super(props);
        this.setStateCover = this.setStateCover.bind(this);
        this.state = {};
    }
    setStateCover(){
        this.props.setStateCover();
    }
    onDrop(acceptedFiles) {
        var formData = new FormData();
        formData.append('file', acceptedFiles[0]);
        const { submitImage } = this.props;

        if (this.props.userId) {
          submitImage(this.props.userId, formData).then(() => {
            this.setStateCover();
          });
        }else {
          submitImage(formData).then(() => {
            this.setStateCover();
          });
        }

    }

    onDropRejected() {
      //change it later on
      alert("La taille maximale d'une image est fixée à 2Mo, veuillez changer ou redimensionner l'image désirée");
    }

    render(){

        let targetLink = (cover) => {
          let url = cover? cover.path: null
          if(url)
          {
              return url.indexOf('http') !== -1 ?
                  url: '/' + url
          }
          return null;
        };

        const { uploadedFile } = this.props,
              field = this.props.field ? this.props.field: {name: "", meta: {touched: false, error: false}}
        return (
            <div style={{width:"auto", height:"auto"}}>
                <Dropzone
                    style={{
                        width:"auto",
                        height:"auto",
                        borderColor: "RGBA(169, 169, 169, 1)",
                        borderStyle: "dashed",
                        borderRadius: "5px"
                    }}
                    multiple={false}
                    accept="image/*"
                    name={field.name}
                    onDrop={this.onDrop.bind(this)}
                    maxSize={2097152}
                    onDropRejected={this.onDropRejected.bind(this)}>
                    <div>Glisser une image ou parcourir.</div>
                    <br/>
                    {field.meta.touched &&
                    field.meta.error &&
                    <span className="error">{field.meta.error}</span>}
                    {uploadedFile && (
                        <div style={{textAlign:"center"}}>
                            <img src={targetLink(uploadedFile)} style={{maxWidth:"100%"}}/>
                        </div>
                    )}
                </Dropzone>
            </div>
        )

    }
}
export default DropImage;
