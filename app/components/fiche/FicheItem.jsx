/**
 * Created by hasj on 21/11/2016.
 */
import React, { Component, PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import {browserHistory} from 'react-router';
import { RIGHTS, COVER_ON_IMAGE_LOAD } from '../../constant'

import classNames from 'classnames/bind';
import ficheStyles from '../../css/components/fiche';
import {border} from './FormUtils.jsx';

const cx = classNames.bind(ficheStyles);

export default class FicheItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            alert: false,
            user: {},
            dimens: {}
        };

        this.onImageLoad = this.onImageLoad.bind(this);
    };

    onImageLoad({target: img}) {
      COVER_ON_IMAGE_LOAD(img, (resultingDimens) => {
        this.setState({dimens: {h: resultingDimens[1], w: resultingDimens[0], l: resultingDimens[2], t: resultingDimens[3]}})
      });
    }

    render() {
        const {w, h, l} = this.state.dimens;

        let close = () => this.setState({alert : false});
        const {name, _id, __t, fiche, sessionUser} = this.props;
        const {email, group, profile} = sessionUser;
        let permissionContainer =
            <Link to={'/fichesView/' + __t + '/' + _id}
                  className={cx('btn') + " " + cx('btn-info') + " " + cx('glyphicon') + " " + cx('glyphicon-eye-open')}>
            </Link>;

      const matchingId = fiche.created_by ?
            fiche.created_by._id: fiche.created_by;
      if(RIGHTS.HAS_ENOUGH_PRIVILEGES(sessionUser,matchingId, sessionUser.group )){
            permissionContainer =
                <div>
                    <Link to={'/fichesView/' + __t + '/' + _id}
                          className={cx('btn') + " " + cx('btn-info') + " " + cx('glyphicon') + " " + cx('glyphicon-eye-open')}>
                    </Link>
                    <Button bsStyle="warning" className="glyphicon glyphicon-edit "
                            onClick={() => navigateToEditForm(__t, _id)}/>
                    < Button bsStyle = "danger" className="glyphicon glyphicon-remove "
                             onClick={() => this.setState({alert: true})}/>
                </div>
        }
        const navigateToEditForm = (typeFiche, _id) => {
            switch (typeFiche) {
                case 'People':
                    return browserHistory.push("fichesView/editPersonne/"+_id);
                    break;
                case 'Places':
                    return browserHistory.push("fichesView/editPlace/"+_id);
                    break;
                case 'Media':
                    return browserHistory.push("fichesView/editMedia/"+_id);
                    break;
                case 'Objects':
                    return browserHistory.push("fichesView/editObjet/"+_id);
                    break;
                case 'Events':
                    return browserHistory.push("fichesView/editEvenement/"+_id);
                    break;
                default:
                    break;
            }};

        var options = {
            year: "numeric", month: "numeric",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };

        var presentation = <div dangerouslySetInnerHTML={{__html: fiche.presentation}} />;

        if(__t == "People"){
            presentation = <div dangerouslySetInnerHTML={{__html: fiche.short_bio}} />;
        }else if(__t == "Media"){
            presentation = <div dangerouslySetInnerHTML={{__html: fiche.description}} />;
        }else if(__t == "Events"){
            presentation = <div dangerouslySetInnerHTML={{__html: fiche.presentation}} />;
        }else if(__t == "Objects"){
            presentation = <div dangerouslySetInnerHTML={{__html: fiche.short_description}} />;
        }else if(__t == "Places"){
            presentation = <div dangerouslySetInnerHTML={{__html: fiche.short_description}} />;
        }

        let targetLink = (url) => {

            const createThumbnail = (url) => {
              let splitted = url.split('/');

              //since the split removes the first "/"
              return "/" + splitted[0] + "/thumb/" + splitted[1];
            }

            if(url) {
                return url.indexOf('http') !== -1 ?
                    url : createThumbnail(url);
            }
        };

        const computeImgSrc = (fiche) => {
          //test thumbnail
          return fiche.cover ? targetLink(fiche.cover.path) : "/images/greybackground.png"
        };

        let themeColor = fiche.theme.color,
            imgSrc = '/images/ic_' + __t.toLowerCase() + '.svg';
        return (
            <div className={cx('panel panel-default') + " " + cx('cards')} style={{width: "260px"}}>
                <Link to={'/fichesView/' + __t + '/' + _id}>
                    <div className={cx('panel-heading')}>
                        <div className={cx('imgcard')} style={{width: '100%', height: '160px', backgroundPosition: 'center center', backgroundRepeat: 'no-repeat', overflow: 'hidden', borderColor:themeColor}}>
                            <img src={computeImgSrc(fiche)} style={{position: 'relative', width: w + 'px', height: h + 'px', left: l + 'px'}} onLoad={this.onImageLoad}/>
                        </div>
                        <div className={cx('picto-card')} style={{backgroundColor:themeColor }}>
                          <img src={imgSrc} className={cx('background-' + border(__t))}/>
                        </div>
                    </div>
                </Link>
                <div className={cx('panel-body') + " " + cx('hovereffect')}>
                    <h4 className ={cx('fiche_title-' + border(__t))}>{name}</h4>
                    {presentation}
                    <p>Auteur :  {fiche.created_by.profile.name}</p>
                    <i>Créée le : {new Date(fiche.created_at).toLocaleDateString("fr-FR", options)}</i>
                    <div className={cx('overlay')}>
                        <div className={cx('pull-right')}>
                            {permissionContainer}
                        </div>
                        <div className="clearfix"></div>
                        <AlertFiche
                            show={this.state.alert}
                            onHide={close}
                            name={name}
                            _id={_id}
                            onDestroy={this.props.onDestroy}
                            notifyParentDestroy={this.props.notifyParentDestroy}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

class AlertFiche extends Component {
    constructor(props) {
        super(props);
        this.onDestroy = this.onDestroy.bind(this);
        this.state = {
            close: false,
        };

    };
    onDestroy() {
        const { _id, onDestroy, onHide } = this.props;
      console.log("<<inside alert fiche>>", _id, onDestroy, onHide);
        let me = this;
        //no need a full view refresh here

      onDestroy(_id).then(() => {
        onHide();

        me.props.notifyParentDestroy(_id);
        return true;
      });


    };
    render() {
        const {name} = this.props;
        return (
            <Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">
                        Suppression de {name}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="alert alert-danger">
                    <div>
                        <strong >Attention!</strong>
                    </div>
                    <div>
                        <br/>
                        Êtes vous sûr de vouloir supprimer cette fiche?
                        <div> {name} </div>
                        <div>
                            <Button bsStyle="danger" onClick={this.onDestroy}>Supprimer</Button>
                            <Button bsStyle="primary" onClick={() => this.props.onHide()}>Annuler</Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

AlertFiche.propTypes = {
    name: PropTypes.string,
    descr:PropTypes.string,
    _id : PropTypes.string,
};
