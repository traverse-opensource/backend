import React from 'react';
import  AddFicheButton  from './AddFicheButton.jsx';


const FicheMenu = ({}) => {
    return(
        <div className="sidebar-nav-fixed affix">
            <ul className="nav nav-pills nav-stacked">
                <li role="presentation"><a href="#">Mes brouillons</a></li>
                <li role="presentation"><a href="#">Tous les brouillons</a></li>
            </ul>
            <AddFicheButton/>
        </div>
    );
};
export default FicheMenu;