import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactScrollPagination from 'react-scroll-pagination'
import FicheItem from './FicheItem.jsx';
import InnerSearchForm from './InnerSearchForm.jsx';

//using user prefs here and we don't need fiche actions witgin this component
import * as actions from '../../actions/users.js';
import  AddFicheButton  from './AddFicheButton.jsx';
import {DropdownButton, MenuItem} from 'react-bootstrap';
import { status, FICHE_FILTERS } from '../../constant';

import axios from 'axios';

class FicheSection extends Component {
    constructor(props){
      super(props);
      
      let useUserPrefs = false;
      if (this.props.sessionUser.prefs) {
        if (this.props.sessionUser.prefs) {
          useUserPrefs = false;
        }
      }
      
      this.state = useUserPrefs ? 
        this.props.sessionUser.prefs:
        {
          draft: false,
          all :true,
          mineDraft: false,
          mineAll: false,
          dropdownTitle: 'Choisir les fiches...',
          initSearch: true
        }
      
      this.state.pagination = {
        page: 1,
        limit: 18,
        status: 1,
        filter: FICHE_FILTERS.ALL
      };
      
      this.state.fiches = [];
      
      this.ready = true;
      
      this.mergePrefsIfNeeded = this.mergePrefsIfNeeded.bind(this);
      this.fetchNew = this.fetchNew.bind(this);
      this.updateSearchFilter = this.updateSearchFilter.bind(this);
      this.notifyParentDestroy = this.notifyParentDestroy.bind(this);
    }
  
  fetchNew() {
    
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    let me = this,
        {pagination} = me.state;
    
    let url = '/fiches?page='+pagination.page+'&limit='+pagination.limit+'&state='+pagination.status+'&filter='+pagination.filter+'&userId='+me.props.sessionUser._id;
    
    let countFilters = 0;
    
    if (me.props.sessionUser.prefs && me.props.sessionUser.prefs.filters) {
    //if (pagination.isCustom) {
      
      let filters = me.props.sessionUser.prefs.filters;
      
      Object.keys(filters).forEach((f) => {
        let filter = filters[f];
        if (filter.checked) {
          url += filter.checked ? '&filters='+capitalizeFirstLetter(filter.key): "";
          ++countFilters;  
        }
      });
      
      url += '&query='+me.props.sessionUser.prefs.query;
      
      if (countFilters === 1) {
        url += '&filters';
      }
    }
    
    console.info("url", url,countFilters);
    
    if (me.ready) {
      me.ready = false;
      axios.get(url)
        .then(function (response) {

          let newFiches = response.data.fiches,
              newPagination = me.state.pagination,
              previousFiches = me.state.fiches;
          newPagination.page++;

          me.setState({
            fiches: [...previousFiches, ...newFiches],
            pagination: newPagination
          });
          me.ready = true;
        })
        .catch(function (error) {
          console.log("Axios Error", error);
        });
    }
    
  }
  
  componentWillUpdate(nextProps, nextState) {
  }
  
  mergePrefsIfNeeded(newData, cb = null) {
    const me = this,
          previousPrefs = this.props.sessionUser.prefs;
    let toSet = {};

    if (previousPrefs) {
      let keys = Object.keys(newData);
      keys.map((key) => {
        previousPrefs[key] = newData[key];
      });

      toSet = previousPrefs;
    }else {
      toSet = newData;
    }

    me.setState(toSet, () => {
      me.props.updateUserPrefs({prefs: toSet});

      if (cb !== null) {
        cb();
      }
    });
  }

  componentDidMount() {
    let eventName = "Toutes les fiches publiées";
    let me = this;

    if (this.state.fiches.length === 0) {

      //checking the userPrefs here
      if (this.props.sessionUser.prefs) {
        
        let userPrefs = this.props.sessionUser.prefs;
        
        if (userPrefs.filters && userPrefs.query) {
          let filters = userPrefs.filters;
          let newFilters = Object.keys(filters).map((f) => {
            return filters[f];
          });
          
          let extraParams = {
            status: userPrefs.status,
            filter: userPrefs.filter
          }
          
          this.updateSearchFilter(this.props.sessionUser.prefs.query, newFilters, extraParams);
        }
      }else {
        this.fetchNew();
      }
    }


    if (this.props.sessionUser && this.props.sessionUser.prefs) {
      let prefs = this.props.sessionUser.prefs;

      if (prefs.all) {
        if (prefs.draft) {
          eventName = "Tous les brouillons";
        }
      } else {
        if (prefs.mineDraft) {
          eventName = "Mes brouillons";
        }else {
          eventName = "Mes fiches publiées";
        }
      }
    }

    this.handleEvent(eventName);
  }  

  handleEvent(e){
    let me = this;

    //first case for "Toutes les fiches publiées"
    let filter = FICHE_FILTERS.ALL,
        status = 1,
        toSet = {draft: false, all: true, filter: filter, status: status};
    //ALL: 0, ALL_DRAFT: 1, MY_PUBLISHED: 2, MY_DRAFT: 3

    if (e === "Tous les brouillons") {
      filter = FICHE_FILTERS.ALL_DRAFT;
      status = 0;
      toSet = {draft: true, all: true, filter: filter, status: status};
    } else if (e === "Mes fiches publiées") {
      filter = FICHE_FILTERS.MY_PUBLISHED;
      status = 1;
      toSet = {mineDraft: false, all: false, filter: filter, status: status};
    } else if (e === "Mes brouillons") {
      filter = FICHE_FILTERS.MY_DRAFT;
      status = 0;
      toSet = {mineDraft: true, all: false, filter: filter, status: status};
    }

    //shall be different here!

    me.setState({dropdownTitle: e}, () => {
      me.mergePrefsIfNeeded(toSet);
      me.setState({
        pagination: {
          page: 1,
          limit: 18,
          status: status,
          filter: filter
        },
        fiches: []
      }, () => {
        me.fetchNew();
      });
    });
  }

  updateUserPrefsFromInner(query, filters, realFilters) {
    let searchFilters = {
      query: query,
      filters: realFilters
    };

    let me =this;

    this.updateSearchFilter(query, filters);

    me.mergePrefsIfNeeded(searchFilters);
  }

  updateSearchFilter(query, filters, extraParams = null){

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    let types = filters.filter((f) => {
      return f.value;
    }).map((f) => {
      return capitalizeFirstLetter(f.key);
    });

    let me = this,
        newPagination = me.state.pagination;

    newPagination.isCustom = true;
    newPagination.page = 1;
    newPagination.query = query;
    newPagination.filters = types;

    if (extraParams !== null) {
      newPagination.status = extraParams.status;
      newPagination.filter = extraParams.filter;
    }

    me.setState({
      pagination: newPagination,
      fiches: []
    }, () => {
      me.fetchNew();
    });
  }

  theFuncToFetchNextPage() {
    //alert('should load next page');
    this.fetchNew();
  }
  
  notifyParentDestroy(ficheId, cb) {
    let me = this;

    let userPrefs = me.props.sessionUser.prefs;

    //TODO why this stupid condition ???
    /*if (userPrefs.filters) {

      let filters = userPrefs.filters;
      let newFilters = Object.keys(filters).map((f) => {
        return filters[f];
      });

      let extraParams = {
        status: userPrefs.status,
        filter: userPrefs.filter
      }

      let newQuery = userPrefs.query ?
          userPrefs.query: "";
      
      me.updateSearchFilter(newQuery, newFilters, extraParams);
    }*/
    
    //TODO need to handle the case when we unpublished a fiche from the edit fiche container
    let newFiches = me.state.fiches.filter((fiche) => {
      return fiche._id !== ficheId;
    });

    me.setState({fiches: newFiches});
  }

  render() {
      const {onDestroy, getCreator, fiches} = this.props;
      let allPublishedFiches, AllDrafts, myPublishedFiches, myDraft;
      let searchFilters = null;

      if (this.state.filters && this.state.query) {
        searchFilters = {query: this.state.query, filters: this.state.filters};
      }


      const ficheItem = (fiche, key) => {
          return(
              <div className="col-md-4" key={key}>
                  < FicheItem
                      text={fiche.name}
                      _id={fiche._id}
                      name={fiche.name}
                      __t={fiche.__t}
                      themes={fiche.themes}
                      fiche={fiche}
                      onDestroy={onDestroy}
                      notifyParentDestroy={this.notifyParentDestroy}
                      getCreator={getCreator}
                      sessionUser={this.props.sessionUser}
                  />
              </div>
          )
      };

      //let copyFiches = JSON.parse(JSON.stringify(fiches));

      let loadedFiches = this.state.fiches;
      let items = [];
      if(loadedFiches){
        items = loadedFiches.map((fiche, key) => {
            return ficheItem(fiche, key);
        });
      }
      return (
          <div className="row">
              <div className="col-md-3">
                  <div className="sidebar-nav-fixed affix">
                      <AddFicheButton/>
                      <br/>
                      <DropdownButton key={1} id="flagFiches" title={this.state.dropdownTitle} onSelect={this.handleEvent.bind(this)}>
                          <MenuItem eventKey="Toutes les fiches publiées">Toutes les fiches publiées</MenuItem>
                          <MenuItem eventKey="Tous les brouillons">Tous les brouillons</MenuItem>
                          <MenuItem divider />
                          <MenuItem eventKey="Mes fiches publiées">Mes fiches publiées</MenuItem>
                          <MenuItem eventKey="Mes brouillons">Mes brouillons</MenuItem>
                      </DropdownButton>
                  </div>
              </div>
              <div className="col-md-9">
                  <InnerSearchForm
                    updateUserPrefs={this.updateUserPrefsFromInner.bind(this)}
                    prefs={this.props.sessionUser.prefs ? this.props.sessionUser.prefs.filters: null}
                    query={this.props.sessionUser.prefs? this.props.sessionUser.prefs.query: null}
                    searchFilters={searchFilters}/>
                  <div className="row">
                    {items}
                    <ReactScrollPagination
                      fetchFunc={this.theFuncToFetchNextPage.bind(this)}/>
                  </div>
              </div>
          </div>
      );
  };
}

FicheSection.propTypes = {
    onDestroy : PropTypes.func.isRequired
};


export default connect(() => { return {}}, actions)(FicheSection);