import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import {browserHistory} from 'react-router';
import Promise from 'es6-promise';
import TinyMCE from 'react-tinymce';
import DatePicker from 'react-datepicker';
import Slider from 'material-ui/Slider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import moment from 'moment';
moment.locale('fr');
require ('react-datepicker/dist/react-datepicker.css');

export const maxLengthTitle = 40;
export const maxLengthDescription = 120;
export const tailleDivInfosDeBase = "col-md-8";
export const tailleDivDropzone = "col-md-4";

require ('../../css/global-styles/globalStyles.css');


export const renderBigTextField = ({input, label, type, meta: {touched, error}, ...custom}) => {

  const password = 
    <TextField
      hintText={label}
      errorText={touched && error}
      fullWidth={true}
      type="password"
      {...input}
      {...custom}
    />;
  const textArea = 
    <TextField
      hintText={label}
      errorText={touched && error}
      fullWidth={true}
      multiLine={true}
      {...input}
      {...custom}
    />;
  
  return type === "password" ? password: textArea;
};
export const renderMediumTextField = ({input, label, meta: {touched, error}, ...custom}) => (
    <TextField
        hintText={label}
        errorText={touched && error}
        multiLine={true}
        {...input}
        {...custom}
    />
);
export const renderSmallTextField = ({input, label, meta:{touched, error}, ...custom}) => (
    <TextField
        hintText={label}
        errorText={touched && error}
        {...input}
        {...custom}
    />
);
export const renderDatePicker = ({input, placeholder, defaultValue, meta: {touched, error} }) => (
    <div>
        <DatePicker {...input} dateFormat="DD/MM/YYYY"  onBlur={() => input.onBlur()}
                    selected={input.value ? moment(input.value) : null} placeholderText="jj/mm/aaaa"/>
        {touched && error && <span>{error}</span>}
    </div>
);

export const renderDeltaSlider = ({input, defaultValue, which, onDeltaChange}) => {
  
  const theme1 = getMuiTheme({
    slider: {
      selectionColor: '#2B9CB1',
      trackSize: 4,
      key: "yolo"
    }
  });
  
  return (
    <MuiThemeProvider muiTheme={theme1} key="yolo">
      <Slider  
        min={0}
        value={defaultValue}
        max={5}
        step={1}
        onChange={(e, newvalue) => {onDeltaChange(e, which, newvalue)}}/>
    </MuiThemeProvider>
  );
}

export const validate = values =>{
    const errors = {};
    if(values.status == 1){
        const requiredFields = [ 'name', 'presentation', 'short_description', 'history', 'latitude', 'longitude',
            'start_date', 'description', 'short_bio', 'place_of_birth' ];
        requiredFields.forEach(field => {
            if (!values[ field ]) {
                errors[ field ] = 'champs requis'
            }
        });
    }

    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Adresse mail non valide'
    }
    return errors
};
export const handleInitialize = (fiche, initialize) =>{
    let initData = {};
    let data= ["name","profile.name","short_description", "presentation", "history", "technical_information", "more_information", "schedule", "accessibility",
        "contact", "city", "country", "latitude", "longitude", "start_date", "end_date", "date", "description", "birthday", "deathday",
        "place_of_birth", "short_bio", "path", "attachments", "references", "social", "delta_start", "delta_end", "slug"];
    data.forEach((field) => {
        initData[field] = fiche[field]
    });
    initData["cover_credit"] = fiche.cover["cover_credit"];
    if(fiche.map) {
        initData["country"] = fiche.map.country ? fiche.map.country : "";
        initData["latitude"] = fiche.map.latLng ? fiche.map.latLng.lat : fiche.latitude ;
        initData["longitude"] = fiche.map.latLng ? fiche.map.latLng.lng  : fiche.longitude ;
        initData["old_address"] = fiche.map.fullAddrStr ? fiche.map.fullAddrStr : "";
    }
    fiche.categories.map((category)=>{
        initData[category._id] = true;
    });

    fiche.sousCategories.map((sousCat) => {
        initData[sousCat] = true ;
    });
    initialize(initData);
};


export const submit = (formProps, enumerations, Tags, cover, userId,status, submitTag, themes,mapData,  submitFiche,initialize, deltas = null) =>{
    /**
     errorCallback will only be thrown if there is an error from the database
     */
    const {categories,types, sousCategories} = enumerations;
    let newTags = [];
    let tagsArray = [];
    let sousCategoriesArray=[];
    Tags.map((tag) => {
        if(typeof tag.id === "undefined" && typeof tag._id==="undefined") {
            newTags.push(tag);
        }
        else{
            let idTag = tag.id? tag.id : tag._id;
            tagsArray.push(idTag)
        }
    });
    let tags =() => {
        return Promise.all(newTags.map((tag) => {
            return submitTag({'name': tag.value})
                .then((res) => {
                    tagsArray.push(res._id);
                    return JSON.stringify(res);
                });
        }))
    };
    let categoriesArray = [];
    let typesArray = [];
    for (let key in formProps){
        if(formProps[key] == true) {
            for (let i = 0; i < categories.length; i++) {
                if (key == categories[i]._id) {
                    categoriesArray.push(key);
                }
            }
            for (let i = 0; i < types.length; i++) {
                if (key == types[i]._id) {
                    typesArray.push(key);
                }
            }
            for (let i = 0; i<sousCategories.length; i++){
                if(key == sousCategories[i]){
                    sousCategoriesArray.push(key);
                }
            }
            delete formProps[key];
        }
    }

    formProps['categories'] = categoriesArray;
    formProps['types'] = typesArray;
    formProps['cover'] = {...cover, cover_credit:formProps['cover_credit']};
    formProps['_id'] = userId;
    formProps['status'] = status;
    formProps['sousCategories'] = sousCategoriesArray;
    formProps['themes'] = themes;
    formProps['map'] = mapData;
    
    if (deltas !== null) {
      formProps['delta_start'] = deltas[0];
      if (deltas.length > 1) {
        formProps['delta_end'] = deltas[1];
      }
    }
    
    tags().then(() => {
        formProps['tags'] = tagsArray;
        submitFiche(formProps)
            .then(() => {
                //return browserHistory.push('/fichesView/')
              return browserHistory.push({
                pathname: '/fichesView/',
                state: { keepContext: true }
              })
            });
        initialize('');
    })
};
export const editSubmit = (formProps, enumerations, Tags, cover, submitTag, themes,editFiche,mapData, displayFiche, status, deltas = null) => {
  
    let newCategories=[]; let newTypes=[]; let newTags=[];let tagsArray = []; let newSousCatgeories =[];
    let image= cover;
    image = {...image, cover_credit: formProps['cover_credit']};
    Tags.map((tag) => {
        if (typeof tag.id === "undefined" && typeof tag._id === "undefined") {
            newTags.push(tag);
        }
        else {
            tagsArray.push(tag._id)
        }
    });
    let tags = () => {
        return Promise.all(newTags.map((tag) => {
            return submitTag({'name': tag.label})
                .then((res) => {
                    tagsArray.push(res._id);
                    return JSON.stringify(res);
                });
        }))
    };

    for (let i = 0; i < enumerations.categories.length; i++) {
        if (formProps[enumerations.categories[i]._id] == true) {
            newCategories.push(enumerations.categories[i]._id);
            delete formProps[enumerations.categories[i]._id]
        }
    }
    for (let i = 0; i < enumerations.types.length; i++) {
        if (formProps[enumerations.types[i]._id] == true) {
            newTypes.push(enumerations.types[i]._id);
            delete formProps[enumerations.types[i]._id]
        }
    }
    for(let i = 0; i<enumerations.sousCategories.length; i++){
        if(formProps[enumerations.sousCategories[i]] == true){
            newSousCatgeories.push(enumerations.sousCategories[i]);
            delete formProps[enumerations.sousCategories[i]];
        }
    }
    formProps['categories'] = newCategories;
    formProps['types'] = newTypes;
    formProps['status'] = status;
    formProps['sousCategories'] = newSousCatgeories;
    formProps['themes'] = themes;
    formProps['map'] = mapData;
  
    if (deltas !== null) {
      formProps['delta_start'] = deltas[0];
      if (deltas.length > 1) {
        formProps['delta_end'] = deltas[1];
      }
    }
  
    tags().then(() => {
        formProps['tags'] = tagsArray;
        editFiche(displayFiche._id, {...formProps, cover: image})
            .then(() => {
              return browserHistory.push({
                pathname: '/fichesView/',
                state: { keepContext: true }
              })
                //return browserHistory.push('/fichesView/')
                //return true;
            });
    });
};
export const icon = (type) => {
    switch (type) {
        case 'People':
            return 'user';
        case 'Events':
            return 'time';
        case 'Media':
            return 'picture';
        case 'Objects':
            return 'tag';
        case 'Places':
            return 'map-marker';
        default:
            return 'info-sign';
    }
};
export const border = (type) => {
    switch (type) {
        case 'People':
            return 'person';
        case 'Events':
            return 'event';
        case 'Media':
            return 'media';
        case 'Objects':
            return 'object';
        case 'Places':
            return 'place';
        default:
            return 'object';
    }
};

export const defaultImage= {
    site: {
        "size" : 2431,
        "path" : "images/defaultImage_Site.png",
        "filename" : "defaultImage_Site.png",
        "destination" : "images/",
        "mimetype" : "image/png",
        "encoding" : "7bit",
        "originalname" : "site_default.png",
        "fieldname" : "file"
    },

    person: {
        "size" : 2665,
        "path" : "images/defaultImage_Person.png",
        "filename" : "defaultImage_Person.png",
        "destination" : "images/",
        "mimetype" : "image/png",
        "encoding" : "7bit",
        "originalname" : "peopledefault.png",
        "fieldname" : "file"
    },
    event: {
        "size" : 2631,
        "path" : "images/defaultImage_Event.png",
        "filename" : "defaultImage_Event.png",
        "destination" : "images/",
        "mimetype" : "image/png",
        "encoding" : "7bit",
        "originalname" : "event_default.png",
        "fieldname" : "file"
    },
    media: {
        "size" : 2554,
        "path" : "images/defaultImage_Media.png",
        "filename" : "defaultImage_Media.png",
        "destination" : "images/",
        "mimetype" : "image/png",
        "encoding" : "7bit",
        "originalname" : "media_default.png",
        "fieldname" : "file"
    },
    object: {
        "size" : 2582,
        "path" : "images/defaultImage_Object.png",
        "filename" : "defaultImage_Object.png",
        "destination" : "images/",
        "mimetype" : "image/png",
        "encoding" : "7bit",
        "originalname" : "object_default.png",
        "fieldname" : "file"
    }
};

const editorConfig = {
    plugins: 'link,lists, preview',
    toolbar: 'undo redo | bold italic underline strikethrough link | bullist numlist | alignleft aligncenter alignright alignjustify | preview | removeformat',
    block_formats: 'Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Paragraph=p',
    menubar: false,
    statusbar: true,
    body_class: 'editable-field-content',
    paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
    paste_retain_style_properties: 'none',
    resize: 'true',
    paste_strip_class_attributes: 'none',
    paste_remove_styles: true,
};

export const renderTinyMCE = (field) => {
    let props = Object.assign({}, field);
    delete props.input;
    delete props.meta;
    return  <TinyMCE
        {...props}
        content={field.input.value}
        config={editorConfig}
        value={field.input.content !== '' ? field.input.content : null}
        onBlur={(event, value) => {
            field.input.onChange(event.target.getContent())
        }}
    />;
};
const configRef = {
    plugins: 'link',
    toolbar: 'undo link',
    block_formats: 'Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Paragraph=p',
    menubar: false,
    statusbar: true,
    body_class: 'editable-field-content',
    paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
    paste_retain_style_properties: 'none',
    paste_strip_class_attributes: 'none',
    resize: 'true',
    paste_remove_styles: true,
};
export const renderWYSIWYGRef = (field) => {
    let props = Object.assign({}, field);
    delete props.input;
    delete props.meta;
    return  <TinyMCE
        {...props}
        content={field.input.value}
        config={configRef}
        value={field.input.content !== '' ? field.input.content : null}
        onBlur={(event, value) => {
            field.input.onChange(event.target.getContent())
        }}
    />;
};