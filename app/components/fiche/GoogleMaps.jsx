import React from 'react';
import MTComponent from '.././MTComponent.jsx';
import {Gmaps} from 'react-gmaps';

//TODO add a super that extends Component and that have conveninent methods such as autobinding needed methods
class SimpleMap extends MTComponent{

    constructor(props){
        super(props);
        this.state = {map: null,
            marker: null};
        this.setAutoBindMethods([
            'getLocation',
            'onMapCreated',
            'showPosition',
            'updateMapPosition',
            'addCustomMarker',
            'onDragEnd',
            'createCoordsObj'
        ]);
    }
    componentWillReceiveProps(nextProps){
        if(this.props.boolean) {
            this.updateMapPosition(nextProps.initialCoords);
            this.props.notifyParent();
        }
    }
    shouldComponentUpdate(nextProps, nextState){
        //console.log('SCU>', nextProps, nextState);
        return false;
    }


    createCoordsObj(event){
        return {
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
        };
    }

    onMapCreated(map) {
        map.setOptions({ disableDefaultUI: true });
        this.setState({ map: map },
            () => {
                if(this.props.initialCoords && Object.keys(this.props.initialCoords).length>0){
                    this.updateMapPosition(this.props.initialCoords);
                }
                else{this.getLocation();}
            });

    }

    onDragEnd(e){
        let coords = this.createCoordsObj(e);
        this.props.giveCoords(coords.lat,coords.lng);
    }

    getLocation() {
        /* if (navigator.geolocation) {
            //navigator.geolocation.getCurrentPosition(this.showPosition, this.onPositionError);
        } else {
            //just leave coordinates variable as is if user does not want to share his location or geolocation is not available
		console.warn("No locations provided");
		let coords = {lat : 46.199944, lng : 6.152609};
        	this.setState({coords : coords});
        	this.updateMapPosition(coords);
        }*/

	let coords = {lat : 46.199944, lng : 6.152609};
        this.setState({coords : coords});
        this.updateMapPosition(coords);
    }

    isMarkerInstanciated(){
        return null !== this.state.marker && undefined !== this.state.marker;
    }

    onPositionError(error) {
     console.warn(error.code + " - " + error.message);
    }

    showPosition(position) {
        let currentCoords = position.coords;
        let coords = {lat : currentCoords.latitude , lng : currentCoords.longitude};
        this.setState({coords : coords});
        this.updateMapPosition(coords);
    }
    //shall a custom marker at the parameter points
    addCustomMarker(lat, lon, message, css){
        let marker;
        if(this.state.marker==null){
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lon),
                map: this.state.map,
                title: message,
                draggable: true,
            });
            let me = this;
            marker.addListener('dragend', function(event) {
                me.onDragEnd(event);
            });
            this.setState({marker});
        }
    }

    updateCurrentMarker(lat, lng, message, css) {
        let targetMarker = this.state.marker;
        targetMarker.setPosition(new google.maps.LatLng(lat, lng));
    }

    updateMapPosition(coords){

        let map = this.state.map;
        if(map) {

            map.setCenter(new google.maps.LatLng(coords.lat, coords.lng));
            map.setZoom(18);
            if (!this.isMarkerInstanciated()){
                this.addCustomMarker(coords.lat, coords.lng, "We are here");
            }else {
                this.updateCurrentMarker(coords.lat, coords.lng, "new place, shall give at least its name ^^");
            }
            this.setState({map});
        }
    }



    render() {

        return (
            <div id='more-info-map'>
                <Gmaps
                    width={'100%'}
                    height={'600px'}
                    zoom={17}
                    loadingMessage={'Loading..'}
                    params={{v: '3.26', key: 'AIzaSyAUo9wq2PF_znYfSgnWOM81jBq8HWmBEAo'}}
                    onClick={this.onClick}
                    onMapCreated={this.onMapCreated}>
                </Gmaps>
           </div>
        );
    }

}

export default SimpleMap;
