import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import FicheSearchForm from '../FicheSearchForm.jsx';

//using user prefs here and we don't need fiche actions witgin this component
import * as actions from '../../actions/fiches.js';

class InnerSearchForm extends Component {
    constructor(props){
      super(props);
      
      this.state = {
        lockSearch : false,
        ready: false
      }
    }

    search(event = null, hasToUpdate = true) {
      
      if (event){
        event.preventDefault();  
      }
      
      if (this.state.ready) {
        let query = this.refs.child.getQuery();

        let filters = this.refs.child.getFilters(false),
            realFilters = this.refs.child.getFilters(true);
        const { searchFiche } = this.props;
        
        //searchFiche(query, filters);

        //TODO check here if we need to check the hasToUpdate flag
        //notifying parent to save user prefs
        this.props.updateUserPrefs(query, filters, realFilters);
      }
    }
  
    componentDidMount() {
      this.setState({ready: true});
    }

    render() {
      
        return (
          <div className="well">
              <FicheSearchForm 
                search={this.search.bind(this)} 
                ref="child" 
                initSearch={true}
                prefs={this.props.prefs}
                query={this.props.query}
                lockSearch={this.state.lockSearch}
                searchFilters={this.props.searchFilters}/>
          </div>
        );
    };
}

InnerSearchForm.propTypes = {
};


export default connect(() => { return {}}, actions)(InnerSearchForm);