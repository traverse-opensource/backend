import React, {Component} from 'react';
import {Field} from 'redux-form';

var  max_chars = 160;
const required = value => value ? undefined : 'Merci de renseigner ce champs';
class LimitedCharsField extends Component{
    constructor(props){
      super(props);
      this.state ={
        chars_left: this.props.max_chars
      };
    }
    handleChange = (event) => {
      let input = event.target.value,
          me = this;

      me.setState({
        chars_left: me.props.max_chars - input.length
      }, () => {
        if (me.props.secondCallback) {
          me.props.secondCallback(event.target.value);
        }
      });
    };
    render() {
        return (

            <div>
                <Field name={this.props.name} id={this.props.id} component={this.props.renderSmallTextField}
                       type="text" label={this.props.label}
                       required={[required]}
                       onChange={this.handleChange.bind(this)} maxLength={this.props.max_chars}/>
                <br/>

                <small id={this.props.id_countdown} >{this.state.chars_left} caractères restants</small>

            </div>
        );
    }
}


export default LimitedCharsField;