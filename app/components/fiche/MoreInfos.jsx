import React from 'react';
import MTComponent from '.././MTComponent.jsx';
import {Field} from 'redux-form';
import {renderSmallTextField,renderMediumTextField, renderBigTextField} from '../../components/fiche/FormUtils.jsx'
import SimpleMap from './GoogleMaps.jsx';

import PlacesAutocomplete, { geocodeByAddress, geocodeByPlaceId } from 'react-places-autocomplete';

///TODO find a way to have a better control over setState and its callback
class MoreInfos extends MTComponent{
    constructor(props){
        super(props);
        this.setAutoBindMethods([
            'giveCoords',
            'onSelect',
            'triggerBoolean',
            'updateAddressData',
            'onCityChange',
            'childrenChangesBoolean',
            'getPlaceFromLatLng'
        ]);
        this.state = ({
          visible: true,
          simpleMapCase: (this.props.simpleMapCase ? this.props.simpleMapCase: false)
        });
        this.coords = this.props.coords;
        this.hasToUpdate = false;
    }
    componentDidMount(){
      if (this.props.flushMap){
        this.props.flushMap();
      }
        /* if(this.props.coords)this.setState({coords: this.props.coords});*/
    }
    giveCoords(lat, lng){
        this.props.change('latitude', lat);
        this.props.change('longitude', lng);
        this.getPlaceFromLatLng(lat, lng, this.updateAddressData);
    }

    updateAddressData(remoteGeocodeObj){
      //const nextCallback = () => {

        if (remoteGeocodeObj) {
          let placeId = remoteGeocodeObj.place_id,
            fullAddrStr = remoteGeocodeObj.formatted_address,
            splitted = fullAddrStr.split(', '),
            country = splitted[splitted.length - 1],
            fullAddrCmp = remoteGeocodeObj.address_components,
            geo = remoteGeocodeObj.geometry,
            latLng = {
                lat: (typeof geo.location.lat === "number") ? geo.location.lat : geo.location.lat(),
                lng: (typeof geo.location.lng === "number") ? geo.location.lng : geo.location.lng()
            };
          let objMap = {placeId ,fullAddrStr,country, latLng};
          
          /*this.setState({map : objMap},
              ()=>this.props.setMap(this.state.map));*/
          this.props.setMap(objMap);
          this.props.change('country', country);
        }
        
      //}

      /*if (this.props.flushMap) {
        this.props.flushMap();
        setTimeout(nextCallback(), 250);
      }else {*/
      // if (remoteGeocodeObj) {
      //   setTimeout(nextCallback, 250);
      // }

        //nextCallback();
      //}
    }

    getPlaceFromLatLng(lat, lng, callback) {
        fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}`)
            .then(response => response.json())
            .then(addrCmp => {
                if (undefined !== callback && null !== callback) {
                    callback(addrCmp.results[0]);
                }
            });
    }

    onCityChange(city){
        this.setState({ city:  city});
        if (this.hasToUpdate){
            geocodeByAddress(city,  (err, { lat, lng }, results) => {

                if (err) {
                    console.log('Pas de coordonnées disponibles pour cette adresse', err);
                    return null;
                }
                this.coords = {lat, lng};
                this.giveCoords(lat, lng);
                this.updateAddressData(results[0]);
            });
        }
    }

    onSelect (city, placeId) {
        this.hasToUpdate = true;
        this.onCityChange(arguments[0]);
    }

    triggerBoolean(city){
        this.onCityChange(city);
    }

    childrenChangesBoolean(){
        this.hasToUpdate = false;
    }
    componentWillReceiveProps(nextProps){

    }

    render(){
        const myStyles = {
          root: { zIndex: 999999 },
          input: { width: '100%' },
          autocompleteContainer: { backgroundColor: 'green' },
          autocompleteItem: { color: 'black' },
          autocompleteItemActive: { color: 'blue' }
        };
        return (
            <div className="container well">
                <label>Plus d'informations</label>
                    {!this.props.simpleMapCase &&
                    <div>
                      <Field name="technical_information" component={renderBigTextField}
                             type="text" label="Information technique"/>
                      <br/>
                      <Field name="accessibility" component={renderBigTextField} type="text"
                             label="Accessibilité"/>
                      <br/>
                      <Field name="schedule" component={renderBigTextField} type="text"
                             label="Horaires"/>
                      <br/>
                      <Field name="contact" component={renderBigTextField} type="text"
                             label="Contact"/>
                      <br/>
                      <Field name="more_information" component={renderBigTextField} type="text"
                             label="Plus d'infos"/>
                      <br/>
                    </div>
                    }
                <br/>
                <div className="col-md-12">
                    {this.props.errorMessage}
                    <PlacesAutocomplete
                      name="city" defaultValue="test"
                      label="Ville"
                      onChange={this.triggerBoolean}
                      value={this.state ? this.state.city ? this.state.city : "" : ""}
                      onSelect={this.onSelect}
                      styles={myStyles}
                    />
                    <br/>
                    <div className="col-md-4">
                        <Field name="country" component={renderSmallTextField} type="text"
                               label="Pays" disabled/>
                        <br/>
                    </div>
                    <div className="col-md-4">
                        <Field name="latitude" component={renderSmallTextField} type="text" id="debugLat"
                               label="Latitude" disabled/>
                        <br/>
                    </div>
                    <div className="col-md-4">
                        <Field name="longitude" component={renderSmallTextField} type="text" id="debugLon"
                               label="Longitude" disabled/>
                        <br/>
                    </div>
                </div>
                <div className="row">
                    {this.props.edit &&
                    <div>
                        <label htmlFor="">Adresse actuelle </label>
                        <Field name="old_address" component={renderBigTextField}
                               label="ancienne addresse" disabled/>
                    </div>
                    }
                    <div className="col-md-12 yolo">
                        <SimpleMap giveCoords ={this.giveCoords}
                                   initialCoords={this.coords}
                                   boolean={this.hasToUpdate}
                                   notifyParent={this.childrenChangesBoolean}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default MoreInfos;
