import React, {Component} from 'react';

/*Components*/
import FicheSearchForm from "../../components/FicheSearchForm";
/*Utils*/
import {icon} from '../../components/fiche/FormUtils.jsx';
import axios from 'axios';

/*Style*/
import classNames from "classnames/bind";
import styles from "../../css/components/playlist-fiche-search.css";
const cx = classNames.bind(styles);

class RelatedFiche extends Component{

    constructor(props) {
      super(props);
      
      this.state = {
        fiches: []
      }
    }
  
    //don't need to affect state fiche list, since it's local search
    search(event) {
        event.preventDefault();
        let query = this.refs.child.getQuery();
        let filters = this.refs.child.getFilters();
        //const {searchFiche} = this.props;
        //searchFiche(query, filters);
      
      let me = this;
      
      let requestArray = [];
      for (let filter of filters) {
          if (filter.value == true) {
            //requestArray.push(axios.get('/' + filter.key + '/search/list?q=' + query + '&bob=3'));
              if (query.length > 0) {
                requestArray.push(axios.get('/' + filter.key + '/search/list?q=' + query + '&bob=3'));
              }else {
                requestArray.push(axios.get('/' + filter.key + '/search/list?bob=3'));
              }
          }
      }
      
      axios.all(requestArray).then(function (results) {

        let res = [];

        for (let subResult of results) {

            res.push(...subResult.data);
        }

        function compare(a,b) {
          if (a.created_at < b.created_at)
            return -1;
          if (a.created_at > b.created_at)
            return 1;
          return 0;
        }

        res.sort(compare);
        let toSet = res.filter((fiche) => {
          return me.props.displayedFiche._id !== fiche._id;
        })

        me.setState({fiches: toSet});
      })
    }

    selectFiche(item) {
        const {addRelatedFicheToFiche} = this.props;
        var data = {fiche: item};
        addRelatedFicheToFiche(this.props.displayedFiche._id, data);
    }

    unselectFiche(item) {
        const {removeRelatedFicheToFiche} = this.props;
        var data = {fiche: item};
        removeRelatedFicheToFiche(this.props.displayedFiche._id, data);
    }

    renderRelation(relation) {
        var items = relation.fiches.map((item, key) => {
            return (
                <li className="list-group-item" key={key}>
                    <span className={"glyphicon glyphicon-" + icon(item.__t)}/>
                    {item.name}
                    <span onClick={this.unselectFiche.bind(this, item)} className={cx('list-item-remove') + " " + "glyphicon glyphicon-remove"}/>
                </li>
            )
        });
        return (
            <div className="col-md-4" key={relation.key}>
                {relation.label}
                <ul className={cx("list-group") + " " + cx("list-group-relation") + " list-group list-group-relation"}>
                    {items}
                </ul>
            </div>
        )
    }
    render(){
        let array = [];
        const relations = {
            Events: {icon: 'time', fiches: [], label: 'Evénements liés', key: 'events'},
            Media: {icon: 'picture', fiches: [], label: 'Medias liés', key: 'media',},
            Objects: {icon: 'tag', fiches: [], label: 'Objets liés', key: 'objects'},
            People: {icon: 'user', fiches: [], label: 'Personnes liés', key: 'people'},
            Places: {icon: 'map-marker', fiches: [], label: 'Sites liés', key: 'places'}
        };
        let items = <div>Tapez un mot clé pour trouver les fiches correspondantes</div>;
        if (this.state.fiches) {
            array = this.state.fiches;
            if (array.length > 0) {
                items = array.map((item, key) => {
                    return (
                        <li className="list-group-item" key={key} onClick={this.selectFiche.bind(this, item)}>
                            <span className={"glyphicon glyphicon-" + icon(item.__t)}/> {item.name}
                        </li>
                    )
                })
            }
            else {
                items = <div>Aucun résultat trouvé</div>
            }

        }
        if (this.props.displayedFiche.related_fiches) {
            for (let rel of this.props.displayedFiche.related_fiches) {
                relations[rel.__t].fiches.push(rel);
            }
        }
        let relationsViews = Object.keys(relations).map((key) => {
            return this.renderRelation(relations[key]);
        });
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-3 well">
                        <label> Rechercher une fiche à lier (entrer un mot clé pour trouver les fiches
                            correspondantes)</label>
                        <FicheSearchForm search={this.search.bind(this)} ref="child"/>
                        <ul className={cx("list-group") + " list-group"}>{items}</ul>
                    </div>
                    <div className="col-md-1">
                    </div>
                    <div className="col-md-8 well">
                        <div className="row">
                            { relationsViews }
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
        )
    }
}
export default RelatedFiche;