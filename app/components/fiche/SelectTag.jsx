import React from 'react';
import Select from 'react-select';
import * as actions from '../../actions/fiches.js';
import { connect } from 'react-redux';
import 'react-select/dist/react-select.css';

/*code imported from a react-select example*/
var SelectTag = React.createClass({
    displayName: 'CreatableTag',
    propTypes: {
        hint: React.PropTypes.string,
        label: React.PropTypes.string
    },
    getInitialState () {
        const createCountLabel = function(tag){
          return tag.name + (tag.count ? '  ( ' + tag.count + ' )': '');
        };
        const createResponseObj = function(tag){
          return {value: tag.name, label: createCountLabel(tag), _id: tag._id}
        };
        const tagsOptions = this.props.tagsField?
            this.props.tagsField.map((tag) => {
              return createResponseObj(tag);
            })
            : [];
        const tagsReceived = this.props.initialValues?
            this.props.initialValues.map((tag) => {
                return createResponseObj(tag);
            }): [];
        return {
            multi: true,
            multiValue: tagsReceived ,
            options: tagsOptions,
            value: undefined
        };
    },
    handleOnChange (values) {
        this.setState({ multiValue: values }, this.sendValueToParent)
    },
    sendValueToParent(){
        this.props.selectedValues(this.state.multiValue);
    },
    render () {
        const { multi, multiValue, options } = this.state;
        return (
            <div className="section">
                <h3 className="section-heading">{this.props.label}</h3>
                <Select.Creatable
                    multi={multi}
                    options={options}
                    onChange={this.handleOnChange}
                    value={multiValue}
                />
            </div>
        );
    }
});

export default connect(null, actions) (SelectTag);