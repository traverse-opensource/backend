import React, { Component } from 'react';
import {Field} from 'redux-form';
import {renderMediumTextField, renderBigTextField} from '../../components/fiche/FormUtils.jsx'
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class Social extends Component {
    constructor (props) {
        super(props);
        this.state = {
            multi: true,
            multiValue: [] ,
            multiFacebook:[],
            multiInstagram:[],
            multiTwitter:[],
            options: [],
            value: undefined};
        this.handleOnChange=this.handleOnChange.bind(this);
        this.handleOnChangeFacebook=this.handleOnChangeFacebook.bind(this);
        this.handleOnChangeInstagram=this.handleOnChangeInstagram.bind(this);
        this.handleOnChangeTwitter=this.handleOnChangeTwitter.bind(this);
        this.createResponseObj=this.createResponseObj.bind(this);
    }
    createResponseObj(tag){
        this.setState({value: tag.name, _id: tag._id});
    }
    handleOnChange (values) {
        this.setState({ multiValue: values })
    }
    handleOnChangeFacebook (values) {
        this.setState({ multiFacebook: values })
    }
    handleOnChangeInstagram (values) {
        this.setState({ multiInstagram: values })
    }
    handleOnChangeTwitter (values) {
        this.setState({ multiTwitter: values })
    }
    render(){
        return(
            <div className="container well">
                <label for="slug">Hashtag généré</label>
                <Field name="slug" label="Hashtag customisé" disabled component={renderBigTextField}/>
                <Field name="social.web.link" label="Lien Web" component={renderBigTextField}/>
                <Field name="social.facebook.link" label="Lien Facebook" component={renderBigTextField}/>

                <label>Quels Hashtags?</label>
                <Field name="social.facebook.tags" multi={true} component={(props) =>
                    <Select.Creatable
                        value={props.input.value}
                        onChange={props.input.onChange}
                        onBlur={() => props.input.onBlur(props.input.value)}
                        multi
                    />}/>
                <br/>
                <Field name="social.instagram.link" label="Lien Instagram" component={renderBigTextField}/>
                <label>Quels Hashtags?</label>
                <Field name="social.instagram.tags" multi={true} component={(props) =>
                        <Select.Creatable
                            value={props.input.value}
                            onChange={props.input.onChange}
                            onBlur={() => props.input.onBlur(props.input.value)}
                            multi
                        />
                     }
                />
                <br/>
                <Field name="social.twitter.link" label="Lien Twitter" component={renderBigTextField}/>
                <label>Quels Hashtags?</label>
                <Field name="social.twitter.tags" multi={true} component={(props) =>
                    <Select.Creatable
                        value={props.input.value}
                        onChange={props.input.onChange}
                        onBlur={() => props.input.onBlur(props.input.value)}
                        multi
                    />
                }/>
            </div>
        )
    }
}
export default Social;

