import React, { Component, PropTypes } from 'react';

/*Components*/
import SelectTag from './SelectTag.jsx';
import { Collapse, Well, FormGroup } from 'react-bootstrap';
import { Field } from 'redux-form';
import Checkbox from 'material-ui/Checkbox';
//Style pour les champs requis
import styles from '../../css/components/login';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);
class Taxonomies extends Component {
    constructor(props) {
        super(props);
        this.handleTags = this.handleTags.bind(this);
        this.renderCheckBoxSousCat = this.renderCheckBoxSousCat.bind(this);
        this.state = {
            categories:[],
            type:[],
            visible: this.props.sousCategories? this.props.sousCategories.length ? true : false :false,
            sousCategories: this.props.sousCategories? this.props.sousCategories: []
        };
    }
    handleTags(tag){
        this.props.selectedTags(tag);
    }
    renderCheckBoxSousCat = ({input, label, meta: {touched, error}, ...custom}) => {
        return (
            <Checkbox checked={input.value} onCheck={(e, checked) => {
                console.log(input.value);
                input.onChange(checked)}}
            />
        )
    };
    manageSousCategories(cat, checked){
        let sousCatForState = this.state.sousCategories;
        let arrayOfSousCat = [];
        let categories = this.state.categories;
        let trouve = false;
        if(checked){
            categories.push(cat);
            this.setState({categories : categories });
            if(cat.sousCategories) {
                cat.sousCategories.map((sousCat) => {
                    sousCatForState.push(sousCat);
                    this.setState({visible: true})
                })
            }
        }
        else{
            if(cat.sousCategories) {
                cat.sousCategories.map((sousCat)=> {
                    arrayOfSousCat.push(sousCat);
                });
            }
            for(let j = 0; j<sousCatForState.length; j++){
                if(arrayOfSousCat[0] == sousCatForState[j]){
                    trouve = true;
                    sousCatForState.splice(j, arrayOfSousCat.length);
                    for (let i = sousCatForState.length; i >= 0; i--) {
                        for(let j=0; j < arrayOfSousCat.length; j++){
                            if (sousCatForState[i] === arrayOfSousCat[j]) {
                                sousCatForState.splice(i, 1);
                            }
                        }
                    }
                }
            }
            if(trouve){
                for(let i = 0; i<arrayOfSousCat.length; i++){
                    this.props.change(arrayOfSousCat[i], false);
                }
            }
        }
        this.setState({sousCategories: sousCatForState});
        if(this.state.sousCategories.length<1){
            this.setState({visible : false});
        }
    }
    flushSousCategories(){
        this.setState({sousCategories : []});
    }
    render() {
        const {categories, tags, initialValues, values} = this.props;
        const categoriesCheckboxes = categories ? categories.map((cat, key) => {
            return (
                <div className="col-md-2" key={key} >
                    <label htmlFor={cat.name} key={key}>{cat.name}</label>
                    <Field name={cat._id} id={cat._id} component=
                        {({input, label, meta: {touched, error}, ...custom}) => {
                            let value= false;
                            typeof input.value==="boolean" ? value= input.value : value=false;
                            return (
                                <Checkbox checked={value} onCheck={(e, checked) => {
                                    this.manageSousCategories(cat, checked);
                                    input.onChange(checked)}}
                                />
                            )
                        }} />
                </div>
            )
        }) : <div></div>;

        const renderSousCategories = this.state.sousCategories.map((sousCat, key) => {
            return <div className="col-md-3" key={key}>
                <label htmlFor={sousCat} key={key}>{sousCat}</label>
                <Field name={sousCat} id={sousCat} component={this.renderCheckBoxSousCat} />
            </div>
        });
        const tagsField = tags?
            <div>
                <label>Mots-clés</label>
                <SelectTag tagsField={tags}
                           selectedValues={this.handleTags}
                           initialValues={initialValues}/>
            </div>
            : <div></div>;

        return (
            <div className="container well">
                {this.props.typeFiche!="fichePersonne" && this.props.typeFiche!="editPersonne"?
                    <div className="row">
                    <div className="col-md-12">
                        <label>Classification</label><br/>
                        <p className={cx('message', {
                            'message-show': this.props.message && this.props.message.length > 0
                        })}>{this.props.message}</p>
                        <p>Veuillez cocher au moins une catégorie</p>
                        <Well>
                            <FormGroup>
                                <div className="row">
                                    {categoriesCheckboxes}
                                </div>
                            </FormGroup>
                        </Well>
                        <br/>
                        <Collapse in={this.state.visible}>

                            <div>
                                <p>Ajouter au moins une sous Catégorie</p>
                                <br/>
                                <Well>
                                    <FormGroup>
                                        <div className="row">
                                            {renderSousCategories}
                                        </div>
                                    </FormGroup>
                                </Well>
                            </div>
                        </Collapse>
                    </div>
                </div> : <div></div>}
                {tagsField}
            </div>
        )
    }
}

export default Taxonomies;

