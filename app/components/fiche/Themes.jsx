import React, {Component} from 'react';
import styles from '../../css/components/slider.css';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

import Slider from 'material-ui/Slider';
import RaisedButton from 'material-ui/RaisedButton';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import * as actions from '../../actions/fiches.js';
import { connect } from 'react-redux';

class Themes extends Component{
    constructor(props){
        super(props);
        this.state={themesArray : this.props.initialThemes};
        this.handleChange = this.handleChange.bind(this);
        this.isInSecondList = this.isInSecondList.bind(this);
        this.handleChangeSlider = this.handleChangeSlider.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }
    componentDidMount(){
        this.state.themesArray ?
          this.props.addTheme(this.state.themesArray) : null ;
    }
    componentWillUnmount(){
        this.props.flushThemes();
    }

    handleChange(event){
        let theme = JSON.parse(event.target.value);
        let themesArray = this.state.themesArray;
        theme  = {name: theme.name , description: theme.description, ponderation : 0, color: theme.color};
        themesArray.push(theme);
        this.setState({themesArray : themesArray});
    }

    handleRemove(theme) {
      console.error('Themes @handleRemove: event', arguments);

      let themeName = theme.name;
      let themesArray = this.state.themesArray;

      let getThemePosition = function(toCompare){
        let comparedTheme = null;
        for (let i = 0, len = themesArray.length; i < len; ++i){
          comparedTheme = themesArray[i];
          if (comparedTheme.name === toCompare){
            return i;
          }
        }
        return -1;
      }

      themesArray.splice(getThemePosition(themeName), 1);
      this.setState({themesArray : themesArray});
    }

    handleChangeSlider(event, newValue, theme){
        let themesArray = this.state.themesArray;
        let currentTheme;
        for(let i = 0 ; i<themesArray.length ; i++){
            currentTheme = themesArray[i];
            if(theme.name === themesArray[i].name) {
                themesArray[i].ponderation = newValue;
            }
        }
        this.props.addTheme(themesArray);
        this.setState({themesArray : themesArray});
    }
    isInSecondList(theme){
        let currentTheme;
        for (let i = 0;i<this.state.themesArray.length; i++){
            currentTheme = this.state.themesArray[i];
            if (theme.name == currentTheme.name){
                return true;
            }
        }
        return false;
    }

    render(){
        const {themes} = this.props.enumerations;
        let themesArray= this.state.themesArray? this.state.themesArray.map((theme, key ) => {
            const theme1 = getMuiTheme({
                slider: {
                    selectionColor: theme.color? theme.color: '#2B9CB1',
                    trackSize: 4,
                    key:key
                }
            });

            return(
              <div key={key}>
                  <div className="col-md-8" key={key}>
                    <div className="row">
                      <div className="col-md-8" key={key}>
                      <p>{theme.name}</p>
                      <MuiThemeProvider muiTheme={theme1} key={key}>
                          <Slider  min={10}
                                   value={theme.ponderation}
                                   max={100}
                                   step={10}
                                   key={key}
                                   onChange={(e, newvalue) => this.handleChangeSlider(e, newvalue, theme)}/>
                      </MuiThemeProvider>
                      </div>
                      <div
                          className="col-md-2"
                          style={{
                            'margin': '16px 0 0 0'
                        }}>
                        {theme.ponderation}
                      </div>
                      <div className="col-md-2">
                        <RaisedButton
                            className="btn btn-fail" label="Supprimer"
                            onClick={(currentTheme) => this.handleRemove(theme)}
                            fullWidth={true}
                            type="button"/>
                      </div>
                    </div>
                  </div>
                  <br/>
                  <br/>
                  <br/>
              </div>
            )
        }) : <div></div>;

        let options = themes?themes.map((theme, key) => {
            return this.isInSecondList(theme) ?
              <option key={key} disabled value={JSON.stringify(theme)} >{theme.name}</option>:
              <option key={key} value={JSON.stringify(theme)} >{theme.name}</option>
        }) : <div></div>;
        if(themes)options.push(<option key={Math.random()} disabled value={false} > -- Choisir un ou plusieurs thèmes -- </option>);
        return(
            themes?
          <div>
              <div className="container">
                  <div className="col-md-6">
                      <select onChange={this.handleChange} value={false}>
                          {options}
                      </select>
                  </div>
                  <div className="col-md-6">
                      {themesArray}
                  </div>
              </div>
          </div> : <div></div>
        )
    }
}

export default connect(null, actions) (Themes);