import React, { Component } from 'react';
import {renderBigTextField ,renderSmallTextField, renderMediumTextField, renderTinyMCE, renderDatePicker,
    tailleDivInfosDeBase, tailleDivDropzone  ,
    maxLengthDescription, maxLengthTitle, renderDeltaSlider } from '../FormUtils.jsx'
import { Field } from 'redux-form';
import LimitedCharsField from '../LimitedCharsField.jsx';
import { HASH_DELTAS } from '../../../constant';


class Event extends Component {
  
  constructor(props) {
      super(props);
      
      let valueStart = this.props.deltaStart,
          valueEnd = this.props.deltaEnd;
      
      this.state = {
        birthDateDelta: valueStart,
        birthDateDeltaString: HASH_DELTAS[valueStart],
        deathDateDelta: valueEnd,
        deathDateDeltaString: HASH_DELTAS[valueEnd],
      }
      
      this.onDeltaChange = this.onDeltaChange.bind(this);
      this.setSliderValue = this.setSliderValue.bind(this);
    }
  
    onDeltaChange(event, which, value) {
      this.setSliderValue(which, value);
    }
  
    getDeltas() {
      return [
        this.state.birthDateDelta,
        this.state.deathDateDelta
      ];
    }
  
    //intent to use this method on component will receive props, or will updpate
    setSliderValue(which, value) {
      let toSet = HASH_DELTAS[value];
      
      if (which) {
        this.setState({
          birthDateDelta: value,
          birthDateDeltaString: toSet
        })
      }else {
        this.setState({
          deathDateDelta: value,
          deathDateDeltaString: toSet
        })
      }
    }
  
    createSlider(which) {
      const value = which? this.state.birthDateDelta: this.state.deathDateDelta;
      const params = {
        name: which? "deltaBirthday": "deltaDeathday",
        defaultValue: value,
        label: HASH_DELTAS[value]
      };
      return (
        <div>
          <div className="col-md-7">
            <Field name={params.name} component={renderDeltaSlider} defaultValue={params.defaultValue} which={which} type="text" onDeltaChange={this.onDeltaChange}/>
          </div>
          <div className="col-md-5" style={{marginTop: "25px"}}>
            <label>{params.label}</label>  
          </div>
        </div>
      )
    }
  
    render() {
        const {errorMessage, dropZone} = this.props;
        return (
            <div className="container well">
                <div className="row">
                    <div className="col-md-12">
                        <div className={tailleDivInfosDeBase}>
                            <label>Informations de base</label><br/>
                            {errorMessage}
                            <LimitedCharsField renderSmallTextField={renderMediumTextField} max_chars={maxLengthTitle}
                                               name="name" id="name" label="Nom de l'événement"
                                               id_countdown="name_countdown"
                            />
                            <br/>
                            <LimitedCharsField renderSmallTextField={renderBigTextField} max_chars={maxLengthDescription}
                                               name="presentation" id="presentation" label="Description"
                                               id_countdown="presentation_countdown"
                            />
                            <div>
                                <label>Présentation</label>
                                <Field name="description" component={renderTinyMCE} type="text" label="Description"/>
                            </div>
                            <br/>
                            <div className="col-md-6">
                              <label htmlFor="">Date de début</label>
                              <Field name="start_date" component={renderDatePicker} type="text" label="Date de début"/>
                              {this.createSlider(1)}  
                            </div>
                            <div className="col-md-6">
                              <label htmlFor="">Date de fin</label>
                              <Field name="end_date" component={renderDatePicker} type="text" label="Date de fin"/>
                              {this.createSlider(0)}
                            </div>
                            <br/><br/>
                        </div>
                        <div className={tailleDivDropzone}>
                            <Field name="cover_image" component={dropZone}/>
                            <Field name="cover_credit" component={renderBigTextField}
                                   type="text"
                                   label="Credit de l'image"
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}
export default Event;