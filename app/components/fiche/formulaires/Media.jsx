import React, { Component } from 'react';
import {renderBigTextField ,renderSmallTextField,renderTinyMCE, renderMediumTextField, renderDatePicker,
    tailleDivInfosDeBase, tailleDivDropzone ,
    maxLengthDescription, maxLengthTitle, renderDeltaSlider} from '../FormUtils.jsx'
import { Field } from 'redux-form';
import LimitedCharsField from '../LimitedCharsField.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import { HASH_DELTAS } from '../../../constant';


class Media extends Component {
    constructor(props){
      super(props);
      
      let valueStart = this.props.deltaStart;
      
      this.state = {
        birthDateDelta: valueStart,
        birthDateDeltaString: HASH_DELTAS[valueStart]
      }
      
      this.onDeltaChange = this.onDeltaChange.bind(this);
      this.setSliderValue = this.setSliderValue.bind(this);
      this.fetchPreview = this.fetchPreview.bind(this);
      this.onMediaUrlChange = this.onMediaUrlChange.bind(this);
      this.onReplaceImageClick = this.onReplaceImageClick.bind(this);
      this.propagateCover = this.propagateCover.bind(this);
    }
  
    onDeltaChange(event, which, value) {
      this.setSliderValue(which, value);
    }
  
    getDeltas() {
      return [
        this.state.birthDateDelta
      ];
    }
  
    //intent to use this method on component will receive props, or will updpate
    setSliderValue(which, value) {
      let toSet = HASH_DELTAS[value];
      
      if (which) {
        this.setState({
          birthDateDelta: value,
          birthDateDeltaString: toSet
        })
      }
    }
  
    createSlider(which) {
      const value = which? this.state.birthDateDelta: this.state.deathDateDelta;
      const params = {
        name: which? "deltaBirthday": "deltaDeathday",
        defaultValue: value,
        label: HASH_DELTAS[value]
      };
      return (
        <div>
          <div className="col-md-7">
            <Field name={params.name} component={renderDeltaSlider} defaultValue={params.defaultValue} which={which} type="text" onDeltaChange={this.onDeltaChange}/>
          </div>
          <div className="col-md-5" style={{marginTop: "25px"}}>
            <label>{params.label}</label>  
          </div>
        </div>
      )
    }
  
    //will only do this once istead of component will receive props
    componentDidMount(){
      if (this.props.remotePath){
        this.fetchPreview(this.props.remotePath);
      }
    }
    
    fetchPreview(targetUrl){
        let me = this;
        
        fetch('/extractMedia?targetUrl=' + targetUrl).then(function(response) {
          return response.json();
        }).then(function(response) {
          if (response.status === 1){
            //also check if the cover on dropzone is the same
            me.setState({previewUrl: response.thumb, remotePathError: false})  
          }else{
            me.setState({remotePathError: true});
          }
        });
    }
  
    onMediaUrlChange({target: field}){
      let me = this;
      
      //need this since it's a bit async, otherwise we won't get the last character of the whole string
      setTimeout(function(){
        let targetUrl = '' + field.innerHTML;

        if (targetUrl.length > 0){
          me.fetchPreview(targetUrl);
        }else {
          me.setState({remotePathError: false});
        }
      }, 250);
    }
  
    propagateCover(){
      this.props.childUpdateCover(this.state.previewUrl);
    }
  
    onReplaceImageClick(){
      //need to make this whole block usable by the whole projet
      const postParams = {
        remotePath: this.state.previewUrl
      };
      
      let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      };
      
      let url = "/fiches/" + this.props.ficheId + "/updateCover",
          method = 'POST',
          body = JSON.stringify(postParams),
          me = this;
      
      if (this.props.ficheId){
        //check if we are in edit or new fiche
        fetch(url, {method, headers, body})
        .then(function(res){ return res.json(); })
        .then(function(data){
          me.propagateCover();
        });
      }else{
        //new fiche in this case => just propagate the cover
        this.propagateCover();
      }
    }
  
    render() {
        const {errorMessage, dropZone} = this.props;
      
        const styles = {
          div: {
            top: "-36px"
          },
          button: {
            width: "100%"
          },
          image: {
            width: "100%"
          }
        };
      
        let remoteUrlPreview = this.state.previewUrl ?
            <div>
              <div className="col-md-12">
                <img style={styles.image} src={this.state.previewUrl}/>
              </div>
              <div style={styles.div} className="col-md-12">
                <RaisedButton 
                  label="Remplacer l'image d'ullustration" 
                  labelColor="#FFFFFF" 
                  backgroundColor="#286090"
                  style={styles.button} 
                  onClick={this.onReplaceImageClick}/>
              </div>
            </div>
            :
            <p></p>;
      
        let cheminErrorText = "Aucun preview n'est disponible à cette adresse";
      
        let cheminEditText = this.state.remotePathError ?
            <Field name="path" errorText={cheminErrorText} component={renderBigTextField} type="text" label="Chemin" onChange={this.onMediaUrlChange}/>:
            <Field name="path" component={renderBigTextField} type="text" label="Chemin" onChange={this.onMediaUrlChange}/>;
      
        
      
        return (
            <div className="container well">
                <div className="row">
                    <div className="col-md-12">
                        <div className={tailleDivInfosDeBase}>
                            <label>Informations de base</label><br/>
                            {errorMessage}
                            <LimitedCharsField renderSmallTextField={renderMediumTextField} max_chars={maxLengthTitle}
                                               name="name" id="name" label="Nom du media"
                                               id_countdown="name_countdown"
                            />
                            <br/>
                            <LimitedCharsField renderSmallTextField={renderBigTextField} max_chars={maxLengthDescription}
                                               name="description" id="description" label="Description du media"
                                               id_countdown="description_countdown"
                            />
                            <div className="row">
                              <div className="col-md-9">
                                {cheminEditText}
                              </div>
                            </div>
                            <div>
                                <label>Présentation</label>
                                <Field name="presentation" component={renderTinyMCE} type="text" label="Présentation"/>
                            </div>
                            <br/>
                           {/* <Field name="attachments" component={renderSmallTextField} type="text" label="Objet joint"/>*/}
                            <br/>
                            <label>Date</label>
                            <Field name="date.date" component={renderDatePicker} type="text" label="Date"/>
                            <br/>
                            {this.createSlider(1)}
                        </div>
                        <div className={tailleDivDropzone}>
                            <Field name="cover_image" component={dropZone}/>
                            <Field name="cover_credit" component={renderBigTextField}
                                   type="text"
                                   label="Credit de l'image"
                            />
                          <div>
                            {remoteUrlPreview}
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}
export default Media;