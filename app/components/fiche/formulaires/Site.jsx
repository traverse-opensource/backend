import React, { Component } from 'react';
import {renderMediumTextField, renderTinyMCE, renderBigTextField,
    tailleDivInfosDeBase, tailleDivDropzone } from '../FormUtils.jsx'
import LimitedCharsField from '../LimitedCharsField.jsx';
import { Field } from 'redux-form';

class Site extends Component {
  
    getDeltas() {
      return [
        null
      ];
    }
  
    render() {
        const {errorMessage, dropZone} = this.props;
        return (
            <div className="container well">
                <div className="row">
                    <div className="col-md-12">
                        <div className={tailleDivInfosDeBase}>
                            <label >Informations de base</label><br/>
                            {errorMessage}
                            <LimitedCharsField renderSmallTextField={renderMediumTextField} max_chars={40}
                                               name="name" id="name" label="Nom du site"
                                               id_countdown="name_countdown"
                            />

                            <small>Par exemple: Gare de Modane</small>
                            <br/>
                            <LimitedCharsField renderSmallTextField={renderBigTextField} max_chars={80}
                                               name="short_description" id="short_description" label="Qualification du site"
                                               id_countdown="short_description_countdown"
                            />
                            <br/>
                            <div><label>Description</label>
                                <Field name="presentation" component={renderTinyMCE} type="text"
                                       label="Description"/>
                                <br/>
                            </div>
                            <div>
                                <label>Histoire</label>
                                <Field name="history" component={renderTinyMCE} type="text"
                                       label="Histoire"/>
                            </div>
                        </div>
                        <div className={tailleDivDropzone}>
                            <Field name="cover_image" component={dropZone}/>
                            <Field name="cover_credit" component={renderBigTextField}
                                   type="text"
                                   label="Credit de l'image"/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}
export default Site;