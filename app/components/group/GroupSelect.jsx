import React, {Component} from 'react';
import MTComponent from '../../components/MTComponent';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

/**
 * Aims to only handle option selection, does not update stuff from here, but call its parent component to update the group of
 * targetted user
 */
export default class GroupSelect extends MTComponent {
  
  constructor(props) {
    super(props);
    
    this.setAutoBindMethods([
      'handleChange',
      'createItems'
    ]);
    
    this.state = {
      value: this.seekSelectValue(this.props),
      user: this.props.targetUser
    };
  }
  
  seekSelectValue = (props) => {
    let user = props.targetUser;
    let groups = props.groups;
    let groupId = props.selectedGroupId;
    
    if (user) {
      return groups && user? 
        groups.findIndex(group => group.title === user.group.title):
        0;
    }else {
      //must have groupId here
      return groupId?
        groups.findIndex(group => group._id === groupId):
        0
    }
  }
  
  componentWillReceiveProps(newProps){
    if(this.props.groups!=newProps.groups) {
    
      let me = this;
      
      me.setState({
        user: newProps.targetUser,
        value: this.seekSelectValue(newProps)
      });
    }
  }
  
  createItems() {
    let items = [],
        groups = this.props.groups ?
          this.props.groups: [];

    for (let i = 0; i < groups.length; i++ ) {
      items.push(<MenuItem value={i} key={i} primaryText={groups[i].description} />);
    }
    return items;
  }

  handleChange(event, index, value){
    let me = this;
    me.setState({value}, () => {
      if (me.state.user) {
        me.props.updateUserGroup( 
          me.state.user._id, 
          me.props.groups[index]._id 
        );
      }else {
        me.props.updateUserGroup( 
          null, 
          me.props.groups[index]._id 
        );
      }
    });
  };

  render() {
    
    let items = this.createItems();
    
    return (
      <SelectField
        value={this.state.value}
        onChange={this.handleChange}
        maxHeight={200}
        style={{width: "100%"}}
      >
        {items}
      </SelectField>
    );
  }
}