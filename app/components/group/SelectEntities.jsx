import React, {Component} from 'react';
import {permission} from '../../constant/index.js';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

/**
 * `SelectField` can handle multiple selections. It is enabled with the `multiple` property.
 */
export default class SelectEntities extends Component {
    constructor(props){
        super(props);
      
        this.state = {
          options: this.props.entities? 
            this.createOptionValues(this.props): [],
          value: this.props.value
        };
    }
  
    /**
     * mode 0 stands for normal case
     * 1 for handling admins nor super admins
     */
    createOptionValues = (props, mode = 0) => {
      let adminEntitiesIds = [];
      if (props.adminEntities && props.adminEntities.length > 0) {
        adminEntitiesIds = props.adminEntities.map((entity) => {
          return entity._id;
        });
      }
      
      let toReturn = [];
      
      if (props.entities) {
        if (mode === 0) {
          toReturn = props.entities.map((entity) => {
            return {
              label: entity.name + ": " + entity.description,
              disabled: adminEntitiesIds.indexOf(entity._id) < 0 && adminEntitiesIds.length > 0,
              value: entity._id,
              clearableValue: adminEntitiesIds.indexOf(entity._id) >= 0
            }
          });  
        }else {
          toReturn = props.entities.map((entity) => {
            return {
              label: "",
              disabled:true,
              value: entity._id ,
              clearableValue: false
            }
          })
        }
      }
          
      return toReturn;
    }
  
    componentWillReceiveProps(nextProps) {
        //TODO check why we would need this stuff ??
        //if (this.state.options.length === 0) {
        if (nextProps.entities.length > 0) {
          if(nextProps.userGroup===permission.SUPER_ADMIN){
            this.setState({
              options: this.createOptionValues(nextProps, 1),
              value: nextProps.value? nextProps.value: this.state.value
            })
          }else {
            this.setState({
              options: this.createOptionValues(nextProps),
              value: nextProps.value ? nextProps.value : this.state.value
            })
          }
        }
        //}
    }

    handleSelectChange (value) {
        let me = this;
        me.setState({ value }, () => {
            me.props.updateUserEntities(this.props.userId, me.state.value);
        });
    }

    render () {
        return (
            <div className="section">
                <h3 className="section-heading">{this.props.label}</h3>
                <Select multi
                        simpleValue
                        disabled={this.state.disabled}
                        value={this.state.value}
                        placeholder="Choix des groupes"
                        options={this.state.options}
                        clearable={false}
                        onChange={this.handleSelectChange.bind(this)} />
            </div>
        );
    }
}