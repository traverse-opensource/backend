import React, {Component} from 'react';

import Select from 'react-select';
import 'react-select/dist/react-select.css';

/**
 * `SelectField` can handle multiple selections. It is enabled with the `multiple` property.
 */
export default class SelectRoles extends Component {
    constructor(props){
        super(props);
        this.state = {
            options: [],
            value: this.props.value ?
              this.props.value: []
        };
    }
  
    componentWillUpdate(nextProps, nextState) {
      if (this.state.options.length === 0) {
        
        let user = this.props.targetUser;
        let groups = this.props.groups;
        
        if (groups.length > 0){
          let groupIndex = user?
            groups.findIndex(group => group.title === user.group.title):
            0;
          
          this.setState({
            options: groups.map((entity) => {
              return { label: entity.description, value: entity._id }
            }),
            //value: nextProps.value? nextProps.value: this.state.value
            value: groups[groupIndex]._id
          })
        }
      }
    }
   
    handleSelectChange (value) {
      let me = this;
      me.setState({ value }, () => {
        me.props.updateUserGroup(this.props.userId, me.state.value);
      });
    }
  
    render () {
      return (
        <div className="section">
          <h3 className="section-heading">{this.props.label}</h3>
          <Select simpleValue disabled={this.state.disabled} value={this.state.value} placeholder="Choix des groupes" options={this.state.options} onChange={this.handleSelectChange.bind(this)} />				
        </div>
      );
    }
}