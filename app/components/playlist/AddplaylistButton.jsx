import React , {Component , PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import PlaylistForm from './PlaylistForm.jsx';
//TODO remove this
import request from 'axios';

export default class AddPlaylistButton extends Component {
    constructor(props){
        super(props);
        this.state = {show : false};
    }

    render(){
        let close = () => {
            this.setState({show : false});
        };
        let showModal = () => {this.setState({show : true})};
        return(
            <div>
                <Button bsStyle="primary" onClick={showModal}>+ Ajouter une playlist</Button>
                <AddPlaylistModal 
                  show={this.state.show}
                  onHide={close}
                  sessionUser={this.props.sessionUser}/>
            </div>
        )
    }
}

class AddPlaylistModal extends Component {

    render() {
        return (
            <Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Ajouter une playlist</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <PlaylistForm 
                          close={this.props.onHide} 
                          sessionUser={this.props.sessionUser}/>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

AddPlaylistModal.propTypes = {
    name: PropTypes.string,
};