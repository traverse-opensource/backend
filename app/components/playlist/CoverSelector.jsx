import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {Button, Modal} from 'react-bootstrap';

import classNames from 'classnames/bind';
import styles from '../../css/components/playlist';

const cx = classNames.bind(styles);

export default class CoverSelector extends Component {

    onItemClicked(item) {
        const {onSelect} = this.props;
        onSelect(item);
    }

    render() {

        var coverList = [];
        if(this.props.playlist && this.props.playlist.linkedFiches) {
            this.props.playlist.linkedFiches.forEach((fiche) => {
                if (fiche.fiche.cover != null) {
                    coverList.push(fiche.fiche.cover);
                }
            });
        }

        var computeRemotePath = function(toTest = null){
          let toReturn = "";
          
          if (toTest) {
            toReturn = toTest.indexOf('http') !== -1 ?
            '' + toTest: '/' + toTest;
          }
          return toReturn;
        };
      
        return (
            <div className={cx('cover-container')}>
                <div className="col-md-12">
                    <label>Choisir une image de couverture</label>
                </div>
                {coverList.map((item, key) => {
                    return (
                        <div className="col-md-3" key={key}>
                            <li className={cx('cover-item') + ' ' + ((this.props.playlist.cover != null && this.props.playlist.cover.filename == item.filename)?cx('selected'):'')}>
                                <img className={cx('imgcard')} src={computeRemotePath(item.path)} style={{height: '100%', width: '100%', objectFit: 'cover'}} onClick={this.onItemClicked.bind(this, item)}/>
                            </li>
                        </div>
                    )
                })}
            </div>
        )
    }
}
