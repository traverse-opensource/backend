import React , { Component }from 'react';
import {reduxForm, Field} from 'redux-form';
import { connect } from 'react-redux';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

//TODO seems never used
const form = reduxForm({
    form : 'EditPlaylistForm'
});
const renderField = field => (
        <div>
            <label>{field.input.label}</label>
            <input{...field.input} />
            {field.touched && field.error && <div className="error">{field.error}</div>}
        </div>
    );

class EditPlaylistForm extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount() {
        this.handleInitialize();
    }
    handleInitialize() {
        const { fiches, name} = this.props;
        const initData = {
            "name": name,
            "fiches": fiches
        };
        this.props.initialize(initData);
    }

    handleFormSubmit(formProps){
        const { editFiche } = this.props;
        editFiche(formProps);
    }

    render(){
        let options = [];
        const { handleSubmit, fiches } = this.props;
        const fichesList = (fiches.map((fiche,key) => {
            options.push({value:fiche._id, label:fiche.name});
            return <option key={key}>{fiche.name +" " +fiche._id}</option>;
        }));

        return(
            <div>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <div>
                        <label htmlFor="id">ID : {this.props.id}</label>
                    </div>
                    <div>
                        <label htmlFor="name">Name</label>
                        <Field name="name" component={renderField} type="text"/>
                    </div>
                    <div>
                        <label htmlFor="fiches">Fiches</label>
                        <Field name="fiches"
                               component={props =>
                                   <Select
                                       multi={true}
                                       value={props.input.value}
                                       onChange={props.input.onChange}
                                       onBlur={() => props.input.onBlur(props.input.value)}
                                       placeholder="Select"
                                       joinValues
                                   />
                               }
                        />
                    </div>
                    <button type="submit">Enregistrer</button>
                </form>
            </div>
        )
    }
}


function mapStateToProps(state){
    return {
        playlist: state.form
    };
}
export default connect(mapStateToProps) (form(EditPlaylistForm));