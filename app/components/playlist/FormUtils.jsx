import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import TinyMCE from 'react-tinymce';
import {browserHistory} from 'react-router';

export const renderBigTextField = ({input, label, meta: {touched, error}, ...custom}) => (
    <TextField
        hintText={label}
        errorText={touched && error}
        fullWidth={true}
        multiLine={true}
        rows={2}
        {...input}
        {...custom}
    />
);

export const renderSmallTextField = ({input, label, meta:{touched, error}, ...custom}) => (
    <TextField
        hintText={label}
        errorText={touched && error}
        {...input}
        {...custom}
    />
);

export const validate = values =>{
    const errors = {};
    const requiredFields = [ 'name', 'description' ];
    requiredFields.forEach(field => {
        if (!values[ field ]) {
            errors[ field ] = 'champs requis'
        }
    });

    return errors
};

export const handleInitialize = (playlist, initialize) =>{
    let initData = {};
    let data= ["name","description"];
    data.forEach((field) => {
        initData[field] = playlist[field]
    });

    initialize(initData);
};

export const editSubmit = (formProps, editPlaylist, displayedPlaylist, hasToGoBack = false) => {

    editPlaylist(displayedPlaylist._id, formProps, displayedPlaylist, () => {
      if (hasToGoBack){
          browserHistory.goBack();
      }  
    });
};

const editorConfig = {
    plugins: 'link,image,lists,paste,code',
    toolbar: 'undo redo | formatselect bullist numlist | bold italic link | code paste',
    block_formats: 'Paragraph=p;Heading 3=h3',
    menubar: false,
    statusbar: false,
    body_class: 'editable-field-content',
    paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
    paste_retain_style_properties: 'none',
    paste_strip_class_attributes: 'none',
    paste_remove_styles: true,
};

export const renderTinyMCE = (field) => {
    let props = Object.assign({}, field);
    delete props.input;
    delete props.meta;
    return  <TinyMCE
        {...props}

        content={field.input.value}
        value={field.input.content !== '' ? field.input.content : null}
        onBlur={(event, value) => {
            field.input.onChange(event.target.getContent())
        }}
    />;
};

export const renderTinyMCEditor = (field) => {
    let props = Object.assign({}, field);
    delete props.input;
    delete props.meta;
    return field.input.value ? <TinyMCE
        {...props}
        content={field.input.value}
        value={field.input.content !== '' ? field.input.content : null}
        onBlur={(event, value) => {
            field.input.onChange(event.target.getContent())
        }}
    /> : <p>Chargement du contenu...</p>;
};