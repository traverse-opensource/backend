import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {Button, Modal} from 'react-bootstrap';

import classNames from 'classnames/bind';
import styles from '../../css/components/playlist';

import PlaylistFichesSelectableChip from './timeline/PlaylistFichesSelectableChip';

const cx = classNames.bind(styles);

export default class LabelDisplayer extends Component {
  
    render() {
        const labelList = this.props.items;
        return (
            <div className={cx('cover-container')}>
                <div className="col-md-12">
                    <label>{this.props.label}</label>
                </div>
                
                {labelList.map((item, key) => {
                    return (
                        <div key={key}>
                          <PlaylistFichesSelectableChip
                          itemName={item.name}
                          itemId={item.id}
                          onItemTap={this.props.onTagTap}
                          selected={true}
                          />
                        </div>
                    )
                })}
            </div>
        )
    }
}
