import React ,{ Component, PropTypes } from 'react';
import { Field, reduxForm, initialize } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../actions/playlists';

const form = reduxForm({
    form: 'playlistAddLinkedFicheForm'
});

const renderField = field =>
    (
        <div>
            <label>{field.input.label}</label>
            <input {...field.input}  required="required" />
            {field.touched && field.error && <div className="error">{field.error}</div>}
        </div>
    );

class PlaylistAddLinkedFicheForm extends Component {
    constructor(props){
        super(props);
    }

    handleFormSubmit(formProps){

        var data = { fiche: this.props.fiche, linkValue: formProps.linkValue};
        this.props.callback(this.props.playlistId, data);
        this.props.initialize('');

        this.props.close();
    }

    componentDidMount() {
        this.props.initialize({linkValue: this.props.fiche.value})
    }

    render(){
        const { handleSubmit } = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <div>
                        <label>Valeur du lien</label>
                        <div>
                            <Field name="linkValue" component={renderField} type="text" placeholder="Valeur" />
                        </div>
                    </div>

                    <button type="submit">Enregistrer</button>
                </form>
            </div>
        )
    }
}
function mapStateToProps(state){
    return {
        linkedFicheValue: state.form
    };
}
export default connect(mapStateToProps, actions)(form(PlaylistAddLinkedFicheForm));