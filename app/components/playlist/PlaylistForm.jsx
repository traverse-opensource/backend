import React ,{ Component, PropTypes } from 'react';
import { Field, reduxForm, initialize } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../actions/playlists';

import { browserHistory } from 'react-router';
import {Button} from 'react-bootstrap';

const form = reduxForm({
    form: 'addPlaylistForm'
});
const required = value => value ? undefined : 'Nom requis!';
const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span style={{color:'red'}}>{error}</span>) || (warning && <span style={{color:'orange'}}>{warning}</span>))}
        </div>
    </div>
);
class PlaylistForm extends Component {
    constructor(props){
        super(props);
    }

    redirectToPlaylist(data) {
        return browserHistory.push('/playlistsView/playlist/' + data._id);
    }

    handleFormSubmit(formProps){

        //adding userId from the base props
        formProps.created_by = this.props.sessionUser._id;
        this.props.addPlaylist(formProps, this.redirectToPlaylist);
        this.props.initialize('');

        this.props.close();
    }

    render(){

        const { handleSubmit } = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <div>
                        <label>Nom de la playliste</label>
                        <div>
                            <Field name="name" component={renderField} validate={[required]} type="text" placeholder="Name"/>
                        </div>
                    </div>
                    <br/>
                    <Button bsStyle="primary" type="submit">Enregistrer</Button>
                </form>
            </div>
        )
    }
}
function mapStateToProps(state){
    return {
        playlist: state.form
    };
}
export default connect(mapStateToProps, actions)(form(PlaylistForm));