import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {Button, Modal} from 'react-bootstrap';
import {} from 'react-bootstrap/'
import classNames from 'classnames/bind';
import playlistStyles from '../../css/components/playlist';

import { COVER_ON_IMAGE_LOAD, status } from '../../constant'

const cx = classNames.bind(playlistStyles);

export default class PlaylistItem extends Component {
    constructor(props) {
        super(props);

        this.state= {
            show: false,
            fiches : [],
            dimens: {}
        }

        this.onDestroy = this.onDestroy.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.onImageLoad = this.onImageLoad.bind(this);
    }
    onDestroy() {
        const {onDestroy, playlist} = this.props;
        onDestroy(playlist._id);
    }

    onOpen() {
        const {onOpen, playlist} = this.props;

        onOpen(playlist._id);
    }

    onImageLoad({target: img}) {
        COVER_ON_IMAGE_LOAD(img, (resultingDimens) => {
            this.setState({dimens: {h: resultingDimens[1], w: resultingDimens[0], l: resultingDimens[2], t: resultingDimens[3]}})
        });
    }

    render() {

        const {playlist} = this.props;
        const {name, description, _id, fiches} = playlist;

        const {w, h, l} = this.state.dimens;

        let close = () => this.setState({show : false});
        var options = {
            year: "numeric", month: "numeric",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };

        var computeRemotePath = function(toTest){
            if (toTest){
                //show thumbnail on gridview for images hosted in Traverse server
                if (toTest.path.indexOf('http') !== -1) {
                  return '' + toTest.path;
                }else {
                  let splitted = toTest.path.split('/');
                  return splitted[0] + '/thumb/' + splitted[1]; 
                }
            }
            return "/images/greybackground.png";
        };

        const draftOrNormalStyle = {
          backgroundColor: playlist.status==0?'gray':'',
          opacity: playlist.status==0?0.6:''
        };
      
        //just to make it clearer for IDE
        const draftOverlay = playlist.status === status.DRAFT? 
              <h4 style={{color: 'orange'}}>
                  <span className="glyphicon glyphicon-pencil "/>
                  Brouillon
              </h4>: "";
      
        return (
            <div className={cx('panel panel-default') + " " + cx('cards')} style={draftOrNormalStyle}>
                <div className={cx('panel-heading')} style={draftOrNormalStyle}>
                    <div className={cx('imgcard')} style={{width: '100%', height: '160px', backgroundPosition: 'center center', backgroundRepeat: 'no-repeat', overflow: 'hidden', borderColor:playlist.theme.color}}>
                        <img src={computeRemotePath(playlist.cover)} style={{position: 'relative', width: w + 'px', height: h + 'px', left: l + 'px'}} onLoad={this.onImageLoad}/>
                    </div>

                    <div className={cx('picto-card')} style={{backgroundColor:playlist.theme.color}}>
                        <img src="images/ic_playlist.svg" className={cx('background-playlist')}/>
                    </div>
                </div>
                <div className={cx('panel-body') + " " + cx('hovereffect')}>
                    {draftOverlay}
                    <p>{description}</p>
                    <p>Auteur :  {playlist.created_by.profile.name}</p>
                    <i>Créée le : {new Date(playlist.created_at).toLocaleDateString("fr-FR", options)}</i>

                    <div className={cx('overlay')}>

                        <div className={cx('pull-right')}>
                            <Button bsStyle="warning" className="glyphicon glyphicon-edit "
                                    onClick={this.onOpen}/>
                            <Button bsStyle="danger" className="glyphicon glyphicon-remove "
                                    onClick={this.onDestroy}/>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
        )
    }
}
PlaylistItem.propTypes = {
    playlist: PropTypes.object.isRequired
};
