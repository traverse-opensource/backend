import React from 'react';
import  AddPlaylist  from './AddplaylistButton.jsx';

//TODO implement and use this to show the playlists according to their status and their respective owner
const PlaylistMenu = ({}) => {
    return(
        <div className="sidebar-nav-fixed affix">
            <ul className="nav nav-pills nav-stacked">
                <li role="presentation"><a href="#">Mes brouillons</a></li>
                <li role="presentation"><a href="#">Tous les brouillons</a></li>
            </ul>
            <AddPlaylist/>
        </div>
    );
};
export default PlaylistMenu;