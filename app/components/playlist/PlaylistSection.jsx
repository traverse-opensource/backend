/**
 * Composant qui contient la liste des items de playlists
 *
 */
import React, { Component, PropTypes } from 'react';
import ReactScrollPagination from 'react-scroll-pagination';
import PlaylistItem from './PlaylistItem.jsx';
import AddPlaylist  from './AddplaylistButton.jsx';
import PlaylistSearchForm from '../PlaylistSearchForm.jsx';
import {DropdownButton, MenuItem, Radio, ToggleButton, ButtonToolbar, Button, ButtonGroup} from 'react-bootstrap';
import removeAccents from 'remove-accents';

const GROUP_CONST = {
  ALL: "all",
  ADMIN: "admins",
  CONTRIB: "contrib",
  MINE: "mine",
  NOMAD: "nomad"
};

export default class PlaylistSection extends Component {

    constructor(props){
        super(props);
        this.state = {
          dropdownTitle: 'Filtrer les playlistes...',
          listOfPlaylists: [],
          listBeforeSearch: [],
          listAfterSearch:[],
          search: '',
          searching:false,
          pagination: {
            page: 1,
            limit: 18
          },
          group: GROUP_CONST.ALL,
          needToFetch: true,
          fetching: false
        };

        this.ready = false;

        this.fetchNew = this.fetchNew.bind(this);
    }

    //in order to always have the list updated (adding/updating playlists, concurrent playlist adding)
    componentDidMount() {
      const me = this;
      me.ready = true;
      this.fetchNew();
    }

    fetchNew() {



      let me = this,
          {pagination} = me.state;

      let url = '/playlists/?page=' + pagination.page +
        '&limit=' + pagination.limit +
        '&group=' + me.state.group +
        '&userId=' + me.props.sessionUser._id;

      const fetchCallback = () => {
        fetch(url)
        .then(function(response) {
          return response.json();
        })
        .then(function(data) {

          let previousPlaylists = me.state.listOfPlaylists,
            newPlaylists = data,
            newPagination = me.state.pagination;

          newPagination.page++;

          me.setState({listOfPlaylists: [...previousPlaylists, ...newPlaylists], pagination: newPagination, needToFetch: newPlaylists.length === newPagination.limit, fetching: false});
          me.ready = true;
        });
      };

      if (me.ready) {
        me.ready = false;

        me.setState({ fetching: true }, () => { fetchCallback() });
      }
    }

    theFuncToFetchNextPage() {
      //alert('should load next page');
      if (this.state.needToFetch) {
        this.fetchNew();
      }
    }

    handleEvent(e){
        let me = this;
        let playlistItem = <h3>Aucune playlist</h3>;

        let newGroup = GROUP_CONST.ALL;

        switch(e) {
            case "Toutes les playlistes":
                break;
            case "Mes playlists":
                newGroup = GROUP_CONST.MINE;
                break;
            case "Playlists des Admins":
                newGroup = GROUP_CONST.ADMIN;
                break;
            case "Playlists des Contributeurs":
              newGroup = GROUP_CONST.CONTRIB;
                break;
            case "Playlists des Nomades":
                newGroup = GROUP_CONST.NOMAD;
                break;
            default:
                newGroup = GROUP_CONST.ALL;
                break;
        }

        let pagination = { page: 1, limit: 18 },
          listOfPlaylists = [];

        me.setState({ dropdownTitle: e, group: newGroup, pagination, listOfPlaylists }, () => {
          me.fetchNew();
        });
    }
    filterWord(char){
        this.setState({search : char, searching : true});
        this.filterListbySearch(char);
    }
    filterListbySearch(char){
      //console.log(arguments);
        if(char && char.trim()!==''){
          let toCompare = removeAccents(char);
          let listAfterSearch = this.state.listOfPlaylists.filter((p) => {
            if(p){
              if(p.name && removeAccents(p.name.toLowerCase()).indexOf(char) != -1 ||
                p.description && removeAccents(p.description.toLowerCase()).indexOf(char)!= -1 ) {
                return true;
              }
            }
            return false;
          });
          this.setState({listAfterSearch: listAfterSearch});
        }
        else {
          this.setState({searching : false})
        }
    }

    onDestroy(pId) {
      let me = this;
      //getting playlist item
      return fetch('/playlists/' + pId, {
        method: 'delete'
      })
      .then(response => response.json().then(json => {
        const { listOfPlaylists } = me.state;

        let toSet = listOfPlaylists.filter((playlist) => {
          return playlist._id !== pId;
        });

        me.setState({listOfPlaylists: toSet});
      }));
    }

    render() {
        const { onOpen} = this.props;

        const onDestroy = this.onDestroy.bind(this);

        const {
            listOfPlaylists, listAfterSearch, searching
        } = this.state;

        let message = "Chargement des playlistes";

        if (!this.state.fetching && listOfPlaylists.length === 0) {
          message = "Aucune playliste à afficher";
        }

        return (
            <div className="row">
                <div className="col-md-3">
                    <div className="sidebar-nav-fixed affix">
                        <AddPlaylist sessionUser={this.props.sessionUser}/>
                        <br/>
                        <div>
                            <DropdownButton key={1} id="flagFiches" title={this.state.dropdownTitle} onSelect={this.handleEvent.bind(this)}>
                                <MenuItem eventKey="Toutes les playlists">Toutes les playlists</MenuItem>
                                <MenuItem divider />
                                <MenuItem eventKey="Mes playlists">Mes playlists</MenuItem>
                                <MenuItem eventKey="Playlists des Admins">Playlists des Admin</MenuItem>
                                <MenuItem eventKey="Playlists des Contributeurs">Playlists des Contributeurs</MenuItem>
                                <MenuItem eventKey="Playlists des Nomades">Playlists des Nomades</MenuItem>
                            </DropdownButton>
                        </div>
                        <br/>
                    </div>
                </div>
                <div className="col-md-9">
                    <div className="well">
                        <PlaylistSearchForm filterWord={this.filterWord.bind(this)} ref="child" />
                    </div>
                    {
                        listOfPlaylists.length==0 ?
                            <h2>{ message }</h2>
                            :
                            listOfPlaylists.map((p, key)=>{
                                if(p && p.theme) {
                                    return <div className="col-md-4" key={key}>
                                        <PlaylistItem playlist={p}
                                                      onDestroy={onDestroy}
                                                      onOpen={onOpen}
                                        />
                                    </div>
                                }
                            })
                    }
                    <ReactScrollPagination
                      fetchFunc={this.theFuncToFetchNextPage.bind(this)}/>
                </div>
            </div>
        );
    };
}
PlaylistSection.propTypes = {
    onDestroy : PropTypes.func.isRequired
};
