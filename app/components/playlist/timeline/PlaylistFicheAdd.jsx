import React, {Component, PropTypes} from 'react';
import classNames from 'classnames/bind';
import styles from '../../../css/components/playlist-fiche-timeline.css';

const cx = classNames.bind(styles);

export default class PlaylistFicheAdd extends Component {
    render() {
        return (
            <article className={cx("panel") + " " + cx("panel-success") + " " + cx("panel-outline") + " panel panel-success panel-outline"}>

                <div className={cx("panel-heading") + " " + cx("icon") + " panel-heading icon"}>
                    <i className={cx("glyphicon") + " " +cx("glyphicon-ok") + "glyphicon glyphicon-ok"}></i>
                </div>
            </article>
        )
    }
}
