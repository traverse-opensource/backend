import React, {Component, PropTypes} from 'react';
import classNames from 'classnames/bind';
import styles from '../../../css/components/playlist-fiche-timeline.css';
import {Button} from 'react-bootstrap';
import PlaylistFichesSelectableChip from './PlaylistFichesSelectableChip';
const cx = classNames.bind(styles);
import { CALLBACK_MODES, status as FICHE_STATUS } from '../../../constant';

export default class PlaylistFicheItem extends Component {
    constructor(props){
        super(props);
        this.state=({categories:[], tags:[]})
    }
    selectFiche() {
        const {onSelected} = this.props;
        onSelected(this.props.fiche);
    }

    removeFiche() {
        const {onRemoved} = this.props;
        onRemoved(this.props.fiche);
        this.setState({selected: this.props.fiche});
        this.forceUpdate();
    }
    themeDominant(themes){
        let themeColor = "#386F8D";
        let themeName ="";
        if(themes.length > 0){
            let themePonderation =themes[0].ponderation;
            themeName = themes[0].name;
            themeColor=themes[0].color;
            for(let i =0; i<themes.length; i++){
                if(themes[i].ponderation > themePonderation)
                {themeColor=themes[i].color; themePonderation=themes[i].ponderation; themeName=themes[i].name}
            }
        }
        return {themeName: themeName, themeColor: themeColor};
    }

    render() {
        const icon = (type) => {
            switch (type) {
                case 'People':
                    return 'user';
                case 'Events':
                    return 'time';
                case 'Media':
                    return 'picture';
                case 'Objects':
                    return 'tag';
                case 'Places':
                    return 'map-marker';
                default:
                    return 'info-sign';
            }
        };
        
        const getPropagationList = (mode) => {
          if (mode === CALLBACK_MODES.CATEGORIES) {
            return this.props.onPropagateTap.cat;
          } else if (mode === CALLBACK_MODES.SUB_CATEGORIES){
            return this.props.onPropagateTap.sub;
          }
          return this.props.onPropagateTap.tag;
        };
      
        const getPropagationTap = (mode) => {
          if (mode === CALLBACK_MODES.CATEGORIES) {
            return this.props.onCategoryTap;
          } else if (mode === CALLBACK_MODES.SUB_CATEGORIES){
            return this.props.onSubCategoryTap;
          }
          return this.props.onTagTap;
        };
      
        const label = (labels, mode) => {
            return (
              labels.map((label, key)=> {
                
                let toTest = getPropagationList(mode),
                    selected = false;
                    
                let index = toTest.labels.map((item) => { return item.name; }).indexOf(label.name);
                
                //if the propagation comes from parent call, this is needed to be taken into account
                if (index > -1 ){
                  selected = toTest.selected[index];
                }
                
                return (
                  <div key={key}>
                    <PlaylistFichesSelectableChip
                    itemName={label.name}
                    itemId={label.id}
                    onItemTap={getPropagationTap(mode)}
                    selected={selected}
                    />
                  </div>
                );
              })
            )};
      
        const {fiche} = this.props.fiche;
      
        const {__t,short_description,  short_bio , presentation, description, status} = fiche;
        let theme = this.themeDominant(fiche.themes);
        let tags  = fiche.tags.map((tag) => {
              return {name: tag.name, id: tag._id};
            }),
            categories = fiche.categories.map((category) => {
              return {name: category.name, id: category._id};
            }),
            subCategories = fiche.sousCategories.map((sub) => {
              return {name: sub, id: null};
            });
        
        let hexToRgb =(hex) => {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        };
      
        //default, send published one
        const getContainerColor = (status) => {
          if (status !== FICHE_STATUS.PUBLISHED) {
            return { backgroundColor: theme.themeColor, borderColor: theme.themeColor ,opacity : 0.1 };
          }
          
          return { backgroundColor: theme.themeColor, borderColor: theme.themeColor };
        };
      
        const getBorderColor = (status) => {
          if (status !== FICHE_STATUS.PUBLISHED) {
            let c = hexToRgb(theme.themeColor);
            let rgba = 'rgba('+c.r+','+c.g+','+c.b+',0.1)';
            
            return { borderColor: rgba };
          }
          
          return { borderColor : theme.themeColor };
        };
      
        const style ={
            container : getContainerColor(status),
            border :  getBorderColor(status)
        };
        let itemeDecription = (status, __t) => {
            let style = status !== FICHE_STATUS.PUBLISHED ? {color: "red", float: "right"} : {};
            let errorMsg = "";
            let toReturn =
                __t == "Places" ? short_description :
                    __t == "People" ? short_bio :
                        __t == "Events" ? presentation :
                            __t == "Objects" ? short_description :
                                __t == "Media" ? description :
                                    "Aucune info à afficher ici";
          
            if (status === FICHE_STATUS.DRAFT) {
              errorMsg = "Cette fiche n'est pas encore publiée, veuillez la supprimer de la liste";
            }else if (status === FICHE_STATUS.DELETED) {
              errorMsg = "Cette fiche a été suprimée, veuillez la supprimer de la liste";
            }

            return (<div className="row"><p className="col-md-6">{toReturn}</p><p className="col-md-6" style={style}>{errorMsg}</p></div>);
        };
      
        return (
            <article className={cx("panel") + " " + cx("panel-primary") + " panel panel-primary " + (this.props.selected?( " panel-selected " + cx("panel-selected") ):"")} style={style.border}>
                <div className={cx("panel-heading") + " " + cx("icon") + " panel-heading icon"} style={style.container}>
                    <i className={cx("glyphicon") + " " +cx("glyphicon-" + icon(this.props.fiche.fiche.__t)) + "glyphicon glyphicon-" + icon(this.props.fiche.fiche.__t)}/>
                </div>

                <div className={cx("panel-heading") + " panel-heading "} style={style.container}>
                    <h2 className={cx("panel-title") + " panel-title "}>{this.props.fiche.fiche.name}</h2>
                    {fiche.themes[0] && <small className={"col-md"}>Thème principal: {theme.themeName}</small>}
                </div>


                <div className={cx("panel-body") + " panel-body"} style={style.text}>
                    {itemeDecription(status, __t)
                    }
                </div>

                <div className={cx("panel-footer") + " panel-footer"}>
                    <div className={cx('overlay')}>
                        {categories.length>0 &&
                        <div className="col-md-12">
                            <div className="col-md-3">Catégories:</div>
                            {label(categories, CALLBACK_MODES.CATEGORIES)}
                        </div>}

                        {subCategories.length>0 &&
                        <div className="col-md-12">
                            <div className="col-md-3">Sous catégories:</div>
                            {label(subCategories, CALLBACK_MODES.SUB_CATEGORIES)}
                        </div>}
                        {tags.length>0 &&
                        <div className="col-md-12">
                            <div className="col-md-3">Mots clés:</div>
                            {label(tags, CALLBACK_MODES.TAGS)}
                        </div>}
                        <div className={cx('pull-right')}>
                            <Button bsStyle="info" className="glyphicon glyphicon-retweet "
                                    onClick={this.selectFiche.bind(this)}/>
                            <Button bsStyle="danger" className="glyphicon glyphicon-remove "
                                    onClick={this.removeFiche.bind(this)}/>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>

            </article>
        )
    }
}
