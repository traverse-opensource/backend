import React, {Component, PropTypes} from 'react';
import classNames from 'classnames/bind';
import styles from '../../../css/components/playlist-fiche-timeline.css';
import {Button} from 'react-bootstrap';

const cx = classNames.bind(styles);

export default class PlaylistFicheLink extends Component {

    editLink() {
        const {onEdit} = this.props;
        onEdit(this.props.fiche);
    }

    render() {
        const {editLink} = this.props;
        return (
            <article className={cx("panel") + " " + cx("panel-info") + " " + cx("panel-outline") + " panel panel-info panel-outline"}>

                <div className={cx("panel-heading") + " " + cx("icon") + " panel-heading icon"}>
                    <i className={cx("glyphicon") + " " +cx("glyphicon-arrow-down") + "glyphicon glyphicon-arrow-down"}></i>
                </div>

                <div className={cx("panel-body") + " panel-body"}>
                    <strong>Fiche suivante : </strong> {this.props.value == null?(<b style={{color:'red'}}>Lien non qualifé!</b>):this.props.value}
                    <Button bsStyle="warning" className={"glyphicon glyphicon-edit panel-button " + cx("panel-button")}
                            onClick={this.editLink.bind(this)}/>
                </div>

            </article>

        )
    }
}
