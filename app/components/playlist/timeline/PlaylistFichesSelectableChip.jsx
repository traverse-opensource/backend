import React, {Component, PropTypes} from 'react';
import classNames from 'classnames/bind';
import styles from '../../../css/components/playlist-fiche-timeline.css';
import {Button} from 'react-bootstrap';
import {Chip} from 'material-ui';

const cx = classNames.bind(styles);

export default class PlaylistFichesSelectableChip extends Component {
  
  constructor(props){
      super(props);
      this.selected = (this.props.selected != null ? this.props.selected: false);
      
      this.consts = {
        background: {
          //md-lightblue 400
          selected: '#29B6F6',
          //md-bluegray 400
          deselected: '#78909C'
        },
        text: {
          //mb-lightblue 50
          selected: '#E1F5FE',
          //md-bluegray 50
          deselected: '#ECEFF1'
        }
      };
      
      this.toggleState = this.toggleState.bind(this);
      this.getBgColor = this.getBgColor.bind(this);
      this.getTextColor = this.getTextColor.bind(this);
  }
  
  componentWillUpdate(nextProps, nextState) {
    //no need to use state here since we might have an instance in other linked fiche, ig: can have Culture label inside another linked fiche of this playlist
    this.selected = nextProps.selected ? nextProps.selected: false;
  }
  
  toggleState(){
    this.selected = !this.selected;
    this.props.onItemTap({name: this.props.itemName, id: this.props.itemId}, this.selected);
  }
  
  getBgColor(){
    return this.selected ?
      this.consts.background.selected: this.consts.background.deselected;
  }
  
  getTextColor(){
    return this.selected ?
      this.consts.text.selected: this.consts.text.deselected;
  }
  
  render(){
    const style = {
      marginTop: '4px',
      marginRight: '3px',
      float: 'left'
    };
    
    return (
        <Chip style={style} 
          onTouchTap={this.toggleState}
          backgroundColor={this.getBgColor()}
          labelColor={this.getTextColor()}>
          {this.props.itemName}
        </Chip>
    );
  }
}