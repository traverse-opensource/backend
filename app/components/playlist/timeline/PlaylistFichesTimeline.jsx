import React, {Component, PropTypes} from 'react';
import classNames from 'classnames/bind';
import styles from '../../../css/components/playlist-fiche-timeline.css';
import PlaylistFicheItem from './PlaylistFicheItem';
import PlaylistFicheLink from './PlaylistFicheLink';
import PlaylistFicheAdd from './PlaylistFicheAdd';
const cx = classNames.bind(styles);

export default class PlaylistFichesTimeline extends Component {

    renderFiches() {
        const {onSelected, onRemoved, onEditLink} = this.props;
        if (this.props.playlist.linkedFiches) {
            const fiches = this.props.playlist.linkedFiches.map((item, key) => {
                if (item.next != null) {
                    return (
                        <div key={item._id}>
                            <PlaylistFicheItem
                                key={item._id}
                                fiche={item}
                                selected={this.props.selectedFiche != null && (item._id == this.props.selectedFiche._id)}
                                onSelected={onSelected}
                                onRemoved={onRemoved}
                                onCategoryTap={this.props.onCategoryTap}
                                onSubCategoryTap={this.props.onSubCategoryTap}
                                onTagTap={this.props.onTagTap}
                                onPropagateTap={this.props.onPropagateTap}
                            />
                            <PlaylistFicheLink
                                fiche={item}
                                value={item.value}
                                onEdit={onEditLink}
                            />
                        </div>
                    )
                }
                else {
                    return (
                        <div key={item._id}>
                            <PlaylistFicheItem key={item._id} fiche={item} selected={this.props.selectedFiche != null && (item._id == this.props.selectedFiche._id)} onSelected={onSelected} onRemoved={onRemoved} onCategoryTap={this.props.onCategoryTap} onSubCategoryTap={this.props.onSubCategoryTap} onTagTap={this.props.onTagTap} onPropagateTap={this.props.onPropagateTap}/>
                            <PlaylistFicheAdd/>
                        </div>
                    )
                }
            });

            return fiches;
        }
    }

    render() {
        return (
            <div>
                <div className={cx("timeline") + " timeline"}>
                    <div className={cx("line") + " " + cx("text-muted") + " line text-muted"}></div>

                    {
                        this.renderFiches()
                    }
                </div>
            </div>
        )
    }
}