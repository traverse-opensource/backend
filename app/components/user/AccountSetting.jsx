import React,{Component} from 'react';
import {Field, reduxForm, SubmissionError } from 'redux-form';
import {connect} from 'react-redux';
import {renderSmallTextField, renderMediumTextField, renderBigTextField} from '../fiche/FormUtils';
import Paper from 'material-ui/Paper';
import { List, ListItem } from 'material-ui/List';
import classNames from 'classnames/bind';
import { genders } from '../../../app/constant';
import GenderSelect from './GenderSelect';
import axios from 'axios';

//shall create a new one maybe for user profile ?
import styles from '../../css/components/login';
const cx = classNames.bind(styles);

const errorMessage = {
  default: "Champ requis",
  oldpass: "L'ancien mot de passe ne correspond pas",
  twopass: "Les mots de passe ne correspondent pas",
  shortpass: "Le mot de passe choisi est trop court",
  mailexists: "Cette adresse mail est déjà prise"
};

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const asyncValidate = (values/*, dispatch */) => {
  
  let passwdErr = null;
  
  
  //checking if we have at least the previous password, if not just ask the user to input its previous one
  if (values.newpass || values.confpass) {
    if (Object.keys(values).indexOf("oldpass") === -1) {
      passwdErr = errorMessage.default;
    }else {
      axios.post('/user/' + values.userId + '/compare-password', {
        pass: values.oldpass
      })
      .then(function (response) {


          if (values.oldpass.length > 0){
            if (!response.data.match) {
              passwdErr = errorMessage.oldpass;
            }
          }
      });
    }  
  }
  
  
  
  return sleep(500)
    .then(() => {
        if (passwdErr) {
          throw { oldpass: passwdErr }
        }
    });
}

const validate = values => {
  
  const errors = {}
  const requiredFields = [ 'name', 'email' ]
  
  requiredFields.forEach(field => {
    if (!values[ field ]) {
      errors[ field ] = errorMessage[field] ?
        errorMessage[field]: errorMessage.default;
    }
  })
  
  if (values.newpass || values.confpass) {
    if (values.newpass !== values.confpass) {
      errors.newpass = errorMessage.twopass;
      errors.confpass = errorMessage.twopass;
    } else if (values.newpass.length < 4 || values.confpass.length < 4 ) {
      errors.newpass = errorMessage.shortpass;
      errors.confpass = errorMessage.shortpass;
    }
  }
  
  //checking email structure
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  
  return errors
}

class AccountSetting extends Component {
  
    constructor(props) {
      super(props);
      
      this.handleFormSubmit = this.handleFormSubmit.bind(this);
      this.updateUserGender = this.updateUserGender.bind(this);
      
      let groupKey = Object.keys(genders);
    
      //custom attribute
      this.groups = groupKey.map((key) => {
        return genders[key];
      });

      let targetGender = this.props.user.profile.gender ? this.props.user.profile.gender: "";
      let index = this.groups.indexOf(targetGender);
      
      this.state = {
        gender: targetGender,
        genderValue: index
      }
    }
  
    componentDidMount(){
      //TODO ask hassan how to use this
      this.handleInitialize();
    }
  
    componentWillReceiveProps(newProps){

      let me = this;
      let index = me.groups.indexOf(newProps.user.profile.gender);

      me.setState({
        gender: newProps.user.profile.gender,
        genderValue: index > -1 ? index: 0
      });
    }
  
    handleInitialize() {
      const {profile} = this.props.user;
      const initData = {
        "userId": this.props.user._id,
        "name": profile.name,
        "email": this.props.user.email,
        "website": profile.website,
        "gender": profile.gender
      };
      this.props.initialize(initData);
    }
  
    updateUserGender(choosenGender, index) {
      let me = this;
      this.setState({gender: choosenGender, genderValue: index}, () => {
        //a bit wierd callback...
        const {profile} = me.props.user;
        const initData = {
          "name": profile.name,
          "email": me.props.user.email,
          "website": profile.website,
          "gender": choosenGender
        };
        me.props.initialize(initData);
      });
    }
  
    createUpdateParams(remoteProps) {
      let toReturn = remoteProps.name || remoteProps.website || remoteProps.gender ?
          {profile: {}}: {};
      
      if (remoteProps.name) {
        toReturn.profile.name = remoteProps.name;
      }
      if (remoteProps.email) {
        toReturn.email = remoteProps.email;
      }
      if (remoteProps.website) {
        toReturn.profile.website = remoteProps.website;
      }
      if (remoteProps.gender) {
        toReturn.profile.gender = remoteProps.gender;
      }
      if (remoteProps.newpass) {
        toReturn.password = remoteProps.newpass;
      }
      
      return toReturn;
    }
  
    
  
    handleFormSubmit(formProps) {
      //will update stuff on handleFormSubmit keeping the database structure on post here
      this.props.updateUserFromProfile(this.props.user._id, this.createUpdateParams(formProps));
    }

    render() {
      
      //never forget that this shall match what we have in database
        const { handleSubmit } = this.props;
      
        
      
        return (
          <div>
            <form
              className="form-horizontal" 
              onSubmit={handleSubmit(this.handleFormSubmit)}
              style={{padding: "20px 16px 16px"}}>
              <div style={{display: "none"}}>
                <Field name="userId" type="hidden" component={renderBigTextField}/>
              </div>
              <div>
                <label>Nom Complet</label>
                <Field name="name" component={renderBigTextField} type="text"/>
              </div>
              <div>
                <label>Email</label>
                <Field name="email" component={renderBigTextField} type="text"/>
              </div>
              <div>
                <Paper style={{padding: '15px 20px', marginBottom: '15px', display: 'block', width: '100%'}} zDepth={1} >
                  <div>
                    <h4 style={{float: 'right'}}>Mot de passe</h4>
                  </div>
                  <div>
                    <label>Ancien mot de passe</label>
                    <Field name="oldpass" component={renderBigTextField} type="password"/>
                  </div>
                  <div>
                    <label>Nouveau mot de passe</label>
                    <Field name="newpass" component={renderBigTextField} type="password"/>
                  </div>
                  <div>
                    <label>Confirmation mot de passe</label>
                    <Field name="confpass" component={renderBigTextField} type="password"/>
                  </div>
                </Paper>
              </div>
              <div>
                <label>Site Web</label>
                <Field name="website" component={renderBigTextField} type="text" label="https://traverse-patrimoines.com"/>
              </div>
              <div style={{display: "none"}}>
                <Field name="gender" type="hidden" component={renderBigTextField} value={this.state.gender}/>
              </div>
              <div>
                <label>Genre</label>
                <GenderSelect 
                  groups={this.groups}
                  gender={this.state.gender}
                  genderValue={this.state.genderValue}
                  updateUserGender={this.updateUserGender}/>
              </div>
              <div>
                <input
                  className={cx('button')}
                  type="submit"
                  style={{fontSize: '16px', marginTop: '16px'}}
                  value="Enregistrer les modifications" />
              </div>
            </form>
          </div>
        )

    };
}

const form = reduxForm({
  form: 'accountSetting',
  validate,
  asyncValidate
});
function mapStateToProps(state){
  return {
    user: state.user.user
  }
}
export default connect(mapStateToProps, {})(form(AccountSetting));