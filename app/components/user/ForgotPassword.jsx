import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import styles from '../../css/components/login';

const cx = classNames.bind(styles);
class ForgotPassword extends Component{
    constructor(args){
        super(args);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event){
        event.preventDefault();
        const {forgotPassword} = this.props;
        const email = ReactDOM.findDOMNode(this.refs.forgotPassword).value;
        forgotPassword(email);
    }
    render() {
        return (
            <div className={cx('email-container')}>
                <form onSubmit={this.handleSubmit}>
                    <input  className={cx('input')}
                            type="email"
                            ref="forgotPassword"
                            placeholder="Email"/>
                    <input
                        className={cx('button')}
                        type="submit"
                        style={{fontSize: '16px'}}
                        value="Reinitialiser le mot de passe"/>
                </form>
            </div>
        );
    }
}

export default ForgotPassword;