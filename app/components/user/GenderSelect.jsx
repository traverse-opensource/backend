import React, {Component} from 'react';
import MTComponent from '../../components/MTComponent';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


/**
 * Aims to only handle option selection, does not update stuff from here, but call its parent component to update the group of
 * targetted user
 */
export default class GenderSelect extends MTComponent {
  
  constructor(props) {
    super(props);
    
    this.setAutoBindMethods([
      'handleChange',
      'createItems'
    ]);
    
    this.state = {
      value: this.props.genderValue
    };
    
    this.groups = this.props.groups ?
      this.props.groups: [];
  }
  
  componentWillReceiveProps(newProps){
    
    let me = this;

    me.groups = newProps.groups;
    
    me.setState({
      value: newProps.genderValue
    });
  }
  
  createItems() {
    let items = [];

    let genders = this.groups;
    
    let groupKey = Object.keys(genders);
    
    let groups = groupKey.map((key) => {
      return genders[key];
    });
    
    for (let i = 0; i < groups.length; i++ ) {
      items.push(<MenuItem value={i} key={groups[i]} primaryText={groups[i]} />);
    }
    return items;
  }

  handleChange(event, index, value){
    let me = this;
    
    let choosenGender = this.groups[index];
    
    me.setState({value: index}, () => {
      me.props.updateUserGender(choosenGender, index);  
    });
  };

  render() {
    
    let items = this.createItems();
    
    return (
      <SelectField
        value={this.state.value}
        onChange={this.handleChange}
        maxHeight={200}
        fullWidth={true}
      >
        {items}
      </SelectField>
    );
  }
}