
export const MISSING_CATEGORY       = 'Ajouter au moins une catégorie et une sous catégorie';
export const MISSING_FIELDS         ='Tous les champs de cette section sont requis';
export const MISSING_LOCATION       = 'Longitude et latitude sont requis';
export const ALERT_PUBLISH      = 'Veuillez saisir tous les champs requis pour pouvoir publier votre fiche';

//needed to distinguish which callbacks we want to call in playlist label selection
export const CALLBACK_MODES = {
    CATEGORIES: 1,
    SUB_CATEGORIES: 2,
    TAGS: 0
};

export const permission = {CONTRIBUTEUR:'contributeur', ADMIN: 'admin', SUPER_ADMIN:'super_admin', DEACTIVATED: 'deactivated', NOMAD: 'nomad'};
export const genders = {MALE:'Homme', FEMALE: 'Femme', UNKNOWN:'Autre'};
export const status = { DRAFT: 0, PUBLISHED: 1, DELETED: 2 };
export const FICHE_FILTERS = { ALL: 0, ALL_DRAFT: 1, MY_PUBLISHED: 2, MY_DRAFT: 3 };

export const FEED_STATUS = {
  DELETED: -1,
  FLAGGED: 0,
  POSTED: 1,
  APPROVED: 2
};

export const FEED_STATUS_STRING = {
  DELETED: "Supprimés",
  FLAGGED: "Signalés",
  POSTED: "Récemment postés",
  APPROVED: "Approuvés"
};

export const FEED_STATUS_PATH = {
  DELETED: "deleted",
  FLAGGED: "flagged",
  POSTED: "posted",
  APPROVED: "approved"
};

export const FEED_STATUS_LABELS = {
  DELETED: "Supprimer",
  FLAGGED: "Repporter",
  POSTED: "Reposter",
  APPROVED: "Approuver",
  RESTORED: "Restaurer"
};

export const EDITORIAL_HEADER = {
  'x-mtsk': 'Qydlc3QgcGFzIGxhIG3DqHJlIHF1aSBwcmVuZCBsJ2hvbW1l'
}

export const HASH_DELTAS = [
    "entre 0 à 1 an",
    "à 10 ans prés",
    "à 25 ans près",
    "à 50 ans près",
    "à 100 ans près",
    "plus de 100 ans près"
];

export const HASH_TYPES = [
    "Event",
    "Media",
    "Objet",
    "People",
    "Places"
]

export const RIGHTS = {
    //checking inside this method if the params is a string or a user object
    //toCompare can be an instance of fiche nor an instance of playlist
    //the matching item would be created_by props so that we don't need to to check the instance of the comparing object
    HAS_ENOUGH_PRIVILEGES: (userOrUserId, toCompare, group = null) => {

        let toReturn = false;
        //since first call has not filled created_by column yet, we need to return at least false
        if (toCompare) {
            let matchUserId = userOrUserId._id ? userOrUserId._id: userOrUserId,
                matchComparedUserId = toCompare._id ? toCompare._id: toCompare;

            toReturn = RIGHTS.COMPARE_GROUP(group, matchUserId === matchComparedUserId);

            return toReturn;
        }

        toReturn = RIGHTS.COMPARE_GROUP(group, toReturn);

        return toReturn;
    },
    IS_SUPER_ADMIN: (userOrUserId) => {
        return userOrUserId._id && (userOrUserId.group.title===permission.SUPER_ADMIN);
    },
    IS_ADMIN: (userOrUserId) =>{
        return userOrUserId._id && userOrUserId.group.title ===permission.ADMIN;
    },
    IS_CONTRIBUTOR: (userOrUserId) => {
        return userOrUserId._id &&  userOrUserId.group.title === permission.CONTRIBUTEUR;
    },
    IS_GROUP_ADMIN: (userOrUserId, entity)=>{
        let isGroupAdmin = false;
        if(userOrUserId){
            userOrUserId.entities.forEach((UserEntity)=>{
                if(UserEntity._id === entity) isGroupAdmin= true;
            })
        }
        return isGroupAdmin;
    },

    COMPARE_GROUP: (group, previousResult = false) => {

        if (group !== null){
            previousResult |= group.title === permission.SUPER_ADMIN || group.title == permission.ADMIN;
        }

        return previousResult;
    }
};

export const FICHE_TYPES = {
    PLACES: "Places",
    PEOPLE: "People",
    MEDIA: "Media",
    OBJECTS: "Objects",
    EVENTS: "Events"
};

export const COVER_ON_IMAGE_LOAD = (img, cb) => {
  let upperNode = img.parentNode,
      image = [img.offsetWidth, img.offsetHeight, img.offsetWidth / img.offsetHeight],
      parent = [upperNode.offsetWidth, upperNode.offsetHeight, upperNode.offsetWidth / upperNode.offsetHeight],
      //target will be the final computation we send so that the offset is stored also in this variable
      target = [img.offsetWidth, img.offsetHeight, 0];

  //maybe we need to check these ratios
  if (image[2] > parent[2]) {
      //need to crop horizontally
      /*target[0] = 260;
      target[1] = 160;*/
      target[0] = parent[0];
      target[1] = parent[1];
  }else {
      //need to crop vertically
      target[1] = parent[1];
      //round to two decimals for percentage
      target[0] = target[1] * image[0] / image[1];
      target[2] = (parent[0] - target[0]) / 2;
      target[3] = 0;

      //checking if we have to resize
      if (target[0] < parent[0]) {
          //console.debug('debug dimens second condition', image, parent, target);
          //crop vertically => more width and height in overflow with top offset
          target[0] = parent[0];
          target[1] = target[0] * image[1] / image[0];
          target[2] = 0;
          target[3] = (target[1] - parent[1]) / 2;
      }else {
          //crop image horizontally => more height, width overflow and left offset
          target[1] = parent[1];
          target[0] = target[1] * image[0] / image[1];
          target[2] = (target[0] - parent[0]) / 2;
          target[3] = 0;
      }
  }

  return cb(target);
}
