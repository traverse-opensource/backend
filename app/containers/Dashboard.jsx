import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/users';
/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Dashboard extends Component{
    render() {
        const {user} = this.props.user;
        return (
            <div>
              <h3>hello {user.name}</h3>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}
export default connect(mapStateToProps, actions)(Dashboard);
