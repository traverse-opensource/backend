import React, {Component} from 'react';
import classNames from 'classnames/bind';
import ReactDOM from 'react-dom';
import MTComponent from '../components/MTComponent';
import styles from 'css/components/about';
import fstyles from '../css/components/login';
import * as actions from '../actions/groups';
import { connect } from 'react-redux';
import {List, ListItem} from 'material-ui/List';
import {Card, CardTitle} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import ActionDelete from 'material-ui/svg-icons/action/delete';

import { RIGHTS } from '../constant';

const cx = classNames.bind(styles);
const fs = classNames.bind(fstyles);

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Groups extends MTComponent {
    constructor(props) {
        super(props);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.state = {
            currentGroups: this.props.groups
        };
    }
    handleOnSubmit(event) {
        event.preventDefault();
        const { createGroup } = this.props;
        const name = ReactDOM.findDOMNode(this.refs.name).value;
        const description = ReactDOM.findDOMNode(this.refs.description).value;
        const creator = ReactDOM.findDOMNode(this.refs.creator).value;

        //need group also

        createGroup({ name, description, creator });
    }
    render() {
        const isMainGroup = (group) => {
            return group.title === "contributeur" || group.title==="admin" || group.title==="super_admin"
        };
        const loggedUser = this.props.user;
        let children = (group) => {
            return (
                <div style={{float: "right"}}>
                    {
                        RIGHTS.IS_SUPER_ADMIN(loggedUser) || RIGHTS.IS_GROUP_ADMIN(loggedUser, group._id)?
                            <IconButton className="disabled" tooltip="Supprimer ce groupe " style={{float: "right"}}>
                                <ActionDelete onClick={() => this.props.deleteGroup(group._id)}/>
                            </IconButton>:
                            ""
                    }
                </div>
            )};
        const buildForm = () => {
            return (
                <form onSubmit={this.handleOnSubmit}>
                    <input
                        className={fs("input")}
                        type="name"
                        ref="name"
                        placeholder="Nom du groupe"/>
                    <input
                        className={fs('input')}
                        type="description"
                        ref="description"
                        placeholder="Description du groupe"
                    />
                    <input
                        className={fs('input')}
                        type="hidden"
                        ref="creator"
                        value={this.props.user._id}
                    />
                    <input
                        className={fs('button')}
                        type="submit"
                        style={{fontSize: '16px'}}
                        value="Créer le groupe" />
                </form>
            );
        };
        const groupsList = this.props.groups.map((group, key) => {
            return <div>
                <ListItem primaryText={group.name}
                          secondaryText={group.description}
                          children={children(group)}/></div>
        });
        return (
            <div>
                <Card style={{marginTop: "16px"}}>
                    <CardTitle title="Gérer les groupes"/>
                    <div style={{padding: "16px"}}>
                        {buildForm()}
                    </div>
                </Card>
                <Card style={{marginTop: "16px"}}>
                    <CardTitle title="Liste des groupes"/>
                    <List>
                        {groupsList}
                    </List>
                </Card>
            </div>
        );
    };
}
function mapStateToProps(state){
    return {
        users: state.user.users,
        user: state.user.user,
        groups: state.user.groups
    }
}

export default connect(mapStateToProps, actions)(Groups);
