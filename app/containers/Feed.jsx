import React, { Component, PropTypes } from 'react';
import MTComponent from '../components/MTComponent.jsx';
import { connect } from 'react-redux';
import * as actions from '../actions/users.js';

import { FEED_STATUS } from '../constant';
import FeedSection from '../components/feed/FeedSection.jsx';

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */



class Feed extends MTComponent{

  /*constructor(props) {
    super(props);
    this.state = {
      shown: FEED_STATUS.POSTED
    };

  };*/
    /*componentWillUpdate(nextProps, nextState) {

      if (nextProps.location){
        if (nextProps.location.state)
          if (nextProps.location.state.keepContext){
            //TODO do something here since we context
          }
        }else {
          //TODO do something else
        }
    }*/

  render() {
    const {getCreator} = this.props;

    return (
        <div>
            {this.props.children}
            <div className="page-header">
                <h1>Liste des posts du fil d'actualité</h1>
            </div>
            <FeedSection
              getCreator={getCreator}
              sessionUser={this.props.user}/>
        </div>
    );
  }
}

function mapStateToProps(state){
    return {
        user: state.user.user
    }
}
export default connect(mapStateToProps, actions)(Feed);
