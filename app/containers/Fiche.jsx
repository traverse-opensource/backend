import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
//import { fiches } as actions from '../actions';
import * as actions from '../actions/fiches.js';

import FicheSection from '../components/fiche/FicheSection.jsx';
/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Fiche extends Component{
  
    componentWillUpdate(nextProps, nextState) {
      
      if (nextProps.location){
        if (nextProps.location.state)
          if (nextProps.location.state.keepContext){
            //TODO do something here since we context
          }
        }else {
          //TODO do something else
        }
    }
  
    render() {
        const {fiches, destroyFiche, getCreator, enumerations} = this.props;
      
        return (
            <div>
                {this.props.children}
                <div className="page-header">
                    <h1>Liste des fiches</h1>
                </div>
                <FicheSection fiches={fiches}
                              onDestroy={destroyFiche}
                              enumerations={enumerations}
                              getCreator={getCreator}
                              sessionUser={this.props.user}
                />
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        fiches: state.fiche.fiches,
        enumerations: state.enumerations,
        user: state.user.user
    }
}
export default connect(mapStateToProps, actions)(Fiche);
