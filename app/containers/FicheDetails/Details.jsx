import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/fiches.js';
import {EventDetailed, MediaDetailed, PlaceDetailed, ObjectDetailed, PersonDetailed,} from './';
import {browserHistory} from 'react-router';
import moment from 'moment';
moment.locale('fr');

class Details extends Component {
    constructor(props){
        super(props);
        this.onBackAction = this.onBackAction.bind(this);
        this.onEditAction = this.onEditAction.bind(this);
        this.computeRemotePath = this.computeRemotePath.bind(this);
    }
    componentDidMount(){
        this.props.fetchOneFiche(this.props.params.ficheId);
    }
    componentWillUnmount(){
        this.props.flushDisplayedFiche();
    }
    onBackAction(){
        browserHistory.goBack();
    }
    onEditAction(){
        let currentPath = this.props.location.pathname;let newPath="/";
            if(currentPath.indexOf("People")!==-1){ newPath = currentPath.replace("People", "editPersonne")}
            if(currentPath.indexOf("Place")!==-1){ newPath = currentPath.replace("Places", "editPlace")}
            if(currentPath.indexOf("Media")!==-1){  newPath = currentPath.replace("Media", "editMedia")}
            if(currentPath.indexOf("Object")!==-1){  newPath = currentPath.replace("Objects", "editObjet");}
            if(currentPath.indexOf("Event")!==-1){ newPath = currentPath.replace("Events", "editEvenement");}
        browserHistory.push(newPath);
    }
    computeRemotePath =() =>{
        //shall always have a cover so no need to test this attribute, but the child attribute needs to be tested
        return (this.props.fiche.cover.path.indexOf('http') !== -1 ?
        '' + this.props.fiche.cover.path: '/' + this.props.fiche.cover.path);
    }
    champsNonRenseigne(champs){
        return champs? champs : ""
    }
    formatageDeLaDate(date) {
        let retour = date;
        moment(date).isValid()? retour = moment(date).format('LL') :retour= ""  ;
        return retour;
    }
    social(network){
        let link = "", tags = [];
        if(network){
            if(network.link!== "") link = network.link;
            if(network.tags && network.tags.length > 0 )tags = network.tags;
        }
        return {link ,tags}
    }
    render(){
        const hashtag = (network, internal = false) => {
            return internal ? (
              <div style={{color: '#288FD3'}}>
                    {network}
              </div>
            ):(
                <div>
                    <h5>Hashtags: </h5>
                    {this.social(network).tags.length > 0 ?
                        <div style={{display:'inline-block'}}>
                            {this.social(network).tags.map((tag) => {
                                return <p style={{float : 'left'}}>{tag.value}&nbsp;</p>
                            })}
                        </div> : <p style={{float : 'left', display:'inline-block'}}> Il n'y a pas de hashtags renseignés</p>}
                </div>
            )
        };
        const style = {
            float: 'left',
            marginLeft: '5px'
        };
        let typeFiche = this.props.route.path.slice(0,-9);
        let component =
            typeFiche ==="media" ?
                <MediaDetailed
                    champsNonRenseigne={this.champsNonRenseigne}
                    formatageDeLaDate={this.formatageDeLaDate}
                    social={this.social}
                    media={this.props.fiche}
                    onBackAction={this.onBackAction}
                    onEditAction={this.onEditAction}
                    style={style}
                    user={this.props.user}
                    hashtag={hashtag}
                    /> :
                typeFiche==="people" ?  <PersonDetailed
                    champsNonRenseigne={this.champsNonRenseigne}
                    formatageDeLaDate={this.formatageDeLaDate}
                    social={this.social}
                    fiche={this.props.fiche}
                    onBackAction={this.onBackAction}
                    onEditAction={this.onEditAction}
                    style={style}
                    user={this.props.user}
                    hashtag={hashtag}
                    />:
                    typeFiche==="objects" ?  <ObjectDetailed
                        champsNonRenseigne={this.champsNonRenseigne}
                        formatageDeLaDate={this.formatageDeLaDate}
                        social={this.social}
                        object={this.props.fiche}
                        onBackAction={this.onBackAction}
                        onEditAction={this.onEditAction}
                        style={style}
                        user={this.props.user}
                        hashtag={hashtag}
                        />:
                        typeFiche==="places" ?  <PlaceDetailed
                            champsNonRenseigne={this.champsNonRenseigne}
                            formatageDeLaDate={this.formatageDeLaDate}
                            social={this.social}
                            place={this.props.fiche}
                            onBackAction={this.onBackAction}
                            onEditAction={this.onEditAction}
                            style={style}
                            user={this.props.user}
                            hashtag={hashtag}
                            />:
                            typeFiche==="events" ?  <EventDetailed
                                champsNonRenseigne={this.champsNonRenseigne}
                                formatageDeLaDate={this.formatageDeLaDate}
                                social={this.social}
                                event={this.props.fiche}
                                onBackAction={this.onBackAction}
                                onEditAction={this.onEditAction}
                                style={style}
                                user={this.props.user}
                                hashtag={hashtag}
                                />:null;
        return(<div>{component}</div>)
    }
}
function mapStateToProps(state){
    return {
        fiches: state.fiche.fiches,
        fiche: state.fiche.displayedFiche,
        user: state.user.user
    }
}
export default connect(mapStateToProps, actions)(Details);