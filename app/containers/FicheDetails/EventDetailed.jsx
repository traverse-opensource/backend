import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { RIGHTS ,HASH_DELTAS} from '../../constant';
class EventDetailed extends Component{
    render() {
        const {social,champsNonRenseigne,
            formatageDeLaDate, event} = this.props;

        const style = {
            float: 'left',
            marginLeft: '5px'
        };
        const {user} = this.props,
              {group} = user,
              {_id} = user,
              {created_by} = this.props.event;
        let actionButtons =
            <div className="affix">
                <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
            </div>;


        if(RIGHTS.HAS_ENOUGH_PRIVILEGES(user, created_by, group)){
            actionButtons =
                <div className="affix">
                    <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
                    <Button bsStyle="primary" className="glyphicon glyphicon-edit" style={style} onClick={this.props.onEditAction}> Éditer</Button>
                </div>
        }
        return(
            <div>
                <div className="page-header">
                    {
                        this.props.event?
                            <h1>Détail de l'événement : {this.props.event.name}</h1>
                            :
                            <h1>Erreur de chargement</h1>
                    }
                </div>
                <div className="row">
                    <div className="col-md-3">
                        {actionButtons}
                    </div>
                    <div className="col-md-9">
                        <div>
                            {
                                this.props.event &&
                                <div>
                                    <div className="container-fluid well center">
                                        <img className="img-responsive" src={this.props.event.cover ? '/'+this.props.event.cover.path  : "/images/greybackground.png"} />
                                        <h4>Source</h4> <p>{champsNonRenseigne(this.props.event.cover.cover_credit)}</p>
                                        <p dangerouslySetInnerHTML={{__html: '<h4>Nom</h4>' + champsNonRenseigne(this.props.event.name)}}/>
                                        <h4>Description</h4> <p>{champsNonRenseigne(this.props.event.presentation)}</p>
                                        <p dangerouslySetInnerHTML={{__html: '<h4>Présentation</h4>' + champsNonRenseigne(this.props.event.description)}}/>
                                        <p><h4>Date de début </h4> {formatageDeLaDate(this.props.event.start_date)} {HASH_DELTAS[this.props.event.delta_start]}</p>
                                        <p><h4>Date de fin </h4>{formatageDeLaDate(this.props.event.end_date)} {HASH_DELTAS[this.props.event.delta_end]}</p>
                                    </div>

                                    <div className="container-fluid well">
                                        <h2>Thèmes</h2>
                                        <br/>
                                        {this.props.event.themes.map((theme) => {
                                            return (<div className="col-md-3"><p style={{backgroundColor:theme.color}}>{theme.name} {theme.ponderation}%</p></div>)
                                        })}
                                    </div>
                                    <div className="container-fluid well">
                                        <h2>Biographie/Sources du web</h2>
                                        <p dangerouslySetInnerHTML={{__html: this.props.event.references}}/>
                                    </div>

                                    <div className="container-fluid well">
                                        <h2>Classification</h2>
                                        <div>
                                            <label>Catégories</label>
                                            <br/>
                                            {this.props.event.categories.map((category,key) => {
                                                return (<div  key={key}className="col-md-3"><p>{category.name}</p></div>)
                                            })}
                                        </div>
                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Sous catégories</label>
                                            <br/>
                                            {this.props.event.sousCategories.map((sc,key) => {
                                                return (<div  key={key}className="col-md-3"><p>{sc}</p></div>)
                                            })}
                                        </div>

                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Mots clés</label>
                                            <br/>
                                            {this.props.event.tags.map((tag,key) => {
                                                return (<div key={key}  className="col-md-3"><p>{tag.name}</p></div>)
                                            })}
                                        </div>
                                    </div>
                                    {this.props.event.social &&
                                    <div className="container-fluid well">
                                        <h3>Liens externes</h3>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Traverse:</h4> {social(this.props.event.slug).hashtag}
                                            {this.props.hashtag(this.props.event.slug, true)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Facebook:</h4> {social(this.props.event.social.facebook).link}
                                            {this.props.hashtag(this.props.event.social.facebook)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Instagram:</h4> {social(this.props.event.social.instagram).link}
                                            {this.props.hashtag(this.props.event.social.instagram)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Twitter:</h4>  {social(this.props.event.social.twitter).link}.
                                            {this.props.hashtag(this.props.event.social.twitter)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Web:</h4>  {social(this.props.event.social.web).link}
                                        </div>
                                    </div>
                                    }
                                    <div className="container-fluid well">
                                        <h3>Adresse</h3>
                                        <h4>{this.props.event.map ? this.props.event.map.fullAddrStr : "Non renseignée."}</h4>
                                    </div>
                                    <div className="container-fluid well">
                                        <h2>Meta-données</h2>
                                        <p>Date de création : {formatageDeLaDate(this.props.event.created_at)}</p>
                                        <p>Auteur : {this.props.event.created_by.profile.name}</p>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}
export default (EventDetailed);