import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { RIGHTS,HASH_DELTAS } from '../../constant';

class Media extends Component{
    constructor(props){
        super(props);
    }
    render() {
        const {social,champsNonRenseigne,
            formatageDeLaDate, media} = this.props;
        let targetLink = (url) => {
            return url.indexOf('http') !== -1 ?
                url: '/' + url;
        };

        const style = {
            float: 'left',
            marginLeft: '5px'
        };
        const {user} = this.props,
            {group} = user,
            {_id} = user,
            {created_by} = this.props.media;
        let actionButtons =
            <div className="affix">
                <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
            </div>;


        if(RIGHTS.HAS_ENOUGH_PRIVILEGES(user, created_by, group)){
            actionButtons =
                <div className="affix">
                    <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
                    <Button bsStyle="primary" className="glyphicon glyphicon-edit" style={style} onClick={this.props.onEditAction}> Éditer</Button>
                </div>
        }
        return(
            <div>
                <div className="page-header">
                    {
                        this.props.media?
                            <h1>Détail du media : {this.props.media.name}</h1>
                            :
                            <h1>Erreur de chargement</h1>
                    }
                </div>
                <div className="row">
                    <div className="col-md-3">
                        {actionButtons}
                    </div>
                    <div className="col-md-9">
                        <div>
                            {
                                this.props.media&&
                                <div>
                                    <h2>Informations de base</h2>
                                    <div className="container-fluid well center">
                                        <img className="img-responsive" src={this.props.media.cover ? targetLink(this.props.media.cover.path) : "/images/greybackground.png"} />
                                        <br/>
                                        <p><h4>Crédits de l'image</h4>{champsNonRenseigne(this.props.media.cover.cover_credit)}</p>
                                        <p><h4>Nom</h4> {champsNonRenseigne(this.props.media.name)}</p>
                                        <p dangerouslySetInnerHTML={{__html: '<h4>Presentation</h4>' + champsNonRenseigne(this.props.media.presentation)}}/>
                                        <p><h4>Chemin</h4> {champsNonRenseigne(this.props.media.path)}</p>
                                        <p><h4>Date</h4> {this.props.media.date ? formatageDeLaDate(this.props.media.date.date): "Pas de date renseignée"} {HASH_DELTAS[this.props.media.delta_start]}</p>
                                        <p><h4>Description de la date</h4> {this.props.media.date ?  champsNonRenseigne(this.props.media.date.description) : "Ce champs n'a pas été renseigné"}</p>

                                    </div>
                                    <div className="container-fluid well">
                                        <h2>Thèmes</h2>
                                        <br/>
                                        {this.props.media.themes.map((theme) => {
                                            return (<div className="col-md-3"><p style={{backgroundColor:theme.color}}>{theme.name} {theme.ponderation}%</p></div>)
                                        })}
                                    </div>
                                    <div className="container-fluid well">
                                        <h2>Biographie/Sources du web</h2>
                                        <p dangerouslySetInnerHTML={{__html: this.props.media.references}}/>
                                    </div>

                                    <div className="container-fluid well">
                                        <h2>Classification</h2>
                                        <div>
                                            <label>Catégories</label>
                                            <br/>
                                            {this.props.media.categories.map((category,key) => {
                                                return (<div key={key} className="col-md-3"><p>{category.name}</p></div>)
                                            })}
                                        </div>
                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Sous catégories</label>
                                            <br/>
                                            {this.props.media.sousCategories.map((sc,key) => {
                                                return (<div key={key} className="col-md-3"><p>{sc}</p></div>)
                                            })}
                                        </div>

                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Mots clés</label>
                                            <br/>
                                            {this.props.media.tags.map((tag,key) => {
                                                return (<div key={key} className="col-md-3"><p>{tag.name}</p></div>)
                                            })}
                                        </div>
                                    </div>
                                    {this.props.media.social  &&
                                    <div className="container-fluid well">
                                        <h3>Liens externes</h3>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Traverse:</h4> {social(this.props.media.slug).hashtag}
                                            {this.props.hashtag(this.props.media.slug, true)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Facebook:</h4> {social(this.props.media.social.facebook).link}
                                            {this.props.hashtag(this.props.media.social.facebook)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Instagram:</h4> {social(this.props.media.social.instagram).link}
                                            {this.props.hashtag(this.props.media.social.instagram)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Twitter:</h4>  {social(this.props.media.social.twitter).link}.
                                            {this.props.hashtag(this.props.media.social.twitter)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Web:</h4>  {social(this.props.media.social.web).link}
                                        </div>
                                    </div>
                                    }
                                    <div className="container-fluid well">
                                        <h3>Adresse</h3>
                                        <h4>{this.props.media.map ? this.props.media.map.fullAddrStr : "Non renseignée."}</h4>
                                    </div>
                                    <div className="container-fluid well">
                                        <h2>Meta-données</h2>
                                        <p>Date de création : {formatageDeLaDate(this.props.media.created_at)}</p>
                                        <p>Auteur : {this.props.media.created_by.profile.name}</p>
                                    </div>
                                </div>

                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default (Media);