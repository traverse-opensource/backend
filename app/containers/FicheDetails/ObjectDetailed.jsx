import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { RIGHTS ,HASH_DELTAS} from '../../constant';

class ObjectDetailed extends Component{
    render() {
        const {social,champsNonRenseigne,
            formatageDeLaDate, object} = this.props;
      
        const style = {
            float: 'left',
            marginLeft: '5px'
        };
        const {user} = this.props,
              {group} = user,
              {_id} = user,
              {created_by} = this.props.object;
        let actionButtons =
            <div className="affix">
                <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
            </div>;
      
        if(RIGHTS.HAS_ENOUGH_PRIVILEGES(user, created_by, group)){
            actionButtons =
                <div className="affix">
                    <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
                    <Button bsStyle="primary" className="glyphicon glyphicon-edit" style={style} onClick={this.props.onEditAction}> Éditer</Button>
                </div>
        }
        return(
            <div>
                <div className="page-header">
                    {
                        this.props.object?
                            <h1>Détail de l'objet : {this.props.object.name}</h1>
                            :
                            <h1>Erreur de chargement</h1>
                    }
                </div>
                <div className="row">
                    <div className="col-md-3">
                        {actionButtons}
                    </div>
                    <div className="col-md-9">
                        <div>
                            {
                                this.props.object &&
                                    <div>
                                        <h2>Informations de base</h2>
                                        <div className="container-fluid well center">
                                            <img className="img-responsive" src={this.props.object.cover ? '/'+this.props.object.cover.path  : "/images/greybackground.png"} />
                                            <br/>
                                            <p><h4>Source  de l'image: </h4>{this.props.object.cover.cover_credit}</p>
                                            <p><h4>Nom :</h4> {this.props.object.name}</p>
                                            <h4>Description courte:</h4> <p>{this.props.object.short_description}</p>
                                            <p dangerouslySetInnerHTML={{__html: '<h4>Description</h4>' + champsNonRenseigne(this.props.object.presentation)}}/>
                                            <p dangerouslySetInnerHTML={{__html: '<h4>Histoire</h4>' + champsNonRenseigne(this.props.object.history)}}/>
                                            <p><h4>Date :</h4> {this.props.object.date ?formatageDeLaDate(this.props.object.date.date) : "Pas de date renseignée"} {HASH_DELTAS[this.props.object.delta_start]}</p>
                                            <p><h4>Description de la date :</h4> {this.props.object.date ?  champsNonRenseigne(this.props.object.date.description) : "Ce champs n'a pas été renseigné"}</p>

                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Thèmes</h2>
                                            <br/>
                                            {this.props.object.themes.map((theme) => {
                                                return (<div className="col-md-3"><p style={{backgroundColor:theme.color}}>{theme.name} {theme.ponderation}%</p></div>)
                                            })}
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Biographie/Sources du web</h2>
                                            <p dangerouslySetInnerHTML={{__html: this.props.object.references}}/>
                                        </div>

                                        <div className="container-fluid well">
                                            <h2>Classification</h2>
                                            <div>
                                                <label>Catégories</label>
                                                <br/>
                                                {this.props.object.categories.map((category,key) => {
                                                    return (<div key={key} className="col-md-3"><p>{category.name}</p></div>)
                                                })}
                                            </div>
                                            <br/>
                                            <br/>
                                            <div>
                                                <label>Sous catégories</label>
                                                <br/>
                                                {this.props.object.sousCategories.map((sc,key) => {
                                                    return (<div key={key} className="col-md-3"><p>{sc}</p></div>)
                                                })}
                                            </div>

                                            <br/>
                                            <br/>
                                            <div>
                                                <label>Mots clés</label>
                                                <br/>
                                                {this.props.object.tags.map((tag,key) => {
                                                    return (<div key={key} className="col-md-3"><p>{tag.name}</p></div>)
                                                })}
                                            </div>
                                        </div>

                                        {this.props.object.social  &&
                                        <div className="container-fluid well">
                                            <h3>Liens externes</h3>
                                            <div className="row" style={{paddingLeft: "19px"}}>
                                                <h4>Traverse:</h4> {social(this.props.object.slug).hashtag}
                                                {this.props.hashtag(this.props.object.slug, true)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}>
                                                <h4>Facebook:</h4> {social(this.props.object.social.facebook).link}
                                                {this.props.hashtag(this.props.object.social.facebook)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}><h4>
                                                Instagram:</h4> {social(this.props.object.social.instagram).link}
                                                {this.props.hashtag(this.props.object.social.instagram)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}><h4>
                                                Twitter:</h4>  {social(this.props.object.social.twitter).link}.
                                                {this.props.hashtag(this.props.object.social.twitter)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}><h4>
                                                Web:</h4>  {social(this.props.object.social.web).link}
                                            </div>
                                        </div>
                                        }
                                        <div className="container-fluid well">
                                            <h3>Adresse</h3>
                                            <h4>{this.props.object.map ? this.props.object.map.fullAddrStr : "Non renseignée."}</h4>
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Meta-données</h2>
                                            <p>Date de création : {formatageDeLaDate(this.props.object.created_at)}</p>
                                            <p>Auteur : {this.props.object.created_by.profile.name}</p>
                                        </div>
                                    </div>

                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default (ObjectDetailed);