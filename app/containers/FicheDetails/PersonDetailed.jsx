import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/fiches.js';
//Below required for buttons
import { Button } from 'react-bootstrap';
import { RIGHTS, HASH_DELTAS } from '../../constant';
class PersonDetailed extends Component{
    constructor(props){
        super(props);
        this.computeRemotePath = this.computeRemotePath.bind(this);
    }
    computeRemotePath(){
        //shall always have a cover so no need to test this attribute, but the child attribute needs to be tested
        if(this.props.person.cover && this.props.person.cover.path) {
            return (this.props.person.cover.path.indexOf('http') !== -1 ?
            '' + this.props.person.cover.path : '/' + this.props.person.cover.path);
        }
        return "images/defaultImage_Site.png"
    }
    render() {
        const {social,champsNonRenseigne,
            formatageDeLaDate, person} = this.props;
        const style = {
            float: 'left',
            marginLeft: '5px'
        };

        const {user} = this.props,
              {group} = user,
              {_id} = user,
              {created_by} = this.props.person;
        let actionButtons =
            <div className="affix">
                <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
            </div>;


        if(RIGHTS.HAS_ENOUGH_PRIVILEGES(user, created_by, group)){
            actionButtons =
                <div className="affix">
                    <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
                    <Button bsStyle="primary" className="glyphicon glyphicon-edit" style={style} onClick={this.props.onEditAction}> Éditer</Button>
                </div>
        }
        return(
            <div>
                <div className="page-header">
                    {
                        this.props.person?
                            <h1>Détail de la personne : {this.props.person.name}</h1>
                            :
                            <h1>Erreur de chargement</h1>
                    }
                </div>
                <div className="row">

                    <div className="col-md-3">
                        {actionButtons}
                    </div>
                    <div className="col-md-9">
                        <div>
                            {
                                this.props.person ?
                                    <div>
                                        <h2>Informations de base</h2>
                                        <div className="container-fluid well center">
                                            <img className="img-responsive" src={this.computeRemotePath()} />
                                            <br/>
                                            <h4>Source : </h4> <p>{champsNonRenseigne(this.props.person.cover.cover_credit)}</p>
                                            <h4>Nom :</h4><p> {champsNonRenseigne(this.props.person.name)}</p>
                                            <h4>Anniversaire :</h4><p> {formatageDeLaDate(this.props.person.birthday)}  {HASH_DELTAS[this.props.person.delta_start]} </p>
                                            <h4>Décès :</h4> <p>{formatageDeLaDate(this.props.person.deathday)}  {HASH_DELTAS[this.props.person.delta_end]}</p>
                                            <p dangerouslySetInnerHTML={{__html: '<h4>Biographie courte</h4>' + this.props.person.short_bio}}/>
                                            <p dangerouslySetInnerHTML={{__html: '<h4>Présentation</h4>' + champsNonRenseigne(this.props.person.presentation)}}/>
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Thèmes</h2>
                                            <br/>
                                            {this.props.person.themes.map((theme, key) => {
                                                return (<div key={key} className="col-md-3"><p style={{backgroundColor:theme.color}}>{theme.name} {theme.ponderation}%</p></div>)
                                            })}
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Biographie/Sources du web</h2>
                                            <p dangerouslySetInnerHTML={{__html: this.props.person.references}}/>
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Classification</h2>
                                            <br/>
                                            <div>
                                                <label>Mots clés</label>
                                                <br/>
                                                {this.props.person.tags.map((tag, key) => {
                                                    return (<div className="col-md-3" key={key}><p>{tag.name}</p></div>)
                                                })}
                                            </div>
                                        </div>
                                        {this.props.person.social  &&
                                        <div className="container-fluid well">
                                            <h2>Liens externes</h2>
                                            <div className="row" style={{paddingLeft: "19px"}}>
                                                <h4>Traverse:</h4> {social(this.props.person.slug).hashtag}
                                                {this.props.hashtag(this.props.person.slug, true)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Facebook:</h4> {social(this.props.person.social.facebook).link}
                                                {this.props.hashtag(this.props.person.social.facebook)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}><h4>
                                                Instgram:</h4> {social(this.props.person.social.instagram).link}
                                                {this.props.hashtag(this.props.person.social.instagram)}
                                            </div>
                                            <div className="row" style={{paddingLeft: "19px"}}><h4>
                                                Twitter:</h4>  {social(this.props.person.social.twitter).link}
                                                {this.props.hashtag(this.props.person.social.twitter)}
                                            </div>
                                            <div><h4>
                                                Web:</h4>  {social(this.props.person.social.web).link}
                                            </div>
                                        </div>
                                        }
                                        <div className="container-fluid well">
                                            <h3>Adresse</h3>
                                            <h4>{this.props.person.map ? this.props.person.map.fullAddrStr : "Non renseignée."}</h4>
                                        </div>
                                        <div className="container-fluid well">
                                            <h2>Meta-données</h2>
                                            <p>Date de création : {formatageDeLaDate(this.props.person.created_at)}</p>
                                           <p>Auteur : {this.props.person.created_by.profile.name}</p>
                                        </div>
                                    </div>
                                    :
                                    <p>Chargement en cours</p>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
function mapStateToProps(state){
    return {
        fiches: state.fiche.fiches,
        person: state.fiche.displayedFiche
    }
}
export default connect(mapStateToProps, actions)(PersonDetailed);