import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { RIGHTS } from '../../constant';
class PlaceDetailed extends Component{
    render() {
        const {social,champsNonRenseigne,
            formatageDeLaDate, place} = this.props;
        const style = {
            float: 'left',
            marginLeft: '5px'
        };
        const {user} = this.props,
            {group} = user,
            {_id} = user,
            {created_by} = this.props.place;
        let actionButtons =
            <div className="affix">
                <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
            </div>;


        if(RIGHTS.HAS_ENOUGH_PRIVILEGES(user, created_by, group)){
            actionButtons =
                <div className="affix">
                    <Button bsStyle="primary" className="glyphicon glyphicon-arrow-left" style={style} onClick={this.props.onBackAction}> Revenir</Button>
                    <Button bsStyle="primary" className="glyphicon glyphicon-edit" style={style} onClick={this.props.onEditAction}> Éditer</Button>
                </div>
        }
        return(
            <div>
                <div className="page-header">
                    {
                        this.props.place?
                            <h1>Détail de la place : {this.props.place.name}</h1>
                            :
                            <h1>Erreur de chargement</h1>
                    }
                </div>
                <div className="row">
                    <div className="col-md-3">
                        {actionButtons}
                    </div>

                    <div className="col-md-9">
                        <div>
                            {
                                this.props.place &&
                                <div>
                                    <h3>Informations de base</h3>
                                    <div className="container-fluid well center">
                                        <img className="img-responsive" src={this.props.place.cover ? '/'+this.props.place.cover.path : "http://images.gadmin.st.s3.amazonaws.com/n30096/images/keyvisuals/img_9879-2.jpg"} />
                                        <br/>
                                        <h4>Source : </h4> <p>{champsNonRenseigne(this.props.place.cover.cover_credit)}</p>
                                        <h4>Nom :</h4><p> {champsNonRenseigne(this.props.place.name)}</p>
                                        <h4>Qualification du site:</h4> <p>{champsNonRenseigne(this.props.place.short_description)}</p>
                                        <p dangerouslySetInnerHTML={{__html: '<h4>Description</h4>' + champsNonRenseigne(this.props.place.presentation)}}/>
                                        <p dangerouslySetInnerHTML={{__html: '<h4>Histoire</h4>' + champsNonRenseigne(this.props.place.history)}}/>
                                    </div>
                                    <div className="container-fluid well">
                                        <h3>Thèmes</h3>
                                        <br/>
                                        {this.props.place.themes.map((theme) => {
                                            return (<div className="col-md-3"><p style={{backgroundColor:theme.color}}>{theme.name} {theme.ponderation}%</p></div>)
                                        })}
                                    </div>
                                    <div className="container-fluid well">
                                        <h3>Biographie/Sources du web</h3>
                                        <p dangerouslySetInnerHTML={{__html: this.props.place.references}}/>
                                    </div>
                                    <div className="container-fluid well">
                                        <h3>Classification</h3>
                                        <div>
                                            <label>Catégories</label>
                                            <br/>
                                            {this.props.place.categories.map((category,key) => {
                                                return (<div className="col-md-3"><p>{category.name}</p></div>)
                                            })}
                                        </div>
                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Sous catégories</label>
                                            <br/>
                                            {this.props.place.sousCategories.map((sc,key) => {
                                                return (<div key={key} className="col-md-3"><p>{sc}</p></div>)
                                            })}
                                        </div>

                                        <br/>
                                        <br/>
                                        <div>
                                            <label>Mots clés</label>
                                            <br/>
                                            {this.props.place.tags.map((tag,key) => {
                                                return (<div key={key} className="col-md-3"><p>{tag.name}</p></div>)
                                            })}
                                        </div>
                                    </div>
                                    {this.props.place.social &&
                                    <div className="container-fluid well">
                                        <h3>Liens externes</h3>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Traverse:</h4> {social(this.props.place.slug).hashtag}
                                            {this.props.hashtag(this.props.place.slug, true)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}>
                                            <h4>Facebook:</h4> {social(place.social.facebook).link}
                                            {this.props.hashtag(place.social.facebook)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Instagram:</h4> {social(place.social.instagram).link}
                                            {this.props.hashtag(place.social.instagram)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Twitter:</h4>  {social(place.social.twitter).link}.
                                            {this.props.hashtag(place.social.twitter)}
                                        </div>
                                        <div className="row" style={{paddingLeft: "19px"}}><h4>
                                            Web:</h4>  {social(place.social.web).link}
                                        </div>
                                    </div>
                                    }

                                    <div className="container-fluid well">
                                        <h3>Plus d'informations</h3>
                                        <p><h4>Information Technique:</h4> {champsNonRenseigne(this.props.place.technical_information)}</p>
                                        <p><h4>Accessibilité:</h4> {champsNonRenseigne(this.props.place.accessibility)}</p>
                                        <p><h4>Programme:</h4> {champsNonRenseigne(this.props.place.schedule)}</p>
                                        <p><h4>Contact:</h4> {champsNonRenseigne(this.props.place.contact)}</p>
                                        <p><h4>Plus D'infos:</h4> {champsNonRenseigne(this.props.place.more_information)}</p>

                                        <h3>Adresse</h3>
                                        <h4>{this.props.place.map ? this.props.place.map.fullAddrStr : "Non renseignée."}</h4>
                                    </div>
                                    <div className="container-fluid well">
                                        <h3>Meta-données</h3>
                                        <p>Date de création : {formatageDeLaDate(this.props.place.created_at)}</p>
                                        <p>Auteur : {place.created_by.profile.name}</p>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default (PlaceDetailed);