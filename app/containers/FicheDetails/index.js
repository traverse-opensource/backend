export {default as EventDetailed} from './EventDetailed.jsx';
export {default as MediaDetailed} from './MediaDetailed.jsx';
export {default as PlaceDetailed} from './PlaceDetailed.jsx';
export {default as ObjectDetailed} from './ObjectDetailed.jsx';
export {default as PersonDetailed} from './PersonDetailed.jsx';