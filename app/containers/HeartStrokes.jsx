import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { createUser } from '../actions/users';
import styles from '../css/components/login';
import GroupSelect from '../components/group/GroupSelect';
import hourGlassSvg from '../images/hourglass.svg';
import * as actions from '../actions/users';
import PlaylistSearchForm from '../components/PlaylistSearchForm.jsx';
import MixinItem from '../components/MixinItem.jsx';

import { RIGHTS } from '../constant';

const cx = classNames.bind(styles);

const icon = (type) => {
    switch (type) {
        case 'People':
            return 'user';
        case 'Events':
            return 'time';
        case 'Media':
            return 'picture';
        case 'Objects':
            return 'tag';
        case 'Places':
            return 'map-marker';
        default:
            return 'list';
    }
};

const isFiche = (mixin) => {
  switch(mixin.__t) {
      case 'People':
      case 'Events':
      case 'Media':
      case 'Objects':
      case 'Places':
        return true;
      default:
        return false;
  }
}

class HeartStrokeContainer extends Component {
    /*
     * This replaces getInitialState. Likewise getDefaultProps and propTypes are just
     * properties on the constructor
     * Read more here: https://facebook.github.io/react/blog/2015/01/27/react-v0.13.0-beta-1.html#es6-classes
     */
    constructor(props) {
      super(props);
      /*
      this.updateUserGroup = this.updateUserGroup.bind(this);
      this.updateUserEntities = this.updateUserEntities.bind(this);
*/
      this.selectToMove = this.selectToMove.bind(this);
      this.addMixin = this.addMixin.bind(this);
      this.search = this.search.bind(this);
      this.deleteMixin = this.deleteMixin.bind(this);
      
      this.state = {
        //avoids null pointer exeption on forEach
        mixins: [],
        searchResult: [],
        selected: {
          initialized: false,
          isFiche: false,
          id: null
        }
      };
    }

    renderHeader() {
        const { user } = this.props;

        return (
            <div className="page-header">
                <h1>Coups de coeur</h1>
            </div>
        );
    }
  
    addMixin(mixin) {
      let me = this;
      //inde would be the generated automatically
      let toAdd = isFiche(mixin) ? {
        fiche_id: mixin._id
      }:{
        playlist_id: mixin._id
      };
      
      fetch('/heartstrokes', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(toAdd)
      })
      .then(response => response.json())
      .then(heartstrokes => {
          me.setState({mixins: heartstrokes});
      });
    }
  
    deleteMixin(order) {
      let me = this;
      
      let toDelete = {order: order};
      
      fetch('/heartstrokes', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'DELETE',
        body: JSON.stringify(toDelete)
      })
      .then(response => response.json())
      .then(heartstrokes => {
          me.setState({mixins: heartstrokes});
      });
    }
  
    search(event) {
      event.preventDefault();
      let query = this.refs.searchForm.getQuery();
      
      let me = this;
      fetch('/heartstrokes/' + query)
        .then(response => response.json())
        .then(mixins => {
            console.log(mixins);
            me.setState({searchResult: mixins});
        });
    }
  
    selectToMove(isFiche, mixin, index) {

      let me = this;
      //once selected again no one should be selected
      if (this.state.selected.initialized) {
        //means already selected to we need to switch here on the second one
        
        let query = {hearts: [
          this.state.selected.isFiche? {
            fiche_id: this.state.selected.id,
            order: index
          }: {
            playlist_id: this.state.selected.id,
            order: index
          },
          isFiche?
          {
            fiche_id: mixin._id,
            order: this.state.selected.order
          }: {
            playlist_id: mixin._id,
            order: this.state.selected.order
          }
        ]};
        
        fetch("/heartstrokes", {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
            method: 'PATCH',                                                            
            body: JSON.stringify( query )
          })
          .then(response => response.json())
          .then(heartstrokes => {
              console.log(heartstrokes);
              me.setState({mixins: heartstrokes, selected: {intialized: false}});
          });
      } else {
        let toSet = {
          id: mixin._id,
          order: index,
          initialized: true,
          isFiche: isFiche
        };
        this.setState({selected: toSet});  
      }
    }
  
    componentDidMount() {
      //find all the heartstrokes list
      let me = this;
      fetch(`/heartstrokes`)
        .then(response => response.json())
        .then(heartstrokes => {
            me.setState({mixins: heartstrokes});
        });
    }
  
    buildListItemToAdd(item, key) {
      return (
        <li className="list-group-item" key={key} onClick={() => {this.addMixin(item)}}>
            <span className={"glyphicon glyphicon-" + icon(item.__t)}/>
            {item.name}
        </li>
      )
    }

    render() {

      const isSuperAdmin = RIGHTS.IS_SUPER_ADMIN(this.props.user);
      
      let alreadyPresentIds = this.state.mixins.map((mixin) => {
        return mixin._id;
      })
      
      const buildSearchList = () => {
        
        let me = this;
        let items = me.state.searchResult
          //don't add what we already have
          .filter((mixin) => {
            return alreadyPresentIds.indexOf(mixin._id) === -1;
          })
          .map((mixin, key) => {
            return this.buildListItemToAdd(mixin, key);
          })
        
        return (
          <div>
            <ul style={{padding: 0, marginTop: "20px"}}>
              {items}
            </ul>
          </div>
        );
      }
      
      const buildContainer = () => {
        let index = 0;
        let targetCol = isSuperAdmin ?
            "col-md-4": "col-md-3";
        const mixins = this.state.mixins.map((mixin, key) => {
          
          const isSelected = this.state.selected.initialized?
              //don't ask me why +1 here, magick -_-
              (index + 1) === this.state.selected.order: false;
          
          console.log('render yolo', this.state.selected);
          
          const onDestroy = () => {
            
          }
          
          const onOpen = () => {
            
          }
          
          return (
            <div className={targetCol} key={key}>
                <MixinItem 
                  mixinItem={mixin}
                  order={++index}
                  onDestroy={onDestroy}
                  onOpen={onOpen}
                  selected={isSelected}
                  onSelected={this.selectToMove}
                  onDeleted={this.deleteMixin}
                  isSuperAdmin={isSuperAdmin}
                />
            </div>
          );
            
        });

        return isSuperAdmin?(
          <div className="row" style={{marginTop: "20px"}}>
            <div className="col-md-3">
              <div className="well">
                  <PlaylistSearchForm search={this.search} ref="searchForm" />
                  {buildSearchList()}
              </div>
            </div>
            <div className="col-md-9">
              <div>{mixins}</div>
            </div>
          </div>
        ):(
          <div className="row" style={{marginTop: "20px"}}>
            <div className="col-md-12">
              <div>{mixins}</div>
            </div>
          </div>
        )
      };

      return (
        <div className={cx('container')}>
          { !this.props.embedded &&
          this.renderHeader() }
          <img className={cx('loading')} alt="loading" src={hourGlassSvg} />
          {buildContainer()}
        </div>
      );
  }
}

HeartStrokeContainer.propTypes = {
    user: PropTypes.object,
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps(state){
    return {
        user: state.user.user,
    }
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps, { createUser })(HeartStrokeContainer);