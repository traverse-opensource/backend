import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { manualLogin ,forgotPassword} from '../actions/users';
import styles from '../css/components/login';
import hourGlassSvg from '../images/hourglass.svg';
import ForgotPassword from '../components/user/ForgotPassword.jsx';
import { Button } from 'react-bootstrap';

const cx = classNames.bind(styles);

class Login extends Component {
    /*
     * This replaces getInitialState. Likewise getDefaultProps and propTypes are just
     * properties on the constructor
     * Read more here: https://facebook.github.io/react/blog/2015/01/27/react-v0.13.0-beta-1.html#es6-classes
     */
    constructor(props) {
        super(props);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.resetPasswordField = this.resetPasswordField.bind(this);
        this.resetPasswordAction = this.resetPasswordAction.bind(this);
        this.state={showForgotInput:false}
    }

    handleOnSubmit(event) {
        event.preventDefault();

        const { manualLogin, user } = this.props;
        const email = ReactDOM.findDOMNode(this.refs.email).value;
        const password = ReactDOM.findDOMNode(this.refs.password).value;
        const name = ReactDOM.findDOMNode(this.refs.name) ? ReactDOM.findDOMNode(this.refs.name).value : null;

        manualLogin({ email, password });
    }


    renderHeader() {
        const { user } = this.props;

        return (
            <div>
                <h1 >Page de connexion</h1>
            </div>
        );
    }
    resetPasswordField(){
        if(this.state.showForgotInput==false)this.setState({showForgotInput:true});
        else this.setState({showForgotInput:false});
    }
    resetPasswordAction(email) {
        this.props.forgotPassword(email);
    }

    render() {
        const { message } = this.props.user;
        const styles = {
          button: {
            fontSize: '16px'
          }
        };
        return (

            <div className={cx('container')}>
                { this.renderHeader() }
                <img className={cx('loading')} alt="loading" src={hourGlassSvg} />
                <div className={cx('email-container')}>
                    <form onSubmit={this.handleOnSubmit}>
                        <input
                            className={cx('input')}
                            type="email"
                            ref="email"
                            placeholder="Email"
                        />
                        <input
                            className={cx('input')}
                            type="password"
                            ref="password"
                            placeholder="Mot de passe"
                        />
                        <p
                            className={cx('message', {
                                'message-show': message && message.length > 0
                            })}>{message}</p>
                        <input
                            className={cx('button')}
                            type="submit"
                            style={{fontSize: '16px'}}
                            value="Se connecter" />
                    </form>
                    <br/><br/>
                    
                    <Button 
                      style={styles.button}
                      bsStyle="primary" 
                      bsSize="large"
                      onClick={this.resetPasswordField}
                      >
                        Mot de passe oublié?
                    </Button>
                    {this.state.showForgotInput ? <ForgotPassword forgotPassword={this.resetPasswordAction}/> : null}
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    user: PropTypes.object,
    manualLogin: PropTypes.func.isRequired
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps({user}) {
    return {
        user
    };
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps, { manualLogin, forgotPassword })(Login);

