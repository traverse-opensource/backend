import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import classNames from 'classnames/bind';
import { dismissMessage } from '../actions/messages';
import styles from '../css/components/message';

const cx = classNames.bind(styles);
let dismissTimeout = (dismissMessage) => {
    setTimeout(function () {
        dismissMessage();
    }, 2000);
};
const Message = ({message, type, dismissMessage}) => (
    <div className="container">
        {dismissTimeout(dismissMessage)}
        {type ==='ERROR' &&
        <div className={"affix " + cx('message', {
            show: message && message.length > 0,
            error: type === 'ERROR'
        })}>{message}
        </div>
        }
        {type === 'SUCCESS' &&
        <div className={"affix " + cx('message', {
            show: message && message.length > 0,
            success: type === 'SUCCESS'
          })}
          onClick={dismissMessage}>{message}
        </div>
        }
    </div>
);

Message.propTypes = {
    message: PropTypes.string,
    type: PropTypes.string,
    dismissMessage: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {...state.message};
}

export default connect(mapStateToProps, { dismissMessage })(Message);
