import React, { PropTypes, Component } from 'react';
import MTComponent from '../components/MTComponent';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { RIGHTS } from '../constant';
import { logOut } from 'actions/users';
import { browserHistory } from 'react-router';

import classNames from 'classnames/bind';
import styles from 'css/components/navigation';
import { MenuItem } from 'react-bootstrap';

const cx = classNames.bind(styles);

class Navigation extends MTComponent {//= ({ user, logOut }) => {

  constructor(props){
    super(props);

    this.setAutoBindMethods([
      'showAdminItems',
      'hideAdminItems'
    ])

    this.state = {
      adminOpts: false,
      showProgress: false
    };
  }

  showAdminItems(){
    this.setState({adminOpts: true});
  }

  hideAdminItems(){
    this.setState({adminOpts: false});
  }

  render() {

    const { user } = this.props,
          { logOut } = this.props;

    const onLogout = () => {
        logOut().then(()=> {
            return browserHistory.push('/');
        });
    };

    let targetUser = user.user._id ?
        user.user: null;

    return (
        <nav className="navbar navbar-default navbar-fixed-top" role="navigation">
            <div className="container">
                { user.authenticated ? (
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                            <li>
                                <Link to="/"
                                      className={cx('item', 'logo')}
                                      activeClassName={cx('active')}> <img src={'/public/Traverse-logo-small.png'} alt="Logo traverse"  height="20"/></Link>
                            </li>
                            <li>  <Link className={cx('item')} to="/fichesView"><span>Fiches</span></Link>
                            </li>
                            <li> <Link className={cx('item')} to="/playlistsView"><span>Playlists</span></Link>
                            </li>
                            <li> <Link className={cx('item')} to="/heartsView"><span>Coups de coeur</span></Link>
                            </li>

                            <li><Link className={cx('item')} to="/profile">Profil</Link></li>
                            <li><Link to="/utilisateurs" className={cx('item')} activeClassName={cx('active')}>Utilisateurs</Link></li>
                            { RIGHTS.HAS_ENOUGH_PRIVILEGES(targetUser, null, targetUser ? targetUser.group: null) ?
                              <li onMouseEnter = { this.showAdminItems } onMouseLeave = { this.hideAdminItems }>
                                <Link to="#" className={cx('item')} activeClassName={cx('active')}>Admin</Link>
                                {
                                  this.state.adminOpts &&
                                    <ol style={{position: 'absolute', top: '100%', display: 'block', borderRadius: '4px'}} className="dropdown-menu">
                                      <li><Link to="/admin/manage-groups" className={cx('item')} activeClassName={cx('active')} onClick={this.hideAdminItems}>Gérer les groupes</Link></li>
                                      <li><Link to="/admin/manage-users" className={cx('item')} activeClassName={cx('active')} onClick={this.hideAdminItems}>Gérer les utilisateurs</Link></li>
                                      {
                                        RIGHTS.IS_SUPER_ADMIN(targetUser) ?
                                          <li><Link to="/admin/stats" className={cx('item')} activeClassName={cx('active')} onClick={this.hideAdminItems}>Statistiques</Link></li>:
                                          ""
                                      }
                                      <li><Link to="/admin/manage-feeds" className={cx('item')} activeClassName={cx('active')} onClick={this.hideAdminItems}>Modération</Link></li>
                                    </ol>
                                }
                              </li>: ""
                            }
                            <li> <a className={cx('item')} href="https://goo.gl/forms/5ctpJleSa9vr64EY2" target="_blank"><span>Retours utilisateurs</span></a>
                            </li>
                            <li>
                                <Link onClick={onLogout} className={cx('item')} to="/">Se déconnecter</Link>
                            </li>
                        </ul>
                    </div>) : <div>
                    <Link to="/"
                          className={cx('item', 'logo')}
                          activeClassName={cx('active')}>
                        <img src={'/public/Traverse-logo-small.png'} alt="Logo traverse"  height="20"/></Link>
                </div>
                }
            </div>
        </nav>

    );
  }
};

Navigation.propTypes = {
    user: PropTypes.object,
    logOut: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps, { logOut })(Navigation);
