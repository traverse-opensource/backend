import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import styles from 'css/components/about';
import PlaylistSection from '../components/playlist/PlaylistSection.jsx';
import * as actions from '../actions/playlists';
import { browserHistory } from 'react-router';

const cx = classNames.bind(styles);

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Playlist extends Component {

    redirectToPlaylist(id) {
        return browserHistory.push('/playlistsView/playlist/' + id);
    }

   render() {
     
        const { playlist , removePlaylist } = this.props;
     
        return (
            <div>
              {this.props.children}
                <div className="page-header">
                    <h1>Liste des Playlistes</h1>
                </div>
                <PlaylistSection 
                  playlist={playlist}
                  onDestroy={removePlaylist}
                  onOpen={this.redirectToPlaylist}
                  sessionUser={this.props.user}
                />
            </div>
        );
    };
}

function mapStateToProps(state) {
    return{
        playlist : state.playlist,
        user: state.user.user
    }
}
export default connect(mapStateToProps, actions)(Playlist);
