import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/users';

import classNames from 'classnames/bind';
import styles from 'css/components/profile';

import AccountSetting from '../components/user/AccountSetting.jsx';
import DropImage from '../components/fiche/DropImage';
import { List, ListItem } from 'material-ui/List';
import { Card } from 'material-ui/Card';

const cx = classNames.bind(styles);
/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Profile extends Component{
    constructor(props){
      super(props);
      this.state= {
        overview: true,
        setting: false,
        creations: null
      }
      
      this.updateUser = this.updateUser.bind(this);
    }
    toggleOverview(){
      this.setState({
        overview: true,
        setting: false
      })
    }
    toggleSetting(){
      this.setState({
        overview:false,
        setting: true
      })
    }
  
    seekDataIfNeeded(user = null) {
      
      let seekingUser = user != null ? user: this.props.user;
      
      if (seekingUser) {
        let me = this;
        me.props.fetchUserCreationCount(seekingUser._id).
          then((data) => {
            return data;
          }).
          then((res) => {
            me.setState({creations: res.items});
          });
      }
    }
  
    updateUser(userId, params) {
      //shall come from the actions
      this.props.updateUser(userId, params, true);
    }
  
    componentWillReceiveProps(newProps) {
      this.seekDataIfNeeded(newProps.user);
    }
  
    componentDidMount() {
      this.seekDataIfNeeded(this.props.user);
    }
  
    renderDropzoneInput(field) {
      
      let uploadedFile = {
        path: "uploaded_images/user_profiles/default_profile.jpg"
      };
        
      if (this.props.user) {
        if (this.props.user.profile && this.props.user.profile.picture) {
          uploadedFile.path = this.props.user.profile.picture;
        }
      }
        
      return(
          <DropImage
            userId={this.props.user._id}
            field={field}
            uploadedFile={uploadedFile}
            submitImage={this.props.submitImage}
            setStateCover={this.setCoverState}/>
      );
    }
  
    render() {
        const {profile, email} = this.props.user;
      
        const neant = 'Néant';
      
        let publishedFiche = neant,
            draftedFiche = neant,
            publishedPlaylist = neant,
            draftedPlaylist = neant;
      
        if (this.state.creations) {
          publishedFiche = this.state.creations.fiches.published.toString();
          draftedFiche = this.state.creations.fiches.draft.toString();
          publishedPlaylist = this.state.creations.playlists.published.toString();
          draftedPlaylist = this.state.creations.playlists.draft.toString();
        }
      
      //<img src="http://images.gadmin.st.s3.amazonaws.com/n30096/images/keyvisuals/img_9879-2.jpg" className="img-responsive" alt=""/>
      
      const dropZone = this.renderDropzoneInput();
      const entityZone = this.props.user.entities ?
          <ul>
            {this.props.user.entities.map((entity) => {
              return <li>{entity.name}</li>
            })}
          </ul>:
      <div style={{color: "red"}}>Aucun groupe assigné</div>;
      
        return (
            <div>
                <h2>Page profil utilisateur</h2>
                {this.props.user.profile ?
                    <div>
                        <div className="container">
                            <div className="row profile">
                                <div className="col-md-3">
                                    <div className={cx('profile-sidebar')}>

                                        <div className={cx('profile-userpic')}>
                                          {dropZone}
                                        </div>

                                        <div className={cx('profile-usertitle')}>
                                            <div className={cx('profile-usertitle-name')}>
                                                {profile.name}
                                            </div>
                                            <div className={cx('profile-usertitle-job')}>
                                                <div className={cx('profile-usertitle-job-item')}>
                                                  {this.props.user.group && this.props.user.group.description}
                                                </div>
                                                <div style={{textTransform: "none", textAlign: "right"}} className={cx('profile-usertitle-job-item')}>
                                                  {email}
                                                </div>
                                                <div className={cx('profile-usertitle-job-item')}>
                                                  Membre de
                                                  {entityZone}
                                                </div>
                                            </div>
                                        </div>

                                        <div className={cx("profile-usermenu")}>
                                            <ul className="nav">
                                                <li className="active">
                                                    <a onClick={() => this.toggleOverview()}>
                                                        <i className="glyphicon glyphicon-home"/>
                                                        Vue d'ensemble </a>
                                                </li>
                                                <li>
                                                    <a onClick={() => this.toggleSetting()}>
                                                        <i className="glyphicon glyphicon-user"/>
                                                        Paramètres du compte </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-md-9" >
                                    <div className={cx("profile-content")}>
                                        {this.state.overview &&
                                          <Card>
                                            <List>
                                              <ListItem primaryText="Fiches créées:" secondaryText={publishedFiche}/>
                                              <ListItem primaryText="Fiches brouillons:" secondaryText={draftedFiche}/>
                                              <ListItem primaryText="Playlist créées:" secondaryText={publishedPlaylist}/>
                                              <ListItem primaryText="Playlist brouillons:" secondaryText={draftedPlaylist}/>
                                            </List>
                                          </Card>
                                        }
                                        {this.state.setting &&
                                          <Card>
                                            <AccountSetting
                                              updateUserFromProfile={this.updateUser}/>
                                        </Card>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    </div>:
                    <div> </div>}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        user: state.user.user,
    }
}
export default connect(mapStateToProps, actions)(Profile);
