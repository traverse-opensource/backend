import React, { Component, PropTypes } from 'react';
import axios from 'axios';
import classNames from 'classnames/bind';
import ReactDOM from 'react-dom';
import styles from '../css/components/login';
import {browserHistory} from 'react-router'
import { connect } from 'react-redux';
import {changePassword} from '../actions/users';
const cx = classNames.bind(styles);


class ResetPassword extends Component {
    constructor(props){
        super(props);
        this.handleOnSubmit= this.handleOnSubmit.bind(this);
        this.state = {res : null, user:null}
    }

    componentDidMount(){
        axios.get('/reset/'+this.props.params.token)
            .then((res) =>
            {
                console.log(res);
                let user =res.data.user;
                this.setState({user});
            })
    }
    handleOnSubmit(event){
        event.preventDefault();
        const newPassword = ReactDOM.findDOMNode(this.refs.password).value;
        const confirmPass = ReactDOM.findDOMNode(this.refs.confirmPass).value;
        this.props.changePassword(newPassword, confirmPass, this.props.params.token);

    }

    render(){
        const {res} = this.state;
        return(
            <div className={cx('container')} style={{marginTop: '70px'}}>
                <form onSubmit={this.handleOnSubmit}>
                    <input className={cx('input')}
                           type="password"
                           ref="password"
                           placeholder="Nouveau mot de passe"/>
                    <input
                        className={cx('input')}
                        type="password"
                        ref="confirmPass"
                        placeholder="Confirmez le mot de passe"/>
                    <input
                        className={cx('button')}
                        style={{fontSize: "16px"}}
                        type="submit"
                        value="Réinitialiser"/>
                </form>
            </div>
        )
    }

}

export default connect(null, { changePassword })(ResetPassword);