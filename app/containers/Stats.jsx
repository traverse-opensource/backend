import React, {Component} from 'react';
import classNames from 'classnames/bind';
import MTComponent from '../components/MTComponent';
import GroupSelect from '../components/group/GroupSelect';
import styles from 'css/components/about';
import * as actions from '../actions/users';
import { connect } from 'react-redux';
import { Avatar, List, ListItem, IconButton, SelectField, MenuItem, Paper, FlatButton } from 'material-ui';
import {Card, CardTitle, CardActions, CardHeader, CardText} from 'material-ui/Card';


import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';

import ActionDownload from 'material-ui/svg-icons/action/get-app';
import ActionVerifiedUser from 'material-ui/svg-icons/action/verified-user';
import { green500, blue500 } from 'material-ui/styles/colors';
import UserCreation from './UserCreation';
import SelectEntities from '../components/group/SelectEntities';
import Alert from '../components/Alert.jsx';

import axios from 'axios';

import { RIGHTS, permission } from '../constant';

const cx = classNames.bind(styles);

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Users extends MTComponent {
  constructor(props) {
    super(props);
    this.setAutoBindMethods([
      "handleDownloadCSV",
      "handleRowSelection",
      "getActiveUsers",
      "getDeactivatedUsers"
    ]);
    let values = [];
    this.props.users.forEach((user) => {
      if (user.group)
        values.push(user.group.description);
    });
    this.state = {
      currentUsers: this.props.users,
      usersToDownload: [],
      values: values,
      alert: false,
      statsData: {
        fiches: {
          draft: "0",
          published: "0"
        },
        playlists: {
          draft: "0",
          published: "0"
        }
      }
    };
  }
  
  componentDidMount() {
    let stat = this.state.statsData;
    let data;
    let me = this;
    axios.get('/fiches/stats').
    then((response) => {
      data = response.data;
      
      data.forEach((item) => {
        if (item._id === 0) {
          stat.fiches.draft = item.count.toString();
        }
        if (item._id === 1) {
          stat.fiches.published = item.count.toString();
        }
      });
      
      axios.get('/playlists/stats')
      .then((response) => {
        data = response.data;
        
        data.forEach((item) => {
          if (item._id === 0) {
            stat.playlists.draft = item.count.toString();
          }
          if (item._id === 1) {
            stat.playlists.published = item.count.toString();
          }
        });
        
        me.setState({statsData: stat});
      });
    });
    
    
  }
  
  handleDownloadCSV() {
    //console.log("Stats.jsx @handleDownloadCSV> ", this.state.usersToDownload);
    
    axios.post('/users/download', {
      users: this.state.usersToDownload
    })
    .then(function (response) {
      var element = document.createElement('a');
      element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(response.data));
      element.setAttribute('download', "traverse-users.csv");

      element.style.display = 'none';
      document.body.appendChild(element);

      element.click();

      document.body.removeChild(element);
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  
  getActiveUsers() {
    return this.state.currentUsers.filter((user) =>{
      let toReturn = user.group.title !== permission.DEACTIVATED && user.group.title !== permission.NOMAD;
      return toReturn;
    });
  }
  
  getDeactivatedUsers() {
    this.state.currentUsers.filter((user) =>{
      return user.group.title === permission.DEACTIVATED;
    });
  }
  
  //picked from the documentation http://www.material-ui.com/#/components/table
  handleRowSelection(selectedRows) {
    let activeUsers = this.getActiveUsers();
    let toSet = [];
    
    if (selectedRows !== "all") {
      selectedRows.forEach((rowIndex) => {
        toSet.push(activeUsers[rowIndex]);
      });
    }else {
      toSet = activeUsers;
    }
    
    let toSave = toSet.map((user) => {
      return {
        id: user._id,
        name: user.profile.name,
        email: user.email
      };
    });
    
    this.setState({
      usersToDownload: toSave
    });
  }

  render() {
    const loggedUser = this.props.user;
    let close = () => this.setState({alert : false});
    let userFiltrated = this.getActiveUsers();
    
    const emails = userFiltrated.map((user) => {
      //do not need to test here since it's already filtered
      return (
        <TableRow>
          <TableRowColumn>{user._id}</TableRowColumn>
          <TableRowColumn>{user.profile.name}</TableRowColumn>
          <TableRowColumn>{user.email}</TableRowColumn>
        </TableRow>
      );
    });
    
    const emailsContainer = 
      <Table
        selectable={true}
        multiSelectable={true}
        onRowSelection={this.handleRowSelection}
        >
        <TableHeader
          displaySelectAll={true}
          enableSelectAll={true}
        >
          <TableRow>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Nom</TableHeaderColumn>
            <TableHeaderColumn>Email</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
          displayRowCheckbox={true}
          deselectOnClickaway={false}
          showRowHover={true}
        >
          {emails}
        </TableBody>
      </Table>
    
    const stat = this.state.statsData;
          
    const fichesStats = 
      <List>
        <ListItem primaryText="Fiches créées:" secondaryText={stat.fiches.published}/>
        <ListItem primaryText="Fiches brouillons:" secondaryText={stat.fiches.draft}/>
      </List>;
    
    const playlistsStats = 
      <List>
        <ListItem primaryText="Playlists créées:" secondaryText={stat.playlists.published}/>
        <ListItem primaryText="Playlists brouillons:" secondaryText={stat.playlists.draft}/>
      </List>;
    
    return (
      <div className={cx('about')}>
        <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
          <CardTitle title={"Fiches"}/>
            {fichesStats}
        </Card>
        <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
          <CardTitle title={"Playlists"}/>
            {playlistsStats}
        </Card>
        <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
            <CardTitle title={"Utilisateurs"} children={
              <FlatButton 
                label="Télécharger les utilisateurs sélectionnés" 
                labelPosition="after"
                onClick={this.handleDownloadCSV}
                icon={
                  <ActionDownload color={blue500}/>
                }
              />
            }/>
            {emailsContainer}
        </Card>
      </div>
    );
  };
}
function mapStateToProps(state){
  return {
    users: state.user.users,
    user: state.user.user
  }
}

export default connect(mapStateToProps, actions)(Users);
