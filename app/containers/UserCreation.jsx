import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { createUser } from '../actions/users';
import styles from '../css/components/login';
import GroupSelect from '../components/group/GroupSelect';
import hourGlassSvg from '../images/hourglass.svg';
import * as actions from '../actions/users';
import SelectEntities from '../components/group/SelectEntities';

const cx = classNames.bind(styles);

class UserCreation extends Component {
    /*
     * This replaces getInitialState. Likewise getDefaultProps and propTypes are just
     * properties on the constructor
     * Read more here: https://facebook.github.io/react/blog/2015/01/27/react-v0.13.0-beta-1.html#es6-classes
     */
    constructor(props) {
        super(props);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.updateUserGroup = this.updateUserGroup.bind(this);
        this.updateUserEntities = this.updateUserEntities.bind(this);

        this.state = {
            groupId: null,
            entitiesList: null
        }
    }

    handleOnSubmit(event) {
        event.preventDefault();

        let entitiesValue = ReactDOM.findDOMNode(this.refs.entitiesList).value.split(",");
      
        if (entitiesValue.length === 1 && entitiesValue[0] === "") {
          entitiesValue = [];
        }
      
        const { createUser, user } = this.props,
            email = ReactDOM.findDOMNode(this.refs.email).value,
            password = ReactDOM.findDOMNode(this.refs.password).value,
            name = ReactDOM.findDOMNode(this.refs.name) ? ReactDOM.findDOMNode(this.refs.name).value : null,
            creator = ReactDOM.findDOMNode(this.refs.creator).value,
            groupId = ReactDOM.findDOMNode(this.refs.group).value,
            //need to transform this string into an array
            entitiesList = entitiesValue;

        createUser({ email, password, name, creator, groupId, entitiesList });
    }

    renderHeader() {
        const { user } = this.props;

        return (
            <div>
                <h1>Création d'un utilisateur</h1>
            </div>
        );
    }

    //these shall match the function in Users.jsx to be able to reuse it, event if we don't care about the userid here
    updateUserGroup(userId, groupId) {
      this.setState({
        groupId: groupId
      });
    }
  
    updateUserEntities(userId, entities) {
      this.setState({
        entitiesList: entities
      });
    }

    render() {
        const { message } = this.props.user;
        const {groups} = this.props;
        let roles = this.props.groups.filter((group)=> {
            return group.title? (group.title!="super_admin") : "";
        });
        const buildForm = () => {
            return (
                <form onSubmit={this.handleOnSubmit}>
                    <input
                        className={cx("input")}
                        type="name"
                        ref="name"
                        placeholder="Nom"/>
                    <input
                        className={cx('input')}
                        type="email"
                        ref="email"
                        placeholder="Email"
                    />
                    <input
                        className={cx('input')}
                        type="password"
                        ref="password"
                        placeholder="Mot de passe"
                    />
                    <input
                        className={cx('input')}
                        type="hidden"
                        ref="creator"
                        value={this.props.user._id}
                    />
                    <p
                        className={cx('message', {
                            'message-show': message && message.length > 0
                        })}>{message}</p>
                    <label>Choix du Rôle</label>
                    <input
                        className={cx('input')}
                        type="hidden"
                        ref="group"
                        value={this.state.groupId}
                    />
                    <GroupSelect
                        name=""
                        groups={roles}
                        selectedGroupId={this.state.groupId}
                        updateUserGroup={this.updateUserGroup}
                      />
                    <input
                        className={cx('input')}
                        type="hidden"
                        ref="entitiesList"
                        value={this.state.entitiesList}
                    />
                    <label>Choix des groupes</label>
                    <SelectEntities
                        entities={this.props.entities}
                        updateUserEntities={this.updateUserEntities}
                      />

                    <input
                        className={cx('button')}
                        type="submit"
                        style={{fontSize: '16px', marginTop: "16px"}}
                        value="Créer l'utilisateur" />
                </form>
            );
        };

        const buildContainer = () => {
            if (!this.props.embedded) {
                return (
                    <div className={cx('email-container')}>
                        {buildForm()}
                    </div>
                );
            }

            return (
                <div style={{margin: "16px", paddingRight: "16px"}}>
                    {buildForm()}
                </div>
            );
        };

        return (
            <div className={cx('container')}>
                { !this.props.embedded &&
                this.renderHeader() }
                <img className={cx('loading')} alt="loading" src={hourGlassSvg} />
                {buildContainer()}
            </div>
        );
    }
}

UserCreation.propTypes = {
    user: PropTypes.object,
};

// Function passed in to `connect` to subscribe to Redux store updates.
// Any time it updates, mapStateToProps is called.
function mapStateToProps(state){
    return {
        user: state.user.user,
    }
}

// Connects React component to the redux store
// It does not modify the component class passed to it
// Instead, it returns a new, connected component class, for you to use.
export default connect(mapStateToProps, { createUser })(UserCreation);