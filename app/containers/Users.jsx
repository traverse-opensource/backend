import React, {Component} from 'react';
import classNames from 'classnames/bind';
import MTComponent from '../components/MTComponent';
import GroupSelect from '../components/group/GroupSelect';
import styles from 'css/components/about';
import * as actions from '../actions/users';
import { connect } from 'react-redux';
import { Avatar, List, IconButton, SelectField, MenuItem, Paper } from 'material-ui';
import {Card, CardTitle, CardActions, CardHeader, CardText} from 'material-ui/Card';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ActionVerifiedUser from 'material-ui/svg-icons/action/verified-user';
import { green500, red500 } from 'material-ui/styles/colors';
import UserCreation from './UserCreation';
import SelectEntities from '../components/group/SelectEntities';
import Alert from '../components/Alert.jsx';

import axios from 'axios';

import { RIGHTS, permission } from '../constant';

const cx = classNames.bind(styles);

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
class Users extends MTComponent {
  constructor(props) {
    super(props);
    this.setAutoBindMethods([
      'handleRemove',
      'handleActivateUser',
      'updateUserGroup',
      'updateUserEntities'
    ]);
    let values = [];
    this.props.users.forEach((user) => {
      if (user.group)
        values.push(user.group.description);
    });
    this.state = {
      currentUsers: this.props.users,
      groups: [],
      entities: [],
      values: values,
      alert: false,
      deletedUsers : [],
      activeUsers : [],
      nomadUsers: []
    };
  }
  /*
   * ComponentWillReceiveProps will be triggered each time the component receive new props
   * Check if the props is changed because of the add of a new user and update the state if so
   */
  componentWillReceiveProps(newProps){
    let values = [];
    newProps.users.forEach((user) => {
      if (user.group)
        values.push(user.group.description)
    });
    
    let activeUsers = [],
        deletedUsers = [],
        nomadUsers = [];
    
    newProps.users.forEach((user) => {
      if (user.group.title === permission.DEACTIVATED) {
        deletedUsers.push(user);
      }else if (user.group.title === permission.NOMAD) {
        nomadUsers.push(user);
      }else {
        activeUsers.push(user);
      }
    })

    this.setState({
      currentUsers: newProps.users,
      values: values,
      activeUsers,
      deletedUsers,
      nomadUsers
    });
  }
  componentDidMount() {
    let me = this;
    if (this.state.groups.length === 0) {
      axios.get('/groups')
          .then( (response) => {
            me.setState({groups: response.data}, () => {
              axios.get('/entities')
                .then((response) =>{
                  me.setState({entities: response.data});
                });
            });
          });
    }
  }
  handleRemove(userToRemove,loggedUser){
    
    this.props.deleteUser(userToRemove._id,loggedUser._id).then((response)=> {
      let updatedUser = response.user;
      
      let activeUsers = this.state.activeUsers.filter((user) => {
        return user._id !== updatedUser._id;
      });

      let deletedUsers = this.state.deletedUsers;
      deletedUsers.push(updatedUser);  
      
      this.setState({activeUsers, deletedUsers});
    })
  }
  handleActivateUser(userToActivate,loggedUser) {
    
    this.props.activateUser(userToActivate._id, loggedUser._id).then((response) => {
      let updatedUser = response.user;
      
      let deletedUsers = this.state.deletedUsers.filter((user) => {
        return user._id !== updatedUser._id;
      });

      let activeUsers = this.state.activeUsers;
      activeUsers.push(updatedUser);
      
      this.setState({activeUsers, deletedUsers})
    })
  }

  updateUserGroup(userId, groupId){
    this.props.updateUser(userId, {group: groupId});
  }
  //from here shall update the selected user entities
  updateUserEntities(userId, entities) {
    let entitiesValue = entities.split(",");
    if (entitiesValue.length === 1 && entitiesValue[0] === "") {
      entitiesValue = [];
    }
    this.props.updateUser(userId, {entities: entitiesValue}, this.props.user._id === userId);
  }

  createGroupHolder(user) {
    let genKey = "user-group-select" + user._id,
        loggedUser = this.props.user;
    let groupsForAdmin=this.state.groups;
    if(RIGHTS.IS_SUPER_ADMIN(user) || (RIGHTS.IS_ADMIN(loggedUser) && RIGHTS.IS_ADMIN(user)) ){
      return <div>{user.group && user.group.description}</div>
    }
    if( RIGHTS.IS_ADMIN(loggedUser)){
      groupsForAdmin = this.state.groups.filter((group) => {
        return group.title != permission.SUPER_ADMIN;
      });
    }
    return RIGHTS.HAS_ENOUGH_PRIVILEGES(loggedUser, null, loggedUser.group)?
        <GroupSelect
            groups={groupsForAdmin}
            targetUser={user}
            updateUserGroup={this.updateUserGroup}/>:
        <div>{user.group && user.group.description}</div>;
  }

  createEntitiesHolder(user, filteredEntities) {
    let genKey = "user-entities-select" + user._id,
        loggedUser = this.props.user;

    let entities = user.entities ?
        user.entities.map((entity) => entity._id).join():
        [];
    if(RIGHTS.IS_SUPER_ADMIN(user) || (RIGHTS.IS_ADMIN(loggedUser) && RIGHTS.IS_ADMIN(user))){
      return <div>Groupes : {user.entities ? user.entities.map((entity) => entity.name + " "): ""}</div>
    }
    return RIGHTS.HAS_ENOUGH_PRIVILEGES(loggedUser, null, loggedUser.group)?
        <div>
          <SelectEntities
              userId={user._id}
              userGroup={user.group.title}
              value={entities}
              entities={this.state.entities}
              adminEntities={filteredEntities}
              updateUserEntities={this.updateUserEntities}
          />
        </div>
        :
        <div>Groupes : {user.entities ? user.entities.map((entity) => entity.name + " "):""}</div>
  }

  createAdminZone(user) {
    let loggedUser = this.props.user;
    if(RIGHTS.IS_SUPER_ADMIN(user)){
      return ""
    }
    
    return RIGHTS.IS_SUPER_ADMIN(loggedUser)?
        user.group.title != permission.DEACTIVATED?
            <IconButton className="disabled" tooltip="Supprimer cet utilisateur" style={{float: "right"}}>
              <ActionDelete onClick={() => this.handleRemove(user, loggedUser)} color={red500}/>
            </IconButton>
            :
            <IconButton className="verified user" tooltip="Activer cet utilisateur" style={{float: "right"}}>
              <ActionVerifiedUser onClick={() => this.handleActivateUser(user, loggedUser)} color={green500}/>
            </IconButton>:
        ""
  }

  render() {
    const loggedUser = this.props.user;
    let close = () => this.setState({alert : false});
    let userFiltrated = this.state.currentUsers.filter((user) =>{
      return user.group.title!=permission.DEACTIVATED;
    });
    let userDeleted = this.props.users.filter((user) => {
      return user.group.title ===permission.DEACTIVATED;
    });

    let isGroupAdmin=(entity)=>{
      return RIGHTS.IS_SUPER_ADMIN(loggedUser)||RIGHTS.IS_GROUP_ADMIN(loggedUser, entity._id);
    };
    let filteredEntities = this.state.entities.filter(isGroupAdmin);
    let children = (user, isActive = true ) => {
      let cols = RIGHTS.IS_SUPER_ADMIN(loggedUser)?
        ["col-md-11", "col-md-1"]:["col-md-12", "col-md-12"];
      
      if (!isActive) {
        cols = ["col-md-12", "col-md-12"];
      }
      
      return (
          <div style={{position: "inherit"}}>
            {isActive &&
            <div className={cols[0]}>
              <div style={{marginRight: "16px"}}>
                {this.createGroupHolder(user)}
              </div>
              <div style={{position: "relative", marginRight: "16px"}}>
                {this.createEntitiesHolder(user, filteredEntities)}
              </div>
            </div>
            }
            <div className={cols[1]}>
              {this.createAdminZone(user)}
            </div>
          </div>

      )};
    
    const getUserPhotoUrl = (user) => {
      return user.profile.picture.indexOf('http') > -1 ? user.profile.picture: "/" + user.profile.picture;
    };
    
    const userList = this.state.activeUsers.map((user) => {
      let avatar = user.profile.picture ?
          <Avatar src={getUserPhotoUrl(user)} key={"no-photo-" + user._id}/>:
          <Avatar size={30} key={'photo-' + user._id}>
            {user.profile.name.substring(0, 2).toUpperCase()}
          </Avatar>;

      return (
          <Paper rounded={true} style={{padding: "16px", margin: "8px"}}>
            <div key={'list-holder-' + user._id} style={{width: "100%", display: "inline-block"}}>
              <div style={{display: "inline-block"}}>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{avatar}</div>
                </div>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{user.profile.name}</div>
                  <div>{user.email}</div>
                </div>
              </div>
              <div style={{width: "100%", position: "relative"}}>
                {children(user)}
              </div>
            </div>
          </Paper>
      );
    });
    
    const nomadUserList = this.state.nomadUsers.map((user) => {
      let avatar = user.profile.picture ?
          <Avatar src={getUserPhotoUrl(user)} key={"no-photo-" + user._id}/>:
          <Avatar size={30} key={'photo-' + user._id}>
            {user.profile.name.substring(0, 2).toUpperCase()}
          </Avatar>;
      
      return (
          <Paper rounded={true} style={{padding: "16px", margin: "8px"}}>
            <div key={'list-holder-' + user._id} style={{width: "100%", display: "inline-block"}}>
              <div style={{display: "inline-block"}}>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{avatar}</div>
                </div>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{user.profile.name}</div>
                  <div>{user.email}</div>
                </div>
              </div>
              <div style={{width: "100%", position: "relative"}}>
                {children(user, false)}
              </div>
            </div>
          </Paper>
      );
    });
    
    const deletedUserList = this.state.deletedUsers.map((user) => {
      let avatar = user.profile.picture ?
          <Avatar src={getUserPhotoUrl(user)} key={"no-photo-" + user._id}/>:
          <Avatar size={30} key={'photo-' + user._id}>
            {user.profile.name.substring(0, 2).toUpperCase()}
          </Avatar>;

      return (
          <Paper rounded={true} style={{padding: "16px", margin: "8px"}}>
            <div key={'list-holder-' + user._id} style={{width: "100%", display: "inline-block"}}>
              <div style={{display: "inline-block"}}>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{avatar}</div>
                </div>
                <div style={{float: "left", marginRight: "16px"}}>
                  <div>{user.profile.name}</div>
                  <div>{user.email}</div>
                </div>
              </div>
              <div style={{width: "100%", position: "relative"}}>
                {children(user, false)}
              </div>
            </div>
          </Paper>
      );
    });
    return (
        <div className={cx('about')}>
          <List>
            {
              RIGHTS.HAS_ENOUGH_PRIVILEGES(loggedUser, null, loggedUser.group)?
                  <Card>
                    <CardTitle title="Créer un utilisateur"/>
                    <UserCreation
                        embedded="1"
                        groups={this.state.groups}
                        entities={filteredEntities}/>
                  </Card>: ""
            }
            <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
              <CardTitle title={RIGHTS.HAS_ENOUGH_PRIVILEGES(loggedUser, null, loggedUser.group) ? "Gérer les utilisateurs": "Liste des Utilisateurs"}/>
              {userList}
            </Card>
            {
              RIGHTS.HAS_ENOUGH_PRIVILEGES(loggedUser, null, loggedUser.group)?
              <div style={{margin:0, padding: 0}}>
                <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
                  <CardTitle title={"Utilisateurs nomades"}/>
                  {nomadUserList}
                </Card>
                <Card style={{marginTop: "16px", width: "100%", paddingBottom: "2px"}}>
                  <CardTitle title={"Utilisateurs désactivés"} titleColor="RED"/>
                  {deletedUserList}
                </Card>
              </div>: ""
            }
          </List>
        </div>
    );
  };
}
function mapStateToProps(state){
  return {
    users: state.user.users,
    user: state.user.user
  }
}

export default connect(mapStateToProps, actions)(Users);
