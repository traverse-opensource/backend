import React, {Component} from "react";
import {Field, reduxForm} from "redux-form";
import {connect} from "react-redux";
import * as actions from '../../actions/fiches.js';
import RaisedButton from "material-ui/RaisedButton";
/*Component*/
import DropImage from '../../components/fiche/DropImage';
import TaxonomiesForm from '../../components/fiche/TaxonomiesForm.jsx';
import MoreInfos from '../../components/fiche/MoreInfos';
import {Site, Person, Objet, Media, Event } from '../../components/fiche/formulaires';
import {handleInitialize,editSubmit,renderWYSIWYGRef} from '../../components/fiche/FormUtils.jsx'
import Social from '../../components/fiche/Social.jsx';
import RelatedFiche from '../../components/fiche/RelatedFiche';
import styles from '../../css/components/login';

import Themes from '../../components/fiche/Themes.jsx';

import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

const form = reduxForm({
  form: 'editForm',
});

const PAGE_STATUS = {
  LOADING: 0,
  ERROR: -1,
  SUCCESS: 1
};

const ACTIONS_HASH = {
  USER: 0,
  FICHE: 1
};

class EditFiche extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
          deleteFile: false,
          status: 0,
          /*Récupération du type de la fiche grâce au pathname*/
          ficheType: this.props.route.path.match("(.*)/")[1],
          debugThemes: [],
          currentFiche: null,
          pageStatus: PAGE_STATUS.LOADING,
        };

        this.setCoverState = this.setCoverState.bind(this);
        this.retrieveFiche = this.retrieveFiche.bind(this);
        this.addRelation = this.addRelation.bind(this);
        this.removeRelation = this.removeRelation.bind(this);
    }
    setCoverState(remotePath = null){
        let newCover = this.props.uploadedFile;
        if (null !== remotePath && newCover){
            newCover.path = remotePath;
        }
        else if (null !== remotePath) {
          newCover = {};
          newCover.path = remotePath;
        }
        
        this.setState({cover: newCover})
    }

    renderDropzoneInput(field) {
        let uploadedFile = this.state ?
            this.state.cover ? this.state.cover : this.props.uploadedFile
            : this.props.uploadedFile  ;
        return(
            <DropImage
                field={field}
                uploadedFile={uploadedFile}
                submitImage={this.props.submitImage}
                setStateCover={this.setCoverState}/>
        );
    }

    retrieveFiche = (ficheId, callback = null) => {
      let me = this;
      fetch('/fiches/fiche/' + ficheId)
      .then((response) => {
        return response.json();
      })
      .then((fiche) => {
        me.setState({ currentFiche: fiche, cover: fiche.cover, pageStatus: PAGE_STATUS.SUCCESS }, () => {

          let map = fiche.map;

          //TODO to transform this into better one in terms of robustness
          me.props.getSelectedTag(fiche.tags);
          //actions[ACTIONS_HASH.FICHE].getSelectedTag(fiche.tags);
          handleInitialize(fiche, me.props.initialize);
          if (map) {
            let keys = Object.keys(map);

            //will find the right number later on
            if (keys.length > 3) {
              me.props.setMap(map);
              //actions[ACTIONS_HASH.FICHE].setMap(map);
            }
          }

          if (callback) {
            callback();
          }
        });
      })
      .catch((err) => {
        console.info('---------------', err);
        me.setState({ pageStatus: PAGE_STATUS.ERROR });
      })
    }

    componentDidMount() {
        this.props.fetchEnum();
        //actions[ACTIONS_HASH.FICHE].fetchEnum();
        let {flushMapData} = this.props,
              me = this;

        //do we need this ?
        //flushFicheSearch();

        setTimeout(() => {
          me.retrieveFiche(me.props.params.ficheId, () => {
            fetch('/themes')
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              me.setState({debugThemes: data});
            })
          });
        }, 500);


    }

    addRelation = (ficheId, data) => {
      let me = this;
      me.props.addRelatedFicheToFiche(ficheId, data);
      setTimeout(() => {
          this.retrieveFiche(ficheId);
      }, 150);
    };

    removeRelation = (ficheId, data) => {
      this.props.removeRelatedFicheToFiche(ficheId, data);
      setTimeout(() => {
          this.retrieveFiche(ficheId);
      }, 150);
    }

    componentWillUnmount(){
        this.props.flushDisplayedFiche();
        this.props.flushSlectedTags();
    }
    handleFormSubmit(formProps) {
        let cover = this.state.cover? this.state.cover : this.state.currentFiche.cover;
        let submitFiche =
            this.state.ficheType==="editPlace" ? this.props.editFichePlace
                : this.state.ficheType==="editPersonne" ? this.props.editFichePerson
                : this.state.ficheType==="editObjet" ? this.props.editFicheObject
                : this.state.ficheType==="editMedia" ? this.props.editFicheMedia
                : this.state.ficheType==="editEvenement" ? this.props.editFicheEvent
                : null;

        editSubmit(
            formProps,
            this.props.enumerations,
            this.props.myTags,
            cover,
            this.props.submitTag,
            this.props.themes,
            submitFiche,
            this.props.mapData,
            this.state.currentFiche,
            this.state.status,
            this.refs.childForm.getDeltas()
        )
    }
    publishFiche(){
        this.setState({status: 1});
    }

    render() {
        const { handleSubmit, enumerations } = this.props;
        const {categories, types, tags} = enumerations;
        let sousCategories= [];
        this.state.currentFiche? this.state.currentFiche.categories.map((cat) => {
            sousCategories= sousCategories.concat(cat.sousCategories);
        }): sousCategories= [];
        const errorMessage = (message) => {
            return (
                <p className={cx('message', {
                    'message-show': message && message.length > 0
                })}>{message}</p>
            )
        };

        let currentFiche = this.state.currentFiche;

        let deltaStart = 0,
            deltaEnd = 0,
            savedSlug = null,
            remotePath = null,
            ficheId = null;

        if (currentFiche) {
          //TODO shall be done by the model, buyt too long to compile -_-
          deltaStart = currentFiche.delta_start ? currentFiche.delta_start: 0;
          deltaEnd = currentFiche.delta_end ? currentFiche.delta_end: 0;
          savedSlug = currentFiche.slug ? currentFiche.slug: null;
          remotePath = currentFiche.path? currentFiche.path: null;
          ficheId = currentFiche._id;
        }

        //media, why we need ficheId ?? hope it's reliquat
        //remotePath={this.state.currentFiche? this.state.currentFiche.path: null} ficheId={this.state.currentFiche._id

        let formulaire =
            this.state.ficheType==="editPlace" ? <Site ref="childForm" errorMessage={this.props.errorMessage.basicFields} dropZone={this.renderDropzoneInput.bind(this)}/>
                : this.state.ficheType==="editPersonne" ? <Person ref="childForm" deltaStart={deltaStart} deltaEnd={deltaEnd} errorMessage={this.props.errorMessage.basicFields} dropZone={this.renderDropzoneInput.bind(this)} savedSlug={savedSlug}/>
                : this.state.ficheType==="editObjet" ? <Objet ref="childForm" deltaStart={deltaStart} errorMessage={this.props.errorMessage.basicFields} dropZone={this.renderDropzoneInput.bind(this)}/>
                : this.state.ficheType==="editMedia" ? <Media errorMessage={this.props.errorMessage.basicFields} ref="childForm" deltaStart={deltaStart} dropZone={this.renderDropzoneInput.bind(this)}
                                                         remotePath={remotePath} ficheId={ficheId}
                                                         childUpdateCover={this.setCoverState}/>
                : this.state.ficheType==="editEvenement" ? <Event ref="childForm" deltaStart={deltaStart} deltaEnd={deltaEnd} errorMessage={this.props.errorMessage.basicFields} dropZone={this.renderDropzoneInput.bind(this)}/>
                : null;


        let h3Title = "Erreur lors du chargement de la page";

        if (this.state.pageStatus === PAGE_STATUS.LOADING) {
          h3Title = "Chargement du formulaire en cours";
        }else if (this.state.pageStatus === PAGE_STATUS.SUCCESS) {
          h3Title = "Édition de la fiche " + this.state.currentFiche.name;//this.props.displayedFiche.name}
        }

        return (
            <div className="container">{this.state.pageStatus ? (//this.props.displayedFiche ? (
                <div>
                    <div className="row">
                        <h3>{h3Title}</h3>
                        <br/>
                        <div className="row">
                            {this.props.fiche.editForm? this.props.fiche.editForm.values? formulaire : null : null}
                            <div className="container well">
                                <Themes
                                    enumerations={this.props.enumerations}
                                    initialThemes={currentFiche.themes}
                                />
                            </div>
                            <div className="container well">
                                <label>Bibliographie/Sources du web (Nom du lien: URL)</label>
                                <br/>
                                {this.props.fiche.editForm?
                                    this.props.fiche.editForm.values?
                                        <Field name="references" component={renderWYSIWYGRef} label="Source des informations de cette fiche"/>:
                                        null:
                                    null}

                            </div>
                            <TaxonomiesForm
                                categories={categories}
                                types={types}
                                sousCategories={sousCategories}
                                tags={tags}
                                selectedTags={this.props.selectedTags}
                                initialValues={currentFiche.tags}
                                message={this.props.errorMessage.categories}
                                change={this.props.change.bind(this)}
                                typeFiche={this.state.ficheType}
                            />
                            <Social/>
                            {currentFiche &&
                            <MoreInfos
                                errorMessage={errorMessage(this.props.errorMessage.location)}
                                coords={currentFiche.map ? currentFiche.map.latLng : {}}
                                change={this.props.change.bind(this)}
                                setMap={this.props.setMap}
                                simpleMapCase={this.state.ficheType!=="editPlace"}
                                flushMap={this.props.flushMapData}
                                edit={true}/>}
                            <RelatedFiche searchFiche={this.props.searchFiche}
                                          addRelatedFicheToFiche={this.addRelation}
                                          removeRelatedFicheToFiche={this.removeRelation}
                                          displayedFiche={currentFiche}
                                          fiches={this.props.fiches}
                                          ficheId={currentFiche._id}
                            />
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-md-6">
                                        <RaisedButton className="btn btn-success col-md-6"
                                                      label="Enregistrer les modifications"
                                                      fullWidth={true}
                                                      type="button"
                                                      backgroundColor={"#05409A"}
                                                      labelColor={"#FFFFFF"}
                                                      style={{
                                                          width: '50%',
                                                          margin: '0 auto',
                                                          border: '2px solid #05409A',
                                                          backgroundColor: '#05409A',
                                                      }}
                                                      onClick={() => {
                                                          //seems we never reach this block
                                                          this.setState({status: currentFiche.status}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                      }}/>
                                    </div>
                                    {currentFiche.status == 0 &&
                                    <div className="col-md-6">
                                        <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer et Publier"
                                                      onClick={() => {
                                                          this.setState({status: 1}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                      }}
                                                      fullWidth={true}
                                                      type="button"/>
                                    </div>
                                    }
                                    {currentFiche.status == 1 &&
                                    <div className="col-md-6">
                                        <RaisedButton className="btn btn-primary col-md-6" label="Dépublier"
                                                      fullWidth={true}
                                                      type="button"
                                                      backgroundColor={'#FD0002'}
                                                      labelColor={"#FFFFFF"}
                                                      style={{
                                                          width: '50%',
                                                          margin: '0 auto',
                                                          border: '2px solid #FD0002',
                                                          backgroundColor: '#FD0002',
                                                      }}
                                                      onClick={() => {
                                                        this.setState({status: 0}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                      }}/>
                                    </div>
                                    }
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                </div>)
                : <div><h3>{h3Title}</h3></div>
            }
            </div>
        );
    }
}

function mapStateToProps(state) {

    return {
        fiche: state.form,
        fiches: state.fiche.fiches,
        displayedFiche: state.fiche.displayedFiche,
        uploadedFile: state.fiche.uploadedFile ? state.fiche.uploadedFile : state.fiche.displayedFiche.cover,
        enumerations: state.enumerations.enumerations,
        myTags : state.enumerations.selectedTags,
        errorMessage: state.fiche.errorSubmit,
        themes : state.enumerations.themesToPush,
        mapData: state.fiche.mapData
    };
}
export default connect(mapStateToProps, actions)(form(EditFiche));
