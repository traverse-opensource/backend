import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import * as actions from '../../actions/fiches.js';
import RaisedButton from 'material-ui/RaisedButton';
import DropImage from '../../components/fiche/DropImage';
import TaxonomiesForm from '../../components/fiche/TaxonomiesForm.jsx';
import {renderBigTextField,renderSmallTextField, renderMediumTextField, renderTinyMCEditor, handleInitialize, editSubmit,validate} from '../../components/fiche/FormUtils.jsx'
import RelatedFiche from '../../components/fiche/RelatedFiche';
/*temporaire : ajouter un nouveau style porpre au composant newFiche*/
import styles from '../../css/components/login';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);
const form = reduxForm({
    form: 'editFicheEvent',
});

class EditEventForm extends Component {
    constructor(...args) {
        super(...args);
        this.state = {deleteFile: false,
            status: 0};
    }
    setCoverState(){
        this.setState({cover: this.props.uploadedFile})
    }

    renderDropzoneInput(field) {
        let uploadedFile = this.state ?
            this.state.cover ? this.state.cover : this.props.uploadedFile
            : this.props.uploadedFile  ;
        return(
            <DropImage
                field={field}
                uploadedFile={uploadedFile}
                submitImage={this.props.submitImage}
                setStateCover={this.setCoverState.bind(this)}/>
        );
    }
    componentWillUnmount(){
        this.props.flushDisplayedFiche();
        this.props.flushSlectedTags();
    }

    componentDidMount() {
        const {flushFicheSearch} = this.props;
        //flushFicheSearch();

        this.props.fetchOneFiche(this.props.params.eventId)
            .then(() => {
                this.props.getSelectedTag(this.props.event.tags);
                handleInitialize(this.props.event, this.props.initialize);
            });
    }

    handleFormSubmit(formProps) {
        let cover = this.state.cover? this.state.cover : this.props.event.cover;
        editSubmit(
            formProps,
            this.props.enumerations,
            this.props.myTags,
            cover,
            this.props.submitTag,
            this.props.editFicheEvent,
            this.props.event,
            this.state.status
        )
    }

    publishFiche(){
        this.setState({status: 1});
    }
    render() {
        const { handleSubmit, enumerations } = this.props;
        const {categories, types, tags} = enumerations;
        const errorMessage = (message) => {
            return (
                <p className={cx('message', {
                    'message-show': message && message.length > 0
                })}>{message}</p>
            )
        };
        return (
            <div className="container">{this.props.event ? (
                <div>
                    <div className="row">
                        <h3>Edition de la fiche Événement</h3>
                        <br/>
                        <form className="form-horizontal">
                            <div className="row">
                                <div className="container well">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="col-md-6">
                                                <label>Informations de base</label><br/>
                                                {errorMessage(this.props.errorMessage.basicFields)}
                                                <Field name="name" component={renderSmallTextField} type="text" label="Nom de l'événement"/>
                                                <br/>
                                                <Field name="presentation" component={renderBigTextField} type="text" label="Présentation"/>
                                                <Field name="description" component={renderBigTextField} type="text" label="Description"/>
                                                <Field name="start_date" component={renderSmallTextField} type="text" label="Date de début"/>
                                                <br/>
                                                <Field name="end_date" component={renderSmallTextField} type="text" label="Date de fin"/>
                                                <br/><br/>
                                            </div>
                                            <div className="col-md-6">
                                                <Field name="cover_image" component={this.renderDropzoneInput.bind(this)}/>
                                                <Field name="cover_credit" component={renderMediumTextField}
                                                       type="text"
                                                       label="Credit de l'image"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="container well">
                                    <label>Références/Sources</label>
                                    <br/>
                                    <Field name="references" component={renderTinyMCEditor} label="Source des informations de cette fiche"/>
                                </div>
                                <TaxonomiesForm
                                    categories={categories}
                                    types={types}
                                    tags={tags}
                                    selectedTags={this.props.selectedTags}
                                    initialValues={this.props.event.tags}
                                    message={this.props.errorMessage.categories}
                                />
                                <RelatedFiche searchFiche={this.props.searchFiche}
                                              addRelatedFicheToFiche={this.props.addRelatedFicheToFiche}
                                              removeRelatedFicheToFiche={this.props.removeRelatedFicheToFiche}
                                              displayedFiche={this.props.event}
                                              fiches={this.props.fiches}
                                />
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <RaisedButton className="btn btn-success col-md-6"
                                                          label="Enregistrer les modifications"
                                                          fullWidth={true}
                                                          type="button"
                                                          onClick={() => {
                                                              this.setState({status: this.props.event.status}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                          }}/>
                                        </div>
                                        {this.props.event.status == 0 &&
                                        <div className="col-md-6">
                                            <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer et Publier"
                                                          onClick={() => {
                                                              this.setState({status: 1}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                          }}
                                                          fullWidth={true}
                                                          type="button"/>
                                        </div>
                                        }
                                        {this.props.event.status == 1 &&
                                        <div className="col-md-6">
                                            <RaisedButton className="btn btn-primary col-md-6" label="Dépublier"
                                                          fullWidth={true}
                                                          type="button"
                                                          onClick={handleSubmit(this.handleFormSubmit.bind(this))}/>
                                        </div>
                                        }
                                        <br/>
                                        <br/>
                                        <br/>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <br/>
                </div>)
                :
                <div><h3>Chargement du formulaire en cours</h3></div>
            }
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        fiche: state.form,
        fiches: state.fiche.fiches,
        errorMessage: state.fiche.errorSubmit,
        event: state.fiche.displayedFiche,
        uploadedFile: state.fiche.uploadedFile ? state.fiche.uploadedFile : state.fiche.displayedFiche.cover,
        enumerations: state.enumerations.enumerations,
        myTags : state.enumerations.selectedTags
    };
}
export default connect(mapStateToProps, actions)(form(EditEventForm));