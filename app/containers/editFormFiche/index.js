export {default as EditFicheEvent } from './EditFicheEvent' ;
export {default as EditFicheMedia } from './EditFicheMedia' ;
export {default as EditFicheObject } from './EditFicheObject' ;
export {default as EditFichePerson } from './EditFichePerson' ;
export {default as EditFiche } from './EditFiche' ;