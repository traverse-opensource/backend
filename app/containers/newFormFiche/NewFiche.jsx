import React, { Component } from 'react';
import { connect } from 'react-redux';
//Actions
import * as actions from '../../actions/fiches.js';
//Components
import TaxonomiesForm from '../../components/fiche/TaxonomiesForm.jsx';
import {Site, Person, Objet, Media, Event } from '../../components/fiche/formulaires';
import {renderWYSIWYGRef, defaultImage, submit} from '../../components/fiche/FormUtils.jsx';
import DropImage from '../../components/fiche/DropImage';
import MoreInfos from '../../components/fiche/MoreInfos';
import Themes from '../../components/fiche/Themes.jsx';
import Social from '../../components/fiche/Social.jsx';


/*temporaire : ajouter un nouveau style propre au composant newFiche*/
import styles from '../../css/components/login';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

//Bootstrap
import {Row} from 'react-bootstrap';
// GOOGLE UI DESIGN
import RaisedButton from 'material-ui/RaisedButton';
// redux form
import { Field, reduxForm} from 'redux-form';

const form = reduxForm({
    form: 'newFiche'
});

class NewFicheForm extends Component {
    constructor(...args) {
        super(...args);
        this.state = { status: 0, cover: defaultImage.site,
            ficheType:this.props.route.path};
        this.setCoverState = this.setCoverState.bind(this);
        this.setMediaThumbnail = this.setMediaThumbnail.bind(this);
    }

    componentWillUnmount(){
        this.props.flushSlectedTags();
    }
    setCoverState(){
        this.setState({cover: this.props.uploadedFile})
    }
    setMediaThumbnail(remotePath = null) {
        let newCover=this.props.uploadedFile;
        if (null !== remotePath) {
            if (newCover) {
                newCover['path'] = remotePath;
            }
            else{  newCover = {
                path: remotePath
            }}
        }
        this.setState({cover : newCover});
    }

    renderDropzoneInput(field) {
        let uploadedFile = this.state ?
            this.state.cover ? this.state.cover : this.props.uploadedFile
            : this.props.uploadedFile  ;
        return(
            <DropImage
                field={field}
                uploadedFile={uploadedFile?uploadedFile:defaultImage.site}
                submitImage={this.props.submitImage}
                setStateCover={this.props.route.path ==="ficheMedia" ? this.setMediaThumbnail: this.setCoverState}/>
        );
    }
    handleChangeCharsLeft(event) {
        var input = event.target.value;
        this.setState({
            chars_left: this.state.chars_left - input.length
        });
    }

    componentDidMount(){
        this.props.fetchEnum();
        /*this.props.fetchSousCat();*/
    }
    handleFormSubmit(formProps){
        let submitFiche =
            this.state.ficheType==="fichePlace" ? this.props.submitPlace
                : this.state.ficheType==="fichePersonne" ? this.props.submitPerson
                : this.state.ficheType==="ficheObjet" ? this.props.submitObject
                : this.state.ficheType==="ficheMedia" ? this.props.submitMedia
                : this.state.ficheType==="ficheEvenement" ? this.props.submitEvent
                : null;
        submit(
            formProps,
            this.props.enumerations,
            this.props.myTags,
            this.state.cover,
            this.props.user._id,
            this.state.status,
            this.props.submitTag,
            this.props.themes,
            this.props.mapData,
            submitFiche,
            this.props.initialize,
            this.refs.childForm.getDeltas()
        )
    }
    publishFiche(){
        this.setState({status: 1});
    }

    render() {
        let formulaire = <div></div>;
        let title = "";
        const pathname=this.props.route.path;
        const { handleSubmit, enumerations } = this.props;
        const { categories ,types,tags} = enumerations;
        const errorMessage = (message) => {
            return (
                <p className={cx('message', {
                    'message-show': message && message.length > 0
                })}>{message}</p>
            )
        };
        const renderStatus = ({input, label, meta: {touched, error}, ...custom}) => {
            return (
                <div className="display : none">
                    <label>{label}</label>
                    <input {...input} type="text" placeholder={label} value={this.state.status}/>
                    {touched && error && <span>{error}</span>}
                </div>
            )
        };
        if(pathname==="fichePlace"){
            formulaire=
                <Site
                    ref="childForm"
                    errorMessage={this.props.errorMessage.basicFields}
                    dropZone={this.renderDropzoneInput.bind(this)}
                    chars_left={this.state.chars_left}
                    handleChange={this.handleChangeCharsLeft.bind(this)}
                />;
            title="Site";
        }if(pathname==="fichePersonne"){
            formulaire = <Person
                ref="childForm"
                errorMessage={this.props.errorMessage.basicFields}
                dropZone={this.renderDropzoneInput.bind(this)}
            />;
            title="Personne";
        }if(pathname==="ficheObjet"){
            formulaire=<Objet
                ref="childForm"
                errorMessage={this.props.errorMessage.basicFields}
                dropZone={this.renderDropzoneInput.bind(this)}/>;
            title="Objet";
        }if(pathname==="ficheMedia"){
            formulaire=<Media
                ref="childForm"
                errorMessage={this.props.errorMessage.basicFields}
                dropZone={this.renderDropzoneInput.bind(this)}
                childUpdateCover={this.setMediaThumbnail}/>;
            title="Media";
        }if(pathname==="ficheEvenement"){
            formulaire=<Event
                ref="childForm"
                errorMessage={this.props.errorMessage.basicFields}
                dropZone={this.renderDropzoneInput.bind(this)}/>;
            title="Événement";
        }
        return (
            <div className="container">
                <div className="span12">
                    <h3>Création d'une fiche {title}</h3>
                    <br/>
                    <Row>
                        {formulaire}
                        <div className="container well">
                            <Themes
                                enumerations={this.props.enumerations}
                                initialThemes={[]}
                            />
                        </div>
                        <div className="container well">
                            <label>Bibliographie/Sources du web (Nom du lien: URL)</label>
                            <br/>
                            <Field name="references" component={renderWYSIWYGRef} label="Source des informations de cette fiche"/>

                        </div>
                        <div className="hidden">
                            <Field name="status" type="text" component={renderStatus} />
                        </div>
                        <TaxonomiesForm
                            categories={categories}
                            values={this.props.fiche.newFiche ? this.props.fiche.newFiche.values : [] }
                            tags={tags}
                            selectedTags={this.props.selectedTags}
                            initialValues={[]}
                            message={this.props.errorMessage.categories}
                            change={this.props.change.bind(this)}
                            typeFiche={pathname}
                        />
                        <Social/>
                        <MoreInfos
                            errorMessage={errorMessage(this.props.errorMessage.location)}
                            change={this.props.change.bind(this)}
                            simpleMapCase={pathname!=="fichePlace"}
                            setMap={this.props.setMap}/>
                    </Row>
                    <br/>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6">
                                <RaisedButton className="btn btn-success col-md-6"
                                              label="Enregistrer le brouillon"
                                              fullWidth={true}
                                              type="button"
                                              onClick={() => {this.setState({status: 0}, handleSubmit(this.handleFormSubmit.bind(this)))}}/>
                            </div>
                            <div className="col-md-6">
                                <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer et Publier"
                                              onClick={() => {this.setState({status: 1}, handleSubmit(this.handleFormSubmit.bind(this)))}}
                                              fullWidth={true}
                                              type="button"
                                              backgroundColor={"#05409A "}
                                              labelColor={"#FFFFFF"}
                                              style={{
                                                  width: '50%',
                                                  margin: '0 auto',
                                                  border: '2px solid #05409A  ',
                                                  backgroundColor: '#05409A ',
                                              }}/>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state){
  
    return {
        fiche: state.form,
        uploadedFile: state.fiche.uploadedFile,
        message: state.fiche.formMessage,
        errorMessage: state.fiche.errorSubmit,
        enumerations: state.enumerations.enumerations,
        myTags : state.enumerations.selectedTags,
        themes : state.enumerations.themesToPush,
        user: state.user.user,
        mapData: state.fiche.mapData
    };
}

export default connect(mapStateToProps, actions) (form(NewFicheForm));