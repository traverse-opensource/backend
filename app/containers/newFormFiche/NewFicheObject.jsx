import React, { Component } from 'react';
import { connect } from 'react-redux';
//Actions
import * as actions from '../../actions/fiches.js';
//Components
import TaxonomiesForm from '../../components/fiche/TaxonomiesForm.jsx';
import {renderBigTextField,renderSmallTextField,renderMediumTextField ,renderTinyMCE, submit ,validate, defaultImage} from '../../components/fiche/FormUtils.jsx'
import DropImage from '../../components/fiche/DropImage';
//Bootstrap
import { Row} from 'react-bootstrap';
// GOOGLE UI DESIGN
import RaisedButton from 'material-ui/RaisedButton';
/*temporaire : ajouter un nouveau style porpre au composant newFiche*/
import styles from '../../css/components/login';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);
// redux form
import { Field, reduxForm } from 'redux-form';
const form = reduxForm({
    form: 'newFicheObject',
});


class NewObjectForm extends Component {
    constructor(...args) {
        super(...args);
        this.state = { status: 0, cover: defaultImage.object};
    }
    componentWillUnmount(){
        this.props.flushSlectedTags();
    }

    setCoverState(){
        this.setState({cover: this.props.uploadedFile})
    }

    renderDropzoneInput(field) {
        let uploadedFile = this.state ?
            this.state.cover ? this.state.cover : this.props.uploadedFile
            : this.props.uploadedFile  ;
        return(
            <DropImage
                field={field}
                uploadedFile={uploadedFile?uploadedFile:defaultImage.object}
                submitImage={this.props.submitImage}
                setStateCover={this.setCoverState.bind(this)}/>
        );
    }

    handleFormSubmit(formProps) {
        submit(
            formProps,
            this.props.enumerations,
            this.props.myTags,
            this.state.cover,
            this.props.user._id,
            this.state.status,
            this.props.submitTag,
            this.props.submitObject,
            this.props.initialize
        )
    }
    publishFiche(){
        this.setState({status: 1});
    }
    render() {
        let formulaire = <div></div>;
        const pathname=this.props.route.path.substring(16);

        const { handleSubmit, enumerations } = this.props;
        const { categories ,types,tags} = enumerations;
        const errorMessage = (message) => {
            return (
                <p className={cx('message', {
                    'message-show': message && message.length > 0
                })}>{message}</p>
            )
        };
        return (
            <div className="container">
                <h3>Création d'une Fiche Objet</h3>
                <br/>
                <div className="span12">
                    <form className="form-horizontal">
                        <Row>
                            <div className="container well">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="col-md-6">
                                            <label>Informations de base</label><br/>
                                            {errorMessage(this.props.errorMessage.basicFields)}
                                            <Field name="name" component={renderSmallTextField} type="text" label="Nom de l'objet"/>
                                            <br/>
                                            <Field name="short_description" component={renderMediumTextField} type="text" label="Description courte de l'objet"/>
                                            <Field name="presentation" component={renderBigTextField} type="text" label="Présentation de l'objet"/>
                                            <Field name="history" component={renderBigTextField} type="text" label="Histoire de l'objet"/>
                                            <Field name="date.date" component={renderSmallTextField} type="text" label="Date"/>
                                            <br/>
                                            <Field name="date.description" component={renderMediumTextField} type="text" label="A quoi correspond cette date?"/>
                                        </div>
                                        <div className="col-md-6">
                                            <Field name="cover_image" component={this.renderDropzoneInput.bind(this)}/>
                                            <Field name="cover_credit" component={renderMediumTextField}
                                                   type="text"
                                                   label="Credit de l'image"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="container well">
                                <label>Références/Sources</label>
                                <br/>
                                <Field name="references" component={renderTinyMCE} label="Source des informations de cette fiche"/>
                            </div>

                            <TaxonomiesForm
                                categories={categories}
                                types={types}
                                tags={tags}
                                selectedTags={this.props.selectedTags}
                                initialValues={[]}
                                message={this.props.errorMessage.categories}
                            />

                            <div className="container-fluid well">
                                <div className="row">
                                    <div className="col-md-4">
                                        <label>Plus d'informations</label><br/>
                                        <Field name="technical_information" component={renderMediumTextField} type="text" label="Informations techniques"/>
                                    </div>
                                    <div className="col-md-4">
                                        <br/>
                                        <Field name="more_information" component={renderMediumTextField} type="text" label="Informations supplémentaires"/>
                                    </div>
                                </div>
                            </div>
                        </Row>
                        <br/>
                        <br/>
                        <br/>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-6">
                                    <RaisedButton className="btn btn-success col-md-6"
                                                  label="Enregistrer le brouillon"
                                                  fullWidth={true}
                                                  type="button"
                                                  onClick={handleSubmit(this.handleFormSubmit.bind(this))}/>
                                </div>
                                <div className="col-md-6">
                                    <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer et Publier"
                                                  onClick={() => {
                                                      this.setState({status: 1}, handleSubmit(this.handleFormSubmit.bind(this)));
                                                  }}
                                                  fullWidth={true}
                                                  type="button"/>
                                </div>
                                <br/>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        fiche: state.form,
        errorMessage: state.fiche.errorSubmit,
        uploadedFile: state.fiche.uploadedFile,
        enumerations: state.enumerations.enumerations,
        myTags : state.enumerations.selectedTags,
        user: state.user.user,
    };
}
export default connect(mapStateToProps, actions) (form(NewObjectForm));