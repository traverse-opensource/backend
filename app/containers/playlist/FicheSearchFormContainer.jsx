import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../../actions/playlists';
import FicheSearchForm from '../../components/FicheSearchForm';

const SearchFormContainer = React.createClass({

    search: function(event) {
        event.preventDefault();

        let query = this.refs.child.getQuery();
        let filters = this.refs.child.getFilters();

        const { searchFiche } = this.props;
        searchFiche(query, filters);
    },

    render: function() {
        return (
            <FicheSearchForm search={this.search} ref="child" />
        );
    }

});

function mapStateToProps(state){
    return {
        fiches: state.fiche,
    }
}

export default connect(mapStateToProps, actions)(SearchFormContainer);