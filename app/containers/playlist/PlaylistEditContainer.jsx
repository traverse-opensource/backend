import React, {Component} from 'react';

import {Field, reduxForm} from "redux-form";

import {connect} from 'react-redux';
import * as actions from '../../actions/playlists';
import PlaylistFichesTimeline from '../../components/playlist/timeline/PlaylistFichesTimeline.jsx';
import FicheSearchFormContainer from '../../containers/playlist/FicheSearchFormContainer.jsx';
import PlaylistAddLinkedFicheForm from '../../components/playlist/PlaylistAddLinkedFicheForm.jsx';
import CoverSelector from '../../components/playlist/CoverSelector.jsx';
import LabelDisplayer from '../../components/playlist/LabelDisplayer.jsx';
import LimitedCharsField from '../../components/fiche/LimitedCharsField.jsx';
import {Modal, Button} from 'react-bootstrap';

import {
    renderBigTextField, renderSmallTextField, handleInitialize, editSubmit } from '../../components/playlist/FormUtils.jsx'
import RaisedButton from "material-ui/RaisedButton";

import classNames from 'classnames/bind';
import styles from '../../css/components/playlist-fiche-search.css';

import { CALLBACK_MODES, status } from '../../constant';

const cx = classNames.bind(styles);

const form = reduxForm({
    form: 'editPlaylistForm',
});
const required = value => value ? undefined : 'Merci de renseigner ce champs';
const icon = (type) => {
    switch (type) {
        case 'People':
            return 'user';
        case 'Events':
            return 'time';
        case 'Media':
            return 'picture';
        case 'Objects':
            return 'tag';
        case 'Places':
            return 'map-marker';
        default:
            return 'info-sign';
    }
};

/**
 TODO add the callback on submit a method to show if the process to update the playlist,
 since when we create a playlist, it's created within the database,
 has succeed nor failed
 */
class PlaylistEditContainer extends Component {

    constructor(props) {
        super(props);

        let defaultState = {
            selected: {categories: [], tags:[], subcategories: []},
            propagate: {
                cat: {
                    labels: [],
                    selected: []
                },
                sub: {
                    labels: [],
                    selected: []
                },
                tag: {
                    labels: [],
                    selected: []
                }
            }
        };

        this.state = defaultState;

        this.playlistStatus = this.props.playlist.status;

        this.updateLabelState = this.updateLabelState.bind(this);
        this.retrieveSelectedField = this.retrieveSelectedField.bind(this);
        this.retrieveUpdatedLabels = this.retrieveUpdatedLabels.bind(this);
        this.onChipTap = this.onChipTap.bind(this);
        this.updateSelectedItems = this.updateSelectedItems.bind(this);
        this.updatePlaylistStatus = this.updatePlaylistStatus.bind(this);

        this.onSlugChange = this.onSlugChange.bind(this);
    }

    componentDidMount() {
        const {getPlaylistData, resetFicheResults} = this.props,
            me = this;
        resetFicheResults();
        getPlaylistData(this.props.params.id).then(() => {
            handleInitialize(this.props.playlist, this.props.initialize);

            me.updateLabelState(this.props.playlist.labels);
        });
    }

    updateLabelState(labels) {
        let cat = labels.categories.map((item) => { return {id: item._id, name: item.name} }),
            sub = labels.sousCategories.map((item) => { return {id: null, name: item}; }),
            tag = labels.tags.map((item) => { return {id: item._id, name: item.name} });
        this.setState({
            selected: {
                categories: cat,
                subcategories: sub,
                tags: tag
            },
            //must be selected since it originally comes from the database
            propagate: {
                cat: {
                    labels: cat,
                    selected: cat.map((item) => { return true; })
                },
                sub: {
                    labels: sub,
                    selected: sub.map((item) => { return true; })
                },
                tag: {
                    labels: tag,
                    selected: tag.map((item) => { return true; })
                }
            }
        });
    }

    selectFiche(item) {
        const {setFicheSelected, setFicheToReplaceBy} = this.props;
        let data;
        if (this.props.selectedFichePlaylist == null) {
            if (this.props.playlist.linkedFiches.length > 0) {
                setFicheSelected(item);
            }
            else {
                const {addLinkedFicheToPlaylist} = this.props;
                data = {fiche: item, linkValue: null};
                addLinkedFicheToPlaylist(this.props.playlist._id, data);

                let me = this;

                //we need to this after the playlist has been added actually since we need to wait until the server respond
                setTimeout(() => {
                    //updating also the cover if we're at the first chosen fiche
                    this.coverSelected(item.cover);
                }, 500);
            }
        }
        else {
            data = {toReplace: this.props.selectedFichePlaylist, replaceBy: item};
            setFicheToReplaceBy(data);
        }
    }

    onFicheFromPlaylistSelected(fiche) {
        const {setPlaylistFicheSelected, setPlaylistFicheUnselected} = this.props;
        if (this.props.selectedFichePlaylist != null) {

            if (this.props.selectedFichePlaylist._id != fiche._id) {
                var data = {fiche1: fiche._id, fiche2: this.props.selectedFichePlaylist._id};
                this.props.swapPlaylistFiches(this.props.playlist._id, data);
            }
            else {
                setPlaylistFicheUnselected();
            }
        }
        else {
            setPlaylistFicheSelected(fiche);
        }
    }

    onFicheFromPlaylistRemoved(fiche) {

        this.props.removeFicheFromPlaylist(this.props.playlist._id, {fiche: fiche._id});
    }

    onLinkEdited(link) {
        const {setPlaylistLinkSelected} = this.props;
        setPlaylistLinkSelected(link);
    }

    //avoiding using the state here
    updatePlaylistStatus(callback, state){

        this.playlistStatus = state;
        callback();
    }

    handleFormSubmit(formProps) {
        formProps.status = this.playlistStatus;

        //console.log("edit yolo", formProps, this.props.editPlaylist, this.props.playlist)

        editSubmit(
            formProps,
            this.props.editPlaylist,
            this.props.playlist,
            //last parameter to load all created playlists from the previous fragment
            true
        )
    }

    close() {
        const {deselectFiche} = this.props;
        deselectFiche()
    }

    closeLinkDialog() {
        const {setPlaylistLinkUnselected} = this.props;
        setPlaylistLinkUnselected()
    }

    replace() {
        var data = { toReplace: this.props.selectedFiche.toReplaceData._id,
            replaceBy: this.props.selectedFiche.replaceByData._id};

        this.props.replacePlaylistFiche(this.props.playlist._id, data)
        ;
    }

    addLinkedFiche(id, data) {
        this.props.addLinkedFicheToPlaylist(id, data);
    }

    editFicheLink(id, data) {
        this.props.updateLinkFichePlaylist(id, data);
    }

    coverSelected(cover) {
        this.props.updatePlaylistCover(this.props.playlist._id, cover);
    }

    onLabelsSelected(labels) {
        this.props.updatePlaylistLabels(this.props.playlist._id, labels);
    }

    updateSelectedItems(item, hasToAdd, mode) {
        let me = this,
            prop = {},
            propIndex = '',
            newValue = {},
            index = 0,
            debugString = '',
            toUpdate = me.state.propagate,
            callback = () => {
                let labels = {
                    tags: this.state.selected.tags.map((tag) => { return tag.id}),
                    categories: this.state.selected.categories.map((tag) => { return tag.id}),
                    sousCategories: this.state.selected.subcategories.map((tag) => { return tag.name})
                };
                me.onLabelsSelected(labels);
            };

        switch (mode) {
            case CALLBACK_MODES.CATEGORIES:
                prop = me.state.propagate.cat;
                propIndex = 'cat';
                debugString = 'Categories';
                break;
            case CALLBACK_MODES.SUB_CATEGORIES:
                prop = me.state.propagate.sub;
                propIndex = 'sub';
                debugString = 'SubCategories';
                break;
            case CALLBACK_MODES.TAGS:
                prop = me.state.propagate.tag;
                propIndex = 'tag';
                debugString = 'Tags';
                break;
        }

        index = prop.labels.map((val) => { return val.name; }).indexOf(item.name);

        if (index === -1) {
            index = prop.labels.length;
            prop.labels.push(item);
        }

        prop.selected[index] = hasToAdd;

        toUpdate[propIndex] = prop;

        me.setState({propagate: toUpdate}, callback);
    }

    retrieveSelectedField(mode) {
        if (mode === CALLBACK_MODES.CATEGORIES) {
            return this.state.selected.categories;
        } else if (mode === CALLBACK_MODES.SUB_CATEGORIES) {
            return this.state.selected.subcategories;
        }
        return this.state.selected.tags;
    }

    retrieveUpdatedLabels(mode, toUpdate) {
        let toReturn = this.state.selected;
        switch (mode) {
            case CALLBACK_MODES.CATEGORIES:
                toReturn.categories = toUpdate;
                break;
            case CALLBACK_MODES.SUB_CATEGORIES:
                toReturn.subcategories = toUpdate;
                break;
            case CALLBACK_MODES.TAGS:
                toReturn.tags = toUpdate;
                break;
        }
        return toReturn;
    }

    onChipTap(item, hasToAdd, mode) {

        let me = this,
            toUpdate = me.retrieveSelectedField(mode);

        if (hasToAdd) {
            toUpdate.push(item);
        }else {
            toUpdate = toUpdate.filter((val) => { return val.name !== item.name });
        }

        me.setState({selected: me.retrieveUpdatedLabels(mode, toUpdate)}, () => {
            me.updateSelectedItems(item, hasToAdd, mode);
        });
    }

    onCategoryChipTap(item, hasToAdd) {
        this.onChipTap(item, hasToAdd, CALLBACK_MODES.CATEGORIES);
    }

    onSubCategoryChipTap(item, hasToAdd) {
        this.onChipTap(item, hasToAdd, CALLBACK_MODES.SUB_CATEGORIES);
    }

    onTagChipTap(item, hasToAdd) {
        this.onChipTap(item, hasToAdd, CALLBACK_MODES.TAGS);
    }

    //need to fetch here if it's possible to assign this slug
    onSlugChange(val) {

    }

    render() {

        const { handleSubmit } = this.props,
            selectedCategories = this.state.selected.categories,
            selectedSubCategories = this.state.selected.subcategories,
            selectedTags = this.state.selected.tags;

        let array = [];

        var items = <div>Tappez un mot clé pour trouver les fiches correspondantes</div>

        if (Object.keys(this.props.fiches).length > 0) {

            let playlistFicheIds = this.props.playlist.linkedFiches.map((lf) => {
              return lf.fiche._id;
            });

            array = this.props.fiches.filter((searchFiche) => {
              return playlistFicheIds.indexOf(searchFiche._id) === -1;
            });

            if (array.length > 0) {
                items = array.map((item, key) => {
                    return (
                        <li className="list-group-item" key={key} onClick={this.selectFiche.bind(this, item)}>
                            <span className={"glyphicon glyphicon-" + icon(item.__t)}/> {item.name}
                        </li>
                    )
                })
            }
            else {
                items = <div>Aucun résultat trouvé</div>
            }

        }

        const styles = {
            hidden: {
                display: 'none'
            },
            draft: {
                width: '50%',
                margin: '8px auto',
            },
            publish: {
                width: '50%',
                margin: '8px auto',
                border: '2px solid #05409A',
                backgroundColor: '#05409A ',
            },
            unpublish: {
                width: '50%',
                margin: '8px auto',
                border: '2px solid #FD0002',
                backgroundColor: '#FD0002',
            }
        };

        let submitCallback = handleSubmit(this.handleFormSubmit.bind(this));

        const maxLengthDescription = 140,
            maxLengthSlug = 30;

        let createSlugHolder = () => {
            return <LimitedCharsField renderSmallTextField={renderBigTextField} max_chars={maxLengthSlug}
                                      name="slug" id="slug" label="Hashtag customisé"
                                      id_countdown="slug_countdown" secondCallback={this.onSlugChange}
            />
        };



        /*
         <Field name="description" component={renderBigTextField} type="text"
         label="Description"/>
         */

        return (
            <div className="container">
                <div className="row">
                    <h3>Édition de la playlist {this.props.playlist.name}</h3>
                    <br/>
                    <form className="form-horizontal">
                        <div className="row">
                            <div className="container well">
                                <div className="row">
                                    <div className="col-md-4">

                                        <label >Informations de base</label><br/>
                                        <Field name="name" component={renderSmallTextField} type="text"
                                               label="Nom de la playlist"
                                               required={[required]}
                                        />
                                        <br/>
                                        <small>Par exemple: Autour du lac</small>
                                        <br/>
                                        <LimitedCharsField
                                            renderSmallTextField={renderBigTextField} max_chars={maxLengthDescription}
                                            name="description" id="description" label="Description"
                                            id_countdown="description_countdown"
                                        />
                                        {
                                            //maybe we will need this later on
                                            //createSlugHolder()
                                        }
                                        <br/>
                                        {this.props.playlist.status ?
                                            <div>
                                                <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer les modifications"
                                                              fullWidth={true}
                                                              type="button"
                                                              backgroundColor={"#05409A"}
                                                              labelColor={"#FFFFFF"}
                                                              style={styles.publish}
                                                              onClick={() => {this.updatePlaylistStatus(submitCallback, status.PUBLISHED)}}/>
                                                <RaisedButton className="btn btn-primary col-md-6" label="Dépublier"
                                                              fullWidth={true}
                                                              type="button"
                                                              backgroundColor={'#FD0002'}
                                                              labelColor={"#FFFFFF"}
                                                              style={styles.unpublish}
                                                              onClick={() => {this.updatePlaylistStatus(submitCallback, status.DRAFT)}}/>
                                            </div>:
                                            <div>
                                                <RaisedButton className="btn btn-success col-md-6" label="Enregistrer le brouillon"
                                                              fullWidth={true}
                                                              type="button"
                                                              style={styles.draft}
                                                              onClick={() => {this.updatePlaylistStatus(submitCallback, status.DRAFT)}}/>
                                                <RaisedButton className="btn btn-primary col-md-6" label="Enregistrer et publier"
                                                              fullWidth={true}
                                                              type="button"
                                                              backgroundColor={"#05409A"}
                                                              labelColor={"#FFFFFF"}
                                                              style={styles.publish}
                                                              onClick={() => {this.updatePlaylistStatus(submitCallback, status.PUBLISHED)}}/>
                                            </div>
                                        }

                                    </div>

                                    <div className="col-md-8">
                                        <CoverSelector playlist={this.props.playlist} onSelect={this.coverSelected.bind(this)}/>
                                        {selectedCategories.length>0 &&
                                        <LabelDisplayer label="Catégories sélectionnées" items={selectedCategories} onTagTap={this.onCategoryChipTap.bind(this)}/>
                                        }
                                        {selectedSubCategories.length>0 &&
                                        <LabelDisplayer label="Sous catégories sélectionnées" items={selectedSubCategories} onTagTap={this.onSubCategoryChipTap.bind(this)}/>
                                        }
                                        {selectedTags.length > 0 &&
                                        <LabelDisplayer label="Mots clés sélectionnés" items={selectedTags} onTagTap={this.onTagChipTap.bind(this)}/>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


                <div className="row">
                    <div className="playlist-fiche-search col-md-4">
                        <div className={cx("dual-list") + " " + cx("list-right") + " dual-list list-right"}>
                            <div className="well">
                                <FicheSearchFormContainer />

                                <ul className={cx("list-group") + " list-group"}>{items}</ul>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-8">
                        <PlaylistFichesTimeline
                            playlist={this.props.playlist}
                            selectedFiche={this.props.selectedFichePlaylist}
                            onSelected={this.onFicheFromPlaylistSelected.bind(this)}
                            onRemoved={this.onFicheFromPlaylistRemoved.bind(this)}
                            onEditLink={this.onLinkEdited.bind(this)}
                            onCategoryTap={this.onCategoryChipTap.bind(this)}
                            onSubCategoryTap={this.onSubCategoryChipTap.bind(this)}
                            onTagTap={this.onTagChipTap.bind(this)}
                            onPropagateTap={this.state.propagate}
                        />
                    </div>

                </div>

                <ModalUpdateLink
                    show={this.props.playlist.showLinkDialog}
                    onHide={this.closeLinkDialog.bind(this)}
                    fiche={this.props.playlist.selectedLinkPlaylist}
                    playlistId={this.props.playlist._id}
                    callback={this.editFicheLink.bind(this)}
                />
                <ModalAddLinkedFiche
                    show={this.props.selectedFiche.show}
                    onHide={this.close.bind(this)}
                    fiche={this.props.selectedFiche.data}
                    playlistId={this.props.playlist._id}
                    callback={this.addLinkedFiche.bind(this)}
                />
                <ModalReplaceLinkedFiche
                    show={this.props.selectedFiche.showReplace}
                    onHide={this.close.bind(this)}
                    onReplace={this.replace.bind(this)}
                    ficheToReplace={this.props.selectedFiche.toReplaceData}
                    fiche={this.props.selectedFiche.replaceByData}
                    playlistId={this.props.playlist._id}
                />
            </div>
        )
    }
}

/*

 */

function mapStateToProps(state) {
    return {
        playlist: state.playlist.displayedPlaylist,
        fiches: state.playlist.ficheSearchResult,
        selectedFiche: state.playlist.selectedFiche,
        selectedFichePlaylist: state.playlist.displayedPlaylist.selectedFichePlaylist
    }
}

export default connect(mapStateToProps, actions)(form(PlaylistEditContainer));

class ModalAddLinkedFiche extends Component {
    render() {
        const {show, fiche, playlistId, onHide, callback} = this.props;
        return (
            <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Ajouter la fiche {fiche != null ? fiche.name : ''}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <PlaylistAddLinkedFicheForm close={this.props.onHide} fiche={fiche} playlistId={playlistId} callback={callback}/>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

class ModalReplaceLinkedFiche extends Component {
    render() {
        const {show, ficheToReplace, fiche, playlistId, onHide, onReplace} = this.props;
        return (
            <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Remplacer</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Remplacer la fiche <b>{ficheToReplace.fiche != null ? ficheToReplace.fiche.name : ''}</b> par <b>{fiche != null ? fiche.name : ''}</b> ?
                </Modal.Body>
                <Modal.Footer >
                    <Button onClick={onHide}>Annuler</Button>
                    <Button onClick={onReplace} bsStyle="primary">Remplacer</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

class ModalUpdateLink extends Component {
    render() {
        const {show, fiche, playlistId, onHide, callback} = this.props;
        return (
            <Modal show={show} onHide={onHide} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Modifier le lien</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <PlaylistAddLinkedFicheForm close={this.props.onHide} fiche={fiche} playlistId={playlistId} callback={callback}/>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}

/*
 to put back
 */
