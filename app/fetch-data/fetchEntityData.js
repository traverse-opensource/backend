import { entityService} from '../services';
import { FETCH_GROUPS, ERROR_FICHES} from '../types';

const fetchData = () => entityService.getEntites();
const fetchSuccess = ({ store, response }) => store.dispatch({ type: FETCH_GROUPS, data: response });
const fetchError = ({ store, error }) => store.dispatch({ type: ERROR_FICHES });

export default {
    fetchData,
    fetchSuccess,
    fetchError
};
