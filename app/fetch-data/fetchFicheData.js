import { ficheService} from '../services';
import { LOAD_FICHES, ERROR_FICHES, NONE} from '../types';

const fetchData = () => ficheService.getFiches();

//TODO below carefull since we send a fake type for the dispatcher since we don't want to update the whole list
const fetchSuccess = ({ store, response }) => {
  //store.dispatch({ type: LOAD_FICHES, data: response }) 
  /*if (store.getState().fiche.fiches.length === 0) {
    store.dispatch({ type: LOAD_FICHES, data: response });  
  }else {
    store.dispatch({ type: NONE, data: response }) 
  }*/
  
  let state = store.getState();
  
  let loadOrNone = true;
  
  if (state.routing){
    if (state.routing.locationBeforeTransitions){
      if (state.routing.locationBeforeTransitions.state){
        if (state.routing.locationBeforeTransitions.state.keepContext){
          loadOrNone = false;
        }
      }
    }
  }
  if (loadOrNone) {
    store.dispatch({ type: LOAD_FICHES, data: response });  
  }else {
    store.dispatch({ type: NONE, data: response }) 
  }
};

/* //on head long time ago
const fetchData = () => ficheService.getFiches();
const fetchSuccess = ({ store, response }) => store.dispatch({ type: LOAD_FICHES, data: response });
*/

const fetchError = ({ store, error }) => {
  console.error("error", store, error);
  store.dispatch({ type: ERROR_FICHES });
};

export default {
    fetchData,
    fetchSuccess,
    fetchError
};
