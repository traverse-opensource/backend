import { playlistService} from '../services';
import { LOAD_PLAYLIST, LOAD_PLAYLISTS, ERROR_PLAYLIST} from '../types';

const fetchAllData = () => playlistService.getPlaylists();

const fetchData = (id) => playlistService.getPlaylist(id);

const fetchAllDataSuccess = ({ store, response }) => store.dispatch({ type: LOAD_PLAYLISTS, data: response.data });
const fetchDataSuccess = ({ store, response }) => store.dispatch({ type: LOAD_PLAYLIST, data: response.data });
const fetchError = ({ store, error }) => store.dispatch({ type: ERROR_PLAYLIST });

export default {
  fetchAllData,
  fetchData,
  fetchDataSuccess,
  fetchAllDataSuccess,
  fetchError
};
