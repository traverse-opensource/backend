import { userService} from '../services';
import { FETCH_USERS, ERROR_FICHES} from '../types';

const fetchData = () => userService.getUsers();
const fetchSuccess = ({ store, response }) => store.dispatch({ type: FETCH_USERS, data: response });
const fetchError = ({ store, error }) => store.dispatch({ type: ERROR_FICHES });

export default {
    fetchData,
    fetchSuccess,
    fetchError
};
