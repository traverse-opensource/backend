import React, { Component, PropTypes } from 'react';
import Page from 'pages/Page';
import FeedContainer from 'containers/Feed';

class Feed extends Component {
    render() {
        return (
            <Page {...this.getMetaData()}>
                <FeedContainer {...this.props} />
            </Page>
        );
    }

    getMetaData() {
        return {
            title: this.pageTitle(),
            meta: this.pageMeta(),
            link: this.pageLink()
        };
    }

    pageTitle() {
        return 'Modération | Traverse';
    }

    pageMeta() {
        return [
            { name: "description", content: "Traverse | médias externes" }
        ];
    }

    pageLink() {
        return [];
    }
}

export default Feed;
