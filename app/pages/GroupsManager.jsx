import React, { Component } from 'react';
import Page from '../pages/Page';
import GroupsManagerContainer from '../containers/Entities';

class GroupsManager extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Manage Group | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'groups management page' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <GroupsManagerContainer {...this.props} />
      </Page>
    );
  }
}

export default GroupsManager;
