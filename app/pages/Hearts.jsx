import React, { Component } from 'react';
import Page from '../pages/Page';
import HeartStrokeContainer from '../containers/HeartStrokes';

class HeartStroke extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Coups de coeur | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'Gestion des coups de coeurs' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <HeartStrokeContainer {...this.props} />
      </Page>
    );
  }
}

export default HeartStroke;
