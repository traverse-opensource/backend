import React, { Component, PropTypes } from 'react';
import Page from 'pages/Page';
import NewFicheContainer from '../containers/newFormFiche/NewFiche';

class NewFiche extends Component {
    render() {
        return (
            <Page {...this.getMetaData()}>
                <NewFicheContainer {...this.props} />
            </Page>
        );
    }

    getMetaData() {
        return {
            title: this.pageTitle(),
            meta: this.pageMeta(),
            link: this.pageLink()
        };
    }

    pageTitle() {
        return 'NewFiche | Traverse';
    }

    pageMeta() {
        return [
            { name: "description", content: "Ajouter une Place" }
        ];
    }

    pageLink() {
        return [];
    }
}

export default NewFiche;
