import React, { Component } from 'react';
import Page from '../pages/Page';
import ProfileContainer  from '../containers/Profile';

class Profile extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Profil | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'Profil utilisateur' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <ProfileContainer {...this.props} />
      </Page>
    );
  }
}

export default Profile;

