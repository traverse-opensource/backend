import React, { Component } from 'react';
import Page from '../pages/Page';
import ResetPasswordContainer from '../containers/ResetPassword.jsx';

class Login extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Reset Password | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'Reset password page' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <ResetPasswordContainer {...this.props} />
      </Page>
    );
  }
}

export default Login;
