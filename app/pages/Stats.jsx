import React, { Component } from 'react';
import Page from '../pages/Page';
import StatsContainer from '../containers/Stats';

class Stats extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Statistiques | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'Données de statistique relatives à Traverse' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <StatsContainer {...this.props} />
      </Page>
    );
  }
}

export default Stats;
