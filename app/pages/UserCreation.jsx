import React, { Component } from 'react';
import Page from '../pages/Page';
import UserCreationContainer from '../containers/UserCreation';

class UserCreation extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Create User | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'user creation page' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <UserCreationContainer {...this.props} />
      </Page>
    );
  }
}

export default UserCreation;
