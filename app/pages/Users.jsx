import React, { Component } from 'react';
import Page from '../pages/Page';
import UserContainer from '../containers/Users';

class Users extends Component {
  getMetaData() {
    return {
      title: this.pageTitle(),
      meta: this.pageMeta(),
      link: this.pageLink()
    };
  }

  pageTitle() {
    return 'Users | Traverse';
  }

  pageMeta() {
    return [
      { name: 'description', content: 'Utilisateurs de Traverse' }
    ];
  }

  pageLink() {
    return [];
  }

  render() {
    return (
      <Page {...this.getMetaData()}>
        <UserContainer {...this.props} />
      </Page>
    );
  }
}

export default Users;
