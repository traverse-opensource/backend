export { default as App } from 'pages/App';
export { default as Vote} from 'pages/Vote';
export { default as Dashboard } from 'pages/Dashboard';
export { default as LoginOrRegister } from 'pages/LoginOrRegister';
export { default as Login } from 'pages/Login';
export { default as UserCreation } from 'pages/UserCreation';
export { default as Users } from 'pages/Users';
export { default as Playlist} from 'pages/Playlist';
export { default as Fiche} from 'pages/Fiche';
export { default as HeartStrokes} from 'pages/Hearts';
export { default as DetailedFiche} from 'pages/DetailedFiche';
export { default as Profile} from 'pages/Profile';
export { default as NewFiche} from 'pages/NewFiche';
export { default as GroupsManager} from 'pages/GroupsManager';
export { default as StatsManager} from 'pages/Stats';
export { default as ResetPassword} from 'pages/ResetPassword';
export { default as FeedsManager} from 'pages/Feed';
