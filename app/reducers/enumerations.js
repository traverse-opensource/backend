import * as types from 'types';
import {combineReducers} from 'redux';

const enumerations = (state = [],
                action) => {
    switch (action.type) {
        case types.FETCH_ENUM:
            return action.data;
        default:
            return state;
    }
};
const selectedTags = (state = [], action) => {
    switch (action.type){
        case types.SELECTED_TAG:
            return action.data;
        case types.FLUSH_TAGS:
            return [];
        case types.INITIALISE_TAGS:
            return action.data;
        default:
            return state;
    }
};
const themesToPush = (state = [], action) => {
    switch (action.type){
        case types.ADD_THEME:
            return action.data;
        case types.FLUSH_THEMES:
            return [];
        default:
            return state;
    }
};

const enumReducer =  combineReducers({
    enumerations,
    selectedTags,
    themesToPush
});

export default enumReducer;