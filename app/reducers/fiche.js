/**
 * Created by hasj on 21/11/2016.
 */
import {combineReducers} from 'redux';
import * as types from 'types';

const fiche = (state = {},
               action) => {
    switch (action.type) {
        case types.CREATE_FICHE_REQUEST:
            return {
                _id: action.data._id,
                type: action.data.__t,
                name: action.data.name,
                date: action.data.date,
                categories: action.data.categories,
                short_description: action.data.short_description,
                presentation: action.data.presentation,
                history: action.data.history,
                technical_information: action.data.technical_information,
                more_information: action.data.more_information,
                path: action.data.path,
                cover: action.data.cover,
                attachments: action.data.attachments,
                start_date: action.data.start_date,
                end_date: action.data.end_date,
                description: action.data.description,
                schedule: action.data.schedule,
                accessibility: action.data.accessibility,
                contact: action.data.contact,
                city: action.data.city,
                country: action.data.country,
                latitude: action.data.latitude,
                longitude: action.data.longitude,
                place_of_birth: action.data.place_of_birth,
                deathday: action.data.deathday,
                short_bio: action.data.short_bio,
                created_by: action.data.created_by,
                references: action.data.references,
                sousCategories: action.sousCategories,
                themes  :action.themes,
                social: action.social,
                map : action.map,
                //avoiding side effects
                status: action.data.status
            };
        /*case types.EDIT_FICHE_REQUEST:
            if (state._id === action._id) {
                return {
                    ...state,
                    _id: action.data._id,
                    type: action.data.__t,
                    name: action.data.name,
                    categories: action.data.categories,
                    short_description: action.data.short_description,
                    presentation: action.data.presentation,
                    history: action.data.history,
                    technical_information: action.data.technical_information,
                    more_information: action.data.more_information,
                    path: action.data.path,
                    cover: action.data.cover,
                    attachments: action.data.attachments,
                    start_date: action.data.start_date,
                    end_date: action.data.end_date,
                    description: action.data.description,
                    schedule: action.data.schedule,
                    accessibility: action.data.accessibility,
                    contact: action.data.contact,
                    city: action.data.city,
                    country: action.data.country,
                    latitude: action.data.latitude,
                    longitude: action.data.longitude,
                    place_of_birth: action.data.place_of_birth,
                    deathday: action.data.deathday,
                    short_bio: action.data.short_bio,
                    references: action.data.references,
                    sousCategories: action.sousCategories,
                    themes  :action.themes,
                    social: action.social,
                    map : action.map,
                    status: action.data.status
                }
            }
            return state;*/
        default:
            return state;
    }
};

const fiches = (state = [], action) => 
{
    switch (action.type) {
        case types.LOAD_FICHES:
            //data is already sorted in action
            return action.data? action.data: state;
        //only in search state, if there is an error just send the previous state
        /*case types.ERROR_FICHES:
            return [];*/
        case types.GET_FICHES_SUCCESS:
            return action.payload;
        case types.CREATE_FICHE_REQUEST:
            //append the new one also here
            state.push(action.data);
            //return [...state, fiche({}, action)];
            return state;
        case types.CREATE_FICHE_FAILURE:
            return state.filter(f => f._id !== action._id);
        case types.EDIT_FICHE_REQUEST:
          //append the new fiche maybe ??
        //so we need to give back the result from the backend also
            let index = state.map(function(fiche) { return fiche._id; }).indexOf(action.fiche._id);
            state[index] = action.fiche;
            return state;
        case types.DESTROY_FICHE:
            return state.filter(f => f._id !== action._id);
        /*case types.FLUSH_FICHE_SEARCH:
          //avoiding side effects
            return [];*/
        default:
            return state;
    }
};

const formMessage = (
    state = '',
    action
) => {
    switch (action.type) {
        case types.CHAMPS_MANQUANTS:
        case types.CHAMPS_TOUS_REMPLIS:
            return action.message;
        default:
            return state;
    }
};
const errorSubmit = (
    state = {categories: '', basicFields: '', location: ''},
    action
) => {
    switch (action.type){
        case types.CHAMPS_MANQUANTS:
            return {...state, basicFields: action.message };
        case types.MISSING_CATEGORY:
            return {...state,categories: action.message };
        case types.MISSING_LOCATION:
            return {...state,location: action.message };
        case types.CHAMPS_TOUS_REMPLIS:
            return {...state, basicFields: '', categories: '', location: ''};
        default:
            return state;
    }
};

const displayedFiche= (
    state = '',
    action
) => {
    switch (action.type) {
        case types.ONE_FICHE:
            return action.data;
        case types.ONE_MEDIA:
            return {
                media : action.data
            };
        case  types.ONE_EVENT:
            return{
                event: action.data
            };
        case types.ONE_OBJECT:
            return{
                object: action.data
            };
        case types.ONE_PLACE:
            return{
                place: action.data
            };
        case types.ONE_PERSON:
            return{
                person: action.data
            };
        case types.FLUSH_FICHE:
            return '';
        default:
            return state;
    }
};

const uploadedFile = (
    state = null,
    action
) => {
    switch (action.type) {
        case types.UPLOAD_SUCCESS:
            return action.data;
        default:
            return null;
    }
};

const mapData = (state = {}, action) => {
      switch (action.type){
        case types.SET_MAP_DATA:
            return action.data;
        case types.FLUSH_MAP_DATA:
          return {};
        default:
            return state;
      }
  };


const ficheReducer = combineReducers({
    fiches,
    displayedFiche,
    uploadedFile,
    formMessage,
    errorSubmit,
    mapData
    //
});

export default ficheReducer;