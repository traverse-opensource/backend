import * as types from '../types';

/*
 * Message store for global messages, i.e. Network messages / Redirect messages
 * that need to be communicated on the page itself. Ideally
 * messages/notifications should appear within the component to give the user
 * more context. - My 2 cents.
 */
//TODO need to recheck what is wrong with the process to dispatch events
export default function message(
    state = {message: '', type: 'SUCCESS', error: ''},
    action = {}) {

    switch (action.type) {
        case types.LOGIN_SUCCESS_USER:
        case types.SIGNUP_SUCCESS_USER:
        case types.CREATE_USER_SUCCESS:
        case types.UPDATE_USER_SUCCESS:
        case types.UPDATE_USER_SUCCESS_SELF:
        case types.DESTROY_FICHE:
        case types.CREATE_FICHE_REQUEST:
        case types.EDIT_FICHE_REQUEST:
        case types.CHANGE_PASSWORD_REQUEST:
        case types.CHANGE_PASSWORD_SUCCESS:
        case types.DESTROY_PLAYLIST:
            return {...state, message: action.message, type: 'SUCCESS'};
        case types.DISMISS_MESSAGE:
            return {...state, message: '', type: 'SUCCESS'};
        case types.SHOW_ALERT:
        case types.EDIT_PLAYLIST_FAILURE:
            return {...state, message: action.message, type: 'ERROR'};
        case types.CHAMPS_MANQUANTS:
        case types.CREATE_PLAYLIST_FAILURE:
            return {...state, error: action.message, type: 'ERROR'};
        case types.CREATE_USER_ERROR:
        case types.CHANGE_PASSWORD_FAILED:
        case types.NO_RECORD_FOR_USER:
        case types.CHANGE_PASSWORD_REQUEST_FAILED:
        case types.ERROR_UPLOAD_FAIL:
            return {...state, error: action.message, message: action.message, type: 'ERROR'};
        default:
            return state;
    }
}
