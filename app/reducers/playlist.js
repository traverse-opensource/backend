/**
 * Created by hasj on 21/11/2016.
 */
import { combineReducers } from 'redux';
import * as types from 'types';

const playlist = (
    state = {},
    action
) => {
    switch(action.type){
        case types.CREATE_PLAYLIST_REQUEST:
            return {
                _id: action.payload._id,
                name: action.payload.name,
                description: action.payload.description,
                linkedFiches: action.payload.linkedFiches
            };
    default:
        return state;
}
};

const playlists = (
    state = [],
    action
) => {
    switch(action.type){
        case types.LOAD_PLAYLISTS:
            return action.data;
        case types.ERROR_PLAYLIST:
            return [];
        case types.CREATE_PLAYLIST_REQUEST:
            return [...state, playlist({}, action)];
        case types.DESTROY_PLAYLIST:            
            return state.filter(function(p) {
              console.error('playlistAction @playlist_destroy_case p._id, action._id', p, action);
              return (p._id !== action.id);
            });
            /*return Object.assign({}, state, {
              playlist: [
                ...state.playlist.playlists,
                {
                  show: false
                }
              ]
            })*/
        case types.EDIT_PLAYLIST:
            return state.map(p => playlist(p,action));
        default :
            return state;
    }
};

const displayedPlaylist = (
    state = '',
    action
) => {
    switch (action.type) {
        case types.PLAYLIST_LOADED_SUCESS:
            return action.payload;
        case types.EDIT_PLAYLIST:
            return action.payload;
        case types.FICHE_ADDED_TO_PLAYLIST_SUCCESS:
            return action.payload;
        case types.SELECTED_FICHE_PLAYLIST:
            return {... state, selectedFichePlaylist: action.payload};
        case types.UNSELECTED_FICHE_PLAYLIST:
            return {... state, selectedFichePlaylist: null};
        case types.SELECTED_LINK_PLAYLIST:
            return {... state, selectedLinkPlaylist: action.payload, showLinkDialog: true};
        case types.UNSELECTED_LINK_PLAYLIST:
            return {... state, selectedLinkPlaylist: null, showLinkDialog: false};
        default:
            return state;
    }
};


const ficheSearchResult = (
    state = {},
    action
) => {
    switch (action.type) {
        case types.GET_PLAYLIST_FICHES_SUCCESS:
            return action.payload;
        case types.RESET_FICHE_RESULTS:
            return {};
        default:
            return state;
    }
};

const selectedFiche = (
    state = {
        show: false,
        showReplace: false,
        data: {},
        toReplaceData: {},
        replaceByData: {}
    },
    action
) => {
    switch (action.type) {
        case types.SELECTED_PLAYLIST:
            return {
                show: true,
                showReplace: false,
                data: action.payload,
                toReplaceData: {},
                replaceByData: {}
            };
        case types.SELECTED_PLAYLIST_CANCEL:
            return {
                show: false,
                showReplace: false,
                data: {},
                toReplaceData: {},
                replaceByData: {}
            };
        case types.SELECTED_PLAYLIST_SUCCESS:
            return {
                show: false,
                showReplace: false,
                data: {},
                toReplaceData: {},
                replaceByData: {}
            };
        case types.SELECTED_FICHE_TO_REPLACE_BY_PLAYLIST:
            return {
                show: false,
                showReplace: true,
                data: {},
                toReplaceData: action.payload.toReplace,
                replaceByData: action.payload.replaceBy
            };
        case types.SELECTED_FICHE_TO_REPLACE_BY_PLAYLIST_CANCEL:
            return {
                show: false,
                showReplace: false,
                data: {},
                toReplaceData: {},
                replaceByData: {}
            };
        case types.SELECTED_FICHE_TO_REPLACE_BY_PLAYLIST_SUCCESS:
            return {
                show: false,
                showReplace: false,
                data: {},
                toReplaceData: {},
                replaceByData: {}
            };
        default:
            return state;
    }
};

const playlistReducer = combineReducers({
    playlists,
    displayedPlaylist,
    ficheSearchResult,
    selectedFiche
});

export default playlistReducer;