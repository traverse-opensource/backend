import { combineReducers } from 'redux';
import * as types from '../types';

//session user
const user = (
    state = {},
    action
) => {
    switch (action.type){
        case types.FETCH_USER:
          return action.user.user;
            /*return {
                profile: action.user.user.profile,
                email: action.user.user.email,
                group : action.user.user.group,
                entities : action.user.user.entities,
                _id : action.user.user._id
            };*/
        case types.UPDATE_USER_SUCCESS_SELF:
            /*return {
                profile: action.user.profile,
                email: action.user.email,
                group : action.user.group,
                entities : action.user.entities,
                _id : action.user._id
            };*/
            return action.user;
        case types.FLUSH_USER:
            return {};
        case types.USER_PREFS_SUCCESS:
          //checking here if we have to replace or to keep stuff
          state.prefs = action.prefs;
          return state;
        case types.USER_SHOW_PROGRESS:
          state.showProgress = true;
          return state;
        case types.USER_HIDE_PROGRESS:
          state.showProgress = false;
          return state;
        default:
            return state;
    }
};
const users = ( state = [], action ) =>
{
    switch (action.type){
        case types.FETCH_USERS:
            return action.data;
        case types.CREATE_USER_SUCCESS:
            return [...state, action.user];
        case types.UPDATE_USER_SUCCESS:
            let index = state.map(function(user) { return user._id; }).indexOf(action.user._id);
            state[index] = action.user;
            return state;
        default:
            return state;
    }
};
const groups = (state = [], action) => {
    switch (action.type){
        case types.FETCH_GROUPS:
            return action.data;
        case types.CREATE_GROUPS_SUCCESS:
            console.log(action);
            return [...state, action.data];
        case types.DELETE_GROUP_REQUEST:
            return state.filter(g => g._id !== action.id);
        default:
            return state;
    }
};

const showProgress = (
    state = false,
    action
) => {
    switch (action.type) {
        case types.USER_SHOW_PROGRESS:
            state = true;
            break;
        default:
            state = false;
            break;
    }
    return state;
};

const isLogin = (
    state = true,
    action
) => {
    switch (action.type) {
        case types.TOGGLE_LOGIN_MODE:
            return !state;
        default:
            return state;
    }
};

const message = (
    state = '',
    action
) => {
    switch (action.type) {
        case types.TOGGLE_LOGIN_MODE:
        case types.MANUAL_LOGIN_USER:
        case types.SIGNUP_USER:
        case types.LOGOUT_USER:
        case types.LOGIN_SUCCESS_USER:
        case types.SIGNUP_SUCCESS_USER:
        case types.CREATE_USER_SUCCESS:
            return '';
        case types.LOGIN_ERROR_USER:
        case types.SIGNUP_ERROR_USER:
        case types.CREATE_USER_ERROR:
            return action.message;
        default:
            return state;
    }
};

const isWaiting = (
    state = false,
    action
) => {
    switch (action.type) {
        case types.MANUAL_LOGIN_USER:
        case types.SIGNUP_USER:
        case types.CREATE_USER:
        case types.LOGOUT_USER:
        case types.UPDATE_USER:
            return true;
        case types.LOGIN_SUCCESS_USER:
        case types.SIGNUP_SUCCESS_USER:
        case types.CREATE_USER_SUCCESS:
        case types.LOGOUT_SUCCESS_USER:
        case types.LOGIN_ERROR_USER:
        case types.SIGNUP_ERROR_USER:
        case types.CREATE_USER_ERROR:
        case types.LOGOUT_ERROR_USER:
        case types.UPDATE_USER_SUCCESS:
        case types.UPDATE_USER_SUCCESS_SELF:
            return false;
        default:
            return state;
    }
};

const authenticated = (
    state = false,
    action
) => {
    switch (action.type) {
        case types.LOGIN_SUCCESS_USER:
        case types.SIGNUP_SUCCESS_USER:
        case types.LOGOUT_ERROR_USER:
            return true;
        case types.LOGIN_ERROR_USER:
        case types.SIGNUP_ERROR_USER:
        //case types.CREATE_USER_SUCCESS:
        //case types.CREATE_USER_ERROR:
        case types.LOGIN_EXPIRED_USER:
        case types.LOGOUT_SUCCESS_USER:
            return false;
        default:
            return state;
    }
};

const userReducer = combineReducers({
    isLogin,
    showProgress,
    isWaiting,
    authenticated,
    message,
    user,
    users,
    groups
});

export default userReducer;
