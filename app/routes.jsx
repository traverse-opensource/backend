import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { fetchPlaylistData, fetchFicheData, fetchUsersData , fetchGroupsData, fetchEntityData} from './fetch-data';
import { App, Dashboard, Users, Login, Fiche, Playlist, HeartStrokes, Profile, NewFiche, GroupsManager, ResetPassword, StatsManager, FeedsManager} from './pages';
import  EditFiche from './containers/editFormFiche/EditFiche.jsx';
import  Details from './containers/FicheDetails/Details.jsx';
import PlaylistEditContainer from './containers/playlist/PlaylistEditContainer.jsx';
import { config } from '../config/app';
import { autoLogout } from './actions/users';

import { RIGHTS } from './constant';
/*
 * @param {Redux Store}
 * We require store as an argument here because we wish to get
 * state from the store after it has been authenticated.
 */
export default (store) => {
    const requireAuth = (nextState, replace, callback) => {
        const { user: { authenticated }} = store.getState();
        if (!authenticated) {
            replace({
                pathname: '/login',
                state: { nextPathname: nextState.location.pathname }
            });
        }
        callback();
    };

    //TODO keep this for later on
    /*const logOutIfNeeded = (user, callback) => {
      let lastLoginDate = null;
      let { authenticated } = user;

      if (user.user) {
        lastLoginDate = user.user.lastLoginDate;
      }

      console.log("moda", user, lastLoginDate);

      //TODO, check also if user has been logged for too long
      if (!lastLoginDate || lastLoginDate.getTime() < config.app.lastUpdate) {
        autoLogout();
        authenticated = false;
        alert("hum0..." + lastLoginDate + "..." + config.app.lastUpdate + "..." + user.user.email);
      }else {
        alert("hum1..." + lastLoginDate + "..." + config.app.lastUpdate + "..." + user.user.email);
        return callback(authenticated);
      }
    }*/

    const redirectAuth = (nextState, replace, callback) => {

        const { user: { authenticated }} = store.getState();
        if (authenticated) {
            replace({
                pathname: '/fichesView'
            });
        }
        callback();
    };

    //using this route can only be achieved from a click, since we need the state to be updated
    const redirectIfHasNotEnoughPrivilege = (nextState, replace, callback) => {
        const { user } = store.getState(),
            targetUser = user.user;

        if (!RIGHTS.HAS_ENOUGH_PRIVILEGES(targetUser, null, targetUser._id ? targetUser.group: null)){
            replace({
                pathname: '/fichesView'
            });
        }

        callback();
    };

    const redirectIfNotSuperAdmin = (nextState, replace, callback) => {
        const { user } = store.getState(),
            targetUser = user.user;

        if (!RIGHTS.IS_SUPER_ADMIN(targetUser)){
            replace({
                pathname: '/fichesView'
            });
        }

        callback();
    };

    /*<Route path="login" component={Login}/>*/

    return (
        <Route path="/" component={App} >
            <IndexRoute path="" component={Login} onEnter={redirectAuth} />
            {/*Routes pour les gestions en général*/}
            <Route path="admin" onEnter={redirectIfHasNotEnoughPrivilege} >
                {/*Creation d'un utilisateur*/}
                <Route path="manage-users" component={Users} onEnter={requireAuth}
                   fetchData={fetchUsersData.fetchData}
                   fetchSuccess={fetchUsersData.fetchSuccess}
                   fetchError={fetchUsersData.fetchError} />
                {/*Gestion des groupes*/}
                <Route path="manage-groups" component={GroupsManager}
                       fetchData={fetchEntityData.fetchData}
                       fetchSuccess={fetchEntityData.fetchSuccess}
                       fetchError={fetchEntityData.fetchError}/>
                {/*Voir différentes statistiques*/}
                <Route path="stats" component={StatsManager}
                       onEnter={redirectIfNotSuperAdmin}
                       fetchData={fetchEntityData.fetchData}
                       fetchSuccess={fetchEntityData.fetchSuccess}
                       fetchError={fetchEntityData.fetchError}/>
                <Route path="manage-feeds" component={FeedsManager}
                      fetchData={fetchEntityData.fetchData}
                      fetchSuccess={fetchEntityData.fetchSuccess}
                      fetchError={fetchEntityData.fetchError}/>
            </Route>
            {/*Routes des fiches*/}
            <Route path="fichesView" onEnter={requireAuth} >
                {/*Composant principal*/}
                <IndexRoute component={Fiche}
                            //fetchData={fetchFicheData.fetchData}
                            fetchSuccess={fetchFicheData.fetchSuccess}
                            fetchError={fetchFicheData.fetchError}/>
                {/*Details*/}
                <Route path="people/:ficheId" component={Details}/>
                <Route path="media/:ficheId" component={Details}/>
                <Route path="objects/:ficheId" component={Details}/>
                <Route path="places/:ficheId" component={Details}/>
                <Route path="events/:ficheId" component={Details}/>
                {/*Formulaires de creation*/}
                <Route path="fichePlace" component={NewFiche}/>
                <Route path="fichePersonne" component={NewFiche}/>
                <Route path="ficheEvenement" component={NewFiche}/>
                <Route path="ficheObjet" component={NewFiche}/>
                <Route path="ficheMedia" component={NewFiche}/>
                {/*Formulaires d edition*/}
                <Route path="editPersonne/:ficheId" component={EditFiche} /> {/*onEnter={redirectIfHasNotEnoughPrivilege}*/}
                <Route path="editEvenement/:ficheId" component={EditFiche}/>
                <Route path="editObjet/:ficheId" component={EditFiche}/>
                <Route path="editPlace/:ficheId"  component={EditFiche}/>
                <Route path="editMedia/:ficheId"  component={EditFiche}/>
            </Route>

            <Route path="playlistsView" onEnter={requireAuth}>
                <IndexRoute component={Playlist}
                            fetchData={fetchPlaylistData.fetchAllData}
                            fetchSuccess={fetchPlaylistData.fetchAllDataSuccess}
                            fetchError={fetchPlaylistData.fetchError}/>
                <Route path="playlist/:id" component={PlaylistEditContainer}/>
            </Route>
            <Route path="heartsView" onEnter={requireAuth}>
                <IndexRoute component={HeartStrokes}
                            fetchData={fetchPlaylistData.fetchAllData}
                            fetchSuccess={fetchPlaylistData.fetchAllDataSuccess}
                            fetchError={fetchPlaylistData.fetchError}/>
                <Route path="playlist/:id" component={PlaylistEditContainer}/>
            </Route>
            <Route path="login" component={Login} onEnter={redirectAuth}/>
            <Route path="dashboard" component={Dashboard} onEnter={requireAuth} />
            <Route path="profile" component={Profile} onEnter={requireAuth} />
            <Route path="utilisateurs" component={Users} onEnter={requireAuth}
                   fetchData={fetchUsersData.fetchData}
                   fetchSuccess={fetchUsersData.fetchSuccess}
                   fetchError={fetchUsersData.fetchError} />

            <Route path="resetPassword/:token" component={ResetPassword}/>
        </Route>
    );
};
