import axios from 'axios';
const entityService = {
    getEntites: () => axios.get('/entities')
        .then(
            (response) =>
            {
                return response.data;
            }
        )
};
export default entityService;
