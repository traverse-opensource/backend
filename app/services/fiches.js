/**
 * Created by hasj on 02/12/2016.
 */
import axios from 'axios';
const ficheService = {
    getFiches: () => axios.get('/fiches')
        .then(
            (response) =>
            {
                return response.data;
            }
        )
};
export default ficheService;
