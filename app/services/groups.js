import axios from 'axios';
const groupService = {
    getGroups: () => axios.get('/groups')
        .then(
            (response) =>
            {
                return response.data;
            }
        )
};
export default groupService;
