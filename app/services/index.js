export { default as voteService } from './topics';
export { default as playlistService } from './playlists';
export { default as ficheService } from './fiches';
export { default as userService } from './users';
export { default as groupService } from './groups';
export { default as entityService } from './entity';