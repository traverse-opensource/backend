
import axios from 'axios';
const userService = {
    getUsers: () => axios.get('/users')
        .then(
            (response) =>
            {
                return response.data;
            }
        )
};
export default userService;
