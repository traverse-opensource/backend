const getMatchedRoute = ({ routes }) => routes[routes.length - 1];

export default getMatchedRoute;