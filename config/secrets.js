/** Important **/
/** You should not be committing this file to GitHub **/
/** Repeat: DO! NOT! COMMIT! THIS! FILE! TO! YOUR! REPO! **/
export const sessionSecret = process.env.SESSION_SECRET || 'Your Session Secret goes here';

//TODO make this stuff clearer, since process.env are environment variables that are given on runtime!
//TODO we need to make the process detect if we are in production server nor development one using the same mecanisme for the database detection

export const google = {
  clientID: process.env.GOOGLE_CLIENTID || 'Your google clientID goes here',
  clientSecret: process.env.GOOGLE_SECRET || 'Your google secret',
  callbackURL: process.env.GOOGLE_CALLBACK || '/auth/google/callback',
  mapKey: 'your google mapkey'
};

export const facebook = {
  clientID: process.env.FACEBOOK_CLIENTID || 'Your facebook clientID goes here',
  clientSecret: process.env.FACEBOOK_SECRET || 'Your facebook secret',
  callbackURL: process.env.FACEBOOK_CALLBACK || '/auth/facebook/callback'
};

export const twitter = {
  clientID: process.env.FACEBOOK_CLIENTID || 'Your twitter clientID goes here',
  clientSecret: process.env.FACEBOOK_SECRET || 'Your twitter secret',
  callbackURL: process.env.FACEBOOK_CALLBACK || '/auth/twitter/callback'
};

