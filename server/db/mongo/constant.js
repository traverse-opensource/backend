/**
 * Back end constants
 */

export const MIXIN_TYPES = {
  FICHE: "fiche",
  PLAYLIST : "playlist"
};
export const status = {
  DRAFT: 0,
  PUBLISHED: 1,
  DELETED: 2
};

export const SERVER_HOSTS = {
  LOCAL: 0,
  MT: 1,
  TRAVERSE: 2
};

export const permission = {CONTRIBUTEUR:'contributeur', ADMIN: 'admin', SUPER_ADMIN:'super_admin', DEACTIVATED: 'deactivated', NOMAD: 'nomad'};

export const RIGHTS = {
  //checking inside this method if the params is a string or a user object
  //toCompare can be an instance of fiche nor an instance of playlist
  //the matching item would be created_by props so that we don't need to to check the instance of the comparing object
  HAS_ENOUGH_PRIVILEGES: (userOrUserId, toCompare, group = null) => {

    let toReturn = false;
    //since first call has not filled created_by column yet, we need to return at least false
    if (toCompare) {
      let matchUserId = userOrUserId._id ? userOrUserId._id: userOrUserId,
          matchComparedUserId = toCompare._id ? toCompare._id: toCompare;

      toReturn = RIGHTS.COMPARE_GROUP(group, matchUserId === matchComparedUserId);

      return toReturn;
    }

    toReturn = RIGHTS.COMPARE_GROUP(group, toReturn);

    return toReturn;
  },

  COMPARE_GROUP: (group, previousResult = false) => {

    if (group !== null){
      previousResult |= group.title === permission.SUPER_ADMIN || group.title == permission.ADMIN;
    }

    return previousResult;
  }
};

export const PAGINATION_DEFAULT = {
  PAGE: 1,
  LIMIT: 18
};

export const LOCATION_DEFAULT = {
  //Geneva location default
  LAT: 46.2050579,
  LNG: 6.1265361,
  MAX_DISTANCE: 40000
};

export const HASH_TYPES = [
  "Event",
  "Media",
  "Objet",
  "People",
  "Places"
]
