//Default detabase for development
import { SERVER_HOSTS } from './constant';
let toExport = process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://localhost/ReactWebpackNode';

const argv = require('minimist')(process.argv.slice(2));

//no need actually but easier to test
process.env.REAL_PRODUCTION = false;
process.env.TRAVERSE_CUSTOM_HOST = SERVER_HOSTS.LOCAL;

if (argv['real-production']){
  //Database in real production server which is https://traverse-patrimoines.com:8000
  toExport = process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://PRODUCTION_MONGO_ADDRESS_HERE';
  process.env.TRAVERSE_CUSTOM_HOST = SERVER_HOSTS.TRAVERSE;
}

export const db = toExport;

export default {
  db
};
