import Category from '../models/categories';

/**
 * List
 */
export function all(req, res) {
    Category.find({}).exec((err, categories) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(categories);
    });
}

export function add (req, res){

    let category = new Category({
        name: req.body.name,
        description: req.body.description
    });
    category.save((err, category) =>{
        if(err){
            res.status(400).send("category not added")
        }
        return res.json(category);
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Category.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("category removed successfully");
    })

}
export function update(req, res) {
    const query = {_id : req.params.id};
    Category.findOneAndUpdate( query, req.body, (err) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        return res.status(200).send('category updated');
    })
}

export function one(req, res){
    const query = {_id : req.params.id};
    Category.findOne(query, (err, category)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(category);
    })
}

export default{
    all,
    one,
    add,
    remove,
    update
}
