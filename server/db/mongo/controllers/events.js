import Event from '../models/events';
import Category from '../models/categories';
import Type from '../models/types';
import Tag from '../models/tags';
import User from '../models/users';
import TagController from './tags';
import FicheController from './fiches';

/**
 * List
 */

export function all(req, res) {
    Event.find({}).exec((err, events) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(events);
    });
}

export function add (req, res){
    let catId = req.body.categories ? req.body.categories.map((category) => {return category}) : [];
    let categoriesArray = [];
    let typeId = req.body.types ? req.body.types.map((type) => {return type}) : [];
    let typesArray = [];
    let tagsId =  req.body.tags? req.body.tags.map((tag) => {return tag}) : [];
    let tagsArray = [];
    User.findOne({_id : req.body._id}).exec((err, user) => {
        if (err) {
            console.log('Error in add category to fiche query');
            return res.status(500).send('Something went wrong getting the data category for fiche');
        }
        Category.find({_id: {$in : catId}}).exec((err, categories)=> {
            if (err) {
                console.log('Error in add category to fiche query');
                return res.status(500).send('Something went wrong getting the data category for fiche');
            }
            categoriesArray = categories;
            Type.find({_id: {$in: typeId}}).exec((err, types) => {
                if (err) {
                    console.log('Error in add type to fiche query');
                    return res.status(500).send('Something went wrong getting the data type for fiche');
                }
                typesArray = types;
                Tag.find({_id: {$in: tagsId}}).exec((err, tags) => {
                    if (err) {
                        console.log('Error in add tag to fiche query');
                        return res.status(500).send('Something went wrong getting the data Tag for fiche');
                    }
                    tagsArray = tags;
                  
                    FicheController.updateGeoPositionIndexIfNeeded(req);
                    const body = req.body;
                  
                    let event = new Event({
                        name: body.name,
                        date: body.date,
                        status: body.status,
                        created_by : user,
                        start_date: body.start_date,
                        end_date: body.end_date,
                        delta_start: body.delta_start,
                        delta_end: body.delta_end,
                        presentation: body.presentation,
                        description: body.description,
                        cover: body.cover,
                        categories: categoriesArray,
                        types: typesArray,
                        tags: tagsArray,
                        related_fiches: body.related_fiches,
                        references: body.references,
                        sousCategories: body.sousCategories,
                        themes: body.themes,
                        social: body.social,
                        country: body.country,
                        latitude: body.latitude,
                        longitude: body.longitude,
                        map: body.map,
                        location: body.location
                    });
                    event.save((err, event) => {
                        if (err) {
                            res.status(400).send(JSON.stringify(err))
                        }
                        TagController.countCallback(tagsArray, 0, []);
                      
                        return FicheController.searchForCreator(req, res, event, body._id);
                        //return res.json(event);
                    })
                })
            })
        })
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Event.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("event removed successfully");
    })

}
export function update(req, res) {
  return FicheController.update(req, res, 0);
}

export function one(req, res){
    const query = {_id : req.params.id};
    Event.findOne(query, (err, event)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(event);
    })
}

export function search(req, res, next) {
  console.log("EventController @delegateSearch > begin the battle");
  return FicheController.search(req, res, next, 0);
}

export default{
    all,
    one,
    add,
    remove,
    update,
    search
}
