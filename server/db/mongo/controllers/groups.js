import _ from 'lodash';
import Group from '../models/groups';
import { permission } from '../constant';

/**
 * List
 */
export function all(req, res) {
  let showAll = false;
  
  if (req.query && req.query.all) {
    showAll = req.query. all;
  }
  let query = showAll ?
      {}:
      { title: {$ne: permission.DEACTIVATED} }
  Group.find(query).exec((err, groups) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(groups);
  });
}

/**
 * Add a Group
 */
export function add(req, res) {
  Group.create(req.body, (err,group) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    return res.status(200).send(group);
  });
}

/**
 * Update a Group
 */
/*export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  Group.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}*/

/**
 * Remove a Group
 */
/*export function remove(req, res) {
  const query = { _id: req.params.id };
  console.log(query);
  Group.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }
    console.log(query);
    return res.status(200).send('Removed Successfully');
  });
}*/

export default {
  all,
  add/*,
  update,
  remove*/
};
