import HeartStroke from '../models/heartstrokes'
import Fiche from '../models/fiches'
import Playlist from '../models/playlists'
import FicheController from './fiches'
import {CONSTANTS} from '../static';

const DB_REFRESH = 250;

function handleError(req, res, customMessage, err, clientMessage) {
  console.log(customMessage, err);
  return res.status(500).send({message: clientMessage});
}

export function all(req, res, next, callback = null) {
    HeartStroke.find({ status: CONSTANTS.STATUS.PUBLISHED }).exec((err, results) => {
        if (err) {
          return handleError(req, res, 'Error in getting the heartstrokes -> function /all', err, 'Something went wrong getting the themes list');
        }
      
        //since the ficheid and the playlistid are auto populated, we just need to unwind the result
        let toReturn = results
          .sort((a, b) => {
            return a.order > b.order;
          })
          .map((item) => {
            return item.collection_type === CONSTANTS.TYPES.FICHE_TYPE ?
              item.fiche_id: item.playlist_id;
          });
      
        if (callback) {
          return callback(toReturn);
        }else {
          return res.json(toReturn);  
        }
    })
}

//don't give a shit if a fiche nor a playlists is already a heartstrokes
export function add(req, res, next) {
  let {fiche_id, playlist_id} = req.body;
  
  console.log(req.body);
  
  //find all items to get the total
  return all(req, res, next, (mixins) => {
    let total = mixins.length + 1;
    
    //side effects
    if (fiche_id || playlist_id) {
      let toAdd = fiche_id ?
          new HeartStroke({
            collection_type: CONSTANTS.TYPES.FICHE_TYPE,
            fiche_id: fiche_id,
            status: CONSTANTS.STATUS.PUBLISHED,
            order: total
          }):
          new HeartStroke({
            collection_type: CONSTANTS.TYPES.PLAYLIST_TYPE,
            playlist_id: playlist_id,
            status: CONSTANTS.STATUS.PUBLISHED,
            order: total
          });

      return toAdd.save((err) => {
        if (err) {
          return handleError(req, res, "@HearstrokesController > add, err: ", err, "Something is wrong in Heartstrokes")
        }

        setTimeout(() => {
          return all(req, res);
        }, DB_REFRESH);
      });  
    } else {
      return all(req, res);
    }
  });
}

//need {objectId: playlist_id/fiche_id and order} for both
//this function can only work if the order integrity is not broken
export function move(req, res) {
  
  let { hearts } = req.body;
  
  //should have at least 2 elements in query response other send alert
  if (hearts && hearts.length == 2) {
    //copy array to new one
    
    const query = [];
    
    //fucking side effect on delete attribute ! even if we d'ont use map nor sort native functions on an array if will affect it ...
    for (let i = 0, currentHeart = null; i < hearts.length; ++i) {
      currentHeart = hearts[i];
      query.push(currentHeart.fiche_id? {fiche_id: currentHeart.fiche_id}: {playlist_id: currentHeart.playlist_id});
    }

    HeartStroke.find({$or: query}, (err, mixins) => {
      if (err) {
        return handleError(req, res, "@HeartStrokeController > move > ", err, "Something went wrong in HeartStrokes");
      }

      mixins.map((mixin) => {
        let targetType = mixin.collection_type === CONSTANTS.TYPES.FICHE_TYPE?
            "fiche_id": "playlist_id";
        let yolo = hearts.map((heart) => {
          return heart["fiche_id"]? heart["fiche_id"]: heart["playlist_id"];
        })
        
        let index = yolo.indexOf(mixin[targetType]._id.toString());
        
        //avoids side effect
        if (index !== -1) {
          mixin.order = hearts[index].order;
          mixin.save();
        }
        return mixin;
      });

      //shall be sent with the newly udpated list
      setTimeout(() => {
        return all(req, res);  
      }, DB_REFRESH);
    });
  } else {
    return all(req, res);
  }
}

//can only use this route if we are super admin (only available in editorial platform)
export function remove(req, res, next) {
  //just take care of the order here
  
  let { order } = req.body;
  
  if (order) {
    HeartStroke.findOne({order: order}, (err, mixin) => {
      if (err) {
        return handleError(req, res, "@HeartStrokeController > remove > err: ", err, "Something went wrong in getting the data");
      }
      
      mixin.remove((err) => {
        //recomputer orders
        //return all(req, res, next, (mixins) => {
        return HeartStroke.find({}, (err, mixins) => {
          
          //reorder
          mixins.sort((a, b) => {
            return a.order > b.order;
          });
          
          //set index if bad one
          for (let i = 0, len = mixins.length, currentMixin = null; i < len; i++) {
            currentMixin = mixins[i];
            
            if (currentMixin.order != i + 1) {
              currentMixin.order = i + 1;
              currentMixin.save();
            }
          }
          
          setTimeout(() => {
            return all(req, res);
          }, DB_REFRESH)
          
        });
      });
    })
  }else {
    return all(req, res);
  }
}

function retrievePlaylists(req, res, next, auxList) {
  let q = req.query.q;
  return Playlist.find({$or: [{name: new RegExp(q, "i")}, {description: new RegExp(q, "i")}]}, (err, playlists) => {
    if (err) {
      return handleError(req, res, "@HeartstrokesController > searchPlaylists, err: ", err, "Something went wrong with this fucking data");
    }
    
    playlists.forEach((p) => {
      if (p.status === CONSTANTS.STATUS.PUBLISHED) {
        auxList.push(p);  
      }
    })
    
    return res.json(auxList);
  });
}

//will search through fiche and playlist
export function search(req, res, next, index = 0, auxList = []) {
  let maxFicheIndex = 5;
  req.query.q = req.params.q;
  req.query.forceStatus = true;
  return FicheController.search(req, res, next, index, (fiches) => {
    if (index < 5) {
      
      fiches.forEach((fiche) => {
        auxList.push(fiche);
      })
      
      return search(req, res, next, ++index, auxList);
    }else {
      return retrievePlaylists(req, res, next, fiches);
      //return res.json(fiches);
    }
  })
} 

export default{
  all,
  add,
  move,
  remove,
  search
}
