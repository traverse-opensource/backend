import topics from './topics'
import users from './users'
import fiches from './fiches'
import playlists from './playlists'
import categories from './categories'
import events from './events'
import media from './media'
import objects from './objects'
import places from './places'
import people from './people'
import types from './types'
import tags from './tags'
import themes from './themes.js'
import heartstrokes from './heartstrokes.js'
import groups from './groups.js'
import entity from './entity'
import slugs from './slugs'

//TODO check why we need this twice ?
export { topics, users, fiches, playlists, categories, events, media, objects, places, people ,types, tags, themes, heartstrokes, groups, entity, slugs};

export default {
  topics,
  users,
  fiches,
  playlists,
  categories,
  events,
  media,
  objects,
  places,
  people,
  types,
  tags,
  themes,
  heartstrokes,
  groups,
  entity,
  slugs
};
