import Media from '../models/media';
import Category from '../models/categories';
import Type from '../models/types';
import Tag from '../models/tags';
import TagController from './tags';
import FicheController from './fiches';

/**
 * List
 */
export function all(req, res) {
    Media.find({}).exec((err, medias) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(medias);
    });
}

export function add (req, res){
    let catId = req.body.categories ? req.body.categories.map((category) => {return category}) : [];
    let categoriesArray = [];
    let typeId = req.body.types ? req.body.types.map((type) => {return type}) : [];
    let typesArray = [];
    let tagsId =  req.body.tags? req.body.tags.map((tag) => {return tag}) : [];
    let tagsArray = [];
    Category.find({_id: {$in : catId}}).exec((err, categories)=> {
        if (err) {
            console.log('Error in add category to fiche query');
            return res.status(500).send('Something went wrong getting the data category for fiche');
        }
        categoriesArray = categories;
        Type.find({_id: {$in: typeId}}).exec((err, types) => {
            if (err) {
                console.log('Error in add type to fiche query');
                return res.status(500).send('Something went wrong getting the data type for fiche');
            }
            typesArray = types;
            Tag.find({_id: {$in: tagsId}}).exec((err, tags) => {
                if (err) {
                    console.log('Error in add tag to fiche query');
                    return res.status(500).send('Something went wrong getting the data Tag for fiche');
                }
                tagsArray = tags;
                
                FicheController.updateGeoPositionIndexIfNeeded(req);
              
                var body = req.body;
                let media = new Media({
                    name: body.name,
                    status: body.status,
                    date: body.date,
                    delta_start: body.delta_start,
                    path: body.path,
                    cover: body.cover,
                    description: body.description,
                    presentation: body.presentation,
                    attachments: body.attachments,
                    categories: categoriesArray,
                    types: typesArray,
                    tags: tagsArray,
                    related_fiches: body.related_fiches,
                    created_by : body._id,
                    references: body.references,
                    sousCategories: body.sousCategories,
                    themes: body.themes,
                    social: body.social,
                    country: body.country,
                    latitude: body.latitude,
                    longitude: body.longitude,
                    map: body.map,
                    location: body.location
                });
                media.save((err, media) => {
                    if (err) {
                        res.status(400).send("media not added")
                    }
                    TagController.countCallback(tagsArray, 0, []);
                  
                    return FicheController.searchForCreator(req, res, media, body._id);
                    //return res.json(media);
                })
            })
        })
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Media.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("media removed successfully");
    })

}
export function update(req, res) {
  return FicheController.update(req, res, 1);
}

export function one(req, res){
    const query = {_id : req.params.id};
    Media.findOne(query, (err, media)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(media);
    })
}

export function search(req, res, next) {
  return FicheController.search(req, res, next, 1);
}

export default{
    all,
    one,
    add,
    remove,
    update,
    search
}
