import Objet from '../models/objects';
import Category from '../models/categories';
import Type from '../models/types';
import Tag from '../models/tags';
import TagController from './tags';
import FicheController from './fiches';

//TODO ask were we have a global function, the one we can use every where
//Since we might need this kind of function later or maybe use _underscore


/**
 * List
 */
export function all(req, res) {
    Objet.find({}).exec((err, objects) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(objects);
    });
}

export function add (req, res){
    let catId = req.body.categories ? req.body.categories.map((category) => {return category}) : [];
    let categoriesArray = [];
    let typeId = req.body.types ? req.body.types.map((type) => {return type}) : [];
    let typesArray = [];
    let tagsId =  req.body.tags? req.body.tags.map((tag) => {return tag}) : [];
    let tagsArray = [];
    let body = req.body;
    Category.find({_id: {$in : catId}}).exec((err, categories)=> {
        if (err) {
            console.log('Error in add category to fiche query');
            return res.status(500).send('Something went wrong getting the data category for fiche');
        }
        categoriesArray = categories;
        Type.find({_id: {$in: typeId}}).exec((err, types) => {
            if (err) {
                console.log('Error in add type to fiche query');
                return res.status(500).send('Something went wrong getting the data type for fiche');
            }
            typesArray = types;
            Tag.find({_id: {$in: tagsId}}).exec((err, tags) => {
                if (err) {
                    console.log('Error in add tag to fiche query');
                    return res.status(500).send('Something went wrong getting the data Tag for fiche');
                }
                tagsArray = tags;
              
                FicheController.updateGeoPositionIndexIfNeeded(req);
              
                var body = req.body;
                let objet = new Objet({
                    name: body.name,
                    status: body.status,
                    date: body.date,
                    delta_start: body.delta_start,
                    short_description: body.short_description,
                    presentation: body.presentation,
                    history: body.history,
                    technical_information: body.technical_information,
                    more_information: body.more_information,
                    cover: body.cover,
                    categories: categoriesArray,
                    types: typesArray,
                    tags: tagsArray,
                    related_fiches: body.related_fiches,
                    created_by : body._id,
                    references: body.references,
                    sousCategories: body.sousCategories,
                    themes: body.themes,
                    social: body.social,
                    country: body.country,
                    latitude: body.latitude,
                    longitude: body.longitude,
                    map: body.map,
                    location: body.location
                });
                objet.save((err, objet) => {
                    if (err) {
                        res.status(400).send("objet not added")
                    }
                    TagController.countCallback(tagsArray, 0, []);
                    return FicheController.searchForCreator(req, res, objet, body._id);
                    //return res.json(objet);
                })
            })
        })
    })
}

export function remove(req, res){
    const query = { _id : req.params.id};
    Objet.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("objet removed successfully");
    })

}

export function update(req, res) {
  return FicheController.update(req, res, 2);
}

export function one(req, res){
    const query = {_id : req.params.id};
    Objet.findOne(query, (err, objet)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(objet);
    })
}

export function search(req, res, next) {
  return FicheController.search(req, res, next, 2);
}

export default{
    all,
    one,
    add,
    remove,
    update,
    search
}
