import People from '../models/people';
import Category from '../models/categories';
import Type from '../models/types';
import Tag from '../models/tags';
import TagController from './tags';
import FicheController from './fiches';

/**
 * List
 */
export function all(req, res) {
    People.find({}).exec((err, people) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }

        return res.json(people);
    });
}

export function add (req, res){
    let catId = req.body.categories ? req.body.categories.map((category) => {return category}) : [];
    let categoriesArray = [];
    let typeId = req.body.types ? req.body.types.map((type) => {return type}) : [];
    let typesArray = [];
    let tagsId =  req.body.tags? req.body.tags.map((tag) => {return tag}) : [];
    let tagsArray = [];
    Category.find({_id: {$in : catId}}).exec((err, categories)=> {
        if (err) {
            console.log('Error in add category to fiche query');
            return res.status(500).send('Something went wrong getting the data category for fiche');
        }
        categoriesArray = categories;
        Type.find({_id: {$in: typeId}}).exec((err, types) => {
            if (err) {
                console.log('Error in add type to fiche query');
                return res.status(500).send('Something went wrong getting the data type for fiche');
            }
            typesArray = types;
            Tag.find({_id: {$in: tagsId}}).exec((err, tags) => {
                if (err) {
                    console.log('Error in add tag to fiche query');
                    return res.status(500).send('Something went wrong getting the data Tag for fiche');
                }
                tagsArray = tags;
              
                FicheController.updateGeoPositionIndexIfNeeded(req);
              
                var body = req.body;
                let person = new People({
                    name: body.name,
                    status: body.status,
                    surname: body.surname,
                    date: body.date,
                    birthday: body.birthday,
                    deathday: body.deathday,
                    delta_start: body.delta_start,
                    delta_end: body.delta_end,
                    place_of_birth: body.place_of_birth,
                    short_bio: body.short_bio,
                    presentation: body.presentation,
                    cover: body.cover,
                    categories: categoriesArray,
                    types: typesArray,
                    tags: tagsArray,
                    related_fiches: body.related_fiches,
                    created_by : body._id,
                    references: body.references,
                    sousCategories: body.sousCategories,
                    themes: body.themes,
                    social: body.social,
                    country: body.country,
                    latitude: body.latitude,
                    longitude: body.longitude,
                    map: body.map,
                    location: body.location
                });
                person.save((err, person) => {
                    if (err) {
                        res.status(400).send("Person not added")
                    }
                    TagController.countCallback(tagsArray, 0, []);
                  
                    return FicheController.searchForCreator(req, res, person, body._id);
                    //return res.json(person);
                })
            })
        })
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    People.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("person removed successfully");
    })

}
export function update(req, res) {
  return FicheController.update(req, res, 3);
}

export function one(req, res){
    const query = {_id : req.params.id};
    People.findOne(query, (err, person)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(person);
    })
}

export function search(req, res, next) {
  return FicheController.search(req, res, next, 3);
}

export default{
    all,
    one,
    add,
    remove,
    update,
    search
}
