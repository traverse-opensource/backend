import Places from '../models/places';
import Category from '../models/categories';
import Type from '../models/types';
import Tag from '../models/tags';
import TagController from './tags';
import FicheController from './fiches';
/**
 * List
 */
export function all(req, res) {
    Places.find({}).exec((err, places) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data ', err);
        }

        return res.json(places);
    });
}

export function add (req, res){
    let catId = req.body.categories ? req.body.categories.map((category) => {return category}) : [];
    let categoriesArray = [];
    let typeId = req.body.types ? req.body.types.map((type) => {return type}) : [];
    let typesArray = [];
    let tagsId =  req.body.tags? req.body.tags.map((tag) => {return tag}) : [];
    let tagsArray = [];
    Category.find({_id: {$in : catId}}).exec((err, categories)=>{
        if (err) {
            console.log('Error in add category to fiche query');
            return res.status(500).send('Something went wrong getting the data category for fiche',err);
        }
        categoriesArray = categories;
        Type.find({_id : {$in : typeId}}).exec((err, types) => {
            if (err) {
                console.log('Error in add type to fiche query');
                return res.status(500).send('Something went wrong getting the data type for fiche',err);
            }
            typesArray = types;
            Tag.find({_id: {$in: tagsId}}).exec((err, tags) => {
                if (err) {
                    console.log('Error in add tag to fiche query');
                    return res.status(500).send('Something went wrong getting the data Tag for fiche',err);
                }
                tagsArray = tags;
                
                FicheController.updateGeoPositionIndexIfNeeded(req);
              
                var body = req.body;
                let place = new Places({
                    name: body.name,
                    status: body.status,
                    categories: categoriesArray,
                    types: typesArray,
                    tags: tagsArray,
                    short_description: body.short_description,
                    presentation: body.presentation,
                    history: body.history,
                    technical_information: body.technical_information,
                    more_information: body.more_information,
                    schedule: body.schedule,
                    accessibility: body.accessibility,
                    contact: body.contact,
                    city: body.city,
                    country: body.country,
                    latitude: body.latitude,
                    longitude: body.longitude,
                    cover: body.cover,
                    related_fiches: body.related_fiches,
                    created_by : body._id,
                    references: body.references,
                    sousCategories: body.sousCategories,
                    themes : body.themes,
                    social: body.social,
                    map: body.map,
                    location: body.location
                });
                place.save((err, place) => {
                    if (err) {
                        res.status(400).send(err)
                    }
                    TagController.countCallback(tagsArray, 0, []);
                  
                    return FicheController.searchForCreator(req, res, place, body._id);
                    //return res.json(place);
                })
            });
        })
    });
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Places.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason',err);
        }
        return res.status(200).send("place removed successfully");
    })

}
export function update(req, res) {
  return FicheController.update(req, res, 4);
}

export function one(req, res){
    const query = {_id : req.params.id};
    Places.findOne(query, (err, place)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data',err);
        }
        return res.json(place);
    })
}

export function search(req, res, next) {
  return FicheController.search(req, res, next, 4);
}

export default{
    all,
    one,
    add,
    remove,
    update,
    search
}
