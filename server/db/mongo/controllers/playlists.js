/**
 * Created by hasj on 21/11/2016.
 */
import Playlist from '../models/playlists';
import LinkedFiche from '../models/linkedFiches';
import Fiche from '../models/fiches';
import User from '../models/users';
import Group from '../models/groups';

import { PAGINATION_DEFAULT, status, permission } from '../constant';
import { CONSTANTS, createSkipQuery } from '../static';
/**
 * List
 */
//TODO, need to check if the user has enough privileges to edit a playlist nor user exists while creating or updating a playlist

function userHasEnoughPrivileges(userID){
  return false;
}

const GROUP_CONST = {
  ALL: "all",
  ADMIN: "admins",
  CONTRIB: "contrib",
  MINE: "mine",
  NOMAD: "nomad"
};


function findChoosenGroups(group, callback) {

  let groupNames = [];

  //default admins
  switch(group) {
    case GROUP_CONST.CONTRIB:
      groupNames = [ permission.CONTRIBUTEUR ];
      break;
    case GROUP_CONST.NOMAD:
      groupNames = [ permission.NOMAD ];
      break;
    default:
      groupNames = [ permission.ADMIN, permission.SUPER_ADMIN ];
      break;
  }

  let groupQuery = { title: { $in: groupNames }};

  return Group.find(groupQuery, (err, groups) => {
    if (err){
      console.log("@PlaylistController > findChoosenGroups: cannot retrieve groups", err);
      return callback(err, null);
    }

    return callback(null, groups.map((group) => {
      return group._id;
    }));
  });
}

function findPlaylists(req, res, extras) {

  let query = extras.query;

  if (extras.userIds) {
    query.created_by = { $in: extras.userIds };
  }

  let toExec = Playlist.find(query)
    .populate([{
      path:'linkedFiches',
      model: 'LinkedFiche',
    },{
      path: 'created_by',
      model: 'User'
    }])
    .exec((err, playlists ) => {
      if (err) {
        console.log('Error in first query');
        return res.status(500).send('Something went wrong getting the data');
      }

      let toReturn = playlists.sort((a, b) => {
        let dateA = new Date(a.created_at).getTime();
        let dateB = new Date(b.created_at).getTime();
        if(a.status==b.status){
            return dateB - dateA;
        }
        if(a.status==status.PUBLISHED){
            return -1
        }
        if(b.status==status.PUBLISHED){
            return 1
        }
      });



      return res.json(toReturn.slice(extras.skipQuery, extras.skipQuery + extras.limit));
  });
}

function findInnerAll(req, res, extras) {

  let groupQuery = extras.groupIds? { group: { $in:  extras.groupIds}}: null;

  console.log("at innerAll query", extras.query);

  //don't need to seek user if we don't have groups
  if (groupQuery) {
    User.find(groupQuery, (err, users) => {
      if (err) {
        console.log("@PlaylistController > findInnerAll: issue on getting users", err);
        return res.status(500).send("Unable to process");
      }

      extras.userIds = users.map((user) => {
        return user._id;
      });

      return findPlaylists(req, res, extras);
    });
  }else {
    return findPlaylists(req, res, extras);
  }
  //var userIds = db.users.find({group: {$in:[ObjectId("593e44d548c8989054726c7c"), ObjectId("593e44e948c8989054726c86")]}}).map(function(user){ return  user._id})
  //ObjectId("58fa1725421315609ed8b7ab")
}

export function all(req, res, next) {

  let query = { status: { $ne: status.DELETED }},
    body = req.query,
    limit = PAGINATION_DEFAULT.LIMIT,
    page = body.page? parseInt(body.page): PAGINATION_DEFAULT.PAGE,
    skipQuery = createSkipQuery(page, limit),
    userGroup = body.group,
    currentUser = body.userId;

    if (userGroup === GROUP_CONST.ALL) {
      return findInnerAll(req, res, { limit, skipQuery, query });
    } else if (userGroup === GROUP_CONST.MINE){
      query.created_by = currentUser;
      return findInnerAll(req, res, { limit, skipQuery, query });
    }else {
      console.log("in good condition, before findChoosenGroups", userGroup);
      return findChoosenGroups(userGroup, (err, groupIds) => {
        return findInnerAll(req, res, { limit, skipQuery, query, groupIds });
      });
    }
}

export function add(req, res){
    let {body} = req,
        playlist = new Playlist({
        name: body.name,
        description: body.description,
        created_by: body.created_by
    });

    Fiche.findOne({_id: body.created_by}, (err, author) => {
      if (err){
        res.status(500).send("user does not exists");
      }

      playlist.save((err) => {
        if(err){
            console.log(err);
            res.status(500).send("playlist not created");
        }
        Playlist.populate(playlist, {path:"linkedFiches", model:'LinkedFiche'}, function(err, playlist){
            if(err){
                res.status(400).send("Populating failed");
            }
            return res.json(playlist);
        });
      });
    });
}

export function swap(req, res) {
    Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
        if (err) {
            res.status(500).send("playlist not found");
        }

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            var id1 = -1;
            var id2 = -1;

            var fiche1Id = req.body.fiche1;
            var fiche2Id = req.body.fiche2;

            playlist.linkedFiches.forEach(function (element, index) {
                if (element._id == fiche1Id) {
                    id1 = index;
                }
                else if (element._id == fiche2Id) {
                    id2 = index;
                }
            });

            if (id1 > -1 && id2 > -1) {
                playlist.linkedFiches[id1].value = null;
                playlist.linkedFiches[id2].value = null;

                var tmpFiche = playlist.linkedFiches[id1].fiche;
                playlist.linkedFiches[id1].fiche = playlist.linkedFiches[id2].fiche;
                playlist.linkedFiches[id2].fiche = tmpFiche;
            }

            playlist.linkedFiches[id1].save((err) => {
                if (err) {
                    console.log(err);
                    res.status(500).send("linkedFiche not created");
                }

                playlist.linkedFiches[id2].save((err) => {
                    if (err) {
                        console.log(err);
                        res.status(500).send("linkedFiche not created");
                    }

                    playlist.save((err, fiche) => {

                        Playlist.populate(playlist, {
                            path: "linkedFiches", model: 'LinkedFiche',
                            populate: {
                                path: 'fiche', model: 'Fiche'}
                        }, function (err, playlist) {
                            if (err) {
                                res.status(400).send("Populating failed");
                            }
                            return res.json(playlist);
                        });
                    });
                });
            });
        });
    });
}

export function deleteFiche(req, res) {
    Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
        if (err) {
            res.status(500).send("playlist not found");
        }

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            var idToRemove = -1;

            var ficheToRemoveId = req.body.fiche;

            playlist.linkedFiches.forEach(function (element, index) {
                if (element._id == ficheToRemoveId) {
                    idToRemove = index;
                }
            });

            if (idToRemove > -1) {

                if (idToRemove > 0) {
                    if (idToRemove < playlist.linkedFiches.length - 1) {
                        playlist.linkedFiches[idToRemove - 1].next = playlist.linkedFiches[idToRemove].next;
                    }
                    else {
                        playlist.linkedFiches[idToRemove - 1].next = null;
                    }

                    playlist.linkedFiches[idToRemove - 1].value = null;
                    playlist.linkedFiches[idToRemove - 1].save();
                }
            }

            playlist.linkedFiches.splice(idToRemove, 1);

            playlist.save((err) => {
                if (err) {
                    console.log(err);
                    res.status(500).send("linkedFiche not created");
                }

                playlist.save((err, fiche) => {

                    Playlist.populate(playlist, {
                        path: "linkedFiches", model: 'LinkedFiche',
                        populate: {
                            path: 'fiche', model: 'Fiche'}
                    }, function (err, playlist) {
                        if (err) {
                            res.status(400).send("Populating failed");
                        }
                        return res.json(playlist);
                    });
                });

            });
        });
    });
}

export function replace(req, res) {
    Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
        if (err) {
            res.status(500).send("playlist not found");
        }

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            var idToReplace = -1;

            var ficheToReplaceId = req.body.toReplace;
            var newFiche = req.body.replaceBy;

            playlist.linkedFiches.forEach(function (element, index) {
                if (element._id == ficheToReplaceId) {
                    idToReplace = index;
                }
            });

            if (idToReplace > -1) {
                playlist.linkedFiches[idToReplace].fiche = newFiche;
                playlist.linkedFiches[idToReplace].value = null;
                if (idToReplace > 0) {
                    playlist.linkedFiches[idToReplace - 1].value = null;
                    playlist.linkedFiches[idToReplace - 1].save();
                }
            }

            playlist.linkedFiches[idToReplace].save((err) => {
                if (err) {
                    console.log(err);
                    res.status(500).send("linkedFiche not created");
                }

                playlist.save((err, fiche) => {

                    Playlist.populate(playlist, {
                        path: "linkedFiches", model: 'LinkedFiche',
                        populate: {
                            path: 'fiche', model: 'Fiche'}
                    }, function (err, playlist) {
                        if (err) {
                            res.status(400).send("Populating failed");
                        }
                        return res.json(playlist);
                    });
                });

            });
        });
    });
}

export function addLink(req, res) {

    Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
        if (err) {
            res.status(500).send("playlist not found");
        }

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            var newLinkedFiche = new LinkedFiche({
                fiche: req.body.fiche._id,
                value: null,
                next: null
            });

            newLinkedFiche.save((err, fiche) => {
                if (err) {
                    console.log(err);
                    res.status(500).send("linkedFiche not created");
                }

                playlist.linkedFiches.push(fiche);

                if (playlist.linkedFiches.length - 1 > 0) {
                    var linkedFiche = playlist.linkedFiches[playlist.linkedFiches.length - 2];
                    linkedFiche.value = req.body.linkValue!==''?req.body.linkValue:null;
                    linkedFiche.next = fiche._id;

                    linkedFiche.save((err) => {
                        if (err) {
                            res.status(500).send("linkedFiche not created");
                        }

                        playlist.save((err, fiche) => {

                            Playlist.populate(playlist, {
                                path: "linkedFiches", model: 'LinkedFiche',
                                populate: {
                                    path: 'fiche', model: 'Fiche'}
                            }, function (err, playlist) {
                                if (err) {
                                    res.status(400).send("Populating failed");
                                }
                                return res.json(playlist);
                            });
                        });
                    })
                }
                else {
                    playlist.save((err, fiche) => {

                        Playlist.populate(playlist, {
                            path: "linkedFiches", model: 'LinkedFiche',
                            populate: {
                                path: 'fiche', model: 'Fiche'}
                        }, function (err, playlist) {
                            if (err) {
                                res.status(400).send("Populating failed");
                            }
                            return res.json(playlist);
                        });
                    });
                }
            });

        });
    });
}

export function updateLink(req, res) {
    Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
        if (err) {
            res.status(500).send("playlist not found");
        }

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            var idToReplace = -1;
            var ficheToReplaceId = req.body.fiche._id;

            playlist.linkedFiches.forEach(function (element, index) {
                if (element._id == ficheToReplaceId) {
                    idToReplace = index;
                }
            });

            playlist.linkedFiches[idToReplace].value = req.body.linkValue;

            playlist.linkedFiches[idToReplace].save((err) => {
                if (err) {
                    console.log(err);
                    res.status(500).send("linkedFiche not created");
                }

                playlist.save((err, fiche) => {

                    Playlist.populate(playlist, {
                        path: "linkedFiches", model: 'LinkedFiche',
                        populate: {
                            path: 'fiche', model: 'Fiche'}
                    }, function (err, playlist) {
                        if (err) {
                            res.status(400).send("Populating failed");
                        }
                        return res.json(playlist);
                    });
                });

            });
        });
    });
}

//just updating the status of the playlist, would be better to keep track of it, since now we are will handle the case of Draf, Published and Deleted
export function remove(req, res) {
  const query = { _id : req.params.id};

  Playlist.findOne(query, (err, playlist) => {
    if(err) {
      console.log("error " + err);
      return res.status(500).send('No playlist were found: '+ query._id);
    }

    Playlist.update(query, {status: CONSTANTS.STATUS.DELETED}, (err, previousOne) => {
      if(err) {
        console.log("error " + err);
        return res.status(500).send('We failed to delete the playlist : '+ query._id);
      }
      return res.status(200).send({message: "Playlist supprimée avec succès"});
    });
  });
}

export function update(req, res) {
    const query = {_id : req.params.id};

    Playlist.findOne( query, (err, playlist) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        playlist.name = req.body.name ? req.body.name : "Pas de nom" ;
        playlist.description = req.body.description ? req.body.description: "Pas de description";
        playlist.status = req.body.status;

        playlist.save((err, fiche) => {

            Playlist.populate(playlist, {
                path: "linkedFiches", model: 'LinkedFiche',
                populate: {
                    path: 'fiche', model: 'Fiche'}
            }, function (err, playlist) {
                if (err) {
                    res.status(400).send("Populating failed");
                }
                return res.json(playlist);
            });
        });
    })
}

export function updateCover(req, res) {
    const query = {_id : req.params.id};

    Playlist.findOne( query, (err, playlist) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        playlist.cover = req.body;

        playlist.save((err, fiche) => {

            Playlist.populate(playlist, {
                path: "linkedFiches", model: 'LinkedFiche',
                populate: {
                    path: 'fiche', model: 'Fiche'}
            }, function (err, playlist) {
                if (err) {
                    res.status(400).send("Populating failed");
                }
                return res.json(playlist);
            });
        });
    })
}

export function updateLabels(req, res) {
    const query = {_id : req.params.id},
          {body} = req;

    Playlist.findOne( query, (err, playlist) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        playlist.labels = {
          categories: body.categories.map((id) => { return {_id: id} }),
          sousCategories: body.sousCategories,
          tags: body.tags.map((id) => { return {_id: id} }),
        };

        playlist.save((err, fiche) => {

            Playlist.populate(playlist, {
                path: "linkedFiches", model: 'LinkedFiche',
                populate: {
                    path: 'fiche', model: 'Fiche'}
            }, function (err, playlist) {
                if (err) {
                    res.status(400).send("Populating failed");
                }
                return res.json(playlist);
            });
        });
    })
}


export function one(req, res){
    const query = {_id : req.params.id};
    Playlist.findOne(query, (err, playlist)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }

        Playlist.populate(playlist, [{
            path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        },{
          path: "labels.categories", model: 'Category'
        },{
          path: "labels.tags", model: 'Tag'
        }], function (err, playlist) {
            if (err) {
                res.status(400).send("Populating failed");
            }

            //custom handling model side to let the population on the linkedFiches
            res.write(JSON.stringify(playlist.toJSON({show: 'linkedFiches'})));
            return res.end();
        });
    })
}

export function stat(req, res) {
  Playlist.aggregate([
    {"$group" : {_id:"$status", count:{$sum:1}}}
  ], (err, results) => {
    if (err) {
      return res.status(500).send("Something wrong in retrieving stats > " + err);
    }

    return res.json(results);
  })
}

export default{
  all,
  add,
  one,
  addLink,
  updateLink,
  updateCover,
  updateLabels,
  swap,
  replace,
  deleteFiche,
  remove,
  update,
  stat
}
