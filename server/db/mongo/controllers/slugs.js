import Slugs from '../models/slugs';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

import async from 'async';
import removeAccents from 'remove-accents';
import cleanSpecialChars from 'clean-special-chars'


import Fiches from '../models/fiches';
import Playlists from '../models/playlists';

import { status } from '../static';
/**
 * List
 */

const SLUG_PREFIX = "#Traverse";
const SLUG_BASE_LIMIT = 3;
const SLUG_LOCALE = "fr-fr";


export function all(req, res) {
  Slugs.find({}).exec((err, slugs) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(slugs);
  });
}

const resolveError = (res, err, serverMessage, clientMessage) => {
  console.log(serverMessage, err);
  return res.status(500).send(clientMessage);
};

//handles non latin words https://stackoverflow.com/questions/33056332/how-to-replace-utf-8-letters-to-similar-latin-letters-using-jquery-or-javascript
const removeDiacritics = (input) => {
  let output = "",

      normalized = input.normalize("NFD"),
      i=0,
      j=0;

  while (i<input.length) {
    output += normalized[j];

    j += (input[i] == normalized[j]) ? 1 : 2;
    i++;
  }

  return output;
}

//handles specials chars here https://stackoverflow.com/questions/4374822/javascript-regexp-remove-all-special-characters
function removeSpecials(str) {
    var lower = str.toLowerCase();
    var upper = str.toUpperCase();

    var res = "";
    for(var i=0; i<lower.length; ++i) {
        if(lower[i] != upper[i] || lower[i].trim() === '')
            res += str[i];
    }
    return res;
}

function saveMixinItemsWithNewSlugs(listToSave, index = 0, callback) {
  //console.log(listToSave);
  let mixinItem = listToSave[index];//listToSave.splice(-1, 1);

  let worm = mixinItem.name;

  console.log(worm);

  //replacing all accented characters to their equivalent
  let lower = removeSpecials(removeDiacritics(worm));
  //make it easier to detect words within it since all words are separated with '-'
  lower = cleanSpecialChars(lower);

  let words = lower.split('-')
    //will only take the words which are large enough
    .filter((word) => {
      return word.length >= SLUG_BASE_LIMIT
    })
    //First letter must be capitalized
    .map((word) => {
      return word[0].toLocaleUpperCase(SLUG_LOCALE) + word.substring(1);
    });

    //Will only take the first three words within this new filtered list
    if (words.length > 3) {
      words = words.slice(0, 3);
    }

    //TODO rename this to make more sense
    lower = words.join("");

  Slugs.findOne({prefix: lower}, (err, slug) => {

    if (err) {
      return callback(err, null);
    }

    if (slug == null || slug == undefined) {
      slug = new Slugs({
        prefix: lower,
        count: 0
      });
    }

    slug.count++;

    let slugCount = slug.count > 1? slug.count: "";

    //check if slug.count increase on save or make it increase automatically when save
    mixinItem.slug = SLUG_PREFIX + slug.prefix + slugCount;

    mixinItem.save((err) => {
      if (err) {
        return callback(err, null);
      }
      slug.save((err) => {
        if (err) {
          return callback(err, null);
        }

        if (listToSave.length -1 != index) {
          return saveMixinItemsWithNewSlugs(listToSave, ++index, callback);
        }else {
          return callback(null, null);
        }
      });
    })
  });
}

//this will only be called on a post save hook, so no need to have req and res
export function generateSlug(mixinItem, next) {
  return saveMixinItemsWithNewSlugs([mixinItem], 0, () => {
    return next(null, mixinItem);
  });
}

/**
 * This method shall only be called once, and must be disabled right after since we need to create slugs from existing documents
 * We will only generate slugs once the document has been at least published once, since we don't want to have slugs of drafts
 *
 * When the fiche/playlist is beeing saved/updated with published status, we just need to call a method of this class so that
 * it will return a consistant slug and follow the flow of the application
 */
export function generateSlugs(req, res, next) {

  let asyncRetrieves = [
    function (done) {
      return Fiches.find({status: status.PUBLISHED}, (err, fiches) => {
        if (err) {
          return done(err, "Fiches: Error in generating slugs");
        }

        //here we look for slug tables if contains the prefix that is the
        // 0) lowercase
        // 1) remove spaces
        return saveMixinItemsWithNewSlugs(fiches, 0, () => {
          return done(null, fiches);
        })
      });
    },
    function (done) {

      return Playlists.find({status: status.PUBLISHED}, (err, playlists) => {
        if (err) {
          return done(err, "Playlists: Error in generating slugs");
        }

        return saveMixinItemsWithNewSlugs(playlists, 0, () => {
          return done(null, playlists);
        })
      });
    }
  ];

  async.series(asyncRetrieves, function allTaskCompleted(err, data) {
    if (err) {
      return resolveError(res, err, "@SlugController: generateSlug > allTaskCompleted >", "Error in generating slugs");
    }
    console.log("yolo0 > " + data.map((items) => {
      return " > " + items.length;
    }))
    return res.json({"m": "007"});
  });
}

export default{
  all,
  generateSlug,
  generateSlugs
}
