import Theme from '../models/themes'


export function all(req, res) {
    Theme.find({}).exec((err, themes) => {
        if (err) {
            console.log('Error in getting the themes -> function \'all\'');
            return res.status(500).send('Something went wrong getting the themes list');
        }
        return res.json(themes)
    })

}
export function one(req, res){
    const query = {_id: req.body.name};
    Theme.findOne( query, (err, theme) => {
        if (err) {
            console.log('Error cannot get the theme with the given id ' + query._id + ' -> function \'one\'');
            return res.status(500).send('Error cannot get the theme with the given id');
        }
        return res.json(theme);
    })
}
export default{
    all,
    one
}
