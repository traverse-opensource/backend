import Types from '../models/types';
import FicheController from './fiches';

/**
 * List
 */
export function all(req, res) {
  return FicheController.getTypes(req, res);
    /*Types.find({}).exec((err, types) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(types);
    });*/
}

export function add (req, res){
    let types = new Types({
        name: req.body.name,
        description: req.body.description
    });
    types.save((err, types) =>{
        if(err){
            res.status(400).send("types not added")
        }
        return res.json(types);
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Types.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("types removed successfully");
    })

}
export function update(req, res) {
    const query = {_id : req.params.id};
    Types.findOneAndUpdate( query, req.body, (err) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        return res.status(200).send('types updated');
    })
}

export function one(req, res){
    const query = {_id : req.params.id};
    Types.findOne(query, (err, types)=> {
        if(err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }
        return res.json(types);
    })
}

export default{
    all,
    one,
    add,
    remove,
    update
}
