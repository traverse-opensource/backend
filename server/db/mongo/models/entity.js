import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const EntitySchema = new Schema({
    name: String,
    description :String
});
export default mongoose.model('Entity', EntitySchema);
