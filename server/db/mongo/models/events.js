/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import FicheSchema from './fiches'

const EventSchema = new Schema({
    start_date: String,
    delta_start: Number,
    end_date: String,
    delta_end: Number,
    presentation: String,
    description: String,
    //seems not used here
    date: {date: String, description: String}
});
export default FicheSchema.discriminator('Events', EventSchema);