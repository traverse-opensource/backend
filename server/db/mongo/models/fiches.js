import GeoJSON from 'mongoose-geojson-schema';
import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
import { MIXIN_TYPES } from '../constant.js';
import { FicheThemeDominant, status } from '../static';

import SlugController from '../controllers/slugs';

const Schema = mongoose.Schema,
      Types = Schema.Types,
      Point = Types.Point,
      ObjectId = Schema.ObjectId;

const FicheSchema = new Schema({
    name: String,
    created_at: { type: Date, default: Date.now },
    last_updated_at: { type: Date, default: Date.now },
    status: { type: Number, default: status.DRAFT },
    created_by: {type: ObjectId, ref: 'User', autopopulate: true},
    last_update_by: {type: Schema.ObjectId, ref: 'User', default: null},
    related_fiches: [{type: Schema.ObjectId, ref: 'Fiche'}],
    playlists: [{type: Schema.ObjectId, ref: 'Playlist'}],
    cover: Object,
    categories: [{type : Schema.ObjectId, ref: 'Category', autopopulate: true}],
    //TODO check if it's used somewhere
    //types : [{type : Schema.ObjectId, ref: 'TypeSchema'}],
    themes : [{type: Schema.Types.Mixed, default: {}}],
    sousCategories: [String],
    tags : [{type : Schema.ObjectId, ref: 'Tag', autopopulate: true}],
    references : {type: String, default: ""},
    social: {type: Schema.Types.Mixed, default: {
      facebook: {tags: [], link: ""},
      instagram: {tags: [], link: ""},
      twitter: {tags: [], link: ""},
      web: {link: ""}
    }},
    //putting location system onto fiche itself
    city: String,
    country: String,
    latitude: Number ,
    longitude: Number,
    location: {
      type: Point,
      index: true,
      coordinates: [Number]
    },
    map: Object,
    slug: String
});

FicheSchema.plugin(autopopulate);

const attachSlugIfNeeded = (fiche) => {
  //maybe the fiche name will change, so we need to take care of that also
  if (fiche.status === status.PUBLISHED && !fiche.slug) {
    SlugController.generateSlug(fiche, (err, result) => {
      console.log("#", err, "++", result);
    });
  }
}

//update slug if and only if the fiche state is published
FicheSchema.post('save', attachSlugIfNeeded);
FicheSchema.post('findOneAndUpdate', attachSlugIfNeeded);

FicheSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    ret.type = ret.__t;
    ret.theme = FicheThemeDominant(ret);
    /*if (ret.__t) {
      delete ret.__t;
    }*/
    delete ret.types;
    delete ret.playlists;
    delete ret.__v;
    return ret;
  }
};

export default mongoose.model('Fiche', FicheSchema);