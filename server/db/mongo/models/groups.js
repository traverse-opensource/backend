import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const GroupSchema = new Schema({
    title: String,
    description :String
});
export default mongoose.model('Groups', GroupSchema);
