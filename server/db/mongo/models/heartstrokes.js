/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
import {CONSTANTS} from '../static';
import autopopulate from 'mongoose-autopopulate';
const Schema = mongoose.Schema;

const HeartStrokeSchema = new Schema({
    collection_type: { type: Number, default: CONSTANTS.TYPES.FICHE_TYPE },
    fiche_id : {
      type: Schema.ObjectId,
      ref: 'Fiche',
      autopopulate: true
    },
    playlist_id: {
      type: Schema.ObjectId,
      ref: 'Playlist',
      autopopulate: true
    },
    status: { type: Number, default: CONSTANTS.STATUS.DRAFT },
    order: Number
});

HeartStrokeSchema.plugin(autopopulate);

export default mongoose.model('HeartStroke', HeartStrokeSchema);