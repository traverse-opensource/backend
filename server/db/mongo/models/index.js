export default function loadModels() {
  require('./topics');
  require('./users');
  require('./linkedFiches');
  require('./playlists');
  require('./fiches');
  require('./categories');
  require('./people');
  require('./events');
  require('./media');
  require('./places');
  require('./objects');
  require('./themes');
  require('./entity');
  require('./slugs');
}
