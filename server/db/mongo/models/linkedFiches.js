import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
const Schema = mongoose.Schema;


const LinkedFicheSchema = new mongoose.Schema({
    fiche: {
        type: Schema.ObjectId,
        ref: 'Fiche',
        autopopulate: true
    },
    value: String,
    next: {
        type: Schema.ObjectId,
        ref: 'LinkedFiche'
    }
});

LinkedFicheSchema.plugin(autopopulate);

export default mongoose.model('LinkedFiche', LinkedFicheSchema);