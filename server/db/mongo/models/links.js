/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const LinkSchema = new Schema({
    name: String,
    type :String,
    link : String,
    information : String
});
export default mongoose.model('Links', LinkSchema);
