/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const {Schema} = mongoose;
import autopopulate from 'mongoose-autopopulate';
import { MIXIN_TYPES, status } from '../constant.js';
import { PlaylistThemeDominant } from '../static';

import SlugController from '../controllers/slugs';

const PlaylistSchema = new Schema({
    name: {type: String, default : ''},
    description: {type: String, default : ''},
    created_at: { type: Date, default: Date.now },
    last_updated_at: { type: Date, default: Date.now },
    status: { type: Number, default: status.DRAFT },
    cover: Object,
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        autopopulate: true
    },
    last_update_by: {
        type: Schema.ObjectId,
        ref: 'User',
        default: null
    },
    linkedFiches: [{
        type: Schema.ObjectId,
        ref: 'LinkedFiche',
        autopopulate: true
    }],
    labels: {
      categories: [{ type : Schema.ObjectId, ref: 'Category' }],
      sousCategories: [{type: String}],
      tags: [{ type : Schema.ObjectId, ref: 'Tag' }]
    },
    //only available from API 1.1 and above
    slug: String
});

PlaylistSchema.plugin(autopopulate);

//don't need it now, maybe we will need it later on
/*
const attachSlugIfNeeded = (playlist) => {
  if (playlist.status === status.PUBLISHED && !playlist.slug) {
    SlugController.generateSlug(playlist, (err, result) => {
      console.log("#", err, "++", result);
    });  
  }
}

PlaylistSchema.post('save', attachSlugIfNeeded);
PlaylistSchema.post('findOneAndUpdate', attachSlugIfNeeded);
*/

//computing themes color here so that each query on that entity would already have it's theme ponderation
PlaylistSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    
    console.log("options", options);
    
    ret.type = MIXIN_TYPES.PLAYLIST;
    ret.theme = PlaylistThemeDominant(ret);
    
    /*if (options.show) {
      
    }else {
      ret.linkedFiches = ret.linkedFiches.map((linkedFiche) => {
        linkedFiche.fiche = linkedFiche.fiche._id;
        return linkedFiche;
      })  
    }*/
    delete ret.__v;
    return ret;
  }
};

export default mongoose.model('Playlist',PlaylistSchema);