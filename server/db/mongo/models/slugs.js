/**
 * This document model aims to keep track of the slug generation.
 * All the slugs will be generated through this class/controller in order to avoid multiple copy and paste and thus side effects
 * Through Fiche nor Playlist documents
 *
 * A fiche will now have a slug, an so a Playlist
 * 
 * The algorithm to generate it is the check if within the collection we already have a prefix
 *  if it's the case we will increase the count of the slug count
 *  if no, we will create a new slug with at least count set to 1
 */

import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const SlugSchema = new Schema({
    prefix: String,
    count: {
      type: Number,
      default: 1
    }
});
export default mongoose.model('Slug', SlugSchema);
