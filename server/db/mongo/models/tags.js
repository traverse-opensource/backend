/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const TagSchema = new Schema({
    name: String,
    count: {
      type: Number,
      default: 1
    }
});
export default mongoose.model('Tag', TagSchema);
