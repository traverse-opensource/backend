/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const ThemeSchema = new Schema({
    name: String,
    description: String,
});
export default mongoose.model('Themes', ThemeSchema);
