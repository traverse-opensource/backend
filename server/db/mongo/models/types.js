/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const TypeSchema = new Schema({
    name: String,
});
export default mongoose.model('Type', TypeSchema);
