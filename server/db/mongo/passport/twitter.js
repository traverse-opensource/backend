import User from '../models/users';

/* eslint-disable no-param-reassign */
export default (req, accessToken, refreshToken, profile, done) => {
  
  console.log("####", accessToken);
  console.log("####", refreshToken);
  console.log("####", profile);
  
  //TODO need to force an email here, so we need to get extra permission from twitter, but we need privacy policy link
  
  //since access token is undefined, just reuse it to use babel for the user.token.push on twitter strategy
  //accessToken = refreshToken.access_token;
  
  if (req.user) {
    
    return User.findOne({ "social.value": profile.id }, (findOneErr, existingUser) => {
      if (existingUser) {
        return done(null, false, { message: 'There is already a twitter account that belongs to you. Sign in with that account or delete it, then link it with your current account.' });
      }
      return User.findById(req.user.id, (findByIdErr, user) => {
        user.social = {kind: 'twitter', value: profile.id};
        user.tokens.push({ kind: 'twitter', accessToken });
        user.profile.name = user.profile.name || profile.displayName;
        //seems gender and email won't be accessible
        user.profile.gender = user.profile.gender || profile.gender;
        
        //find the good stuff here
        user.profile.picture = user.profile.picture || profile._json.profile_image_url;
        
        user.save((err) => {
          done(err, user, { message: 'twitter account has been linked.' });
        });
      });
    });
  }
  
  return User.findOne({ "social.value": profile.id }, (findByTwitterIdErr, existingUser) => {
    if (existingUser) return done(null, existingUser);
    
    return User.findOne({ email: profile._json.email }, (findByEmailErr, existingEmailUser) => {
      if (existingEmailUser) {
        return done(null, false, { message: 'There is already an account using this email address. Sign in to that account and link it with twitter manually from Account Settings.' });
      }
      const user = new User();
      user.email = profile._json.email;
      user.social = {kind: 'twitter', value: profile.id};
      user.tokens.push({ kind: 'twitter', accessToken });
      user.profile.name = profile.displayName;
      user.profile.gender = profile.gender;
      user.profile.picture = profile._json.profile_image_url;
      
      return user.save((err) => {

        done(err, user, { message: 'User has been created with twitter account.' });
      });
    });
  });
};
/* eslint-enable no-param-reassign */
