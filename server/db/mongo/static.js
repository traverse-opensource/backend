/**
 * This file holds generic methods to compute some fields for all controllers and models
 */
import geolib from 'geolib';
import writeFileAtomic from 'write-file-atomic';
import json2csv from 'json2csv';

//heartstrokes
const CONSTANTS = {
  TYPES: { FICHE_TYPE: 0, PLAYLIST_TYPE: 1 },
  STATUS: { DRAFT: 0, PUBLISHED: 1, DELETED: 2}
};

// don't need to export this one
const createDefaultTheme = () => {
  return {
    name: "Default",
    color: "#386F8D",
    description: "",
    ponderation: 0
  }
}

const sortThemesByPonderation = (themes) => {
  return themes.sort((a, b) => {
    if (a.ponderation < b.ponderation) {
      return 1;
    }else if (a.ponderation > b.ponderation) {
      return -1;
    } else {
      return 0;
    }
  });
}

/*
 * return the theme dominant for a fiche, should not be used here
 * todo make it working for single fiche also
 */
const FicheThemeDominant = (fiche) => {
  let { themes } = fiche;
  let toReturn = createDefaultTheme();
  
  let sortedThemes = sortThemesByPonderation(themes);
  
  if (Object.keys(sortedThemes).length > 0) {
    let firstKey = Object.keys(sortedThemes)[0];
  
    toReturn = sortedThemes[firstKey];
  }
  
  return toReturn;
}

//search inside linkedFiches and theme, compte the max ponderation for each found themes and sort them by the computed ponderation
//just send back the theme that has the most ponderation
//if some themes are equal, then just send the first one on the sorted list
const PlaylistThemeDominant = (playlist) => {
  let {linkedFiches} = playlist;
  let toReturn = createDefaultTheme();
  
  let themesHolder = [];
  let innerThemes;
  
  let allThemes = linkedFiches.forEach((linkedFiche) => {
    
    innerThemes = linkedFiche.fiche.themes;
    
    innerThemes.forEach((theme) => {
      if (!themesHolder[theme.name]) {
        themesHolder[theme.name] = {
          name: theme.name,
          color: theme.color,
          ponderation: 0,
          description: theme.description
        };
      }

      //update ponderation
      themesHolder[theme.name].ponderation += theme.ponderation  
    });
  });
  
  //once we get all the computed ponderation, just sort the result having totalPonderation the highest one and pick the first one
  let sortedThemes = sortThemesByPonderation(themesHolder);
  
  if (Object.keys(sortedThemes).length > 0) {
    let firstKey = Object.keys(sortedThemes)[0];
  
    toReturn = sortedThemes[firstKey];
  }
  
  return toReturn;
}

const RANKING_FACTORS = {
  PROXIMITY: 0.2,
  CONNECTIONS: 0.35,
  TYPE: 0.45,
  TYPE_PONDERATION: {
    "Places": 1/5,
    "People": 2/5,
    "Objects": 3/5,
    "Events": 4/5,
    "Media": 5/5
  }
};

//Need this to make javascript not using weird roundings
const strip = (number) => {
    return (parseFloat(number).toPrecision(12));
}

/**
 * Return the score from 1 fiche based distance from location, its fiches connections and also its fiche type
 */
const computeScore = (fiche, currentDistance, distanceMax) => {
  const currentType = fiche.__t;
  
  const connectionNumber = (fiche.related_fiches?
        fiche.related_fiches.length: 0) + 1;
  
  let scoreDistance = strip(RANKING_FACTORS.PROXIMITY * (currentDistance / distanceMax));
  //TODO ensures related_fiches exists!
  let scoreConnections = strip(RANKING_FACTORS.CONNECTIONS * (1 / connectionNumber));
  let scoreType = strip(RANKING_FACTORS.TYPE * RANKING_FACTORS.TYPE_PONDERATION[currentType]);
  
  let toReturn = parseFloat(scoreDistance) + parseFloat(scoreConnections) + parseFloat(scoreType);
  
  return toReturn;
}

/*
 * the mixinItems argument hols fiches and playlists
 * callback should never be null since we need to apply the pagination from here, 
 * be carefull, it is the the mongo native pagination, 
 * we need to use the same logic at the createSkipQuery stated below
 *
 * @params fiches, are the list of fiches that comes from the inner query
 * @params matchingLocation is the location that we need to sort by distance, the $near query on mongo already sort by distance, but we don't have the distance,
 *  so we need to compute it from here
 * @params callback, is the function callback, we need to ask res to send a puurfect json ;)
 */
const sortByRankingAlgorithm = (fiches, matchingLocation, callback) => {
  
  let distances = fiches.map((fiche) => {
    let coordinates = fiche.location.coordinates,
        location = {
          latitude: coordinates[1],
          longitude: coordinates[0]
        }
    return geolib.getDistance(location, matchingLocation);
  });
  
  //returns a copy of distances so that distances won't be affected by the results of the sorting
  let sortedDistances = distances.slice(0);
  
  //sort from greater to lower in order to have the highest distance on first element
  sortedDistances.sort((a, b) => {
    if (a < b) {
      return 1;
    }else if (a > b) {
      return -1;
    } else {
      return 0;
    }
  });
  
  let distanceMax = sortedDistances[0];
  
  /*console.log("---------------", distances);
  console.log("---------------", sortedDistance);
  console.log("---------------", distanceMax);*/
  
  let toReturn = fiches
    //first map, creates a custom item with score and fiche to retrieve
    .map((fiche, index) => {
      
      let distance = distances[index];
      
      return {
        id: fiche._id,
        score: computeScore(fiche, distance, distanceMax),
        fiche: fiche
      }
    //secondly sort items by lowest score
    })
    .sort((a, b) => {
      if (a.score < b.score) {
        return -1;
      }else if (a.score > b.score) {
        return 1;
      } else {
        return 0;
      }
    })
    //thirdly retrieve the ficheItem
    .map((customItem) => {
      return customItem.fiche
    });
  
    //TODO paginate within the callback, after we retrieve the playlists
    return callback(toReturn);
}

/**
 * return a mongoose structure for the pagination system
 */
const createSkipQuery = (pageNumber, nPerPage) => {
  return pageNumber > 0 ? ((pageNumber-1)*nPerPage) : 0;
}

const FICHE_FILTERS = { ALL: 0, ALL_DRAFT: 1, MY_PUBLISHED: 2, MY_DRAFT: 3 };

const status = { DRAFT: 0, PUBLISHED: 1, DELETED: 2 };

const FICHE_TYPES = {
  PLACES: "Places",
  PEOPLE: "People",
  MEDIA: "Media",
  OBJECTS: "Objects",
  EVENTS: "Events"
};

module.exports = {
  CONSTANTS,
  FicheThemeDominant,
  PlaylistThemeDominant,
  sortByRankingAlgorithm,
  createSkipQuery,
  FICHE_FILTERS,
  status,
  FICHE_TYPES
};