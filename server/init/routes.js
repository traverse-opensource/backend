/**
 * Routes for express app yolo
 */
import passport from 'passport';
import unsupportedMessage from '../db/unsupportedMessage';
import { controllers, passport as passportConfig } from '../db';
import ogExtractor from '../services/og-extractor.js';
import thumbnailService from '../services/thumbnail.js';
import { SERVER_HOSTS } from '../db/mongo/constant';

import multer from 'multer' ;

const maxSize = 2 * 1000 * 1000;
const uploadMedia = multer({ dest: 'uploaded_images/', limits: { fileSize: maxSize }});
const upload = uploadMedia.single('file');

const usersController = controllers && controllers.users;
const topicsController = controllers && controllers.topics;
const fichesController = controllers && controllers.fiches;
const playlistsController = controllers && controllers.playlists;
const categoriesController = controllers && controllers.categories;
const eventsController = controllers && controllers.events;
const linksController = controllers && controllers.links;
const mediaController = controllers && controllers.media;
const objectsController = controllers && controllers.objects;
const placesController = controllers && controllers.places;
const peopleController = controllers && controllers.people;
const tagsController = controllers && controllers.tags;
const typesController = controllers && controllers.types;
const themesController = controllers && controllers.themes;
const heartsController = controllers && controllers.heartstrokes;
const groupsController = controllers && controllers.groups;
const entityController = controllers && controllers.entity;
const slugController = controllers && controllers.slugs;
const metaOgController = ogExtractor;
const thumbnailHelper = thumbnailService;



export default (app) => {
  //app.post( '/upload', uploadMedia.single('file'), function( req, res, next ) {
  app.post('/upload', (req, res, next) => {
    upload(req, res, (err) => {
      //console.log('File: ' + JSON.stringify(req.file,null,4));
      let file = req.file;
      console.log('File: ', file);
      if (err || file == null) {
        return res.status(500).send({errorMessage: "La taille maximale d'une image est fixée à 2Mo, veuillez changer ou redimensionner l'image désirée"});
      }

      //first generate a thumbnail and then send the reponse
      return thumbnailHelper.imageThumb({
        baseDir: file.destination,
        fileName: file.filename,
        callback: (err, result) => {
          if (err) {
            console.log("@route index: uppload controller > ", err);
            res.status(500).send({errorMessage: "Erreur interne"});
          }

          let toSend = file;
          toSend.thumb = result.scaled;
          return res.status( 200 ).send( toSend );
        }
      });

    });
  });
  // user routes
  if (usersController) {
    app.post('/login', usersController.login);
    app.post('/signup', usersController.signUp);
    app.post('/logout', usersController.logout);
    app.post('/users/forgotPassword', usersController.forgotPassword);
    app.get('/user/:id' , usersController.getUser);
    app.post('/user/:id', usersController.update);
    app.get('/user/:id/creation-counts' , usersController.getCreatedItemCount);
    app.post('/user/:id/compare-password' , usersController.comparePassword);
    app.post('/user/:id/profile-picture' , usersController.updateProfilePicture);
    app.get('/users', usersController.getUsers);
    app.post('/users/create', usersController.add);
    app.post('/users/download' , usersController.downloadCSV);
    app.delete('/users/:id', usersController.remove);

    app.post('/users/:id/deactivate', usersController.deactivate);
    app.post('/users/:id/restore', usersController.restore);

    app.get('/reset/:token', usersController.getReset);
    app.post('/reset/:token', usersController.doReset);
  } else {
    console.warn(unsupportedMessage('users routes'));
  }
  app.use('*/*.css', function(req, res){
    //console.log(req.originalUrl);
    var toRemove = '\\compiled';
    var oriPath = __dirname;
    var pos = oriPath.indexOf(toRemove);

    var toReturn = oriPath.substring(0, pos) + req.originalUrl;

    res.sendFile(toReturn);
    //res.sendFile(req);
  });

  if (passportConfig && passportConfig.google) {
    // google auth
    // Redirect the user to Google for authentication. When complete, Google
    // will redirect the user back to the application at
    // /auth/google/return
    // Authentication with google requires an additional scope param, for more info go
    // here https://developers.google.com/identity/protocols/OpenIDConnect#scope-param
    app.get('/auth/google', passport.authenticate('google', {
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }));

    // Google will redirect the user to this URL after authentication. Finish the
    // process by verifying the assertion. If valid, the user will be logged in.
    // Otherwise, the authentication has failed.
    app.get('/auth/google/callback',
      passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    );
  }

  if (passportConfig && passportConfig.facebook) {
    // facebook auth
    app.get('/auth/facebook', passport.authenticate('facebook', {
      scope: [
        'email',
        'user_likes'
      ]
    }));

    //callback url
    app.get('/auth/facebook/callback',
      passport.authenticate('facebook', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    );
  }

  if (passportConfig && passportConfig.twitter) {
    // twitter auth
    app.get('/auth/twitter', passport.authenticate('twitter'));

    //callback url
    app.get('/auth/twitter/callback',
      passport.authenticate('twitter', { failureRedirect: '/login' }), (req, res) => {
        // Successful authentication, redirect home.
        res.redirect('/');
      });
  }

  // topic routes
  if (topicsController) {
    app.get('/topic', topicsController.all);
    app.post('/topic/:id', topicsController.add);
    app.put('/topic/:id', topicsController.update);
    app.delete('/topic/:id', topicsController.remove);
  } else {
    console.warn(unsupportedMessage('topics routes'));
  }
  if (fichesController) {
    app.get('/fiches', fichesController.all);
    app.get('/fiches/types', fichesController.getTypes);
    app.get('/fiches/fiche/:id', fichesController.one);
    app.get('/fiches/:id/playlists', fichesController.getAssociatedPlaylists);
    app.post('/fiches/:id/addRelation', fichesController.addRelation);
    app.post('/fiches/:id/removeRelation', fichesController.removeRelation);
    app.post('/fiches/:id/updateCover', fichesController.updateCover);
    app.post('/fiches', fichesController.add);
    app.get('/fiches/stats', fichesController.stat);
    app.delete('/fiches/:id', fichesController.remove);
    app.post('/fiches/:id' ,fichesController.update)
  } else {
    console.warn(unsupportedMessage('fiches routes'));
  }
  if (playlistsController) {
    app.get('/playlists', playlistsController.all);
    app.get('/playlists/playlist/:id', playlistsController.one);
    app.post('/playlists/:id/addLink', playlistsController.addLink);
    app.post('/playlists/:id/updateLink', playlistsController.updateLink);
    app.post('/playlists/:id/updateCover', playlistsController.updateCover);
    app.post('/playlists/:id/updateLabels', playlistsController.updateLabels);
    app.post('/playlists/:id/swap', playlistsController.swap);
    app.post('/playlists/:id/replace', playlistsController.replace);
    app.post('/playlists/:id/deleteFiche', playlistsController.deleteFiche);
    app.post('/playlists', playlistsController.add);
    app.get('/playlists/stats', playlistsController.stat);
    //app.post('/playlists/:id/add', playlistsController.addFiche);
    app.delete('/playlists/:id', playlistsController.remove);
    app.put('/playlists/:id', playlistsController.update);
  } else {
    console.warn(unsupportedMessage('playlists routes'));
  }
  if (categoriesController) {
    app.get('/categories', categoriesController.all);
    app.get('/categories/:id', categoriesController.one);
    app.post('/categories', categoriesController.add);
    app.delete('/categories/:id', categoriesController.remove);
    app.post('/categories/:id', categoriesController.update);
  } else {
    console.warn(unsupportedMessage('categories routes'));
  }
  if (eventsController) {
    app.get('/events', eventsController.all);
    app.get('/events/:id', eventsController.one);
    app.get('/events/search/list', eventsController.search);
    app.post('/events', eventsController.add);
    app.delete('/events/:id', eventsController.remove);
    app.post('/events/:id', eventsController.update);
  } else {
    console.warn(unsupportedMessage('events routes'));
  }
  if (linksController) {
    app.get('/links', linksController.all);
    app.get('/links/:id', linksController.one);
    app.post('/links', linksController.add);
    app.delete('/links/:id', linksController.remove);
    app.post('/links/:id', linksController.update);
  } else {
    console.warn(unsupportedMessage('links routes'));
  }
  if (mediaController) {
    app.get('/media', mediaController.all);
    app.get('/media/:id', mediaController.one);
    app.get('/media/search/list', mediaController.search);
    app.post('/media', mediaController.add);
    app.delete('/media/:id', mediaController.remove);
    app.post('/media/:id', mediaController.update);
  } else {
    console.warn(unsupportedMessage('media routes'));
  }
  if (objectsController) {
    app.get('/objects', objectsController.all);
    app.get('/objects/search/list', objectsController.search);
    app.get('/objects/:id', objectsController.one);
    app.post('/objects', objectsController.add);
    app.delete('/objects/:id', objectsController.remove);
    app.post('/objects/:id', objectsController.update);
  } else {
    console.warn(unsupportedMessage('objects routes'));
  }
  if (peopleController) {
    app.get('/people', peopleController.all);
    app.get('/people/search/list', peopleController.search);
    app.get('/people/:id', peopleController.one);
    app.post('/people', peopleController.add);
    app.delete('/people/:id', peopleController.remove);
    app.post('/people/:id', peopleController.update);
  } else {
    console.warn(unsupportedMessage('people routes'));
  }
  if (placesController) {
    app.get('/places', placesController.all);
    app.get('/places/search/list', placesController.search);
    app.get('/places/:id', placesController.one);
    app.post('/places', placesController.add);
    app.delete('/places/:id', placesController.remove);
    app.post('/places/:id', placesController.update);
  } else {
    console.warn(unsupportedMessage('places routes'));
  }
  if (tagsController) {
    app.get('/tags', tagsController.all);
    app.get('/tags/:id', tagsController.one);
    app.post('/tags', tagsController.add);
    app.delete('/tags/:id', tagsController.remove);
    app.post('/tags/:id', tagsController.update);
  } else {
    console.warn(unsupportedMessage('tag routes'));
  }
  if (typesController) {
    //relicat, we'll need to remove this whole block
    app.get('/types', fichesController.getTypes);
  } else {
    console.warn(unsupportedMessage('types routes'));
  }
  if(themesController){
    app.get('/themes', themesController.all);
    app.get('/themes/:id', themesController.one);
  }else{
    console.warn(unsupportedMessage('themes routes'))
  }
  if (heartsController){
    app.get('/heartstrokes', heartsController.all);
    app.get('/heartstrokes/:q', heartsController.search);
    app.patch('/heartstrokes', heartsController.move);
    app.post('/heartstrokes', heartsController.add);
    app.delete('/heartstrokes', heartsController.remove);
  }
  if (groupsController){
    app.get('/groups', groupsController.all);
    app.post('/groups', groupsController.add);
    //app.delete('/groups/:id', groupsController.remove);
  }

  if (entityController){
    app.get('/entities', entityController.all);
    app.get('/entities/:id', entityController.one);
    app.post('/entities', entityController.add);
    app.delete('/entities/:id', entityController.remove);
  }

  if (slugController){
    app.get('/slugs', slugController.all);
    app.get('/photo/:photoId', (req, res, next) => {

      return thumbnailHelper.imageThumb({
        baseDir: "uploaded_images/",
        fileName: req.params.photoId,
        callback: (err, result) => {
          if (err) {
            console.log("@route index: uppload controller > ", err);
            return res.status(500).send({errorMessage: "Erreur interne"});
          }

          return res.status( 200 ).send( result );
        }
      });

    });

    app.get('/api/port', (req, res, next) => {
      let config = require('../../config/config.json');

      let toReturn = config.port.local.http;
      if (process.env.TRAVERSE_CUSTOM_HOST ===  SERVER_HOSTS.TRAVERSE) {
        toReturn = config.port.api.https
      }

      //return res.json({port: toReturn});
	return res.json({port: 8080});
    });
  }

  if(metaOgController){
    app.get('/extractMedia', metaOgController.videoThumb);
  }

};
