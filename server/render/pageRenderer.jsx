import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { RouterContext } from 'react-router';
import Helmet from 'react-helmet';
import { createAppScript, createTrackingScript } from './createScripts';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { google } from '../../config/secrets';

const createApp = (store, props) => renderToString(
    <MuiThemeProvider muiTheme={getMuiTheme()}>
        <Provider store={store}>
            <RouterContext {...props} />
        </Provider>
    </MuiThemeProvider>
);

const scriptUrl = "https://maps.googleapis.com/maps/api/js?key="+google.mapKey+"&libraries=places";

const buildPage = ({ componentHTML, initialState, headAssets }) => {
  return `
<!doctype html>
<html>
  <head>
    ${headAssets.title.toString()}
    ${headAssets.meta.toString()}
    ${headAssets.link.toString()}
    ${createTrackingScript()}
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/react-select/dist/react-select.css">
  </head>
  <body>
    <div id="app">${componentHTML}</div>
    <script>
      window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
      window.ALLOYEDITOR_BASEPATH = '/alloyeditor/dist/alloy-editor/';
      window.CKEDITOR_BASEPATH = '/alloyeditor/dist/alloy-editor/';
    </script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script type="text/javascript" src="${scriptUrl}"></script>
    ${createAppScript()}
  </body>
</html>`;
};

export default (store, props) => {
  const initialState = store.getState();
  const componentHTML = createApp(store, props);
  const headAssets = Helmet.rewind();
  return buildPage({ componentHTML, initialState, headAssets });
};

