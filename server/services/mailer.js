'use strict';
const nodemailer = require('nodemailer');
//  TODO PREREQUISITE: CONFIGURE MAIL SERVER


// create reusable transporter object using the default SMTP transport
//todo CREATE YOUR EMAIL SERVER AND PUT THE CREDENTIALS BELOW
let transporter = nodemailer.createTransport({
  host: 'mail.XXX.com',
  port: 587,
  secure: false, // secure:true for port 465, secure:false for port 587
  requireTLS: true,
  auth: {
    user: 'noreply@XXX.com',
    pass: 'XXX'
  }
});

const serverAddress = "https://XXX";

function createMailBody(name, mail, password){
  return '<div>Bonjour ' + name + '<br/><br/>Un compte a été créé pour vous à ' + serverAddress + ' <br/><br/>Vos identifiants de connection : <br/><ul><li>Identifiant : ' + mail + '</li><li>Mot de passe : ' + password + '</li></ul><br/>Procédure lors de la première connexion :<ul><li>changer de mot de passe (onglet Profil->Paramètres du compte)</li><li>choisir le logo de votre institution comme photo de profil (Profil->glisser-déposer une image ou parcourir votre disque local)</li><li>renommer votre profil selon le format suivant ACRONYME / Prénom Nom (type "Facim / Philippe Hanus")</li></ul><br/>L\'équipe <b>traverse</b> se tient à votre disposition pour toute information. Vous pouvez les contacter à l\'adresse info@traverse-patrimoines.com<br/><br/>Merci d\'avance pour vos contributions !</div>';
}

/** 
  * params is a json object that contains
  * - user mail
  * - user password
  * - user name
  */
function sendMail(res, params) {
  
  const { name, mail, password } = params;
  
  // setup email data with unicode symbols
  let mailOptions = {
    from: 'Noreply <noreply@XXX>', // sender address
    to: name + ' <' + mail + '>', // list of receivers
    subject: '', // Subject line
    text: '',
    html: createMailBody(name, mail, password) // html body
  };
  
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
  });
}

export default{
    sendMail
}
