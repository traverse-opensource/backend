import MetaExtract from 'og-meta-extract';

//private, no need to export
function startExtraction(res, videoLink) {
  new MetaExtract(videoLink, (err, result) => {
      if (null !== err || !result.image) {
        return res.json({
          mess: err,
          thumb: 'There was an error, check the message',
          status: -1
        });
      }
      
      let remotePath = '',
          name = result.title;
      
      if (result.image.name) {
        //dailymotion, vimeo   
        remotePath = result.image.name;
      }else {
        //google
        remotePath = result.image;
      }
      
      //we can get also the description and the name if needed
      return res.json({
          mess: 'No error here, feel free to check the reponse :)',
          thumb: remotePath,
          name: name,
          status: 1
        });
    });
}

function videoThumb(req, res) {
    console.log("MetaExtract @videoThumb : params", req.query);
    let videoLink = req.query.targetUrl;
    
    //warning edited the node module since it need to be fixed to handle when there is a fake link
    startExtraction(res, videoLink);
}

export default{
    videoThumb
}
