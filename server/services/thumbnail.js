import im from "imagemagick";

/*const maxWidth = 1920,
      maxHeight = 1080;*/

const DIMENS = {
  BIG: {
    w: 1920,
    h: 1080
  },
  MEDIMUM: {
    w: 512,
    h: 315
  },
  THUMB: {
    w: 260,
    h: 160
  }
};

const scaleDown = (basePath, scaleDownPath, scaleFactor, callback) => {
  return im.convert([basePath, '-resize', scaleFactor, scaleDownPath], (err, stdout) => {
    if (err) {
      callback(err, null);
    }
    
    
    console.log('stdout:', stdout);
    return callback(null, {original: basePath, scaled: scaleDownPath});
  });
}

const checkSize = (path, callback) => {
  im.identify(['-format', '%wx%h', path], (err, output) =>{
    if (err) {
      return callback(err, null);
    }
    
    console.log('dimension: '+output);
    // dimension: 3904x2622
    return callback(null, output.split("x"));
  });
}



//need also to overwrite the current file to a scaled version
function createThumbnail(baseDir, fileName, callback = null) {
  
  let basePath = baseDir + fileName,
      mediumPath = baseDir + "medium/" + fileName,
      thumbPath = baseDir + "thumb/" + fileName;
  
  /*
  //once medium called, then move onto thumb one
  const mediumCallback = (err, result) => {
    console.log("medium callback for", result);
    
    if (err) {
      return callback(err, null);
    }
    
    
    
    return scaleDown(basePath, thumbPath, )
    return callback(null, {original: baseDir + "/" + fileName, scaled: mediumPath});
  }*/
  
  
  const finalCallback = (err, result) => {
    
    console.log("final callback for", result);
    
    if (err) {
      return callback(err, null); 
    }
    
    return callback(null, {original: baseDir + "/" + fileName, scaled: mediumPath, thumb: thumbPath});
  }
  
  function detectWhatScaled(basePath, targetPath, targetDimens, bigDimens, callback) {
    console.log("detectWhatScaled > dimens", targetDimens, basePath, targetPath);

    return checkSize(basePath, (err, dimens) => {

      if (err) {
        console.log('@Thumbnail herlper: checkSize > err,', err);
        return callback(err, null);
      }
      //scale down keeping ratio with height
      if (parseInt(dimens[0]) > bigDimens.w) {
        return scaleDown(basePath, basePath, bigDimens.w + "x", () => {
          return scaleDown(basePath, targetPath, targetDimens.w + "x", callback);
        });
      }else if (parseInt(dimens[1]) > bigDimens.h) {
        return scaleDown(basePath, basePath, "x" + bigDimens.h, () => {
          return scaleDown(basePath, targetPath, "x" + targetDimens.h, callback);
        });
      } //no need to scale down if alread smaller than needed, just copy it using downscale with image dimensions
        else if (parseInt(dimens[0]) < bigDimens.w) {
        return scaleDown(basePath, targetPath, dimens[0] + "x", callback);
      } else if (parseInt(dimens[1]) < bigDimens.h) {
        return scaleDown(basePath, targetPath, "x" + dimens[1], callback);
      } else {
        return scaleDown(basePath, targetPath, targetDimens.w + "x", callback);
      }
    });
  }
  
  //firs medium ones
  return detectWhatScaled(basePath, mediumPath, DIMENS.MEDIMUM, DIMENS.BIG, () => {
    //then thumbnail ones
    return detectWhatScaled(mediumPath, thumbPath, DIMENS.THUMB, DIMENS.MEDIMUM, finalCallback);
  })
  
  //check size first
  //need to scale down 512 then 260 pixels wide
  /*checkSize(basePath, (err, dimens) => {
    
    if (err) {
      console.log('@Thumbnail herlper: checkSize > err,', err);
      return callback(err, null);
    }
    //scale down keeping ratio with height
    if (parseInt(dimens[0]) > maxWidth) {
      return scaleDown(basePath, basePath, maxWidth + "x", () => {
        return scaleDown(basePath, scaleDownPath, "260x", finalCallback);
      });
    }else if (parseInt(dimens[1]) > maxHeight) {
      return scaleDown(basePath, basePath, "x" + maxHeight, () => {
        return scaleDown(basePath, scaleDownPath, "x160", finalCallback);
      });
    } //no need to scale down if alread smaller than needed, just copy it using downscale with image dimensions
      else if (parseInt(dimens[0]) < maxWidth) {
      return scaleDown(basePath, scaleDownPath, dimens[0] + "x", finalCallback);
    } else if (parseInt(dimens[1]) < maxHeight) {
      return scaleDown(basePath, scaleDownPath, "x" + dimens[1], finalCallback);
    } else {
      return scaleDown(basePath, scaleDownPath, "260x", finalCallback);
    }
  });*/
}

function imageThumb(params) {
    return createThumbnail(params.baseDir, params.fileName, params.callback);
}

const bitSleep = (basePath, list, index, callback) => {
  if (index < list.length) {
    let file = list[index];
    createThumbnail(basePath, file, (err, result) => {
        //just don't care since we will use this once
        if (err) {
          console.log("@thumbnail service: all > err", err);
        }
      
        setTimeout(() => {
          return bitSleep(basePath, list, ++index, callback);
        }, 100);
      });
  }else {
    return callback();
  }
}

const except = (matching) => {
  const toAvoid = [
    ".",
    "..",
    ".gitignore",
    "/",
    ".DS_Store",
    '"'
  ];
  
  toAvoid.forEach((term) => {
    if (matching.indexOf(term) !== -1){
      return false;
    }
  })
  
  return true;
}

//will generate the thumbnails of all present pictures
function generateThumbs(req, res, next) {
  let basePath = "uploaded_images/";
  
  const fs = require('fs');

  let loopIndex = 0;
  
  let list = fs.readdirSync(basePath);
  
  return bitSleep(basePath, list, 0, (err, result) => {
    return res.status(err? 400: 200).send(err? "ko": 'ok');
  });
}

export default{
    imageThumb,
    generateThumbs
}
